<h3><?php echo VOTERS_TEXT;?></h3>

<div class="editFormDiv">
<form action="<?php echo SITE_ROOT.'/index.php?suffrage&action=edit';?>" method="POST">
	<fieldset>
		<legend><?php echo ADD_NEW_VOTER_TEXT.' / '.MODIFY_VOTER_TEXT;?></legend>
		<div class="editLabelDiv"><label for="firstName"><?php echo FIRST_NAME_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="firstName" name="firstName" value="<?php echo $data['voter']['firstName'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['firstName'])): echo '<span class="editFieldError">'.$data['formErrors']['firstName'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="lastName"><?php echo LAST_NAME_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="lastName" name="lastName" value="<?php echo $data['voter']['lastName'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['lastName'])): echo '<span class="editFieldError">'.$data['formErrors']['lastName'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
			
		<div class="editLabelDiv"><label for="studentNum"><?php echo STUDENT_NUMBER_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="hidden" name="studentNumOld" value="<?php echo $data['voter']['studentNumOld'];?>" ></input>
			<input type="text" id="studentNum" name="studentNum" value="<?php echo $data['voter']['studentNum'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['studentNum'])): echo '<span class="editFieldError">'.$data['formErrors']['studentNum'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
			
		<div class="editLabelDiv"><label for="email"><?php echo EMAIL_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="email" name="email" value="<?php echo $data['voter']['email'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['email'])): echo '<span class="editFieldError">'.$data['formErrors']['email'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>

		<div class="editLabelDiv"><label for="address"><?php echo ADDRESS_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="address" name="address" value="<?php echo $data['voter']['address'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['address'])): echo '<span class="editFieldError">'.$data['formErrors']['address'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="city"><?php echo CITY_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="city" name="city" value="<?php echo $data['voter']['city'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['city'])): echo '<span class="editFieldError">'.$data['formErrors']['city'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="zipCode"><?php echo POSTAL_CODE_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="zipCode" name="zipCode" value="<?php echo $data['voter']['zipCode'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['zipCode'])): echo '<span class="editFieldError">'.$data['formErrors']['zipCode'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label><?php echo RIGHT_TO_VOTE_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="radio" name="canVote" id="canVoteNegative" value="0" <?php if($data['voter']['canVote'] != 1):echo 'checked="checked"';endif;?>><label for="canVoteNegative"><?php echo HAS_NO_VOTING_RIGHT_TEXT;?></label><br>
			<input type="radio" name="canVote" id="canVotePositive" value="1" <?php if($data['voter']['canVote'] == 1):echo 'checked="checked"';endif;?>><label for="canVotePositive"><?php echo HAS_VOTING_RIGHT_TEXT;?></label>
		</div>
	
		<div class="editLabelDiv">&nbsp;</div>
		<div class="editInputDiv">
			<input type=hidden id="back" name="back" value="<?php echo $data['returnPage'];?>"></input>
			<input type="submit" name="voterSubmit" class="editSubmitButton" value="<?php echo SAVE_TEXT; ?>">
			<a href="<?php echo $data['returnPage'];?>" class="editLinkButton"><?php echo CANCEL_TEXT;?></a>
		</div>
	</fieldset>
</form>
</div>