<h3><?php echo ADMINISTRATORS_TEXT;?></h3>

<div class="editFormDiv">
<form action="<?php echo SITE_ROOT.'/index.php?account&action=edit';?>" method="POST">
	<fieldset>
		<legend><?php echo ADD_NEW_ADMIN_TEXT.' / '.MODIFY_ADMIN_TEXT;?></legend>
		<div class="editLabelDiv"><label for="username"><?php echo USERNAME_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="username" name="username" value="<?php echo $data['admin']['username'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['username'])): echo '<span class="editFieldError">'.$data['formErrors']['username'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="firstName"><?php echo FIRST_NAME_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="firstName" name="firstName" value="<?php echo $data['admin']['firstName'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['firstName'])): echo '<span class="editFieldError">'.$data['formErrors']['firstName'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="lastName"><?php echo LAST_NAME_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="lastName" name="lastName" value="<?php echo $data['admin']['lastName'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['lastName'])): echo '<span class="editFieldError">'.$data['formErrors']['lastName'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
			
		<div class="editLabelDiv"><label for="email"><?php echo EMAIL_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="email" name="email" value="<?php echo $data['admin']['email'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['email'])): echo '<span class="editFieldError">'.$data['formErrors']['email'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>

		<div class="editLabelDiv"><label for="address"><?php echo ADDRESS_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="address" name="address" value="<?php echo $data['admin']['address'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['address'])): echo '<span class="editFieldError">'.$data['formErrors']['address'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="city"><?php echo CITY_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="city" name="city" value="<?php echo $data['admin']['city'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['city'])): echo '<span class="editFieldError">'.$data['formErrors']['city'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="zipCode"><?php echo POSTAL_CODE_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="zipCode" name="zipCode" value="<?php echo $data['admin']['zipCode'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['zipCode'])): echo '<span class="editFieldError">'.$data['formErrors']['zipCode'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label><?php echo LEVEL_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="radio" name="adminLevel" id="adminLevel10" value="10" <?php if($data['admin']['adminLevel'] == 10):echo 'checked="checked"';endif;?>><label for="adminLevel10"><?php echo ADMINISTRATOR_TEXT;?></label><br>
			<input type="radio" name="adminLevel" id="adminLevel5" value="5" <?php if($data['admin']['adminLevel'] == 5):echo 'checked="checked"';endif;?>><label for="adminLevel5"><?php echo ELECTION_WORKER_TEXT;?></label>			
			<?php if(isset($data['formErrors']['adminLevel'])): echo '<br><span class="editFieldError">'.$data['formErrors']['adminLevel'].'</span>';endif;?>
		</div>
	
		
		<?php 
		$passwdDivStyle = 'style="width:100%;';
		if(isset($data['admin']['adminId']) && $data['admin']['adminId'] != 0) {?>
		<div class="editLabelDiv"><label for="setPassword"><?php echo CHANGE_PASSWORD_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="checkbox" name="setPassword" id="setPassword" value="true" class="inputCheckBox" 
			<?php if(isset($data['formErrors']['password'])):echo 'checked="checked"';endif;?>>
		</div>
		<?php 
		$passwdDivStyle .= (isset($data['formErrors']['password']))? '': 'display:none;';
		} //end if 
		else {?>
			<input type="hidden" name="setPassword" id="setPassword" value="true"/>
		<?php } //end else
		$passwdDivStyle .= '"'; ?>
		
		<div id="passwdDiv" <?php echo $passwdDivStyle;?>>
			<div class="editLabelDiv"><label for="password"><?php echo PASSWORD_TEXT;?></label></div>
			<div class="editInputDiv">
				<input type="password" id="password" name="password" class="inputText"></input>
				<?php if(isset($data['formErrors']['password'])): echo '<span class="editFieldError">'.$data['formErrors']['password'].'</span>';endif;?>
			</div>
			<div class="editLabelDiv"><label for="passwordConfirm"><?php echo CONFIRM_PASSWORD_TEXT;?></label></div>
			<div class="editInputDiv">
				<input type="password" id="passwordConfirm" name="passwordConfirm" class="inputText"></input>				
			</div>
			<div class="editLabelDiv">&nbsp;</div>
			<div class="editInputDiv"><?php echo '* '.PASSWORD_MUST_HAVE_THESE_TEXT;?></div>
			<div class="editSpacerDiv"></div>
		</div>
	
		<div class="editLabelDiv">&nbsp;</div>
		<div class="editInputDiv">
			<input type="hidden" name="adminId" value="<?php echo (isset($data['admin']['adminId']))? $data['admin']['adminId']: 0;?>"/>
			<input type="submit" name="accountSubmit" class="editSubmitButton" value="<?php echo SAVE_TEXT; ?>">
			<a href="<?php echo SITE_ROOT.'/index.php?account';?>" class="editLinkButton"><?php echo CANCEL_TEXT;?></a>
		</div>
	</fieldset>
</form>
</div>
