<?php 
$lang = ($_SESSION['language'] == 'ENG')? 'en':'fi';
?>

<h3><?php echo INSTRUCTIONS_TEXT;?></h3>
<p></p>

<p><?php echo HOW_TO_VOTE_TEXT;?></p>

<p><?php echo INSTRUCTIONS_001_TEXT?></p>
<img src=<?php echo SITE_ROOT.'/static/img/001_etusivu_'.$lang.'.png';?>>

<p><?php echo INSTRUCTIONS_002_TEXT?></p>
<img src=<?php echo SITE_ROOT.'/static/img/002_login_'.$lang.'.png';?>>
<p></p>

<p><?php echo INSTRUCTIONS_003_TEXT?></p>
<img src=<?php echo SITE_ROOT.'/static/img/003_ehdokas_'.$lang.'.png';?>>

<p><?php echo INSTRUCTIONS_004_TEXT?></p>
<img src=<?php echo SITE_ROOT.'/static/img/004_vahvistus_'.$lang.'.png';?>>

<p><?php echo INSTRUCTIONS_005_TEXT?></p>
<img src=<?php echo SITE_ROOT.'/static/img/005_valmis_'.$lang.'.png';?>>

<p><?php echo INSTRUCTIONS_006_TEXT?></p>
<img src=<?php echo SITE_ROOT.'/static/img/006_logout_'.$lang.'.png';?>>
<p></p>

<a href="<?php echo SITE_ROOT?>" class="buttonLink"><?php echo GO_BACK_TEXT?></a>