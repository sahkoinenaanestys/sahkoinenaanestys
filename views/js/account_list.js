//Dialog text variables
var title;
var msg, msg2;
var btnOkText;
var btnCancelText;
var errorTitle;
var btnCloseText;

//Dialog states
var dialogIsOpen = false;

function showConfirmationDialog(username, url, languageCode, back) {	

    if(dialogIsOpen == false) {
	setTextValues(languageCode);        

	$('<div></div>').appendTo('#mainContents')
	.html('<div><p>'+msg+'?<br />'+msg2+': '+username+'</p></div>')
	.dialog({
	    title: title+'?', 
	    width:  'auto', 
	    height: 'auto',
	    modal: true,
	    resizable: false,
	    buttons: [{
		    text: btnOkText,
		    click: function () {
			removeAccount(username, url, languageCode, back);
			$(this).dialog("close");
			dialogIsOpen = false;
		    }
		},{
		    text: btnCancelText,
		    click: function () {
			$(this).dialog("close");
			dialogIsOpen = false;
		    }
		}],
	    close: function (event, ui) {
		$(this).remove();
		dialogIsOpen = false;
	    }
	});  
    }
}


function removeAccount(username, url, languageCode, back){	
	$.ajax({
        type: "POST",
        url: url+'&username='+username, 
               
        success: function (data) {     
            var response = $.parseJSON(data);            
            if(response.success == false) {            
            	//show error dialog
            	showErrorDialog(response.msg, languageCode);
            }
            else {   
		window.location = back+"&success=adminremoved";
            }
        },
        error: function (xhr, desc, err) {    	
        	//show error dialog        	
        	showErrorDialog('xhr: '+xhr+'<br>desc: '+desc+'<br>err: '+err, languageCode);
        }
    });		
}

function showErrorDialog(msg, languageCode) {
	setTextValues(languageCode);
	$('<div></div>').appendTo('#mainContents').html('<div><p>'+msg+'</p></div>').dialog({            		
		title: errorTitle, 
        width:  'auto', 
        height: 'auto',
        resizable: false,
		buttons: [{
            text: btnCloseText,
            click: function () {                            
                $(this).dialog("close");
            }
        }],
        close: function (event, ui) {
            $(this).remove();
        }
	});
}

function setTextValues(languageCode) {       
    switch (languageCode) {
        case 'FIN':
            title = 'Poistetaanko hallinnoitsija';
            msg = 'Haluatko varmasti poistaa hallinnoitsijan';
            msg2 = 'Jos painat KYLLÄ, seuraava hallinnoitsija poistetaan';
            btnOkText = 'Kyllä';
            btnCancelText = 'Ei';
            errorTitle = 'Tapahtui virhe';
            btnCloseText = 'Sulje';
            break;
        default:
            title = 'Remove administrator';
            msg = 'Are you sure you want to continue';
            msg2 = 'If you press YES, this administrator will be removed';
            btnOkText = 'Yes';
            btnCancelText = 'No';
            errorTitle = 'An error occured';
            btnCloseText = 'Close';
            break;
    }    
}
