//Dialog text variables
var title;
var msg;
var btnOkText;
var btnCancelText;

//Dialog states
var dialogIsOpen = false;

//Election states
var state_closed = 0;
var state_active = 1;
var state_ended = 2;
var state_history = 3;

function showStartOrStopElectionDialog(electionId, electionState, url, languageCode, back){
    
    if(dialogIsOpen == false) {
	setTextValues(languageCode, electionState);

	$('<div></div>').appendTo('#mainContents')
	.html('<div><p>'+msg+'.</p></div>')
	.dialog({
	    title: title+'?', 
	    width:  'auto', 
	    height: 'auto',
	    modal: true,
	    resizable: false,
	    buttons: [{
		    text: btnOkText,
		    click: function () {
			$('#messagesContainer').empty();
			startOrStopElection(electionId, url, back)
			$(this).dialog("close");
			dialogIsOpen = false;
		    }
		},{
		    text: btnCancelText,
		    click: function () {
			$(this).dialog("close");
			dialogIsOpen = false;
		    }
		}],
	    close: function (event, ui) {
		$(this).remove();
		dialogIsOpen = false;
	    }
	});
    dialogIsOpen = true;
    }
}

function startOrStopElection(electionId, url, back){
    
    $.ajax({
        type: "POST",
        url: url + "&electionId=" + electionId,
        success: function (data) {     
            var response = $.parseJSON(data);
            if(response.status == true){
		if(response.election_status == state_active){
		    window.location = back+"&electionId="+electionId+"&success=electionopensuccess";
		}else if(response.election_status == state_closed){
		    window.location = back+"&electionId="+electionId+"&success=electionclosesuccess";
		}
            }else{
		if(response.election_status == state_active){
		    window.location = back+"&electionId="+electionId+"&error=electionopenfailure";
		}else if(response.election_status == state_closed){
		    window.location = back+"&electionId="+electionId+"&error=electionclosefailure";
		}
	    }
        },
        error: function (xhr, desc, err) {
            $('<div class="errorMsgDiv">'+err+'</div>').appendTo('#messagesContainer');
        }
    });
}

function setTextValues(languageCode, electionState){   
    
    if(languageCode == 'FIN') {
	    if(electionState == state_active){ 
		title = 'Suljetaanko vaali'; 
		msg = 'Jos painat KYLLÄ, valittu vaali suljetaan';
	    }else if(electionState==state_closed){ 
		title = 'Merkataanko vaali aktiiviseksi?'; 
		msg = 'Jos painat KYLLÄ, valittu vaali merkataan aktiiviseksi';
	    }
	    btnOkText = 'Kyllä';
	    btnCancelText = 'Ei';
    }else{
	    if(electionState == state_active){ 
		title = 'Close election'; 
		msg = 'If you press YES, chosen election will be marked as closed';
	    }else if(electionState==state_closed){ 
		title = 'Open election?'; 
		msg = 'If you press YES, chosen election will be marked as open';
	    }
	    btnOkText = 'Yes';
	    btnCancelText = 'No';
    }
}

