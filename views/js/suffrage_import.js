var loadingText = 'Saving data...';

$(document).ready(function()
{
    $('#FileUploader').on('submit', function(e)
    {
        e.preventDefault();
        $('#uploadBtn').attr('disabled', ''); // disable upload button
        
        //show loading message
        //$("#output").show();
        $("#output").Loadingdotdotdot({
            "speed": 400,
            "maxDots": 4,
            "word": loadingText
        });
        
        $(this).ajaxSubmit({
            target: '#messagesContainer',
            success:  afterFinished
        });
    });
});

function afterFinished()
{
    $("#output").Loadingdotdotdot("Stop");
    $("#output").hide();
    $('#FileUploader').resetForm();  // reset form
    $('#uploadBtn').removeAttr('disabled'); //enable submit button
}