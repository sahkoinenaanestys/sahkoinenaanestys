var title;
var btnAddText;
var btnCancelText;

$(document).ready(function()
{
    var allianceId = $('#allianceId :selected').val();
    var coalitionId = $('#coalitionId :selected').val();

    if(allianceId == 0){
	$('[name=coalitionId]').val(coalitionId);
	$('#coalitionId').attr('disabled', false);
    }else{
	$('[name=coalitionId]').val(coalitionId);
	$('#coalitionId').attr('disabled', 'disabled');
    }
    
    $('#candidateEditForm').on('submit', function(e){
	$('#coalitionId').attr('disabled', false);
    });
    
    //$("#addAlliancePopup").hide();
    //var allianceTextbox = new AutoSuggestControl(document.getElementById("alliance"), new SuggestionProvider(allianceCtrlUrl), allianceTBId);
    //var coalitionTextbox = new AutoSuggestControl(document.getElementById("coalition"), new SuggestionProvider(coalitionCtrlUrl), coalitionTBId);
});

/*function showAddAlliancePopup(url, languageCode){
    
    $(document).on("click", "#addNewAlliance", function (event) {

	setTextValues(languageCode, 1);
	event.preventDefault();

	$("#addAlliancePopup").dialog({
	    minWidth: 400,
	    width: 500,
	    maxWidth: 600,
	    title: title,
	    modal: true,
	    buttons: [
		{
		    text: btnAddText,
		    click: function () {
			var alliance = {
			    "AId": $("#newAllianceId").val(),
			    "Aname": $("#newAllianceName").val(),
			    "CId": $("#newCoalitionId").val(),
			    "Cname": $("#newCoalitionName").val()
			};
			addAlliance(alliance, url);
			$("#newAllianceId").val('');
			$("#newAllianceName").val('');
			$("#newCoalitionId").val('');
			$("#newCoalitionName").val('');
			$(this).dialog("close");
		    }
		},
		{
		    text: btnCancelText,
		    click: function () {
			$(this).dialog("close");
		    }
		}]
	});
    }); //onClick  
}

function addAlliance(alliance, url){

    $.ajax({
        type: "POST",
	contentType: "text/plain; charset=UTF-8",
        url: url + "&allianceName=" + alliance.Aname + "&allianceId=" + alliance.AId +
	    "&coalitionName=" + alliance.Cname + "&coalitionId=" + alliance.CId,
        success: function (data) {     
            var response = $.parseJSON(data);
            if(response.status == true){
                afterSuccess(response.allianceName, response.allianceId, response.coalitionName, response.coalitionId, response.msg);
            }else{
		$("#successMsgDiv").remove();
		$("#errorMsgDiv").empty();
    
		if($("#errorMsgDiv").length == 0){
		    $('<div class="errorMsgDiv" id="errorMsgDiv">'+response.msg+'</div>').appendTo('#messagesContainer');
		}else{
		    $("#errorMsgDiv").append(response.msg);
		}
	    }
        }
    });
}*/

function getCoalition(url, sel){

    var allianceId = sel.options[sel.selectedIndex].value;
    
    $.ajax({
        type: "POST",
        url: url + "&allianceId=" + allianceId,
        success: function (data) {     
            var response = $.parseJSON(data);
            if(response.status == true){
		$('[name=coalitionId]').val(response.coalitionId);
		if(allianceId == 0){
		    $('#coalitionId').attr('disabled', false);
		}else{
		    $('#coalitionId').attr('disabled', 'disabled');
		}
            }
        }
    });
}

/*function afterSuccess(allianceName, allianceId, coalitionName, coalitionId, msg){
    
    $('#allianceId').append(
	'<option value="'+allianceId+'" selected="selected">'+allianceName+'</option>'
    );
    if(coalitionId > 0){
	$('#coalitionId').append('<option value="'+coalitionId+'" selected="selected">'+coalitionName+'</option>');
    }else{
	$('[name=coalitionId]').val(0);
    }
   
    $("#successMsgDiv").empty();
    $("#errorMsgDiv").remove();
    
    if($("#successMsgDiv").length == 0){
	$('<div class="successMsgDiv" id="successMsgDiv">'+msg+'</div>').appendTo('#messagesContainer');
    }else{
	$("#successMsgDiv").empty();
	$("#successMsgDiv").append(msg);
    }
}*/

/*function AutoSuggestControl(oTextbox, oProvider, caller) {
    this.caller = caller;
    this.cur = -1;
    this.layer = null;
    this.provider = oProvider;
    this.textbox = oTextbox;
    this.timeoutId = null;
    this.userText = oTextbox.value;
    this.init();
}


AutoSuggestControl.prototype.selectRange = function (iStart, iEnd) {
    if (this.textbox.createTextRange) {
        var oRange = this.textbox.createTextRange();
        oRange.moveStart("character", iStart);
        oRange.moveEnd("character", iEnd- this.textbox.value.length);
        oRange.select();
    } else if (this.textbox.setSelectionRange) {
        this.textbox.setSelectionRange(iStart, iEnd);
    }

    this.textbox.focus();
};

AutoSuggestControl.prototype.typeAhead = function (sSuggestion) {
    if (this.textbox.createTextRange || this.textbox.setSelectionRange) {
        var iLen = this.textbox.value.length;
        this.textbox.value = sSuggestion;
	
	if(this.caller == allianceTBId){
		var valueIndex = inArray(sSuggestion, this.aSuggestions);
		document.getElementById("coalition").value = (this.aSuggestions[valueIndex]).coalition;	
	}
	
        this.selectRange(iLen, sSuggestion.length);
    }
};

AutoSuggestControl.prototype.autosuggest = function (aSuggestions, bTypeAhead) {
    
    this.cur = -1;
    
    if (aSuggestions.length > 0) {
	this.aSuggestions = aSuggestions;
        if (bTypeAhead) {
            this.typeAhead(aSuggestions[0].name);
        }
        this.showSuggestions(aSuggestions);
    } else {
        this.hideSuggestions();
	
	if(this.caller == allianceTBId)
	    document.getElementById("coalition").value = "";	
    }
};*/

//AutoSuggestControl.prototype.handleKeyUp = function (oEvent /*:Event*/) {

    /*var iKeyCode = oEvent.keyCode;
    var oThis = this;

    this.userText = this.textbox.value;

    clearTimeout(this.timeoutId);

    if (iKeyCode == 8 || iKeyCode == 46) {

        this.timeoutId = setTimeout(function () {
            oThis.provider.requestSuggestions(oThis, false);
        }, 250);

    } else if (iKeyCode < 32 || (iKeyCode >= 33 && iKeyCode < 46)
               || (iKeyCode >= 112 && iKeyCode <= 123)) {
        //ignore
    } else {
        this.timeoutId = setTimeout(function () {
            oThis.provider.requestSuggestions(oThis, true);
        }, 250);
    }
};

AutoSuggestControl.prototype.handleKeyDown = function (oEvent) {
    switch(oEvent.keyCode) {
        case 38: //up arrow
            this.goToSuggestion(-1);
            break;
        case 40: //down arrow
            this.goToSuggestion(1);
            break;
        case 27: //esc
            this.textbox.value = this.userText;
            this.selectRange(this.userText.length, 0);
            // falls through
        case 13: //enter
            this.hideSuggestions();
            oEvent.returnValue = false;
            if (oEvent.preventDefault) {
                oEvent.preventDefault();
            }
            break;
    }
};

AutoSuggestControl.prototype.init = function () {
    var oThis = this;
    this.textbox.onkeyup = function (oEvent) {
        if (!oEvent) {
            oEvent = window.event;
        }
        oThis.handleKeyUp(oEvent);
    };
    this.textbox.onkeydown = function (oEvent) {
        if (!oEvent) {
            oEvent = window.event;
        }
        oThis.handleKeyDown(oEvent);
    };

    this.textbox.onblur = function () {
        oThis.hideSuggestions();
    };

    this.createDropDown();
};

AutoSuggestControl.prototype.hideSuggestions = function () {
    this.layer.style.visibility = "hidden";
};

AutoSuggestControl.prototype.highlightSuggestion = function (oSuggestionNode) {

    for (var i=0; i < this.layer.childNodes.length; i++) {
        var oNode = this.layer.childNodes[i];
        if (oNode == oSuggestionNode) {
            oNode.className = "current"
        } else if (oNode.className == "current") {
            oNode.className = "";
        }
    }
};

AutoSuggestControl.prototype.showSuggestions = function (aSuggestions) {

    var oDiv = null;
    this.layer.innerHTML = "";

    for (var i=0; i < aSuggestions.length; i++) {
        oDiv = document.createElement("div");
        oDiv.appendChild(document.createTextNode(aSuggestions[i].name));
        this.layer.appendChild(oDiv);
    }

    this.layer.style.left = this.getLeft() + "px";
    this.layer.style.top = (this.getTop()+this.textbox.offsetHeight) + "px";
    this.layer.style.visibility = "visible";
};

AutoSuggestControl.prototype.createDropDown = function () {

    this.layer = document.createElement("div");
    this.layer.className = "suggestions";
    this.layer.style.visibility = "hidden";
    this.layer.style.width = this.textbox.offsetWidth;
    document.body.appendChild(this.layer);

    var oThis = this;

    this.layer.onmousedown = this.layer.onmouseup =
    this.layer.onmouseover = function (oEvent) {
        oEvent = oEvent || window.event;
        oTarget = oEvent.target || oEvent.srcElement;

        if (oEvent.type == "mousedown") {
            var value = oTarget.firstChild.nodeValue;
	    this.textbox.value = value;

	    if(this.caller == allianceTBId){
		var valueIndex = inArray(value, this.aSuggestions);
		document.getElementById("coalition").value = (this.aSuggestions[valueIndex]).coalition;	
	    }
	    
            oThis.hideSuggestions();
        } else if (oEvent.type == "mouseover") {
            oThis.highlightSuggestion(oTarget);
        } else {
            oThis.textbox.focus();
        }
    };
};

AutoSuggestControl.prototype.getLeft = function () {

    var oNode = this.textbox;
    var iLeft = 0;

    while(oNode.tagName != "BODY") {
        iLeft += oNode.offsetLeft;
        oNode = oNode.offsetParent;
    }

    return iLeft;
};

AutoSuggestControl.prototype.getTop = function () {

    var oNode = this.textbox;
    var iTop = 0;

    while(oNode.tagName != "BODY") {
        iTop += oNode.offsetTop;
        oNode = oNode.offsetParent;
    }

    return iTop;
};

AutoSuggestControl.prototype.goToSuggestion = function (iDiff) {
    var cSuggestionNodes = this.layer.childNodes;

    if (cSuggestionNodes.length > 0) {
        var oNode = null;

        if (iDiff > 0) {
            if (this.cur < cSuggestionNodes.length-1) {
                oNode = cSuggestionNodes[++this.cur];
            }
        } else {
            if (this.cur > 0) {
                oNode = cSuggestionNodes[--this.cur];
            }
        }

        if (oNode) {
            this.highlightSuggestion(oNode);
	    var value = oNode.firstChild.nodeValue;
            this.textbox.value = value;

	    if(this.caller == allianceTBId){
		var valueIndex = inArray(value, this.aSuggestions);
		document.getElementById("coalition").value = (this.aSuggestions[valueIndex]).coalition;	
	    }
        }
    }
};

function inArray(value, arr) {
    
    var found = false;
    var index = 0;
    
    $.each(arr, function () {
       if (this.name === value) {
	  found = true;
	  return false;
       }
       index = index + 1; 
    });
    if(found){
	return index;
    }else{
	return -1;
    }
}

function SuggestionProvider(url_par) {
    this.url = url_par;
}

SuggestionProvider.prototype.requestSuggestions = function (oAutoSuggestControl,bTypeAhead) {

    var xhr;
    
    //cancel any active requests
    if(xhr && xhr.readyState != 0){
            xhr.abort();
    }
    
    xhr = $.ajax({
        type: "POST",
        url: this.url + oAutoSuggestControl.userText,
        success: function (data) {     
            var aSuggestions = $.parseJSON(data);

	    //provide suggestions to the control
	    oAutoSuggestControl.autosuggest(aSuggestions, bTypeAhead);
        }
    });

};*/

/*function setTextValues(languageCode, caller){   
    
    if(caller == 1){
	switch (languageCode) {
	    case 'FIN':
		title = 'Lisää uusi vaaliliitto';
		btnAddText = 'Lisää';
		btnCancelText = 'Peruuta';
		break;
	    default:
		title = 'Add new alliance';
		btnAddText = 'Add';
		btnCancelText = 'Cancel';
		break;
	}
    }else if(caller == 2){
	switch (languageCode) {
	    case 'FIN':
		title = 'Lisää uusi vaalirengas';
		btnAddText = 'Lisää';
		btnCancelText = 'Peruuta';
		break;
	    default:
		title = 'Add new coalition';
		btnAddText = 'Add';
		btnCancelText = 'Cancel';
		break;
	}
    }
}*/