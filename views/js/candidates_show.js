//Dialog text variables
var title;
var msg, msg2;
var btnOkText;
var btnCancelText;

//Dialog states
var dialogIsOpen = false;

$( document ).ready( function() {
    //page specific code goes here
});
    
    
function showConfirmationDialog(candidateNum, electionId, url, languageCode, back){
    
    if(dialogIsOpen == false) {
	setTextValues(languageCode);

	$('<div></div>').appendTo('#mainContents')
	.html('<div><p>'+msg+'?<br />'+msg2+'.</p></div>')
	.dialog({
	    title: title+'?', 
	    width:  'auto', 
	    height: 'auto',
	    modal: true,
	    resizable: false,
	    buttons: [{
		    text: btnOkText,
		    click: function () {
			$('#messagesContainer').empty();
			removeCandidate(candidateNum, electionId, url, back);
			$(this).dialog("close");
			dialogIsOpen = false;
		    }
		},{
		    text: btnCancelText,
		    click: function () {
			$(this).dialog("close");
			dialogIsOpen = false;
		    }
		}],
	    close: function (event, ui) {
		dialogIsOpen = false;
		$(this).remove();
	    }
	});
	dialogIsOpen = true;
    }
}

function removeCandidate(candidateNum, electionId, url, back){
    
    $.ajax({
        type: "POST",
        url: url+"&candidateNum="+candidateNum+"&electionId="+electionId,
        success: function (data) {     
            var response = $.parseJSON(data);
            if(response.status == true){
                window.location = back+"&success=candidateremoved";
            }else{
		$("#errorMsgDiv").empty();
		$('<div class="errorMsgDiv">'+response.msg+'</div>').appendTo('#messagesContainer');
	    }
        },
        error: function (xhr, desc, err) {
	    $("#errorMsgDiv").empty();
            $('<div class="errorMsgDiv">'+err+'</div>').appendTo('#messagesContainer');
        }
    });
}

function setTextValues(languageCode){   
    
    switch (languageCode) {
        case 'FIN':
            title = 'Poistetaanko ehdokas';
            msg = 'Haluatko varmasti poistaa ehdokkaan';
            msg2 = 'Jos painat KYLLÄ, valittu ehdokas poistetaan';
            btnOkText = 'Kyllä';
            btnCancelText = 'Ei';
            break;
	case 'ENG':
            title = 'Remove candidate';
            msg = 'Are you sure you want to continue';
            msg2 = 'If you press YES, chosen candidate will be removed';
            btnOkText = 'Yes';
            btnCancelText = 'No';
            break;
        default:
            title = 'Remove candidate';
            msg = 'Are you sure you want to continue';
            msg2 = 'If you press YES, chosen candidate will be removed';
            btnOkText = 'Yes';
            btnCancelText = 'No';
            break;
    }
    
}