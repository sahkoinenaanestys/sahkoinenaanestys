//Dialog text variables
var title;
var msg, msg2;
var btnOkText;
var btnCancelText;
var actionShow = 1;
var actionHide = 0;

//Dialog states
var dialogIsOpen = false;

$( document ).ready( function() {
    
    $(".collapse_btn").each(function(index) {
	if($(this).text() == ''){
	    $(this).text('-');
	}
	
	var currentId = $(this).attr('id');
	currentId = currentId.replace('collapse_btn-','');
	if ($('tr[id^="'+currentId+'"]').siblings('#child-'+currentId).size() == 0){
	    $(this).text('');
	}
    });
    
    var jsonArr = getCookie("rowsToCollapse");
    
    if(jsonArr.length > 0){
	var rowsArr = jQuery.parseJSON(jsonArr);
	
	if(rowsArr.length > 0){
	    $.each(rowsArr, function(i, arrVal){  
		var trId = arrVal.rowId;
		var action = arrVal.action;
		
		if ($('tr.group').siblings('#child-'+trId).size() == 0){
			$('#collapse_btn-'+trId).text('');
		}else{
		    if(action == actionShow){
			$('tr.group').siblings('#child-'+trId).toggle(true);

			if($('#collapse_btn-'+trId).text() == '+') {
			    $('#collapse_btn-'+trId).text('-');
			}
		    }else if(action == actionHide){
			$('tr.group').siblings('#child-'+trId).toggle(false);

			if($('#collapse_btn-'+trId).text() == '-') {
			    $('#collapse_btn-'+trId).text('+');
			}
		    }
		}//if2
	    });
	}//if
    }
});

function collapseRow(trId){
      
    if($('#collapse_btn-'+trId).text() == '-') {
	$('#collapse_btn-'+trId).text('+');
	$('tr.group').siblings('#child-'+trId).toggle(false);
	checkCookie(trId, actionHide);
    }else if($('#collapse_btn-'+trId).text() == '+'){
	$('tr.group').siblings('#child-'+trId).toggle(true);
	$('#collapse_btn-'+trId).text('-');
	checkCookie(trId, actionShow);
    }
}
    
function showConfirmationDialog(id, electionId, url, languageCode, caller, back){
    
    if(dialogIsOpen == false) {
	setTextValues(languageCode, caller);

	$('<div></div>').appendTo('#mainContents')
	.html('<div><p>'+msg+'?<br />'+msg2+'.</p></div>')
	.dialog({
	    title: title+'?', 
	    width:  'auto', 
	    height: 'auto',
	    modal: true,
	    resizable: false,
	    buttons: [{
		    text: btnOkText,
		    click: function () {
			$('#messagesContainer').empty();
			remove(id, electionId, url, back);
			$(this).dialog("close");
			dialogIsOpen = false;
		    }
		},{
		    text: btnCancelText,
		    click: function () {
			$(this).dialog("close");
			dialogIsOpen = false;
		    }
		}],
	    close: function (event, ui) {
		$(this).remove();
		dialogIsOpen = false;
	    }
	});
	dialogIsOpen = true;
    }
}

function remove(id, electionId, url, back){
    
    $.ajax({
        type: "POST",
        url: url+"="+id+"&electionId="+electionId,
        success: function (data) {     
            var response = $.parseJSON(data);
            if(response.status == true){
                window.location = back+"&success=removedsuccessfully";
            }else{
		$('<div class="errorMsgDiv">'+response.msg+'</div>').appendTo('#messagesContainer');
	    }
        },
        error: function (xhr, desc, err) {
            $('<div class="errorMsgDiv">'+err+'</div>').appendTo('#messagesContainer');
        }
    });
}

function setTextValues(languageCode, caller){   

    switch (languageCode) {
	case 'FIN':
	    title = 'Poistetaanko ' + (caller == 1 ? 'vaaliliitto' : 'vaalirengas');
	    msg = 'Haluatko varmasti poistaa ' + (caller == 1 ? 'vaaliliiton' : 'vaalirenkaan');
	    msg2 = 'Jos painat KYLLÄ, valittu ' + (caller == 1 ? 'vaaliliitto' : 'vaalirengas') + ' poistetaan';
	    btnOkText = 'Kyllä';
	    btnCancelText = 'Ei';
	    break;
	case 'ENG':
	    title = 'Remove ' + (caller == 1 ? 'alliance' : 'coalition');
	    msg = 'Are you sure you want to continue';
	    msg2 = 'If you press YES, chosen ' + (caller == 1 ? 'alliance' : 'coalition') + ' will be removed';
	    btnOkText = 'Yes';
	    btnCancelText = 'No';
	    break;
	default:
	    title = 'Remove ' + (caller == 1 ? 'alliance' : 'coalition');
	    msg = 'Are you sure you want to continue';
	    msg2 = 'If you press YES, chosen ' + (caller == 1 ? 'alliance' : 'coalition') + ' will be removed';
	    btnOkText = 'Yes';
	    btnCancelText = 'No';
	    break;
    }
    
}

function setCookie(jsonStr){
    document.cookie = "rowsToCollapse=" + jsonStr;
}

function getCookie(Name){
    
    var search = Name + "="
    var returnvalue = "";
    if (document.cookie.length > 0) {
      offset = document.cookie.indexOf(search)

      if (offset != -1) { 
	offset += search.length

	end = document.cookie.indexOf(";", offset);

	if (end == -1) end = document.cookie.length;
	    returnvalue=unescape(document.cookie.substring(offset, end))
	}
    }
    return returnvalue;
}

function checkCookie(trId, action){
    
    var rowsArr = [];
    var jsonArr = getCookie("rowsToCollapse");
    var jsonStr = "";
    
    if(jsonArr.length < 1){
	rowsArr.push({"rowId":trId,"action":action})
	jsonStr = JSON.stringify(rowsArr); 
	setCookie(jsonStr);
    }else{
	var found = false;
	var index = 0;
	var temp = jQuery.parseJSON(jsonArr);
	if(temp.length > 0){
	    rowsArr = temp;
	    
	    $.each(temp, function(i, arrVal){  
		if (arrVal.rowId === trId) {    
		    found = true;
		    index = i;
		}    
	     });
	 }//if

	 if(found == false){
	     rowsArr.push({"rowId":trId,"action":action})
	     jsonStr = JSON.stringify(rowsArr); 
	     setCookie(jsonStr);
	 }else{
	     rowsArr[index] = {"rowId":trId,"action":action};
	     jsonStr = JSON.stringify(rowsArr); 
	     setCookie(jsonStr);
	 }
    }
    //alert(jsonStr);
}