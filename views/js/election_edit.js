$( document ).ready( function() {
    
    //TODO language selection
    setDatepickerLocalization('FI');
    $("#start_date").datepicker({dateFormat: 'dd.mm.yy'});
    $("#end_date").datepicker({dateFormat: 'dd.mm.yy' });
    
    //$("#startDatepickerImage").click(function(e) { e.preventDefault(); $("#start_date").datepicker("show"); });
    //$("#endDatepickerImage").click(function(e) {e.preventDefault(); $("#end_date").datepicker("show"); });
    
    //toggle();
    $("#stateOpen").click(function(){
        $("#stateOpen").prop('checked', true);
        $("#stateClosed").prop('checked', false);
	$("#stateEnded").prop('checked', false);
    });
    
    $("#stateClosed").click(function(){
        $("#stateClosed").prop('checked', true);
        $("#stateOpen").prop('checked', false);
	$("#stateEnded").prop('checked', false);
    });
    
    $("#stateEnded").click(function(){
        $("#stateClosed").prop('checked', false);
        $("#stateOpen").prop('checked', false);
	$("#stateEnded").prop('checked', true);
    });
    
    $("#countCoalitions").click(function(){
        $("#countCoalitions").prop('checked', true);
        $("#noCoalitions").prop('checked', false);
    });
    
    $("#noCoalitions").click(function(){
        $("#countCoalitions").prop('checked', false);
        $("#noCoalitions").prop('checked', true);
    });

});

/*function toggle(){
    
    if ($('input[name=stateOpen]:checked')) {
        $("#stateOpen").prop('checked', true);
        $("#stateClosed").prop('checked', false);
	$("#stateEnded").prop('checked', false);
    } else if($('input[name=stateClosed]:checked')) {
        $("#stateClosed").prop('checked', true);
        $("#stateOpen").prop('checked', false); 
	$("#stateEnded").prop('checked', false);        
    } else if($('input[name=stateEnded]:checked')) {
        $("#stateClosed").prop('checked', false);
        $("#stateOpen").prop('checked', false); 
	$("#stateEnded").prop('checked', true);        
    }
    
    if ($('input[name=countCoalitions]:checked')) {
        $("#countCoalitions").prop('checked', true);
        $("#noCoalitions").prop('checked', false);
    } else if($('input[name=noCoalitions]:checked')) {
        $("#noCoalitions").prop('checked', true);
        $("#countCoalitions").prop('checked', false); 
    }
}*/

function setDatepickerLocalization(languageCode){  

    switch(languageCode){
        
        case 'FI':
            $.datepicker.regional['fi'] = {
                closeText: 'Sulje',
                prevText: '&laquo;Edellinen',
                nextText: 'Seuraava&raquo;',
                currentText: 'T&auml;n&auml;&auml;n',
                monthNames: ['Tammikuu','Helmikuu','Maaliskuu','Huhtikuu','Toukokuu','Kes&auml;kuu',
                'Hein&auml;kuu','Elokuu','Syyskuu','Lokakuu','Marraskuu','Joulukuu'],
                monthNamesShort: ['Tammi','Helmi','Maalis','Huhti','Touko','Kes&auml;',
                'Hein&auml;','Elo','Syys','Loka','Marras','Joulu'],
                dayNamesShort: ['Su','Ma','Ti','Ke','To','Pe','Su'],
                dayNames: ['Sunnuntai','Maanantai','Tiistai','Keskiviikko','Torstai','Perjantai','Lauantai'],
                dayNamesMin: ['Su','Ma','Ti','Ke','To','Pe','La'],
                weekHeader: 'Vk',
                dateFormat: 'dd.mm.yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults( $.datepicker.regional[ "fi" ] );
            break;
        default:
            $.datepicker.setDefaults( $.datepicker.regional[ "" ] );
            break;
    } 
}