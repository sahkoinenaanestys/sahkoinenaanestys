$( document ).ready( function() {
	var checked;
	
	$('#setPassword').change(function() {		
		
		if ($('#setPassword').is(':checked') && $("#passwdDiv").not(':visible')) {
		    $("#passwdDiv").slideDown("fast");
		} else if($('#setPassword').not(':checked') && $("#passwdDiv").is(':visible')){
		    $("#passwdDiv").slideUp("fast");
		} 
		
	});
});
