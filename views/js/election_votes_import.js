var loadingText = 'Saving data...';

$(document).ready(function()
{
    //adding paper votes to db
    $('#votesFileUploader').on('submit', function(ev2)
    {
        ev2.preventDefault();
        $('#votesUploadBtn').attr('disabled', ''); // disable upload button
        
        //show loading message
        //$("#output").show();
        $("#output").Loadingdotdotdot({
            "speed": 400,
            "maxDots": 4,
            "word": loadingText
        });
        
        $(this).ajaxSubmit({
            target: '#messagesContainer',
            success:  afterFinished
        });
    });
});

function afterFinished()
{
    $("#output").Loadingdotdotdot("Stop");
    $("#output").empty();
    $('#votesFileUploader').resetForm();  // reset form
    $('#votesUploadBtn').removeAttr('disabled'); //enable submit button
}