//Dialog text variables
var title;
var msg, msg2;
var btnOkText;
var btnCancelText;

//Dialog states
var dialogIsOpen = false;

//Election states
var state_closed = 0;
var state_active = 1;
var state_ended = 2;
var state_history = 3;

$( document ).ready( function() {
    //page specific code goes here
});    
    
function showConfirmationDialog(electionId, url, languageCode, caller, back){

    if(dialogIsOpen == false) {
	setTextValues(languageCode, caller);

	$('<div></div>').appendTo('#mainContents')
	.html('<div><p>'+msg+'<br />'+msg2+'.</p></div>')
	.dialog({
	    title: title+'?', 
	    width:  'auto', 
	    modal: true,
	    height: 'auto',
	    resizable: false,
	    buttons: [{
		    text: btnOkText,
		    click: function () {
			$('#messagesContainer').empty();
			ajaxPost(electionId, url, back, caller);
			$(this).dialog("close");
			dialogIsOpen = false;
		    }
		},{
		    text: btnCancelText,
		    click: function () {
			$(this).dialog("close");
			dialogIsOpen = false;
		    }
		}],
	    close: function (event, ui) {
		dialogIsOpen = false;
		$(this).remove();
	    }
	});
	dialogIsOpen = true;
    }
}

function ajaxPost(electionId, url, back, caller){
    
    $.ajax({
        type: "POST",
        url: url + "&electionId=" + electionId,
        success: function (data) {     
            var response = $.parseJSON(data);
	    if(response.status == true){
		switch(caller){
		    case 1: window.location = back+"&success=electionremoved"; break;
		    case 2: window.location = back+"&success=electionended";break;
		    case 3: window.location = back+"&success=electionmovedtohistory";break;
		}//switch
            }else{
		switch(caller){
		    case 1: window.location = back+"&error=electionremovefailure";break;
		    case 2: window.location = back+"&error=electionendfailure";break;
		    case 3: window.location = back+"&error=electionhidefailed";break;
		}//switch
	    }
        },
        error: function (xhr, desc, err) {
            $('<div class="errorMsgDiv">'+err+'</div>').appendTo('#messagesContainer');
        }
    });
}

function startOrStopElection(electionId, url, back){
    
    $.ajax({
        type: "POST",
        url: url + "&electionId=" + electionId,
        success: function (data) {     
            var response = $.parseJSON(data);
            if(response.status == true){
		if(response.election_status == state_active){
		    window.location = back+"&success=electionopensuccess";
		}else if(response.election_status == state_closed){
		    window.location = back+"&success=electionclosesuccess";
		}
            }else{
		if(response.election_status == state_active){
		    window.location = back+"&error=electionopenfailure";
		}else if(response.election_status == state_closed){
		    window.location = back+"&error=electionclosefailure";
		}
	    }
        },
        error: function (xhr, desc, err) {
            $('<div class="errorMsgDiv">'+err+'</div>').appendTo('#messagesContainer');
        }
    });
}

function showStartOrStopElectionDialog(electionId, electionState, url, languageCode, back){
    
     if(dialogIsOpen == false) {
	if(languageCode == 'FIN') {
	       if(electionState==state_active){ 
		   title = 'Suljetaanko vaali'; 
		   msg = 'Jos painat KYLLÄ, valittu vaali suljetaan';
	       }else if(electionState==state_closed){ 
		   title = 'Merkataanko vaali aktiiviseksi?'; 
		   msg = 'Jos painat KYLLÄ, valittu vaali merkataan aktiiviseksi';
	       }
	       btnOkText = 'Kyllä';
	       btnCancelText = 'Ei';
       }else{
	       if(electionState==state_active){ 
		   title = 'Close election'; 
		   msg = 'If you press YES, chosen election will be marked as closed';
	       }else if(electionState==state_closed){ 
		   title = 'Open election?'; 
		   msg = 'If you press YES, chosen election will be marked as open';
	       }
	       btnOkText = 'Yes';
	       btnCancelText = 'No';
       }

       $('<div id="dialog"></div>').appendTo('#mainContents')
       .html('<div><p>'+msg+'.</p></div>')
       .dialog({
	   title: title+'?', 
	   width:  'auto', 
	   height: 'auto',
	   modal: true,
	   resizable: false,
	   buttons: [{
		   text: btnOkText,
		   click: function () {
		       $('#messagesContainer').empty();
		       startOrStopElection(electionId, url, back);
		       dialogIsOpen = false;
		       $(this).dialog("close");
		   }
	       },{
		   text: btnCancelText,
		   click: function () {
		       dialogIsOpen = false;
		       $(this).dialog("close");
		   }
	       }],
	   close: function (event, ui) {
	       dialogIsOpen = false;
	       $(this).remove();
	   }
       });
       dialogIsOpen = true;
    }
         
}

function setTextValues(languageCode, caller){   
    
    if(caller == 1){
	switch (languageCode) {
	    case 'FIN':
		title = 'Poistetaanko vaali';
		msg = 'Haluatko varmasti poistaa vaalin?';
		msg2 = 'Jos painat KYLLÄ, valittu vaali poistetaan';
		btnOkText = 'Kyllä';
		btnCancelText = 'Ei';
		break;
	    default:
		title = 'Remove election';
		msg = 'Are you sure you want to continue?';
		msg2 = 'If you press YES, chosen election will be removed';
		btnOkText = 'Yes';
		btnCancelText = 'No';
		break;
	}
    }else if(caller == 2){
	switch (languageCode) {
	    case 'FIN':
		title = 'Päätetäänkö vaali';
		msg = 'Haluatko varmasti päättää vaalin? Vaalia ei voida enää jatkaa kun se on päätetty.';
		msg2 = 'Jos painat KYLLÄ, valittu vaali merkataan päättyneeksi';
		btnOkText = 'Kyllä';
		btnCancelText = 'Ei';
		break;
	    default:
		title = 'End election';
		msg = 'Are you sure you want to end this election? Once election is ended, it cannot be continued.';
		msg2 = 'If you press YES, chosen election will be marked as ended';
		btnOkText = 'Yes';
		btnCancelText = 'No';
		break;
	}
    }else if(caller == 3){
	switch (languageCode) {
	    case 'FIN':
		title = 'Siirretäänkö vaali historiaan';
		msg = 'Haluatko varmasti siirtää vaalin historiaan?';
		msg2 = 'Jos painat KYLLÄ, valittu vaali siirretään menneisiin vaaleihin';
		btnOkText = 'Kyllä';
		btnCancelText = 'Ei';
		break;
	    default:
		title = 'Move election to history';
		msg = 'Are you sure you want to move this election to history?';
		msg2 = 'If you press YES, chosen election will moved to past elections';
		btnOkText = 'Yes';
		btnCancelText = 'No';
		break;
	}
    }
}