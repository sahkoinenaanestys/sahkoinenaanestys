//Dialog text variables
var removeTitle;
var errorTitle;
var markerTitle;
var msg, msg2;
var msgVoter, msgElectionWorker;
var btnOkText;
var btnCancelText;
var btnCloseText;

//Dialog states
var dialogIsOpen = false;

function showMarkerDialog(adminId, studentNum, url, languageCode) {		
//	debugger;
	setTextValues(languageCode);
		
	$.ajax({
        type: "POST",
        url: url+'&adminId='+adminId, 
        success: function (data) {
            var response = $.parseJSON(data);            
            
            if(response.success == false) {
            	//show error dialog
            	showErrorDialog(response.msg, languageCode);
            }
            else {            	
            	//show info dialog
            	$('<div></div>').appendTo('#mainContents')
            	.html('<div><p>'+msgVoter+': '+studentNum+'</p>'+
            		'<p>'+msgElectionWorker+':</p>' +
            		response.infoTable+'</div>')
            	.dialog({
					title : markerTitle,
					width : 'auto',
					height : 'auto',
					modal : true,
					resizable : false,
					buttons : [ {
						text : btnCloseText,
						click : function() {
							$(this).dialog("close");
						}
					} ],
					close : function(event, ui) {
						$(this).remove();
					}
            	});
            }
        },
        error: function (xhr, desc, err) {    	
        	//show error dialog
        	showErrorDialog('xhr: '+xhr+'<br>desc: '+desc+'<br>err: '+err, languageCode);
        }
    });
}

function showConfirmationDialog(studentNum, url, languageCode, back) {	
    
    if(dialogIsOpen == false) {
	setTextValues(languageCode);        

	$('<div></div>').appendTo('#mainContents')
	.html('<div><p>'+msg+'?<br />'+msg2+': '+studentNum+'</p></div>')
	.dialog({
	    title: removeTitle+'?', 
	    width:  'auto', 
	    height: 'auto',
	    modal: true,
	    resizable: false,
	    buttons: [{
		    text: btnOkText,
		    click: function () {
			removeVoter(studentNum, url, languageCode, back);
			$(this).dialog("close");
			dialogIsOpen = false;
		    }
		},{
		    text: btnCancelText,
		    click: function () {
			$(this).dialog("close");
			dialogIsOpen = false;
		    }
		}],
	    close: function (event, ui) {
		$(this).remove();
		dialogIsOpen = false;
	    }
	});
	dialogIsOpen = true;
    }
}

function removeVoter(studentNum, url, languageCode, back){	
	$.ajax({
        type: "POST",
        url: url+'&studentNum='+studentNum, 
        success: function (data) {     
            var response = $.parseJSON(data);            
            
            if(response.success == false) {
            	//show error dialog
            	showErrorDialog(response.msg, languageCode);
            }
            else {            	
                window.location = back+"&success=voterremoved";
            }
        },
        error: function (xhr, desc, err) {    	
        	//show error dialog
        	showErrorDialog('xhr: '+xhr+'<br>desc: '+desc+'<br>err: '+err, languageCode);
        }
    });
}

function showErrorDialog(msg, languageCode) {
	setTextValues(languageCode);
	
	$('<div></div>').appendTo('#mainContents').html('<div><p>'+msg+'</p></div>').dialog({            		
		title: errorTitle, 
        width:  '365px', 
        height: 'auto',
        resizable: false,
		buttons: [{
            text: btnCloseText,
            click: function () {                            
                $(this).dialog("close");
            }
        }],
        close: function (event, ui) {
            $(this).remove();
        }
	});
}

function setTextValues(languageCode) {       
    switch (languageCode) {
        case 'FIN':
            removeTitle = 'Poistetaanko äänioikeutettu';
            errorTitle = 'Tapahtui virhe';
            markerTitle = 'Vaalivirkailija';
            msg = 'Haluatko varmasti poistaa äänioikeutetun';
            msg2 = 'Jos painat KYLLÄ, seuraava äänioikeutettu poistetaan';
            msgVoter = 'Äänestäjä';
            msgElectionWorker = 'Tämän paperiäänestyksen on merkannut seuraava vaalivirkailija';
            btnOkText = 'Kyllä';
            btnCancelText = 'Ei';            
            btnCloseText = 'Sulje';
            break;
        default:
            removeTitle = 'Remove voter';
	    errorTitle = 'An error occured';
	    markerTitle = 'Election worker';
	    msg = 'Are you sure you want to continue';
            msg2 = 'If you press YES, this voter will be removed';
            msgVoter = 'Voter';
            msgElectionWorker = 'This paper vote has been marked by the following election worker';
            btnOkText = 'Yes';
            btnCancelText = 'No';            
            btnCloseText = 'Close';
            break;
    }    
}