//var loadingText = 'Exporting results...';
//
$(document).ready(function()
{
//    $('#FileDownloader').on('submit', function(e)
//    {
//        e.preventDefault();
//        $('#downloadBtn').attr('disabled', ''); // disable upload button
//        
//        //show loading message
//        //$("#output").show();
//        $("#output").Loadingdotdotdot({
//            "speed": 400,
//            "maxDots": 4,
//            "word": loadingText
//        });
//        
//        $(this).ajaxSubmit({
//            target: '#messagesContainer',
//            success:  afterFinished
//        });
//    });

    if ($('#downloadVoteFiles').is(':checked') && $("#voteFilesDiv").not(':visible')) {
		$("#voteFilesDiv").show()
    } else if($('#downloadVoteFiles').not(':checked') && $("#voteFilesDiv").is(':visible')){
		$("#voteFilesDiv").hide();
    }
    
    $('#downloadVoteFiles').change(function() {		
	    if ($('#downloadVoteFiles').is(':checked') && $("#voteFilesDiv").not(':visible')) {
		$("#voteFilesDiv").slideDown("fast");
	    } else if($('#downloadVoteFiles').not(':checked') && $("#voteFilesDiv").is(':visible')){
		$("#voteFilesDiv").slideUp("fast");
	    } 
    });
});
//
//function afterFinished()
//{
//    $("#output").Loadingdotdotdot("Stop");
//    $("#output").empty();
//    $('#FileDownloader').resetForm();  // reset form
//    $('#downloadBtn').removeAttr('disabled'); //enable submit button
//}