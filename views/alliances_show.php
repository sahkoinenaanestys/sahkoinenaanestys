<h3><?php echo ALLIANCES_AND_COALITIONS_TEXT; ?></h3>
<?php $languageCode = $_SESSION['language']; 
      $electionId = $data['election']['electionId'];
      $back = SITE_ROOT."/index.php?alliance"; ?>

<p><a href="<?php echo SITE_ROOT."/index.php?alliance&action=editCoalition" ?>">[<?php echo NEW_TEXT.' '.mb_strtolower(COALITION_TEXT,'UTF-8');?>]</a></p>
    	
<table class="editListTable">
<thead>	
	<tr>
	    <th>&nbsp;</th>
	    <th><?php echo NAME_TEXT;?></th>		
	    <th><?php echo ID_TEXT;?></th>
	    <th><?php echo EMAIL_OF_PERSON_IN_CHARGE_TEXT;?></th>
	    <th><?php echo ACTIONS_TEXT;?></th>
	</tr>
</thead>
<tbody>
    <?php $url = SITE_ROOT."/index.php?alliance&action=removeAllianceAjax&allianceId"; ?>
    <tr class="group" id="<?php echo $electionId.'-0';?>">
    <td><a href="#" title='<?php echo CLICK_TO_EXPAND_COLLAPSE_TEXT; ?>' class="collapse_btn" id="collapse_btn-<?php echo $electionId.'-0';?>" onclick="collapseRow('<?php echo $electionId.'-0'; ?>');return false;"></a></td>
    <td><b><?php echo NOT_IN_COALITION_TEXT;?></b></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><?php  echo '<a href="'.SITE_ROOT.'/index.php?alliance&action=addAlliance">'.ADD_TEXT.' '.mb_strtolower(ALLIANCE_TEXT,'UTF-8').'</a>';?></td>
    </tr>
	<?php 
	if(!empty($data['alliances'])){ 
	foreach($data['alliances'] as $alliance){
	    $key = $alliance['coalition']; 
	    
	    if (empty($key)) { ?>
		<tr id="child-<?php echo $electionId.'-0'; ?>" class="<?php if(!isset($row)): $row = 1; endif; echo ($row++%2==1)? 'odd': 'even';?>">
		<td>&nbsp;</td>
		<td><?php echo $alliance['allianceName'];?></td>
		<td><?php echo $alliance['allianceId'];?></td>
		<td><?php echo $alliance['personInChargeEmail'];?></td>
		<td><?php 
			echo '<a href="'.SITE_ROOT."/index.php?alliance&action=editAlliance&allianceId=".$alliance['allianceId'].'">'.EDIT_TEXT.'</a> | ';
			echo '<a href="#" onclick="showConfirmationDialog('.$alliance['allianceId'].','.$alliance['election'].',\''.$url.'\',\''.$languageCode.'\', 1, \''.$back.'\');return false;">'.REMOVE_TEXT.'</a>';
		?>
		</td>   
		</tr>
	    <?php }
	}
    }//if
    ?>		
	
    <?php
    $url2 = SITE_ROOT."/index.php?alliance&action=removeCoalitionAjax&coalitionId"; 
    if(!empty($data['coalitions'])){
	
	foreach ($data['coalitions'] as $coalition) { 
	    $row2 = 1 ?>
	<tr class="group" id="<?php echo $electionId.'-'.$coalition['coalitionId']; ?>">
	<td><a href="#" title='<?php echo CLICK_TO_EXPAND_COLLAPSE_TEXT; ?>' class="collapse_btn" id="collapse_btn-<?php echo $electionId.'-'.$coalition['coalitionId'];?>" onclick="collapseRow('<?php echo $electionId.'-'.$coalition['coalitionId']; ?>');return false;"></a></td>
	<td><b><?php echo $coalition['coalitionName'];?></b></td>
	<td><b><?php echo $coalition['coalitionId'];?></b></td>
	<td>&nbsp;</td>
	<td><?php 
		echo '<a href="'.SITE_ROOT.'/index.php?alliance&action=addAlliance&coalitionId='.$coalition['coalitionId'].'">'.ADD_TEXT.' '.mb_strtolower(ALLIANCE_TEXT,'UTF-8').'</a> | ';
		echo '<a href="'.SITE_ROOT."/index.php?alliance&action=editCoalition&coalitionId=".$coalition['coalitionId'].'">'.EDIT_TEXT.'</a> | ';
		echo '<a href="#" onclick="showConfirmationDialog('.$coalition['coalitionId'].','.$coalition['election'].',\''.$url2.'\',\''.$languageCode.'\', 2,\''.$back.'\');return false;">'.REMOVE_TEXT.'</a>';
	?>
	</td>
	</tr>
	<?php 
	$key = $coalition['coalitionId']; 
	    if(!empty($data['alliances'])){
		foreach($data['alliances'] as $alliance){
		    if ($alliance['coalition'] == $key) { ?>
		    <tr id="child-<?php echo $electionId.'-'.$coalition['coalitionId']; ?>" class="<?php if(!isset($row2)): $row2 = 1; endif; echo ($row2++%2==1)? 'odd': 'even';?>">
		    <td>&nbsp;</td>
		    <td><?php echo $alliance['allianceName'];?></td>
		    <td><?php echo $alliance['allianceId'];?></td>
		    <td><?php echo $alliance['personInChargeEmail'];?></td>
		    <td><?php 
			    echo '<a href="'.SITE_ROOT.'/index.php?alliance&action=editAlliance&coalitionId='.$coalition['coalitionId'].'&allianceId='.$alliance['allianceId'].'">'.EDIT_TEXT.'</a> | ';
			    echo '<a href="#" onclick="showConfirmationDialog('.$alliance['allianceId'].','.$alliance['election'].',\''.$url.'\',\''.$languageCode.'\', 1,\''.$back.'\');return false;">'.REMOVE_TEXT.'</a>';
		?>
		</td>   
		</tr>
	    <?php   }
		}//foreach2
	    }
	}//foreach
    }
    ?>
</tbody>	
</table>
