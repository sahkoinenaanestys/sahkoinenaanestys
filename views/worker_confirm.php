<h3><?php echo VOTERS_TEXT;?></h3>

<p><?php echo MARK_THE_VOTE_CONFIRM_TEXT;?></p>

<table class="loginInformation">
	<tr><th><?php echo NAME_TEXT;?>:</th><td><?php echo $data['voter']['firstName']." ".$data['voter']['lastName']?></td></tr>
	<tr><th><?php echo STUDENT_NUMBER_TEXT;?>:</th><td><?php echo $data['voter']['studentNum']?></td></tr>
	<tr><th><?php echo EMAIL_TEXT;?>:</th><td><?php echo $data['voter']['email'];?></td></tr>
</table>
<br>

<form action="<?php echo SITE_ROOT.'/index.php?worker&action=confirm';?>" method="POST">
	<input type="hidden" name="listReferer" value="<?php echo $data['referer'];?>"/>
	<input type="hidden" name="studentNum" value="<?php echo $data['voter']['studentNum'];?>"/>
	<input type="submit" name="markVote" class="editSubmitButton" value="<?php echo YES_TEXT; ?>">	
	<a href="<?php echo SITE_ROOT.'/index.php?worker'.$data['referer'];?>" class="editLinkButton"><?php echo CANCEL_TEXT;?></a>
</form>