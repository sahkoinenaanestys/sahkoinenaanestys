<h3><?php echo IMPORT_ALLIANCES_TEXT; ?></h3>
<p><?php echo IMPORT_CSV_TEXT.' '.PLEASE_WAIT_TEXT ?></p>

<div id="theForm">
<p><table border="0">
<form action="<?php echo SITE_ROOT.'/index.php?alliance&action=importFromCsvAjax'; ?>" id="FileUploader" enctype="multipart/form-data" method="post" >
    <tr><td><?php echo CSV_DELIMITER_TEXT.': '; ?></td></tr>
    <tr><td>
	<select id="csvDelimiter" name="csvDelimiter" class="inputSelect" >
	    <option value="<?php echo CSV_COMMA_DELIMITER;?>"><?php echo CSV_COMMA_DELIMITER_TEXT; ?></option>
	    <option value="<?php echo CSV_SEMICOLON_DELIMITER;?>"><?php echo CSV_SEMICOLON_DELIMITER_TEXT; ?></option>
	    <option value="<?php echo CSV_PIPE_DELIMITER;?>"><?php echo CSV_PIPE_DELIMITER_TEXT; ?></option>
	    <option value="<?php echo CSV_TAB_DELIMITER;?>"><?php echo CSV_TAB_DELIMITER_TEXT; ?></option>
	</select>
    </td></tr>
    <tr><td><?php echo CHOOSE_A_FILE_TEXT.':'; ?></td></tr>
    <tr><td><input type="file" name="mFile" id="mFile" /></td></tr>
    <tr><td><button type="submit" id="uploadBtn"><?php echo UPLOAD_TEXT; ?></button></td></tr>
    <div class="spacer"></div>    
</form>
</table></p>
<div id="output"></div>
</div>