<h3><?php echo VOTERS_TEXT;?></h3>
<?php $back = SITE_ROOT."/index.php?suffrage"; ?>

<div>
<form action="<?php echo SITE_ROOT.'/index.php?suffrage';?>" method="POST"> 
	<input type="text" name="search" value="<?php echo (isset($data['searchStr']))?urldecode($data['searchStr']):'';?>" class="searchField"/>
	<input type="submit" value="<?php echo FIND_TEXT;?>" class="searchButton"/>
</form>
</div>
<p>
	<a href="<?php echo SITE_ROOT.'/index.php?suffrage&action=edit';?>">[<?php echo NEW_VOTER_TEXT;?>]</a>
<?php 
if(empty($data['voters'])) {
	echo '</p><p>'.NO_VOTERS_TEXT.'</p>';
}
else { ?>	
	<a href="<?php echo SITE_ROOT.'/index.php?suffrage&action=removeAll';?>">[<?php echo REMOVE_ALL_VOTERS_TEXT;?>]</a>				
</p>

<div><?php include SERVER_ROOT.'/views/inc/pagination.php';?></div>

<table class="editListTable">
<thead>	
	<tr>
		<th><?php echo STUDENT_NUMBER_TEXT;?></th>
		<th><?php echo NAME_TEXT;?></th>		
		<th><?php echo RIGHT_TO_VOTE_TEXT;?></th>
		<th><?php echo VOTED_TEXT;?></th>
		<th><?php echo ACTIONS_TEXT;?></th>
	</tr>
</thead>
<tbody>
	<?php foreach($data['voters'] as $voter):?>
	<tr class="<?php if(!isset($row)): $row = 1; endif; echo ($row++%2==1)? 'odd': 'even';?>">		
		<td><?php echo $voter['studentNum'];?></td>
		<td><?php echo $voter['firstName']." ".$voter['lastName'];?></td>
		<td><?php echo '<span class="'; echo ($voter['canVote'] == 1)? 'positiveMsg">'.YES_TEXT: 'negativeMsg">'.NO_TEXT; echo '</span>'?></td>
		<td><?php 
		if($voter['hasVoted'] == 1) {
			echo '<span class="positiveMsg">'.YES_TEXT.'</span>';
			switch ($voter['voteMethod']) {
				case 'paper':
					echo ' (<a href="#" onclick="showMarkerDialog('.$voter['electionWorker'].
						', '.$voter['studentNum'].
						', \''.SITE_ROOT.'/index.php?account&action=ajaxFetchInfo'.'\''.
						', \''.$_SESSION['language'].'\')">'.PAPER_VOTE_SHORT_TEXT.'</a>)';
					break;
				case 'www':
					echo ' ('.ELECTRONIC_VOTE_SHORT_TEXT.')';
					break;
				default:
					break;
			}
		} else {
			echo '<span class="negativeMsg">'.NO_TEXT.'</span>';
		}			
		?></td>
		<td><?php 
		echo '<a href="'.SITE_ROOT.'/index.php?suffrage&action=edit&studentNum='.$voter['studentNum'].
				'&referer='.$data['referer'].'">'.EDIT_TEXT.'</a> | ';
		echo '<a href="#" onclick="showConfirmationDialog('.$voter['studentNum'].
			',\''.SITE_ROOT.'/index.php?'.$controllerName.'&action=ajaxRemove'.'\',\''.$_SESSION['language'].'\',\''.$back.'\');return false;">'.REMOVE_TEXT.'</a>';		
		?></td>
	</tr>
	<?php endforeach;?>
</tbody>	
<?php } //end else?>	
</table>