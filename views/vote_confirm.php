<?php 
include SERVER_ROOT.'/views/inc/logininfo.php';
?>

<h2><?php echo PLEASE_CONFIRM_YOUR_CANDIDATE_CHOICE_TEXT;?></h2>

<p> 
<?php 
if($data['isEmpty']) {
	echo YOU_ARE_ABOUT_TO_VOTE_EMPTY_TEXT.".";
} else {
	echo YOU_ARE_ABOUT_TO_VOTE_FOR_THE_FOLLOWING_CANDIDATE_TEXT.":";?>
	<span class="candidateBig"><?php echo NUMBER_ABBREVIATION_TEXT.' '.$data['candidateNum'].', '.$data['firstName'].' '.$data['lastName']?></span><?php 
}?>
</p>

<div class="buttonHolder">
	<a href="<?php echo SITE_ROOT.'/index.php?vote'?>" class="buttonLink"><?php echo GO_BACK_TEXT?></a>
	<form method="post" action="<?php echo SITE_ROOT.'/index.php?vote&action=accept'?>">
		<input type="hidden" name="candidate" value="<?php echo $data['candidateNum']?>" />
		<input type="submit" value="<?php echo VOTE_TEXT;?>" class="buttonSubmit">
	</form>
</div>
