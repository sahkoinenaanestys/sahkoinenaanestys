
<h3><?php echo ADMINISTRATORS_TEXT;?></h3>
<?php $back = SITE_ROOT."/index.php?account"; ?>

<div>
<form action="<?php echo SITE_ROOT.'/index.php?account';?>" method="POST"> 
	<input type="text" name="search" value="<?php echo (isset($data['searchStr']))?urldecode($data['searchStr']):'';?>" class="searchField"/>
	<input type="submit" value="<?php echo FIND_TEXT;?>" class="searchButton"/>
</form>
</div>

<p>
	<a href="<?php echo SITE_ROOT.'/index.php?'.$controllerName.'&action=edit';?>">[<?php echo ADD_NEW_ADMIN_TEXT;?>]</a>
	<?php 
if(empty($data['admins'])) {
	echo '</p><p>'.NO_ADMINISTRATORS_TEXT.'</p>';
}
else { ?>
</p>

<div><?php include SERVER_ROOT.'/views/inc/pagination.php';?></div>

<table class="editListTable">
<thead>	
	<tr>
		<th><?php echo USERNAME_TEXT;?></th>
		<th><?php echo NAME_TEXT;?></th>	
		<th><?php echo EMAIL_TEXT;?></th>
		<th><?php echo LEVEL_TEXT;?></th>
		<th><?php echo ACTIONS_TEXT;?></th>
	</tr>
</thead>
<tbody>
	<?php foreach($data['admins'] as $admin):?>
	<tr class="<?php if(!isset($row)): $row = 1; endif; echo ($row++%2==1)? 'odd': 'even';?>">		
		<td><?php echo $admin['username'];?></td>
		<td><?php echo $admin['firstName']." ".$admin['lastName'];?></td>
		<td><?php echo $admin['email']?></td>
		<td><?php switch ($admin['adminLevel']) {
			case 10:
				echo '<span style="font-weight:bold">'.ADMINISTRATOR_TEXT.'</span>';
				break;
			case 5:
				echo ELECTION_WORKER_TEXT;
				break;
			default:
				break;
		}?></td>
		<td><?php 
			echo '<a href="'.SITE_ROOT.'/index.php?'.$controllerName.'&action=edit&adminId='.$admin['adminId'].'">'.EDIT_TEXT.'</a> | ';
			echo '<a href="#" onclick="showConfirmationDialog(\''.$admin['username'].'\',\''.SITE_ROOT.'/index.php?'.$controllerName.'&action=ajaxRemove'.'\',\''.$_SESSION['language'].'\',\''.$back.'\');return false;">'.REMOVE_TEXT.'</a>';		
		?></td>
	</tr>
	<?php endforeach;?>
</tbody>
<?php } //end else?>
</table>