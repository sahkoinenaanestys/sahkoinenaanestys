<?php 
include SERVER_ROOT.'/views/inc/logininfo.php';
?>


<h2><?php echo YOU_HAVE_ALREADY_VOTED_TEXT;?></h2>
<p><?php echo YOU_HAVE_ALREADY_VOTED_INSTRUCTIONS_TEXT;?></p>
<p><?php echo YOU_WILL_BE_AUTOMATICALLY_LOGGED_OUT_TEXT;?></p>


<p><?php echo TIME_LEFT_TEXT;?>: <span id="logoutTimer"><?php echo AUTOMATIC_LOG_OUT_TIME;?></span><?php echo SECONDS_SYMBOL_TEXT;?></p>
<a href="<?php echo SITE_ROOT.'/index.php?logout&action=samlLogout';?>" class="buttonLink"><?php echo LOG_OUT_TEXT;?></a>