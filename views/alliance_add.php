<h3>
    <?php echo NEW_TEXT.' '.mb_strtolower(ALLIANCE_TEXT,'UTF-8');?>
</h3>

<div class="editFormDiv">
<form action="<?php echo SITE_ROOT.'/index.php?alliance&action=addAlliance';?>" method="POST">
	<fieldset>
		<legend><?php echo ELECTION_DETAILS_TEXT;?></legend>
		<div class="editLabelDiv"><label for="allianceId"><?php echo ALLIANCE_ID_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="hidden" name="allianceIdOld" value="<?php echo 0;?>" ></input>
			<input type="text" id="allianceId" name="allianceId" value="<?php echo $data['alliance']['allianceId'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['allianceId'])): echo '<span class="editFieldError">'.$data['formErrors']['allianceId'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="allianceName"><?php echo ALLIANCE_NAME_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="allianceName" name="allianceName" value="<?php echo $data['alliance']['allianceName'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['allianceName'])): echo '<span class="editFieldError">'.$data['formErrors']['allianceName'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="personInChargeEmail"><?php echo EMAIL_OF_PERSON_IN_CHARGE_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="personInChargeEmail" name="personInChargeEmail" value="<?php echo $data['alliance']['personInChargeEmail'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['personInChargeEmail'])): echo '<span class="editFieldError">'.$data['formErrors']['personInChargeEmail'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
			
		<div class="editLabelDiv"><label for="coalition"><?php echo COALITION_TEXT;?></label></div>
		<div class="editInputDiv">
		    <select id="coalitionId" name="coalitionId" class="inputSelect" >
			<option value="0"><?php echo NO_OPTION_SELECTED_TEXT; ?></option>
			<?php
			    if(isset($data['coalitions'])){
				foreach($data['coalitions'] as $coalition){
				    if($data['alliance']['coalitionId'] == $coalition['coalitionId']){ 
					$selected = 'selected="selected"';
				    }else{ 
					$selected = "";
				    }
				    echo '<option value="'.$coalition['coalitionId'].'"'.$selected.' >'.ID_TEXT.': '.$coalition['coalitionId'].', '.mb_strtolower(NAME_TEXT,'UTF-8').': '.$coalition['coalitionName'].'</option>';
				}
			    }
			?>
		    </select>
		    <?php if(isset($data['formErrors']['coalitionId'])): echo '<span class="editFieldError">'.$data['formErrors']['coalitionId'].'</span>';endif;?>
		</div>
		
		<div class="editLabelDiv">&nbsp;</div>
		<div class="editInputDiv">
			<input type="submit" name="allianceSubmit" class="editSubmitButton" value="<?php echo SAVE_TEXT; ?>">
			<a href="<?php echo SITE_ROOT.'/index.php?alliance';?>" class="editLinkButton"><?php echo CANCEL_TEXT;?></a>
		</div>
	</fieldset>
</form>
</div>