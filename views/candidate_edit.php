<h3>
    <?php if(isset($data['candidate']['candidateNumOld']) && $data['candidate']['candidateNumOld'] == 'new'){
        echo NEW_TEXT.' '.CANDIDATE_TEXT;
    }else if(isset($data['candidate']['candidateNumOld']) && strlen($data['candidate']['candidateNumOld']) > 0){
        echo EDIT_CANDIDATE_TEXT;
    } ?>
</h3>

<?php	$allianceAddUrl = SITE_ROOT."/index.php?candidate&action=addAllianceAjax"; 
	$url2 = SITE_ROOT."/index.php?candidate&action=getCoalitionAjax";
	$languageCode = $_SESSION['language'] ?>

<div class="editFormDiv">
<form action="<?php echo SITE_ROOT.'/index.php?candidate&action=editCandidate';?>" method="POST" id="candidateEditForm">
	<fieldset>
		<legend><?php echo ELECTION_DETAILS_TEXT;?></legend>
		<div class="editLabelDiv"><label for="firstName"><?php echo FIRST_NAME_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="firstName" name="firstName" value="<?php echo $data['candidate']['firstName'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['firstName'])): echo '<span class="editFieldError">'.$data['formErrors']['firstName'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="lastName"><?php echo LAST_NAME_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="lastName" name="lastName" value="<?php echo $data['candidate']['lastName'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['lastName'])): echo '<span class="editFieldError">'.$data['formErrors']['lastName'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="email"><?php echo EMAIL_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="email" name="email" value="<?php echo $data['candidate']['email'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['email'])): echo '<span class="editFieldError">'.$data['formErrors']['email'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
			
		<div class="editLabelDiv"><label for="candidateNum"><?php echo CANDIDATE_NUM_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="hidden" name="candidateNumOld" value="<?php echo $data['candidate']['candidateNumOld'];?>" ></input>
			<input type="text" id="candidateNum" name="candidateNum" value="<?php echo $data['candidate']['candidateNum'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['candidateNum'])): echo '<span class="editFieldError">'.$data['formErrors']['candidateNum'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
			
		<div class="editLabelDiv"><label for="alliance"><?php echo ALLIANCE_TEXT;?></label></div>
		<div class="editInputDiv">
		    <select id="allianceId" name="allianceId" class="inputSelect" onChange="getCoalition('<?php echo $url2; ?>', this)" >
			<option value="0"><?php echo NO_OPTION_SELECTED_TEXT; ?></option>
			<?php
			    if(isset($data['alliances'])){
				foreach($data['alliances'] as $alliance){
				    if($data['candidate']['alliance'] == $alliance['allianceId']){ 
					$selected = 'selected="selected"';
				    }else{ 
					$selected = "";
				    }
				    echo '<option value="'.$alliance['allianceId'].'"'.$selected.' >'.ID_TEXT.': '.$alliance['allianceId'].', '.mb_strtolower(NAME_TEXT,'UTF-8').': '.$alliance['allianceName'].'</option>';
				}
			    }
			?>
		    </select>
		    <?php if(isset($data['formErrors']['alliance'])): echo '<span class="editFieldError">'.$data['formErrors']['alliance'].'</span>';endif;?>
		<?php /*?><div>
		    <button onclick="showAddAlliancePopup('<?php echo $allianceAddUrl; ?>','<?php echo $languageCode; ?>')" id="addNewAlliance" class="button_add" value="<?php echo ADD_NEW_ALLIANCE_TEXT;?>"><?php echo ADD_NEW_ALLIANCE_TEXT;?></button>
		</div><?php */?>
		</div>
		
		<div class="editSpacerDiv"></div>

		<div class="editLabelDiv"><label for="coalition"><?php echo COALITION_TEXT;?></label></div>
		<div class="editInputDiv" id="coalitionContainer">
		    <select id="coalitionId" name="coalitionId" class="inputSelect" >
			<option value="0"><?php echo NO_OPTION_SELECTED_TEXT; ?></option>
			<?php
			    if(isset($data['coalitions'])){
				foreach($data['coalitions'] as $coalition){
				    if($data['candidate']['coalition'] == $coalition['coalitionId']){ 
					$selected = 'selected="selected"';
				    }else{ 
					$selected = "";
				    }
				    echo '<option value="'.$coalition['coalitionId'].'"'.$selected.' >'.ID_TEXT.': '.$coalition['coalitionId'].', '.mb_strtolower(NAME_TEXT,'UTF-8').': '.$coalition['coalitionName'].'</option>';
				}
			    }
			?>
		    </select>    
		</div>
		
		<div class="editSpacerDiv"></div>
	
		<div class="editLabelDiv">&nbsp;</div>
		<div class="editInputDiv">
			<input type=hidden id="back" name="back" value="<?php echo $data['returnPage'];?>"></input>
			<input type="submit" name="candidateSubmit" class="editSubmitButton" value="<?php echo SAVE_TEXT; ?>">
			<a href="<?php echo $data['returnPage'];?>" class="editLinkButton"><?php echo CANCEL_TEXT;?></a>
		</div>
		
		<?php /*?><div id="addAlliancePopup" style="text-align: left">
		    <div>
			<div style="display: inline-block; width: 200px">
			    <?php echo ALLIANCE_ID_TEXT.' *'; ?>
			</div>
			<div style="display: inline-block;">
			    <input type="text" name="newAllianceId" id="newAllianceId" value="" />
			</div>
			<div style="display: inline-block; width: 200px">
			    <?php echo ALLIANCE_NAME_TEXT; ?>
			</div>
			<div style="display: inline-block;">
			    <input type="text" name="newAllianceName" id="newAllianceName" value="" />
			</div>
			<div style="display: inline-block; width: 200px">
			    <?php echo COALITION_ID_TEXT.' *'; ?>
			</div>
			<div style="display: inline-block;">
			    <input type="text" name="newCoalitionId" id="newCoalitionId" value="" />
			</div>
			<div style="display: inline-block; width: 200px">
			    <?php echo COALITION_NAME_TEXT; ?>
			</div>
			<div style="display: inline-block;">
			    <input type="text" name="newCoalitionName" id="newCoalitionName" value="" />
			</div>
		    </div>
		 </div><?php */?>
	</fieldset>
</form>
</div>