<h3><?php echo CANDIDATES_TEXT; ?></h3>

<?php 
if(!empty($data['candidates'])) { 
    $electionId = $data['election']['electionId'];
    $url = SITE_ROOT."/index.php?statistics&action=setOrder";
    ?>
<p><a href="<?php echo SITE_ROOT."/index.php?statistics&action=exportCsvResults&electionId=".$electionId;?>">[<?php echo EXPORT_CSV_LIST_TEXT;?>]</a></p>

<table class="editListTable">
<thead>	
	<tr>
		<th><a href="#" class="white_link" onclick="changeOrder('rank', '<?php echo $url;?>');return false;"><?php echo RANK_TEXT;?></a></th>
		<th><a href="#" class="white_link" onclick="changeOrder('candidateNum', '<?php echo $url;?>');return false;"><?php echo CANDIDATE_NUM_TEXT;?></a></th>
		<th><?php echo NAME_TEXT;?></th>		
		<th><?php echo CANDIDATE_VOTES_TEXT;?></th>
		<th><?php echo COMPARISION_NUM_TEXT;?></th>
		<th><?php echo COALITION_COMPARISION_NUM_TEXT;?></th>
		<th><?php echo ALLIANCE_TEXT;?></th>
		<th><?php echo COALITION_TEXT;?></th>
	</tr>
</thead>
<tbody>
	<?php $row = 1;
	foreach($data['candidates'] as $candidate):?>
	<tr class="<?php if(!isset($row)): $row = 1; endif; echo ($row++%2==1)? 'odd': 'even';?>">	
		<td><?php echo $candidate['rank']; ?></td>
		<td><?php if($candidate['candidateNum'] == Candidate_Model::EMPTY_VOTE){echo "";}else{echo $candidate['candidateNum'];}?></td>
		<td><?php if($candidate['candidateNum'] == Candidate_Model::EMPTY_VOTE){echo EMPTY_VOTES_TEXT;}else{echo $candidate['firstName'].' '.$candidate['lastName'];}?></td>
		<td><?php echo $candidate['votes'];?></td>
		<td><?php echo number_format((double)$candidate['comparisionNum'], 2, '.', '');?></td>
		<td><?php echo number_format($candidate['coalitionComparisionNum'], 2, '.', '');?></td>
		<td><?php echo $candidate['allianceName'];?></td>
		<td><?php echo $candidate['coalitionName'];?></td>
	</tr>
	<?php endforeach; ?>
</tbody>	
</table>
<?php }else{
    echo '<p>'.NO_CANDIDATES_TEXT.'</p>';
} ?>
