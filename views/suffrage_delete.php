<h3><?php echo REMOVE_ALL_VOTERS_TEXT;?></h3>

<p><?php echo REMOVE_ALL_VOTERS_WARNING_TEXT.'<br>'.ACTION_CONFIRM_TEXT.'?';?></p>

<form action="<?php echo SITE_ROOT.'/index.php?suffrage&action=removeAll';?>" method="POST">
	<input type="submit" name="removeAll" class="editSubmitButton" value="<?php echo REMOVE_ALL_TEXT; ?>">
	<a href="<?php echo SITE_ROOT.'/index.php?suffrage';?>" class="editLinkButton"><?php echo CANCEL_TEXT;?></a>
</form>
