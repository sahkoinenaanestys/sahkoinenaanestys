<h3><?php echo VOTERS_TEXT;?></h3>

<div>
<form action="<?php echo SITE_ROOT.'/index.php?'.$controllerName;?>" method="POST"> 
	<input type="text" name="search" value="<?php echo (isset($data['searchStr']))?urldecode($data['searchStr']):'';?>" class="searchField"/>
	<input type="submit" value="<?php echo FIND_TEXT;?>" class="searchButton"/>
</form>
</div>

<div><br><?php include SERVER_ROOT.'/views/inc/pagination.php';?></div>

<table class="editListTable">
<thead>	
	<tr>
		<th><?php echo STUDENT_NUMBER_TEXT;?></th>
		<th><?php echo NAME_TEXT;?></th>		
		<th><?php echo RIGHT_TO_VOTE_TEXT;?></th>
		<th><?php echo ACTIONS_TEXT;?></th>
	</tr>
</thead>
<tbody>
	<?php foreach($data['voters'] as $voter):?>
	<tr class="<?php if(!isset($row)): $row = 1; endif; echo ($row++%2==1)? 'odd': 'even';?>">		
		<td><?php echo $voter['studentNum'];?></td>
		<td><?php echo $voter['firstName']." ".$voter['lastName'];?></td>
		<td><?php echo '<span class="'; echo ($voter['canVote'] == 1)? 'positiveMsg">'.YES_TEXT: 'negativeMsg">'.NO_TEXT; echo '</span>'?></td>
		<td><?php 
			if($voter['hasVoted'] == 1) {
				echo VOTER_HAS_ALREADY_VOTED_TEXT;
			} else if ($voter['canVote'] == 1) {
				echo '<a href="'.SITE_ROOT.'/index.php?worker&action=confirm&studentNum='.
				$voter['studentNum'].'&referer='.$data['referer'].'">'.MARK_THE_VOTE_TEXT.'</a>';	
			}								
		?></td>
	</tr>
	<?php endforeach;?>
</tbody>
</table>