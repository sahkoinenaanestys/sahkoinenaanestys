<h3><?php echo EXPORT_ELECTION_RESULTS_TEXT; ?></h3>
<?php ?><p><?php echo EXPORT_RESULTS_CSV_TEXT.' <font color="red">'.PAPER_VOTES_WARNING_TEXT.'</font>'; ?></p>
<p><?php echo EXPORT_RESULTS_CSV_INFO_TEXT.' '.PLEASE_WAIT_TEXT;?></p>
<p><?php echo DOWNLOAD_RESULTS_CSV_TEXT.' '.MANUAL_RESULTS_CALC_TEXT; ?></p>
<?php $languageCode = $_SESSION['language'] ?>

<form action="<?php echo SITE_ROOT.'/index.php?election&action=exportElectionResults'?>" id="FileDownloader" method="POST" >
    <tr><td><button type="submit" id="downloadBtn"><?php echo DOWNLOAD_TEXT; ?></button></td></tr>   
</form>
<div id="output"></div><br>

<input type="checkbox" name="downloadVoteFiles" id="downloadVoteFiles" value="true" class="inputCheckBox">
<?php echo DOWNLOAD_SEPARATE_VOTES_FILES_TEXT;?>
       
<div id="voteFilesDiv">
<span class="spanFormat">
<form action="<?php echo SITE_ROOT.'/index.php?candidate&action=exportCandidateVotes'?>" id="CandidateVotesFileDownloader" method="POST" >
    <tr><td><button type="submit" id="downloadCandidateVotesBtn"><?php echo DOWNLOAD_CANDIDATE_VOTES_TEXT; ?></button></td></tr>   
</form>
</span>
<span class="spanFormat">
<form action="<?php echo SITE_ROOT.'/index.php?suffrage&action=exportVotes'?>" id="VoterVotesFileDownloader" method="POST" >
    <tr><td><button type="submit" id="downloadVoterVotesBtn"><?php echo DOWNLOAD_VOTER_VOTES_TEXT; ?></button></td></tr>   
</form>
</span>
</div>

<?php  /*
if(empty($data['candidates'])) {
	echo '</p><p>'.NO_CANDIDATES_TEXT.'</p>';
} 
else {?>
<table class="editListTable">
<thead>	
	<tr>
		<th><?php echo CANDIDATE_NUM_TEXT;?></th>
		<th><?php echo NAME_TEXT;?></th>		
		<th><?php echo CANDIDATE_VOTES_TEXT;?></th>
		<th><?php echo ALLIANCE_TEXT;?></th>
		<th><?php echo ALLIANCE_TEXT.' '.VOTES_TEXT;?></th>
		<th><?php echo COALITION_TEXT;?></th>
		<th><?php echo COALITION_TEXT.' '.VOTES_TEXT;?></th>
		<th><?php echo COMPARISION_NUM_TEXT; ?></th>
		<th><?php echo ALLIANCE_TEXT.' '.COMPARISION_NUM_TEXT;?></th>
		<th><?php echo COALITION_TEXT.' '.COMPARISION_NUM_TEXT;?></th>
	</tr>
</thead>
<tbody>
	<?php 
	foreach($data['candidates'] as $candidate):?>
	<tr class="<?php if(!isset($row)): $row = 1; endif; echo ($row++%2==1)? 'odd': 'even';?>">		
		<td><?php echo $candidate['candidateNum'];?></td>
		<td><?php echo $candidate['firstName'].' '.$candidate['lastName'];?></td>
		<td><?php echo $candidate['votes'];?></td>
		<td><?php echo $candidate['allianceId'].': '.$candidate['allianceName'];?></td>
		<td><?php echo $candidate['allianceVotes'];?></td>
		<td><?php echo $candidate['coalitionId'].': '.$candidate['coalitionName'];?></td>
		<td><?php echo $candidate['coalitionVotes'];?></td>
		<td><?php echo $candidate['comparisionNum'];?></td>
		<td><?php echo $candidate['allianceComparisionNum'];?></td>
		<td><?php echo $candidate['coalitionComparisionNum'];?></td>
	</tr>
	<?php endforeach;?>
</tbody>	
</table>
<?php } //else end?>

<br /><br /><?php ?>

<?php 
if(!empty($data['winners'])) { ?>
<table class="editListTable">
<thead>	
	<tr>
		<th><?php echo RANK_TEXT;?></th>
		<th><?php echo CANDIDATE_NUM_TEXT;?></th>
		<th><?php echo NAME_TEXT;?></th>		
		<th><?php echo CANDIDATE_VOTES_TEXT;?></th>
		<th><?php echo COMPARISION_NUM_TEXT;?></th>
		<th><?php echo ALLIANCE_TEXT;?></th>
		<th><?php echo COALITION_TEXT;?></th>
	</tr>
</thead>
<tbody>
	<?php $row = 1;
	$i = 1;
	foreach($data['winners'] as $candidate):?>
	<tr class="<?php if(!isset($row)): $row = 1; endif; echo ($row++%2==1)? 'odd': 'even';?>">	
		<td><?php echo $i; ?></td>
		<td><?php echo $candidate['candidateNum'];?></td>
		<td><?php echo $candidate['firstName'].' '.$candidate['lastName'];?></td>
		<td><?php echo $candidate['votes'];?></td>
		<td><?php echo $candidate['comparisionNum'];?></td>
		<td><?php echo $candidate['allianceId'].': '.$candidate['allianceName'];?></td>
		<td><?php echo $candidate['coalitionId'].': '.$candidate['coalitionName'];?></td>
	</tr>
	<?php $i++;
	endforeach; ?>
</tbody>	
</table>
<?php } */?>