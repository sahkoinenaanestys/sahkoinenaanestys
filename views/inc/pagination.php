<table class="paginationTable">
<tr>
<?php 
/**
 * Pagination table
 * 
 * Should receive an array like this:
 * $data['pagination'] = array('totalRows' => 123, 'offset' => 40, 'limit' => 20, 'totalPages' => 7, 'curPage' => 3, 'maxOffset' => 120);
 * 
 * Can also use search string $data['searchStr']
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */

foreach($data['pagination'] as $key => $val) {$$key = $val;}

$linkRoot = SITE_ROOT.'/index.php?'.$controllerName;
if(isset($data['searchStr']))
	$linkRoot .= '&search='.$data['searchStr'];
$linkRoot .= '&offset=';

//Previous page
echo ((int) $curPage === 1)? '<td class="dimmed">'.PREVIOUS_TEXT.'</td>': '<td><a href="'.$linkRoot.($offset-$limit).'">'.PREVIOUS_TEXT.'</a></td>';

//First page
echo ((int) $curPage === 1)? '<td class="currentPage">1</td>': '<td><a href="'.$linkRoot.'0">1</a></td>';

if((int)$curPage-3 > 1): echo '<td>..</td>'; endif;
if((int)$curPage-2 > 1): echo '<td><a href="'.$linkRoot.($offset-$limit*2).'">'.((int)$curPage-2).'</a></td>' ; endif;
if((int)$curPage-1 > 1): echo '<td><a href="'.$linkRoot.($offset-$limit).'">'.((int)$curPage-1).'</a></td>' ; endif;
if((int)$curPage > 1 && (int)$curPage < (int) $totalPages): echo '<td class="currentPage">'.((int)$curPage).'</td>' ; endif;
if((int)$curPage+1 < (int)$totalPages): echo '<td><a href="'.$linkRoot.($offset+$limit).'">'.((int)$curPage+1).'</a></td>' ; endif;
if((int)$curPage+2 < (int)$totalPages): echo '<td><a href="'.$linkRoot.($offset+$limit*2).'">'.((int)$curPage+2).'</a></td>' ; endif;
if((int)$curPage+3 < (int)$totalPages): echo '<td>..</td>'; endif;

//Last page. Do not show the page number again if there is only one page
if((int)$totalPages > 1)
	echo ((int) $curPage >= (int) $totalPages)? '<td class="currentPage">'.(int) $totalPages.'</td>': '<td><a href="'.$linkRoot.$maxOffset.'">'.(int) $totalPages.'</a></td>';	

//Next page
echo ((int) $curPage >= (int) $totalPages)? '<td class="dimmed">'.NEXT_TEXT.'</td>': '<td><a href="'.$linkRoot.($offset+$limit).'">'.NEXT_TEXT.'</a></td>';
?>
</tr>
</table>