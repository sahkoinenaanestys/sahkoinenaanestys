<div id="adminFooterContainer" class="container_12">
<div class="grid_12">
	<noscript>
	<div class="errorMsgDiv"><?php echo JAVASCRIPT_HAS_TO_BE_ENABLED_TEXT;?></div>
	</noscript>
	<?php 
	
	//Show error message if we are not using specific browsers
	$userAgent = $_SERVER['HTTP_USER_AGENT'];
	if(!preg_match('/firefox/i', $userAgent)	//Mozilla Firefox 
		&& !preg_match('/chrome/i', $userAgent)	//Google Chrome
		&& !preg_match('/safari/i', $userAgent)	//Apple Safari
	):?>
		<div class="errorMsgDiv"><?php echo ADMIN_PAGES_DO_NOT_SUPPORT_YOUR_BROWSER_TEXT;?></div>	
	<?php endif;?>	
</div>
</div>