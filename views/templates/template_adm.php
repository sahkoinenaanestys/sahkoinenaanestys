<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic">
<title><?php echo STUDENT_UNION_OF_THE_UNI_OF_OULU_TEXT.' | '.ELECTRONIC_VOTING_TEXT.' -'.strtolower(ADMINISTRATION_TEXT);?></title>
<script type="text/javascript" src="<?php echo SITE_ROOT.'/lib/js/jquery-1.9.1.min.js';?>"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT.'/lib/js/jquery-ui-1.10.1.custom.js';?>"></script><?php 
//Load page specific javascript file
if(file_exists(SERVER_ROOT.'/views/js/'.$view.'.js')) {
	?><script type="text/javascript" src="<?php echo SITE_ROOT.'/views/js/'.$view.'.js';?>"></script><?php 
}
?>
<link rel="stylesheet" href="<?php echo SITE_ROOT.'/lib/css/960_12_col.css';?>">
<link rel="stylesheet" href="<?php echo SITE_ROOT.'/views/css/main.css';?>">
<link rel="stylesheet" href="<?php echo SITE_ROOT.'/lib/css/smoothness/jquery-ui-1.10.1.custom.css';?>">
</head>
<body>

<?php 
//Including the header file
include SERVER_ROOT.'/views/inc/admin_header.php';
?>

<div id="mainContainer" class="container_12">  

<div id="leftMenu" class="grid_3">
	<ul>
                <?php
		if(isset($data['loggedIn'])) {?>
			<li><a href="<?php echo SITE_ROOT.'/index.php?election';?>"><?php echo CURRENT_ELECTIONS_TEXT?></a></li>
			<li><a href="<?php echo SITE_ROOT.'/index.php?statistics';?>"><?php echo PREVIOUS_ELECTIONS_TEXT?></a></li>
			<li><a href="<?php echo SITE_ROOT.'/index.php?account';?>"><?php echo ADMINISTRATORS_TEXT?></a></li>
			<li><a href="<?php echo SITE_ROOT.'/index.php?logout&caller=admin';?>"><?php echo LOG_OUT_TEXT?></a></li>		
		<?php }else{?>
                <li><a href="http://www.oyy.fi"><?php echo STUDENT_UNION_WEB_PAGE_TEXT?></a></li>
                <li><a href="<?php echo SITE_ROOT.'/index.php';?>"><?php echo ELECTRONIC_VOTING_TEXT?></a></li>
                <?php } ?>
        </ul>
</div>  

<div id="mainContents" class="grid_9">
	<div id="messagesContainer">
	    <?php
	    //Load messages into the view
	    if (isset($data['success'])) {
		echo '<div class="successMsgDiv">' . $data['success'] . '</div>';
	    }
	    if (isset($data['error'])) {
		echo '<div class="errorMsgDiv">' . $data['error'] . '</div>';
	    }?>
	    </div>
	    
	    <?php
	    include SERVER_ROOT . '/views/' . $view . '.php';
	    ?>
</div>

</div><?php //mainContainer ends ?>

<?php 
//Admin footer containing warnings about potential browser compatibility issues 
include SERVER_ROOT.'/views/inc/admin_footer.php';

//Including the footer file
include SERVER_ROOT.'/views/inc/footer.php';
?>

</body>
</html>