<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"><?php 
if(isset($data['automaticLogout']) && $data['automaticLogout'] === true) {
	?><meta http-equiv="refresh" content="<?php echo AUTOMATIC_LOG_OUT_TIME;?>;URL='<?php echo SITE_ROOT.'/index.php?logout&action=samlLogout';?>'"><?php 
}
?>
<title><?php echo STUDENT_UNION_OF_THE_UNI_OF_OULU_TEXT.' | '.ELECTRONIC_VOTING_TEXT;?></title>
<script type="text/javascript" src="<?php echo SITE_ROOT.'/lib/js/jquery-1.9.1.min.js';?>"></script><?php 
//Load page specific javascript file
if(file_exists(SERVER_ROOT.'/views/js/'.$view.'.js')) {
	?><script type="text/javascript" src="<?php echo SITE_ROOT.'/views/js/'.$view.'.js';?>"></script><?php 
}
?>
<link rel="stylesheet" href="<?php echo SITE_ROOT.'/lib/css/960_12_col.css';?>">
<link rel="stylesheet" href="<?php echo SITE_ROOT.'/views/css/main.css';?>">
</head>
<body>

<?php 
//Including the header file
include SERVER_ROOT.'/views/inc/header.php';
?>

<div id="mainContainer" class="container_12">  

<?php //Left menu ?>
<div id="leftMenu" class="grid_3">
	<ul>
		<?php 
		if($controllerName == "instructions") { ?>
			<li><a href="<?php echo SITE_ROOT.'/';?>"><?php echo FRONTPAGE_TEXT;?></a></li>
		<?php } else { ?>
			<li><a href="<?php echo SITE_ROOT.'/index.php?instructions';?>"><?php echo INSTRUCTIONS_TEXT;?></a></li>
		<?php }?>		                
		<li><a href="http://www.oyy.fi"><?php echo STUDENT_UNION_WEB_PAGE_TEXT?></a></li><?php 
		//Log out link
		if(isset($data['loggedIn'])) {?>
			<li><a href="<?php echo SITE_ROOT.'/index.php?logout&action=samlLogout'?>"><?php echo LOG_OUT_TEXT;?></a></li>		
		<?php }?>		
	</ul>
</div>  

<?php //Main contents start ?>
<div id="mainContents" class="grid_9"><?php
	//Load messages into the view
	if(isset($data['success'])) {
		echo '<div class="successMsgDiv">'.$data['success'].'</div>';
	}
	if(isset($data['error'])){
		echo '<div class="errorMsgDiv">'.$data['error'].'</div>';
	}
	include SERVER_ROOT.'/views/'.$view.'.php';	
	?>
</div>
      
</div><?php //mainContainer ends ?>

<?php 
//Including the footer file
include SERVER_ROOT.'/views/inc/footer.php';
?>

</body>
</html>
