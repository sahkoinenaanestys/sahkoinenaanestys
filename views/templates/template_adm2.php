<!DOCTYPE HTML>
<html>
<head>
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo STUDENT_UNION_OF_THE_UNI_OF_OULU_TEXT.' | '.ELECTRONIC_VOTING_TEXT.' -'.strtolower(ADMINISTRATION_TEXT);?></title>
<script type="text/javascript" src="<?php echo SITE_ROOT.'/lib/js/jquery-1.9.1.min.js';?>"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT.'/lib/js/jquery-ui-1.10.1.custom.js';?>"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT.'/lib/js/jquery.form.js';?>"></script>
<script type="text/javascript" src="<?php echo SITE_ROOT.'/lib/js/jquery.loadingdotdotdot.js';?>"></script><?php 
//Load page specific javascript file
if(file_exists(SERVER_ROOT.'/views/js/'.$view.'.js')) {
	?><script type="text/javascript" src="<?php echo SITE_ROOT.'/views/js/'.$view.'.js';?>"></script><?php 
}

?>
<link rel="stylesheet" href="<?php echo SITE_ROOT.'/lib/css/960_12_col.css';?>">
<link rel="stylesheet" href="<?php echo SITE_ROOT.'/views/css/main.css';?>">
<link rel="stylesheet" href="<?php echo SITE_ROOT.'/lib/css/smoothness/jquery-ui-1.10.1.custom.css';?>">
</head>
<body>

<?php 
//Including the header file
include SERVER_ROOT.'/views/inc/admin_header.php';
?>

<div id="mainContainer" class="container_12">  

<div id="adminMenu" class="grid_3">
	<ul id="elections">
		<li class="lh_top"><strong><?php echo ELECTIONS_TEXT?></strong></li>
		<li><a href="<?php echo SITE_ROOT.'/index.php?election';?>"><?php echo CURRENT_ELECTIONS_TEXT?></a></li>
		<li><a href="<?php echo SITE_ROOT.'/index.php?statistics';?>"><?php echo PREVIOUS_ELECTIONS_TEXT?></a></li>
        <li><a href="<?php echo SITE_ROOT.'/index.php?election&view=election_votes_import';?>"><?php echo IMPORT_PAPER_VOTES_TEXT?></a></li>
        <li><a href="<?php echo SITE_ROOT.'/index.php?election&view=election_results';?>"><?php echo ELECTION_RESULTS_TEXT?></a></li>
                
        <li class="lh_caption"><strong><?php echo CANDIDATES_TEXT?></strong></li>
		<li><a href="<?php echo SITE_ROOT.'/index.php?candidate';?>"><?php echo CANDIDATES_TEXT?></a></li>
		<li><a href="<?php echo SITE_ROOT.'/index.php?candidate&view=candidates_import';?>"><?php echo IMPORT_FROM_FILE_TEXT?></a></li>
		
		<li class="lh_caption"><strong><?php echo ALLIANCES_AND_COALITIONS_TEXT?></strong></li>
		<li><a href="<?php echo SITE_ROOT.'/index.php?alliance';?>"><?php echo ALLIANCES_AND_COALITIONS_TEXT?></a></li>
		<li><a href="<?php echo SITE_ROOT.'/index.php?alliance&view=alliances_import';?>"><?php echo IMPORT_FROM_FILE_TEXT?></a></li>
		
		<li class="lh_caption"><strong><?php echo VOTERS_TEXT?></strong></li>
		<li><a href="<?php echo SITE_ROOT.'/index.php?suffrage';?>"><?php echo VOTERS_TEXT?></a></li>
        <li><a href="<?php echo SITE_ROOT.'/index.php?suffrage&action=import';?>"><?php echo IMPORT_FROM_FILE_TEXT?></a></li> 
		
		<li class="lh_caption"><strong><?php echo ADMINISTRATORS_TEXT?></strong></li>
        <li><a href="<?php echo SITE_ROOT.'/index.php?account';?>"><?php echo ADMINISTRATORS_TEXT?></a></li>
                <?php
		if(isset($data['loggedIn'])) {?>
                        <li class="lh_caption"><strong><?php echo ACCOUNT_TEXT?></strong></li>
			<li class="lh_bottom"><a href="<?php echo SITE_ROOT.'/index.php?logout&caller=admin';?>"><?php echo LOG_OUT_TEXT?></a></li>		
		<?php }?>
	</ul>
</div>  

<div id="mainContents" class="grid_9"><?php
	if(isset($data['election']['name']) && isset($data['election']['electionId'])) {
		echo '<span class="blueLink"><a href="'.SITE_ROOT.'/index.php?election">'.ELECTIONS_TEXT.'</a> / ';
		echo '<a href="'.SITE_ROOT.'/index.php?election&action=showActiveElection&electionId='.$data['election']['electionId'].'">'.$data['election']['name'].'</a>';
		switch ($controllerName) {
			case 'suffrage':
				echo ' / <a href="'.SITE_ROOT.'/index.php?suffrage">'.VOTERS_TEXT.'</a>';
				break;
			case 'candidate':
				echo ' / <a href="'.SITE_ROOT.'/index.php?candidate">'.CANDIDATES_TEXT.'</a>';
				break;
			case 'alliance':
				echo ' / <a href="'.SITE_ROOT.'/index.php?alliance">'.ALLIANCES_AND_COALITIONS_TEXT.'</a>';
				break;    
			default:
				break;
		}
		echo '</span><br>';
	} ?>	

	<div id="messagesContainer"><?php
	//Load messages into the view
	if(isset($data['success'])) {
		echo '<div class="successMsgDiv">'.$data['success'].'</div>';
	}
	if(isset($data['error'])){
		echo '<div class="errorMsgDiv">'.$data['error'].'</div>';
	}?>
	</div>
    
	<?php
	include SERVER_ROOT.'/views/'.$view.'.php';	
	?>
</div>

</div><?php //mainContainer ends ?>

<?php
//Admin footer containing warnings about potential browser compatibility issues
include SERVER_ROOT.'/views/inc/admin_footer.php';

//Including the footer file
include SERVER_ROOT.'/views/inc/footer.php';
?>

</body>
</html>
