<?php 
include SERVER_ROOT.'/views/inc/logininfo.php';?>

<p><?php
if($data['isEmpty']) {
	echo YOU_GAVE_A_SCRATCH_VOTE_TEXT.".";
} else {
	echo YOU_VOTED_FOR_CANDIDATE_TEXT.":";?>
	<span class="candidateBig"><?php echo NUMBER_ABBREVIATION_TEXT.' '.$data['candidateNum'].', '.$data['firstName'].' '.$data['lastName']?></span>	
	<?php 
}?>
</p>

<p><?php echo THANK_YOU_FOR_YOUR_VOTE_TEXT;?></p>
<p><?php echo YOU_WILL_BE_AUTOMATICALLY_LOGGED_OUT_TEXT;?></p>

<p><?php echo TIME_LEFT_TEXT;?>: <span id="logoutTimer"><?php echo AUTOMATIC_LOG_OUT_TIME;?></span><?php echo SECONDS_SYMBOL_TEXT;?></p>
<a href="<?php echo SITE_ROOT.'/index.php?logout&action=samlLogout';?>" class="buttonLink"><?php echo LOG_OUT_TEXT;?></a>
