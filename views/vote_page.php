<?php 
include SERVER_ROOT.'/views/inc/logininfo.php';
?>
<h2><?php echo $data['election'];?></h2>
<p><?php echo SELECT_A_CANDIDATE_AND_PRESS_NEXT_TEXT;?></p>
<h3><?php echo CANDIDATES_TEXT;?></h3>


<form method="post" action="<?php echo SITE_ROOT.'/index.php?vote&action=confirm'?>">
<?php
//List the candidates
foreach($data['candidates'] as $alliance => $candidateArr) 
{?>	
	<table class="candidateTable">
	<tr>
		<th colspan="3"><?php echo $alliance;?></th>
	</tr>
	<?php 
	$orderNum = 1;
	$lastNum = count($candidateArr);
	$column = 1;
	foreach($candidateArr as $candidate) { 
	if($column == 1) {?>
		<tr>
	<?php }	?>
	<td onclick="javascript:tickRadio(<?php echo (int)$candidate['candidateNum'];?>);" style="width:33%">
		<span class="candidateRadio">
			<input type="radio" name="candidate" id="candidate<?php echo $candidate['candidateNum'];?>" value="<?php echo $candidate['candidateNum'];?>">
		</span>
		
		<label for="candidate<?php echo $candidate['candidateNum'];?>"><span>
		<?php echo $candidate['candidateNum'].'</span> '.$candidate['firstName'].' '.$candidate['lastName'];?></label>
		
	</td>
	
	<?php if($column >= 3) {
		echo '</tr>';
		$column = 1;
	} else {
		if($orderNum == $lastNum && $column == 1) { 
			echo '<td></td><td></td>';
		} else if($orderNum == $lastNum && $column == 2) {
			echo '<td></td>';
		} else {
			$column += 1;
		}				
	}
	$orderNum += 1;
	} //end inner foreach
} //end outer foreach ?>


<table class="candidateTable">
	<tr><th>&nbsp;</th></tr>
	<tr>
		<td onclick="javascript:tickRadio(-1)">
			<span class="candidateRadio">
				<input type="radio" name="candidate" id="candidate-1" value="-1">
			</span>	
			<label for="candidate-1"><?php echo EMPTY_VOTE_TEXT;?></label>
		</td>	
	</tr>
</table>
<br>

<div class="buttonHolder">
	<input type="submit" value="<?php echo NEXT_TEXT;?>" class="buttonSubmit">
</div>
</form> 