<h3><?php echo ADMINISTRATION_TEXT; ?></h3>
<p><?php echo ADMINISTRATION_FRONTPAGE_TEXT; ?></p>

<form method="POST" action="<?php echo SITE_ROOT.'/index.php?login&action=adminLogin'?>" class="loginForm" autocomplete="off" >
<div class="loginTitle"><?php echo ADMINISTRATION_FRONTPAGE_INSTRUCTIONS_TEXT;?></div>

<div class="loginRow">
	<div class="loginLabelDiv"><label for="loginUsername"><?php echo USERNAME_TEXT.':'; ?></label></div>
	<div class="loginInputDiv"><input type="text" name="username" id="loginUsername" size="30"></div>
</div>

<div class="loginRow noBottomBorder">
	<div class="loginLabelDiv"><label for="loginPasswd"><?php echo PASSWORD_TEXT.':'; ?></label></div>
	<div class="loginInputDiv"><input type="password" name="passwd" id="loginPasswd" size="30"></div>
</div>
    
<div class="loginButtonDiv">
	<input type="submit" class="buttonSubmit" value="<?php echo LOG_IN_TEXT; ?>">
</div>

</form>

<br>

<div>
<font size="1"><b><?php echo SUPPORTED_BROWSERS_TEXT.'. '.JAVASCRIPT_HAS_TO_BE_ENABLED_TEXT;?></b></font>	
</div>