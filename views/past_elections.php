<h3><?php echo PREVIOUS_ELECTIONS_TEXT; ?></h3>

<?php
if (!empty($data['elections'])) { ?>

<table class="editListTable">
<thead>	
	<tr>
		<th><?php echo ELECTION_TEXT;?></th>
		<th><?php echo TIME_AND_DATE_TEXT;?></th>
		<th><?php echo VOTES_TEXT;?></th>
		<th><?php echo WWW_VOTES_TEXT;?></th>
		<th><?php echo PAPER_VOTES_TEXT;?></th>
		<th><?php echo EMPTY_VOTES_TEXT;?></th>
		<th><?php echo ACTIONS_TEXT;?></th>		
	</tr>
</thead>    
<tbody>
<?php foreach ($data['elections'] as $election): ?>
        <tr class="<?php if(!isset($row)): $row = 1; endif; echo ($row++%2==1)? 'odd': 'even';?>">
	    <td width="15%"><?php echo $election['name']; ?></td>
	    <td width="20%"><?php echo date('d.m.Y H:i', strtotime($election['startDate'])).' - '.date('d.m.Y H:i', strtotime($election['endDate']));?>
	    <td width="10%"><font color="red"><?php echo ($election['totalWwwVotes']+$election['totalPaperVotes']);?></font></td>
	    <td width="10%"><?php echo $election['totalWwwVotes'];?></td>
	    <td width="10%"><?php echo $election['totalPaperVotes'];?></td>
	    <td width="10%"><?php echo $election['emptyVotes'];?></td>
	    <td width="15%"><a href="<?php echo SITE_ROOT.'/index.php?statistics&action=showCandidates&electionId='.$election['electionId']; ?>"><?php echo SHOW_ELECTION_CANDIDATES_TEXT; ?></a>
	</tr>     
<?php endforeach; ?>
</tbody>    
</table>  
<?php 
} else {    
	echo '<p>'.NO_PREVIOUS_ELECTIONS_TEXT.'</p>';    
}
    
