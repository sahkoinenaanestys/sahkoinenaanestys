<h3><?php echo REMOVE_ALL_CANDIDATES_TEXT;?></h3>

<p><?php echo REMOVE_ALL_CANDIDATES_WARNING_TEXT.'<br>'.ACTION_CONFIRM_TEXT.'?';?></p>

<form action="<?php echo SITE_ROOT.'/index.php?candidate&action=removeAllCandidates';?>" method="POST">
	<input type="submit" name="bulkDeleteCandidates" class="editSubmitButton" value="<?php echo REMOVE_ALL_TEXT; ?>">
	<a href="<?php echo SITE_ROOT.'/index.php?candidate';?>" class="editLinkButton"><?php echo CANCEL_TEXT;?></a>
</form>
