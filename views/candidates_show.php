<h3><?php echo CANDIDATES_TEXT; ?></h3>

<div>
<form action="<?php echo SITE_ROOT.'/index.php?candidate';?>" method="POST"> 
	<input type="text" name="search" value="<?php echo (isset($data['searchStr']))?urldecode($data['searchStr']):'';?>" class="searchField"/>
	<input type="submit" value="<?php echo FIND_TEXT;?>" class="searchButton"/>
</form>
</div>

<?php $url = SITE_ROOT."/index.php?candidate&action=removeCandidateAjax"; 
    $languageCode = $_SESSION['language'];
    $back = SITE_ROOT."/index.php?candidate";?>

<p><a href="<?php echo SITE_ROOT."/index.php?candidate&action=editCandidate" ?>">[<?php echo NEW_TEXT.' '.mb_strtolower(CANDIDATE_TEXT,'UTF-8');?>]</a>


    
<?php 
if(empty($data['candidates'])) {
	echo '</p><p>'.NO_CANDIDATES_TEXT.'</p>';
} 
else {?>
<a href="<?php echo SITE_ROOT."/index.php?candidate&action=removeAllCandidates" ?>">[<?php echo REMOVE_ALL_TEXT.' '.mb_strtolower(CANDIDATES_TEXT,'UTF-8');?>]</a></p>

<div><?php include SERVER_ROOT.'/views/inc/pagination.php';?></div>

<table class="editListTable">
<thead>	
	<tr>
		<th><?php echo CANDIDATE_NUM_TEXT;?></th>
		<th><?php echo NAME_TEXT;?></th>
		<th><?php echo EMAIL_TEXT;?></th>
		<th><?php echo ALLIANCE_TEXT;?></th>
		<th><?php echo COALITION_TEXT;?></th>
		<th><?php echo ACTIONS_TEXT;?></th>
	</tr>
</thead>
<tbody>
	<?php 
	foreach($data['candidates'] as $candidate):?>
	<tr class="<?php if(!isset($row)): $row = 1; endif; echo ($row++%2==1)? 'odd': 'even';?>">		
		<td><?php echo $candidate['candidateNum'];?></td>
		<td><?php echo $candidate['firstName'].' '.$candidate['lastName'];?></td>
		<td><?php echo $candidate['email'];?></td>
		<td><?php echo $candidate['allianceName'];?></td>
		<td><?php echo $candidate['coalitionName'];?></td>
		<td><?php 
			echo '<a href="'.SITE_ROOT."/index.php?candidate&action=editCandidate&candidateNum=".$candidate['candidateNum'].'">'.EDIT_TEXT.'</a> | ';
			echo '<a href="#" onclick="showConfirmationDialog('.$candidate['candidateNum'].','.$candidate['election'].',\''.$url.'\',\''.$languageCode.'\',\''.$back.'\');return false;">'.REMOVE_TEXT.'</a>';
		?>
		</td>
	</tr>
	<?php endforeach;?>
</tbody>	
</table>
<?php } //else end?>