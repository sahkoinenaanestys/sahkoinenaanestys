<h3>
    <?php if(isset($data['election']['do']) && $data['election']['do'] == 'new'){
        echo NEW_ELECTION_TEXT;
    }else if(isset($data['election']['do']) && $data['election']['do'] == 'edit'){
        echo EDIT_ELECTION_TEXT;
    } ?>
</h3>

<div class="editFormDiv">
<form action="<?php echo SITE_ROOT.'/index.php?election&action=editElection';?>" method="POST">
	<fieldset>
		<legend><?php echo ELECTION_DETAILS_TEXT; ?></legend>
		<div class="editLabelDiv"><label for="name"><?php echo ELECTION_NAME_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="name" name="name" value="<?php if(isset($data['election']['name'])){echo $data['election']['name'];}?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['name'])): echo '<span class="editFieldError">'.$data['formErrors']['name'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="startDate"><?php echo ELECTION_START_DATE_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="start_date" name="startDate" value="<?php if(isset($data['election']['startDate'])){echo $data['election']['startDate'];}?>" class="inputText"></input>
			<?php /*<input class="button_custom" type="image" src="<?php echo SITE_ROOT.'/static/img/webdev2/05/001_44.png';?>" id="startDatepickerImage" width="24" height="24"> */?>
			<?php if(isset($data['formErrors']['startDate'])): echo '<span class="editFieldError">'.$data['formErrors']['startDate'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
                
                <div class="editLabelDiv"><label for="startTime"><?php echo ELECTION_START_TIME_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="start_time" name="startTime" value="<?php if(isset($data['election']['startTime'])){ echo $data['election']['startTime']; }else{ echo date('H:i');} ?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['startTime'])): echo '<span class="editFieldError">'.$data['formErrors']['startTime'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
                
                <div class="editLabelDiv"><label for="endDate"><?php echo ELECTION_END_DATE_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="end_date" name="endDate" value="<?php if(isset($data['election']['endDate'])){echo $data['election']['endDate'];}?>" class="inputText"></input>
			<?php /*<input class="button_custom" type="image" src="<?php echo SITE_ROOT.'/static/img/webdev2/05/001_44.png';?>" id="endDatepickerImage" width="24" height="24"> */?>
			<?php if(isset($data['formErrors']['endDate'])): echo '<span class="editFieldError">'.$data['formErrors']['endDate'].'</span>';endif;?>	
		</div>
		<div class="editSpacerDiv"></div>
                
                <div class="editLabelDiv"><label for="endTime"><?php echo ELECTION_END_TIME_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="end_time" name="endTime" value="<?php if(isset($data['election']['endTime'])){ echo $data['election']['endTime']; }else{ echo date('H:i', strtotime('23:59:59'));} ?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['endTime'])): echo '<span class="editFieldError">'.$data['formErrors']['endTime'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="calcMethod"><?php echo ELECTION_CALC_METHOD_TEXT;?></label></div>
		<div class="editInputDiv">
		    <input type="radio" name="calcMethod" id="countCoalitions" value="countCoalitions" <?php if($data['election']['calcMethod'] == Election_Model::CALCULATE_COALITIONS):echo 'checked="checked"';endif;?>><label for="countCoalitions"><?php echo ELECTION_COUNT_COALITIONS_TEXT;?></label><br>
		    <input type="radio" name="calcMethod" id="noCoalitions" value="noCoalitions" <?php if($data['election']['calcMethod'] == Election_Model::NO_COALITIONS):echo 'checked="checked"';endif;?>><label for="noCoalitions"><?php echo ELECTION_DO_NOT_COUNT_COALITIONS_TEXT;?></label>	
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label><?php echo ELECTION_STATUS_TEXT;?></label></div>
		<div class="editInputDiv">
                    <input type="radio" name="status" id="stateOpen" value="stateOpen" <?php if($data['election']['status'] == Election_Model::STATE_ACTIVE):echo 'checked="checked"';endif;?>><label for="stateOpen"><?php echo ELECTION_STATUS_ACTIVE_TEXT;?></label><br>
		    <input type="radio" name="status" id="stateClosed" value="stateClosed" <?php if($data['election']['status'] == Election_Model::STATE_CLOSED):echo 'checked="checked"';endif;?>><label for="stateClosed"><?php echo ELECTION_STATUS_CLOSED_TEXT;?></label><br>
		</div>
	
		<div class="editLabelDiv">&nbsp;</div>
		<div class="editInputDiv">
			<input type="submit" name="electionSubmitForm" class="editSubmitButton" value="<?php echo SAVE_TEXT; ?>">
			<a href="<?php echo $data['returnPage'];?>" class="editLinkButton"><?php echo CANCEL_TEXT;?></a>
		</div>
		<input type="hidden" id="electionId" name="electionId" value="<?php echo $data['election']['electionId'];?>"></input>
		<input type="hidden" id="back" name="back" value="<?php echo $data['returnPage'];?>"></input>
		<input type="hidden" id="do" name="do" value="<?php echo $data['election']['do'];?>"></input>
	</fieldset>
</form>