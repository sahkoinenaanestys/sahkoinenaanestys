<?php $url = SITE_ROOT."/index.php?election&action=removeElectionAjax"; 
    $url2 = SITE_ROOT."/index.php?election&action=endElectionAjax"; 
    $url3 = SITE_ROOT."/index.php?election&action=startOrStopElectionAjax"; 
    $url4 = SITE_ROOT."/index.php?election&action=putElectionToHistoryAjax"; 
    $back = SITE_ROOT."/index.php?election";
    $languageCode = $_SESSION['language']; ?>

<h3><?php echo ELECTIONS_TEXT; ?></h3>

<p><a href="<?php echo SITE_ROOT."/index.php?election&action=editElection" ?>">[<?php echo NEW_TEXT.' '.mb_strtolower(ELECTION_TEXT,'UTF-8');?>]</a></p>

<?php
if (!empty($data['elections'])) { ?>

<table class="editListTable">
<thead>	
	<tr>
		<th><?php echo ELECTION_TEXT;?></th>
                <th><?php echo ELECTION_STATUS_TEXT;?></th>		
		<th><?php echo ACTIONS_TEXT;?></th>		
	</tr>
</thead>    
<tbody>
<?php
    foreach ($data['elections'] as $election) { ?>
        <tr class="<?php if(!isset($row)): $row = 1; endif; echo ($row++%2==1)? 'odd': 'even';?>">
<?php   echo '<td><a href="'.SITE_ROOT."/index.php?election&action=showActiveElection&electionId=".$election['electionId'].'">'.$election['name'] . ', '.date('d.m.Y H:i', strtotime($election['startDate'])).' - '.date('d.m.Y H:i', strtotime($election['endDate'])).'</a></td>';
        echo '<td>'.Election_Model::getElectionStateString($election['status']);
	if($election['status'] == Election_Model::STATE_ACTIVE){
	    echo Election_Model::checkActiveElectionDates($election['startDate'],$election['endDate']);
	}
	echo '</td>';
	
	if($election['status']==Election_Model::STATE_ENDED){
	    echo '<td><a href="#" onclick="showConfirmationDialog('.$election['electionId'].',\''.$url.'\',\''.$languageCode.'\', 1,\''.$back.'\');return false;">'.REMOVE_TEXT.'</a> | '; 
	    echo '<a href="#" onclick="showConfirmationDialog('.$election['electionId'].',\''.$url4.'\',\''.$languageCode.'\', 3,\''.$back.'\');return false;">'.MOVE_ELECTION_TO_HISTORY_TEXT.'</a></td>'; 
	}else{
	    echo '<td><a href="'.SITE_ROOT."/index.php?election&action=editElection&electionId=".$election['electionId'].'">'.EDIT_TEXT.'</a> | ';
	    echo '<a href="#" onclick="showStartOrStopElectionDialog('.$election['electionId'].','.$election['status'].',\''.$url3.'\',\''.$languageCode.'\',\''.$back.'\');return false;">'.OPEN_OR_CLOSE_ELECTION_TEXT.'</a> | ';
	    echo '<a href="#" onclick="showConfirmationDialog('.$election['electionId'].',\''.$url2.'\',\''.$languageCode.'\', 2,\''.$back.'\');return false;">'.END_ELECTION_TEXT.'</a></td>'; 
	}
	echo '</tr>';       
    } ?>
</tbody>    
</table>  
<?php 
    } else {
    echo '<p>'.NO_ACTIVE_ELECTIONS_TEXT.'</p>';    
}?>
    
