<h3>
    <?php if(isset($data['coalition']['do']) && $data['coalition']['do'] == 'new'){
        echo NEW_TEXT.' '.mb_strtolower(COALITION_TEXT,'UTF-8');
    }else if(isset($data['coalition']['do']) && $data['coalition']['do'] == 'edit'){
        echo EDIT_COALITION_TEXT;
    } ?>
</h3>

<div class="editFormDiv">
<form action="<?php echo SITE_ROOT.'/index.php?alliance&action=editCoalition';?>" method="POST">
	<fieldset>
		<legend><?php echo ELECTION_DETAILS_TEXT; ?></legend>
		<div class="editLabelDiv"><label for="coalitionId"><?php echo ID_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="coalitionId" name="coalitionId" value="<?php echo $data['coalition']['coalitionId'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['coalitionId'])): echo '<span class="editFieldError">'.$data['formErrors']['coalitionId'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
		
		<div class="editLabelDiv"><label for="name"><?php echo NAME_TEXT;?></label></div>
		<div class="editInputDiv">
			<input type="text" id="coalitionName" name="coalitionName" value="<?php echo $data['coalition']['coalitionName'];?>" class="inputText"></input>
			<?php if(isset($data['formErrors']['coalitionName'])): echo '<span class="editFieldError">'.$data['formErrors']['coalitionName'].'</span>';endif;?>
		</div>
		<div class="editSpacerDiv"></div>
	
		<div class="editLabelDiv">&nbsp;</div>
		<div class="editInputDiv">
			<input type="submit" name="coalitionSubmit" class="editSubmitButton" value="<?php echo SAVE_TEXT; ?>">
			<a href="<?php echo SITE_ROOT.'/index.php?alliance';?>" class="editLinkButton"><?php echo CANCEL_TEXT;?></a>
		</div>
		<input type="hidden" name="oldCoalitionId" value="<?php echo $data['coalition']['oldCoalitionId'];?>" ></input>
		<input type="hidden" id="name" name="do" value="<?php echo $data['coalition']['do'];?>"></input>
	</fieldset>
</form>