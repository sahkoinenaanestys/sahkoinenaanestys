<?php
$url = SITE_ROOT."/index.php?election&action=startOrStopElectionAjax"; 
$back = SITE_ROOT."/index.php?election&action=showActiveElection";
$languageCode = $_SESSION['language'];

if (!empty($data['election'])) {
    $election = $data['election'];
?>

<h3><?php echo $election['name']; ?></h3>
<p><b><?php echo ELECTION_DETAILS_TEXT; ?></p></b>
<p><table border="0">
    <tr><td><?php echo ELECTION_START_DATE_TEXT.':'; ?></td><td>&nbsp;</td><td><?php echo $election['startDate']; ?>
	<?php if($election['status'] == Election_Model::STATE_ACTIVE){
	echo Election_Model::checkActiveElectionDates($election['startDate'], null);
    }?>
    </td></tr>
    <tr><td><?php echo ELECTION_END_DATE_TEXT.':'; ?></td><td>&nbsp;</td><td><?php echo $election['endDate']; ?>
    <?php if($election['status'] == Election_Model::STATE_ACTIVE){
	echo Election_Model::checkActiveElectionDates(null, $election['endDate']);
    }?>
    </td></tr>
    <tr><td><?php echo ELECTION_STATUS_TEXT.':'; ?></td><td>&nbsp;</td><td><?php echo Election_Model::getElectionStateString($election['status']); ?></td></tr>
</table></p>

<a href="<?php echo SITE_ROOT.'/index.php?election&action=editElection&electionId='.$election['electionId'].'&back=showActiveElection';?>"><?php echo EDIT_ELECTION_TEXT; ?></a>
<?php if($election['status'] == Election_Model::STATE_ACTIVE || $election['status'] == Election_Model::STATE_CLOSED){ ?> | 
<a href="#" onclick="showStartOrStopElectionDialog(<?php echo $election['electionId'];?>,<?php echo $election['status'];?>,'<?php echo $url;?>','<?php echo $languageCode;?>','<?php echo $back;?>');return false;"><?php echo OPEN_OR_CLOSE_ELECTION_TEXT;?></a>
<?php } ?>

<p><b><?php echo ELECTION_VOTERS_TEXT; ?></b></p>
<p><?php
    if($data['voterCount'] == 0){ 
	echo ELECTION_NO_VOTERS_TEXT.'<br />';
	echo BRING_VOTERS_FROM_TEXT.': '; ?>
	<a href="<?php echo SITE_ROOT.'/index.php?suffrage&action=import';?>"><?php echo CSV_FILE_TEXT; ?></a>
	<?php echo ' '.OR_TEXT.' '; ?>
	<a href="<?php echo SITE_ROOT.'/index.php?suffrage&action=edit&back=showActiveElection';?>"><?php echo mb_strtolower(ADD_VOTERS_MANUALLY_TEXT,'UTF-8'); ?></a>
    <?php
    }else{ ?>
	<a href="<?php echo SITE_ROOT.'/index.php?suffrage';?>"><?php echo SHOW_ELECTION_VOTERS_TEXT; ?></a>
    <?php
    } ?>
</p> 
    
<p><b><?php echo ELECTION_CANDIDATES_TEXT; ?></b></p>
<p><?php
    if($data['$candidateCount'] == 0){ 
	echo ELECTION_NO_CANDIDATES_TEXT.'<br />';
	echo BRING_CANDIDATES_FROM_TEXT.': '; ?>
	<a href="<?php echo SITE_ROOT.'/index.php?candidate&view=candidates_import';?>"><?php echo CSV_FILE_TEXT; ?></a>
	<?php echo ' '.OR_TEXT.' '; ?>
	<a href="<?php echo SITE_ROOT.'/index.php?candidate&action=editCandidate&back=showActiveElection';?>"><?php echo mb_strtolower(ADD_CANDIDATES_MANUALLY_TEXT,'UTF-8'); ?></a>
    <?php
    }else{ ?>
	<a href="<?php echo SITE_ROOT.'/index.php?candidate';?>"><?php echo SHOW_ELECTION_CANDIDATES_TEXT; ?></a>
    <?php
    } ?>   
</p>

    <?php } ?>