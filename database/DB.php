<?php
/**
 * Adapter class for MySQL database
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class DB_Class
{
	//Settings
	private $hostname;
	private $username;
	private $password;
	private $database;
	private $dbdriver;
	
	//Database handle
	public $dbh = null;	
	 
	/**
	 * Constructor connects to the database server and to the selected database
	 * 
	 * @param array $settings
	 */
	public function __construct($settings = array())
	{
		$this->hostname = $settings['hostname'];
		$this->username = $settings['username'];
		$this->password = $settings['password'];		
		$this->dbdriver = $settings['dbdriver'];	
		
		if(!isset($settings['script'])) {
			$this->database = $settings['database'];
		}
		
		//Check that selected database driver is supported
		if(!in_array($this->dbdriver, array('mysql')))
		{
			die('Database driver "'.$this->dbdriver. '" is not supported.');
		}
						
		//Connect to the selected database
		try {
                    $this->dbh = new PDO('mysql:host='.$this->hostname.';dbname='.$this->database.';charset=utf8', $this->username, $this->password);
                    //$this->dbh->query('SET CHARACTER SET utf8');
                    
                    //Prior to PHP 5.3.6, the charset option was ignored
                    if (version_compare(PHP_VERSION, '5.3.6') < 0) {
                        $this->dbh->exec("set names utf8");
                    }
            
		}
		catch(PDOException $e) {
			echo "Unable to connect: " .$e->getMessage();
		}
	}
	
	public function close()
	{
		$this->dbh = null;
	}			
}