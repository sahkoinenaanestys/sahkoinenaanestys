<?php
/*
 * This file populates the database with test data
 * The first parameter should be the database username
 * The second parameter should be the database password (cannot be empty)
 * The third parameter should be the database server
 *    
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */

require_once 'db_create.php';

if ($argc != 4 || in_array($argv[1], array('--help', '-help', '-h', '-?'))) {
    ?>

This is a command line PHP script for populating database tables with test data

Usage:
php populate.php [database user] [database password] [server]

database user: database username
database password: database password (cannot be empty)
server: localhost (If 'localhost' does not work, check with your host provider about what to use.)

With the --help, -help, -h, or -? options, you can get this help.

    <?php
} else {

    $errors = array();
    
    //TODO syotteen tarkistus
    $dbConfig = array(
        "username" => $argv[1],
        "password" => $argv[2],
        "hostname" => $argv[3], 
        "database" => DATABASE_NAME,
        "dbdriver" => DATABASE_DRIVER
    );
    
    if (!populateDatabase($dbConfig)) {
        $errors[] = "Failed populating the database. All of the information was not added."."\n";
    } 

    if (count($errors) > 0) {
        echo "\n".'Setup encountered errors and failed.'."\n";

        foreach ($errors as $error) {
            echo $error;
        }
    } else {
        ?>

Test data was succesfullly added to the database.

Now you should close the command line tool and remove the scripts folder from your server.
This is important. Do it now.

<?php
    }
}?>