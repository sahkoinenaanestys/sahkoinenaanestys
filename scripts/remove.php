<?php
/*
 * This file clears all database tables
 * and also drops the tables if option drop_tables is used in arguments
 * The first parameter should be the database username
 * The second parameter should be the database password (cannot be empty)
 * The third parameter should be the database server
 * The fourth parameter can be an option to drop tables from the database
 *   
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */

require_once 'db_remove.php';

if (($argc > 5 || $argc < 4) || in_array($argv[1], array('--help', '-help', '-h', '-?'))) {
    ?>

This is a command line PHP script for clearing all database tables of the online voting system.
This script can also be used to drop all tables if option drop_tables is used in arguments.

Usage:
php remove.php [database user] [database password] [server] [option]

database user: database username
database password: database password (cannot be empty)
server: localhost (If 'localhost' does not work, check with your host provider about what to use.)
option: optional, should be empty (Use 'drop_tables' if you want to also remove all database tables)

With the --help, -help, -h, or -? options, you can get this help.

    <?php
} else {

    $errors = array();
    
    //TODO syötteen tarkistus
    $dbConfig = array(
        "username" => $argv[1],
        "password" => $argv[2],
        "hostname" => $argv[3], 
        "database" => DATABASE_NAME,
        "dbdriver" => DATABASE_DRIVER
    );                         
    
    if (!clearTables($dbConfig)) {
        $errors[] = "Failed clearing database tables."."\n";
    } 
    
    if(isset($argv[4]) && $argv[4] == 'drop_tables') {
        if (!removeTables($dbConfig)) {
            $errors[] = "Failed removing database tables."."\n";
        } 
    }

    if (count($errors) > 0) {
        echo "\n".'Script encountered errors and failed.'."\n";

        foreach ($errors as $error) {
            echo $error;
        }
    } else {
        ?>

All information was removed from the database 

Now you should close the command line tool and remove the scripts folder from your server.
This is important. Do it now.

<?php
    }
}?>