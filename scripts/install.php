<?php
/* 
 * This file creates the database and tables
 * and adds an admin user to the database with admin privileges
 * The first parameter should be the database username
 * The second parameter should be the database password (cannot be empty)
 * The third parameter should be the database server
 * The fourth parameter should be the user's login name
 * The fifth parameter should be the user's password
 *  
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */

require_once 'db_create.php';

if ($argc != 6 || in_array($argv[1], array('--help', '-help', '-h', '-?'))) {
    ?>

This is a command line PHP script for creating the database and tables for the online voting system.
This script will also add an user to the database with admin privileges.

Usage:
php install.php [database user] [database password] [server] [login] [password]

database user: database username
database password: database password (cannot be empty)
server: localhost (If 'localhost' does not work, check with your host provider about what to use.)
login: desired login name for the user that will be created
password: desired password for the user that will be created

With the --help, -help, -h, or -? options, you can get this help.

    <?php
} else {

    $errors = array();
    
    $dbConfig = array(
        "username" => $argv[1],
        "password" => $argv[2],
        "hostname" => $argv[3], 
        "script" => true,
        "database" => DATABASE_NAME,
        "dbdriver" => DATABASE_DRIVER
    );
    
    $hasher = new PasswordHash(8, false);
    $userConfig = array(array(  "username" => DataValidator::sanitizeUsername($argv[4]), 
                                "password" => $hasher->HashPassword(DataValidator::sanitizePassword($argv[5]))
                         )
    );
                         
    if (!createDatabase($dbConfig)) {
        $errors[] = "Failed creating database."."\n";
    } 

    if (!writeConfig($dbConfig)) {
        $errors[] = "Error in creating config file. File was not created."."\n";
    }

    unset($dbConfig["script"]); 
    
    if (!createUser($userConfig, $dbConfig)) {
        $errors[] = "Failed creating user(s). User information was not added."."\n";
    } 

    if (count($errors) > 0) {
        echo "\n".'Setup encountered errors and failed.'."\n";

        foreach ($errors as $error) {
            echo $error;
        }
    } else {
        ?>

That went well. Database and tables were created. User with admin privileges was added to the database.
You should now be able to access the admin panel with login and password that you chose.

Now you should close the command line tool and remove the scripts folder from your server.
This is important. Do it now.

<?php
    }
}?>