<?php
/* 
 * Has functions for creating database tables and content
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */

require_once '../config/config_env.php';
require_once '../config/constants.php';
require_once '../database/DB.php';
require_once '../models/model.php';
require_once '../models/election.php';
require_once '../models/admin.php';
require_once '../models/voter.php';
require_once '../lib/phpass/PasswordHash.php';
require_once '../lib/utils/data_validator.php';

define("DATABASE_NAME", 'e-voting');
define("DATABASE_DRIVER", 'mysql');
define("FILE_NAME", 'config_db.php');


function createDatabase($dbConfig) {
	
    try {
        $db = new DB_Class($dbConfig);
//      $db->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
        echo "\n".'Creating database...';        
        $db->dbh->exec("CREATE DATABASE IF NOT EXISTS `e-voting` DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;");
        $db->dbh->exec("USE `e-voting`;");               		
        echo 'done'."\n";
 
        echo 'Creating tables...';
        $query = "CREATE TABLE IF NOT EXISTS `administrator` (
        `adminId` int(11) NOT NULL AUTO_INCREMENT,
	`adminLevel` int(11) NOT NULL DEFAULT '10' COMMENT '0 = no rights, 5 = election worker, 10 = admin',
        `firstName` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
        `lastName` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
        `email` varchar(200) COLLATE utf8_swedish_ci NOT NULL,
        `address` varchar(150) COLLATE utf8_swedish_ci NOT NULL,
        `city` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
        `zipCode` varchar(10) COLLATE utf8_swedish_ci NOT NULL,
        `username` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
        `password` varchar(60) COLLATE utf8_swedish_ci NOT NULL,
	`lastSeen` datetime NOT NULL,
	`loggedIn` varchar(255) COLLATE utf8_swedish_ci NOT NULL COMMENT 'Contains login hash',
        PRIMARY KEY (`adminId`),
        UNIQUE KEY `username` (`username`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AUTO_INCREMENT=1;";
        $db->dbh->exec($query);               
        
        $query = "CREATE TABLE IF NOT EXISTS `alliance` (
        `allianceId` int(11) NOT NULL,
	`allianceName` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
	`election` int(11) NOT NULL,
	`coalition` int(11) NOT NULL,
	`personInChargeEmail` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
	`votes` bigint(20) NOT NULL,
        PRIMARY KEY (`allianceId`,`election`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;";   
        $db->dbh->exec($query);

        $query = "CREATE TABLE IF NOT EXISTS `candidate` (
        `candidateNum` int(11) NOT NULL,
        `election` int(11) NOT NULL,
        `firstName` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
        `lastName` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
	`email` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
	`sortHash` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
        `votes` bigint(20) NOT NULL,
	`paperVotes` bigint(20) NOT NULL,
	`rank` int(11) NOT NULL,
	`alliance` int(11) NOT NULL,
	`coalition` int(11) NOT NULL,
        `allianceComparisionNum` double NOT NULL,
        `coalitionComparisionNum` double NOT NULL,
        `comparisionNum` double NOT NULL,
        PRIMARY KEY (`candidateNum`,`election`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;";
        $db->dbh->exec($query);

        $query = "CREATE TABLE IF NOT EXISTS `coalition` (
        `coalitionId` int(11) NOT NULL,
	`coalitionName` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
	`election` int(11) NOT NULL,
	`votes` bigint(20) NOT NULL,
	PRIMARY KEY (`coalitionId`,`election`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;";
        $db->dbh->exec($query);

        $query = "CREATE TABLE IF NOT EXISTS `election` (
        `electionId` int(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
	`status` int(11) NOT NULL COMMENT '0 = closed, 1 = active, 2 = has ended, 3 = removed',
	`startDate` datetime NOT NULL,
	`endDate` datetime NOT NULL,
	`calcMethod` int(11) NOT NULL COMMENT '0 = do not calculate coalitions, 1 = calculate coalitions',
	`resultsCalculated` tinyint(1) NOT NULL,
	`totalWwwVotes` bigint(20) NOT NULL,
	`totalPaperVotes` bigint(20) NOT NULL,
        PRIMARY KEY (`electionId`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AUTO_INCREMENT=1;";
        $db->dbh->exec($query);

        $query = "CREATE TABLE IF NOT EXISTS `voter` (
        `studentNum` int(11) NOT NULL,
        `election` int(11) NOT NULL,
        `voteDate` datetime NOT NULL,
        `voteMethod` enum('none','paper','www') COLLATE utf8_swedish_ci NOT NULL COMMENT 'none = has not voted, paper = voted in normal election, www = voted online',
        `hasVoted` tinyint(4) NOT NULL,
        `firstName` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
        `lastName` varchar(255) COLLATE utf8_swedish_ci NOT NULL,
        `email` varchar(200) COLLATE utf8_swedish_ci NOT NULL,
        `canVote` tinyint(1) NOT NULL,
        `address` varchar(150) COLLATE utf8_swedish_ci NOT NULL,
        `city` varchar(100) COLLATE utf8_swedish_ci NOT NULL,
        `zipCode` varchar(10) COLLATE utf8_swedish_ci NOT NULL,
	`lastSeen` datetime NOT NULL,
	`loggedIn` varchar(255) COLLATE utf8_swedish_ci NOT NULL COMMENT 'Contains login hash',
	`electionWorker` INT(11) NULL COMMENT 'contains the adminId of the election worker who marked this vote',
        PRIMARY KEY (`studentNum`,`election`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;";
        $db->dbh->exec($query);
      
        $db->close();
        echo 'done'."\n";
        
        return true;
    } catch(PDOException $ex) {
        echo 'Unexpected error happened: ' . $ex;
        return false;
    }
    
}


function writeConfig($dbConfig) {
    
    echo 'Creating configuration file...';
    
    //chmod(SERVER_ROOT.'/config/', 0755);
    
    $fh = fopen(SERVER_ROOT.'/config/' . FILE_NAME, "w");   
    if (!is_resource($fh)) {
        return false;
    }
    
    fwrite($fh, "<?php"."\n");
    fwrite($fh, "$"."dbConfig = array();" . "\n");
    fwrite($fh, "$"."dbConfig['hostname'] = '".$dbConfig['hostname']."';"."\n");
    fwrite($fh, "$"."dbConfig['username'] = '".$dbConfig['username']."';"."\n");
    fwrite($fh, "$"."dbConfig['password'] = '".$dbConfig['password']."';"."\n");
    fwrite($fh, "$"."dbConfig['database'] = '".$dbConfig['database']."';"."\n");
    fwrite($fh, "$"."dbConfig['dbdriver'] = '".$dbConfig['dbdriver']."';"."\n");
    fwrite($fh, "?>");
    fclose($fh);

    echo 'done'."\n";
    return true;
}



function createUser($userConfig, $dbConfig) {
    
    try {
        echo 'Creating user(s)...';
        
        $db = new DB_Class($dbConfig);
        $db->dbh->query('SET CHARACTER SET utf8');
        //$db->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $i = 0;
        while ($i < count($userConfig)) {
            $user = $userConfig[$i];

            $query = "INSERT INTO administrator (username, password, adminLevel) 
                        VALUES (:username,:password,:adminLevel)";
            $q = $db->dbh->prepare($query);
            $success = $q->execute(array(':username'=>$user['username'],':password'=>$user['password'],':adminLevel'=>Admin_Model::LEVEL_ADMIN));
            
            $i++;
        } 
        $db->close();
        
        echo 'done'."\n";
        
        return $success;
    } catch(PDOException $ex) {
        echo 'Unexpected error happened: ' . $ex;
        return false;
    }
}


function populateDatabase($dbConfig) {
    
    try {
        echo 'Populating the database with test data...';
        
	$salt = '5f2b5e892dc48fc7bd3372a0fae18b0719ea57ba';
        $db = new DB_Class($dbConfig);
        //$db->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
        $db->dbh->query('SET CHARACTER SET utf8');
       
        //create elections
        $query = "INSERT INTO election (name, status, startDate, endDate, calcMethod) 
                    VALUES ('Vaali A', '".Election_Model::STATE_ACTIVE."', '2013-01-01 00:00:00', '2013-10-09 23:59:59', '".Election_Model::CALCULATE_COALITIONS."')";
        $db->dbh->exec($query);
        $election1Id = $db->dbh->lastInsertId();
        
        $query = "INSERT INTO election (name, status, startDate, endDate, calcMethod) 
                    VALUES ('Vaali B', '".Election_Model::STATE_CLOSED."', '2012-05-06 00:00:00', '2012-08-09 23:59:59', '".Election_Model::CALCULATE_COALITIONS."')";
        $db->dbh->exec($query);
        //$election2Id = $db->dbh->lastInsertId();  
        
        //create adminstrator
        $hasher = new PasswordHash(8, false);
        $adminPasswd = $hasher->HashPassword('adminsala');
        $query = "INSERT INTO administrator (firstName, lastName, email, address, city, zipCode, username, password, adminLevel) 
                VALUES ('Matti', 'Moderaattori', 'masa@gmail.com', 'Paratiisikatu 13', 'Ankkalinna', '54657', 'admin2', '".$adminPasswd."', '".Admin_Model::LEVEL_ADMIN."');";       
        $db->dbh->exec($query);
        
        
        //create coalitions
        $query = "INSERT INTO coalition (coalitionId, election, coalitionName) 
                    VALUES ('1', '".$election1Id."', 'Vaalirengas X')";
        $db->dbh->exec($query);
        //$coalition1Id = $db->dbh->lastInsertId();            
        
        $query = "INSERT INTO coalition (coalitionId, election, coalitionName) 
                    VALUES ('2', '".$election1Id."', 'Vaalirengas Y')";
        $db->dbh->exec($query);
        
        
        //create alliances
        $query = "INSERT INTO alliance (allianceId, election, coalition, allianceName) 
                     VALUES ('1','".$election1Id."', '1', 'Vaaliliitto A')";
        $db->dbh->exec($query);
        //$alliance1Id = $db->dbh->lastInsertId();
        
        $query = "INSERT INTO alliance (allianceId, election, coalition, allianceName) 
                     VALUES ('2','".$election1Id."', '1', 'Vaaliliitto B')";
        $db->dbh->exec($query);
        //$alliance2Id = $db->dbh->lastInsertId();
        
        $query = "INSERT INTO alliance (allianceId, election, coalition, allianceName) 
                     VALUES ('3','".$election1Id."', '', 'Vaaliliitto C')";
        $db->dbh->exec($query);
        //$alliance3Id = $db->dbh->lastInsertId();
        
        
        //create voters
        $query = "INSERT INTO voter (election, studentNum, firstName, lastName, canVote) 
                    VALUES ('".$election1Id."', '12345', 'Antti', 'Ranta', '".Voter_Model::HAS_VOTING_RIGHT."')";
        $db->dbh->exec($query);
        
        $query = "INSERT INTO voter (election, studentNum, firstName, lastName, canVote) 
                    VALUES ('".$election1Id."', '23456', 'Janne', 'Seppänen', '".Voter_Model::HAS_VOTING_RIGHT."')";
        $db->dbh->exec($query);
        
        $query = "INSERT INTO voter (election, studentNum, firstName, lastName, canVote) 
                    VALUES ('".$election1Id."', '34567', 'Matias', 'Ylipelto', '".Voter_Model::HAS_VOTING_RIGHT."')";
        $db->dbh->exec($query);
        
        
        //create candidates
        $query = "INSERT INTO candidate (election, firstname, lastname, candidateNum, alliance, votes, sortHash) 
                    VALUES ('".$election1Id."', 'Aarne', 'Aarnio', '1111', '1', '100', '".sha1($salt.'Aarnio'.$salt.'Aarne'.$salt.'1111')."')";
        $db->dbh->exec($query);
        
        $query = "INSERT INTO candidate (election, firstname, lastname, candidateNum, alliance, votes, sortHash) 
                    VALUES ('".$election1Id."', 'Bertta', 'Inne', '1108', '1', '200', '".sha1($salt.'Bertta'.$salt.'Inne'.$salt.'1108')."')";
        $db->dbh->exec($query);
        
        $query = "INSERT INTO candidate (election, firstname, lastname, candidateNum, alliance, votes, sortHash) 
                    VALUES ('".$election1Id."', 'Celsius', 'Neo', '1107', '1', '300', '".sha1($salt.'Celsius'.$salt.'Neo'.$salt.'1107')."')";
        $db->dbh->exec($query);
        
        $query = "INSERT INTO candidate (election, firstname, lastname, candidateNum, alliance, votes, sortHash) 
                    VALUES ('".$election1Id."', 'Daavid', 'Becham', '1106', '2', '101', '".sha1($salt.'Daavid'.$salt.'Becham'.$salt.'1106')."')";
        $db->dbh->exec($query);
        
        $query = "INSERT INTO candidate (election, firstname, lastname, candidateNum, alliance, votes, sortHash) 
                    VALUES ('".$election1Id."', 'Eemeli', 'Vaahteramäki', '1105', '2', '201', '".sha1($salt.'Eemeli'.$salt.'Vaahteramäki'.$salt.'1105')."')";
        $db->dbh->exec($query);
        
        $query = "INSERT INTO candidate (election, firstname, lastname, candidateNum, alliance, votes, sortHash) 
                    VALUES ('".$election1Id."', 'Faarao', 'Ilmarinen', '1104', '2', '301', '".sha1($salt.'Faarao'.$salt.'Ilmarinen'.$salt.'1104')."')";
        $db->dbh->exec($query);
        
        $query = "INSERT INTO candidate (election, firstname, lastname, candidateNum, alliance, votes, sortHash) 
                    VALUES ('".$election1Id."', 'Gideon', 'Otso', '1103', '3', '102', '".sha1($salt.'Gideon'.$salt.'Otso'.$salt.'1103')."')";
        $db->dbh->exec($query);
        
        $query = "INSERT INTO candidate (election, firstname, lastname, candidateNum, alliance, votes, sortHash) 
                    VALUES ('".$election1Id."', 'Heikki', 'Heikkinen', '1102', '3', '202', '".sha1($salt.'Heikki'.$salt.'Heikkinen'.$salt.'1102')."')";
        $db->dbh->exec($query);      
        
        $query = "INSERT INTO candidate (election, firstname, lastname, candidateNum, alliance, votes, sortHash) 
                    VALUES ('".$election1Id."', 'Iivari', 'Iisalmi', '1101', '3', '302', '".sha1($salt.'Iivari'.$salt.'Iisalmi'.$salt.'1101')."')";
        $db->dbh->exec($query);    
        
        $db->close();
        echo 'done'."\n";
        
        return true;
    } catch(PDOException $ex) {
        echo 'Unexpected error happened: ' . $ex;
        return false;
    }
}

?>