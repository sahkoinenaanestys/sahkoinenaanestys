<?php
/* 
 * For removing database contents
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */

require_once '../config/config_env.php';
require_once '../config/constants.php';
require_once '../database/DB.php';

define("DATABASE_NAME", 'e-voting');
define("DATABASE_DRIVER", 'mysql');


function clearTables($dbConfig) {
         
    try {
        echo 'Clearing database tables...';
        
        $db = new DB_Class($dbConfig);
        
        $query = "DELETE FROM alliance";
        $db->dbh->exec($query);
        
        $query = "DELETE FROM candidate";
        $db->dbh->exec($query);
        
        $query = "DELETE FROM coalition";
        $db->dbh->exec($query);
        
        $query = "DELETE FROM voter"; 
        $db->dbh->exec($query);
        
        $query = "TRUNCATE TABLE election"; 
        $db->dbh->exec($query);
        
        $db->close();  
        echo 'done'."\n";
        
        return true;
    } catch(PDOException $ex) {
        echo 'Unexpected error happened: ' . $ex;
        return false;
    }
}

function removeTables($dbConfig) {
         
    try {
        echo 'Removing database tables...';
        
        $db = new DB_Class($dbConfig);
        
        $query = "DROP TABLE administrator";
        $db->dbh->exec($query);
        
        $query = "DROP TABLE alliance";
        $db->dbh->exec($query);
        
        $query = "DROP TABLE candidate";
        $db->dbh->exec($query);
        
        $query = "DROP TABLE coalition";
        $db->dbh->exec($query);
        
        $query = "DROP TABLE election";
        $db->dbh->exec($query);
        
        $query = "DROP TABLE voter";
        $db->dbh->exec($query);
        
        $db->close();  
        echo 'done'."\n";
        
        return true;
    } catch(PDOException $ex) {
        echo 'Unexpected error happened: ' . $ex;
        return false;
    }
}

?>