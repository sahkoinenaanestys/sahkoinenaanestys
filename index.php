<?php
//Load settings
include_once 'config/config_env.php';
include_once 'config/config_db.php';
include_once 'config/constants.php';

//Start session
session_set_cookie_params(COOKIE_EXPIRY, "/", COOKIE_DOMAIN, COOKIE_SECURE, true);
session_start();

//Load translations
if(!isset($_SESSION['language'])) 
{
	$_SESSION['language'] = DEFAULT_LANGUAGE;
}
switch ($_SESSION['language']) {
	case 'FIN':
		include_once 'language/fin_lang.php';
		break;
	case 'ENG':
		include_once 'language/eng_lang.php';
		break;
}

//Include superclasses for models and controllers
include_once SERVER_ROOT.'/controllers/controller.php';
include_once SERVER_ROOT.'/models/model.php';

//Include model for handling view functionality
include_once SERVER_ROOT.'/models/view.php';

//Include the router file that handles all the page requests
include SERVER_ROOT.'/controllers/router.php';