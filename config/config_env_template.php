<?php
/**
 * This file contains environment specific configurations
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */

//Parent folder containing the site's documents
$config['server_root'] = 'C:\xampp\htdocs\sahkoinenaanestys';

//Webserver address
$config['site_root'] = 'http://127.0.0.1/sahkoinenaanestys';

//Default user language (FIN, ENG..)
$config['default_language'] = 'FIN';

//Session settings
ini_set ("session.use_cookies", true);
$config['cookie_expiry'] = time() + 3*60*60;
$config['cookie_domain'] = null;
$config['cookie_secure'] = false; //When set to TRUE, the cookie will only be set if a secure connection exists.

//Log Info
$config['log_i'] = array(
        'enabled'		=> 1, // 0 = disabled, 1 = enabled
        'log_file'		=> 'main.txt',
        'log_max_file_size'	=> 512000, //500 KB
        'log_max_backup_index'  => 5,
        'log_pattern'           => '{time} {name} - {msg}',
        'log_time_format'	=> 'd.m.Y H:i:s.ms'
);

//simpleSAMLphp settings
$config['sp_private_key'] = 'e-voting.pem';
$config['sp_certificate'] = 'e-voting.crt';
$config['sp_redirect_sign'] = false; //When set to TRUE, authentication requests, logout requests and logout responses are signed
$config['idp_certificate'] = 'server.crt';
$config['idp_entity_id'] = 'http://idp.mightydus.dy.fi/simplesaml/saml2/idp/metadata.php';
$config['idp_single_sign_on_service'] = 'http://idp.mightydus.dy.fi/simplesaml/saml2/idp/SSOService.php';
$config['idp_single_logout_service'] = 'http://idp.mightydus.dy.fi/simplesaml/saml2/idp/SingleLogoutService.php';
$config['idp_artifact_resolution_service'] = array ();
$config['idp_scope'] = array ();
$config['idp_logout_url'] = '';
$config['idp_studentnum_attribute'] = 'studentNum';
$config['idp_studentnum_attribute_value_namespace'] = '';
$config['simplesamlphp_cert_dir'] = $config['server_root'].'/lib/simplesamlphp-1.10.0/cert/';
$config['simplesamlphp_log_dir'] = 'log/';
$config['simplesamlphp_data_dir'] = 'data/';
$config['simplesamlphp_secret_salt'] = 'f2ld2046fz8y6p750vpkl6i8fsr98ouu';
$config['simplesamlphp_contact_name'] = 'Matti Meikäläinen';
$config['simplesamlphp_contact_email'] = 'na@example.org';
$config['simplesamlphp_admin_passwd'] = '123';