<?php
/**
 * Constants are defined here.
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */

//Must include 'config/config_env.php' before this file
if(empty($config)) {
	die('Configuration is not set.');
}

define('SERVER_ROOT' , $config['server_root']);
define('SITE_ROOT' , $config['site_root']);
define('DEFAULT_LANGUAGE', $config['default_language']);
define('UPLOAD_DIR' , $config['server_root'].'/tmp/uploads/');
define('LOG_DIR' , $config['server_root'].'/tmp/log/');
define("LOG_I", serialize($config['log_i']));
define('AUTOMATIC_LOG_OUT_TIME', 15);
define('ADMIN_PASSWD_MIN_LENGTH', 8);
define('MAX_OFFLINE_TIME', 20);
define('CSV_TAB_DELIMITER', "\t");
define('CSV_COMMA_DELIMITER', ",");
define('CSV_PIPE_DELIMITER', "|");
define('CSV_SEMICOLON_DELIMITER', ";");
define('COOKIE_NAME', 'sid');
define('COOKIE_EXPIRY', $config['cookie_expiry']);
define('COOKIE_DOMAIN', $config['cookie_domain']);
define('COOKIE_SECURE', $config['cookie_secure']);

//SimpleSAMLphp
define('SP_PRIVATE_KEY', $config['sp_private_key']);
define('SP_CERTIFICATE', $config['sp_certificate']);
define('SP_REDIRECT_SIGN', $config['sp_redirect_sign']);
define('IDP_CERTIFICATE', $config['idp_certificate']);
define('IDP_ENTITY_ID', $config['idp_entity_id']);
define('IDP_SINGLE_SIGN_ON_SERVICE', serialize($config['idp_single_sign_on_service']));
define('IDP_SINGLE_LOGOUT_SERVICE', serialize($config['idp_single_logout_service']));
define('IDP_ARTIFACT_RESOLUTION_SERVICE', serialize($config['idp_artifact_resolution_service']));
define('IDP_SCOPE', serialize($config['idp_scope']));
define('IDP_LOGOUT_URL', $config['idp_logout_url']);
define('IDP_STUDENTNUM_ATTRIBUTE', $config['idp_studentnum_attribute']);
define('IDP_STUDENTNUM_ATTRIBUTE_VALUE_NAMESPACE', $config['idp_studentnum_attribute_value_namespace']);
define('SIMPLESAMLPHP_BASEURL', $config['site_root'].'/simplesaml/');
define('SIMPLESAMLPHP_CERT_DIR', $config['simplesamlphp_cert_dir']);
define('SIMPLESAMLPHP_LOG_DIR', $config['simplesamlphp_log_dir']);
define('SIMPLESAMLPHP_DATA_DIR', $config['simplesamlphp_data_dir']);
define('SIMPLESAMLPHP_SECRET_SALT', $config['simplesamlphp_secret_salt']);
define('SIMPLESAMLPHP_CONTACT_NAME', $config['simplesamlphp_contact_name']);
define('SIMPLESAMLPHP_CONTACT_EMAIL', $config['simplesamlphp_contact_email']);
define('SIMPLESAMLPHP_ADMIN_PASSWD', $config['simplesamlphp_admin_passwd']);