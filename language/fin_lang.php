<?php
/**
 * This file contains Finnish translations
 *
 * The naming convention for translation constants is:
 * all caps and suffix '_TEXT' (e.g. 'ELECTRONIC_VOTING_TEXT')
 *
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
define('STUDENT_UNION_OF_THE_UNI_OF_OULU_TEXT', 'Oulun yliopiston ylioppilaskunta');
define('ELECTRONIC_VOTING_TEXT', 'Sähköinen äänestys');
define('ADMINISTRATION_TEXT', 'Hallintapaneeli');
define('CANDIDATES_TEXT', 'Ehdokkaat');
define('INSTRUCTIONS_TEXT','Käyttöohje');
define('STUDENT_UNION_WEB_PAGE_TEXT','OYY.fi');
define('FRONTPAGE_TEXT','Etusivu');
define('LOG_IN_TEXT', 'Kirjaudu');
define('LOG_OUT_TEXT', 'Kirjaudu ulos');
define('GO_BACK_TEXT', 'Takaisin');

//Frontpage
define('FRONTPAGE_TITLE_TEXT', 'Tervetuloa OYY:n sähköiseen äänestysjärjestelmään!');
define('FRONTPAGE_LOG_IN_INSTRUCTIONS_TEXT', 'Tarvitset äänestämiseen Pajun tai Koivun tunnukset.');

//Login infromation
define('NAME_TEXT', 'Nimi');
define('STUDENT_NUMBER_TEXT', 'Opiskelijanumero');
define('ADDRESS_TEXT', 'Osoite');

//Navigation
define('NEXT_TEXT', 'Seuraava');
define('PREVIOUS_TEXT', 'Edellinen');

//Voting page
define('LOGGED_IN_SUCCESSFULLY_TEXT', 'Kirjautuminen onnistui!');
define('LOGGED_OUT_SUCCESSFULLY_TEXT', 'Uloskirjautuminen onnistui!');
define('VOTE_TEXT', 'Äänestä');
define('PLEASE_CONFIRM_YOUR_CANDIDATE_CHOICE_TEXT', 'Ole hyvä ja vahvista ehdokasvalintasi');
define('YOU_ARE_ABOUT_TO_VOTE_EMPTY_TEXT', 'Olet äänestämässä tyhjää');
define('YOU_ARE_ABOUT_TO_VOTE_FOR_THE_FOLLOWING_CANDIDATE_TEXT', 'Olet äänestämässä seuraavaa ehdokasta');
define('NUMBER_ABBREVIATION_TEXT', 'N:o');
define('YOUR_VOTING_WAS_SUCCESSFUL_TEXT', 'Äänestäminen onnistui!');
define('YOU_VOTED_FOR_CANDIDATE_TEXT','Äänestit ehdokasta');
define('YOU_GAVE_A_SCRATCH_VOTE_TEXT','Äänestit tyhjää');
define('THANK_YOU_FOR_YOUR_VOTE_TEXT','Kiitos äänestäsi!');
define('YOU_HAVE_ALREADY_VOTED_TEXT', 'Olet jo antanut äänesi');
define('YOU_HAVE_ALREADY_VOTED_INSTRUCTIONS_TEXT', 'Olet jo äänestänyt. Ääntä ei voi vaihtaa jälkikäteen.');
define('PLEASE_LOG_OUT_TEXT', 'Ole hyvä ja kirjaudu ulos.');
define('THERE_ARE_CURRENTLY_NO_ELECTIONS_UNDERWAY_TEXT', 'Tällä hetkellä ei ole mitään vaaleja menossa');
define('YOU_WILL_BE_AUTOMATICALLY_LOGGED_OUT_TEXT', 'Teidät kirjataan ulos automaattisesti.');
define('TIME_LEFT_TEXT', 'Aikaa jäljellä');
define('SECONDS_SYMBOL_TEXT', 's');
define('SELECT_A_CANDIDATE_AND_PRESS_NEXT_TEXT','Valitse ehdokas ja paina seuraava.');
define('EMPTY_VOTE_TEXT', 'Tyhjä ääni');

//Administration
define('ADMINISTRATION_FRONTPAGE_TEXT', 'Tämä sivu on Oulun yliopiston ylioppilaskunnan luotetuille jäsenille tarkoitettu hallinnointisivu, joka on tarkoitettu sähköiseen äänestysjärjestelmään liittyvien muutosten tekemiseen.');
define('ADMINISTRATION_FRONTPAGE_INSTRUCTIONS_TEXT' , 'Kirjautuaksesi sisään, ole hyvä ja syötä käyttäjätunnuksesi sekä salasana.');
define('USERNAME_TEXT', 'Käyttäjätunnus');
define('PASSWORD_TEXT', 'Salasana');
define('ADMINISTRATORS_TEXT', 'Hallinnoitsijat');
define('VOTERS_TEXT', 'Äänioikeutetut');
define('ACCOUNT_TEXT', 'Tili');
define('ADD_NEW_ADMIN_TEXT', 'Lisää uusi hallinnoitsija');
define('MODIFY_ADMIN_TEXT', 'Muokkaa hallinnoitsijaa');
define('NEW_ADMIN_ADDED_SUCCESSFULLY_TEXT', 'Uusi hallinnoitsija lisätty onnistuneesti!');
define('ADMIN_INFORMATION_UPDATED_SUCCESSFULLY_TEXT', 'Hallinnoitsijan tiedot päivitetty onnistuneesti!');
define('NO_ADMINISTRATORS_TEXT', 'Ei hallinnoitsijoita');
define('CHANGE_PASSWORD_TEXT', 'Vaihda salasana');
define('CONFIRM_PASSWORD_TEXT', 'Varmista salasana');
define('LEVEL_TEXT', 'Taso');
define('ADMINISTRATOR_TEXT', 'Hallinnoitsija');
define('ELECTION_WORKER_TEXT', 'Vaalivirkailija');
define('MARK_THE_VOTE_TEXT', 'Merkitse äänestys');
define('MARK_THE_VOTE_CONFIRM_TEXT', 'Oletko varma että haluat merkitä tämän henkilön äänestäneen?');
define('SUPPORTED_BROWSERS_TEXT', 'Tuetut selaimet ovat Mozilla Firefox (Suositus), Google Chrome ja Apple Safari');

define('ELECTION_RESULTS_TEXT', 'Vaalin tulokset');
define('ELECTION_TEXT', 'Vaali');
define('ELECTIONS_TEXT', 'Vaalit');
define('CURRENT_ELECTIONS_TEXT', 'Nykyiset vaalit');
define('PREVIOUS_ELECTIONS_TEXT', 'Menneet vaalit');
define('ACTIONS_TEXT', 'Toiminnot');
define('NO_ACTIVE_ELECTIONS_TEXT', 'Ei meneillään olevia vaaleja');
define('NO_PREVIOUS_ELECTIONS_TEXT', 'Ei menneitä vaaleja');
define('ELECTION_DETAILS_TEXT', 'Tiedot');
define('ELECTION_NAME_TEXT', 'Nimi');
define('ELECTION_START_DATE_TEXT', 'Alkamispäivä');
define('ELECTION_START_TIME_TEXT', 'Alkamisaika');
define('ELECTION_END_DATE_TEXT', 'Päättymispäivä');
define('ELECTION_END_TIME_TEXT', 'Päättymisaika');
define('ELECTION_CALC_METHOD_TEXT', 'Vaalin tulosten laskenta');
define('ELECTION_COUNT_COALITIONS_TEXT', 'Laske vaalirenkaiden äänet');
define('ELECTION_DO_NOT_COUNT_COALITIONS_TEXT', 'Älä laske vaalirenkaiden ääniä');
define('ELECTION_STATUS_TEXT', 'Tila');
define('ELECTION_STATUS_ACTIVE_TEXT', 'Meneillään');
define('ELECTION_STATUS_CLOSED_TEXT', 'Suljettu');
define('ELECTION_STATUS_ENDED_TEXT', 'Päättynyt');
define('ELECTION_HAS_NOT_STARTED_YET_TEXT', 'Alkamassa');
define('OPEN_OR_CLOSE_ELECTION_TEXT', 'Keskeytä/jatka vaalia');
define('TIME_AND_DATE_TEXT', 'Aikaväli');
define('VOTER_TURNOUT_TEXT', 'Äänestysprosentti');
define('VOTES_TEXT', 'Äänet');
define('WWW_VOTES_TEXT', 'Sähköiset äänet');
define('PAPER_VOTES_TEXT', 'Uurnavaalin äänet');
define('EMPTY_VOTES_TEXT', 'Tyhjät äänet');
define('ELECTION_VOTERS_TEXT', 'Vaaliin kuuluvat äänioikeutetut');
define('ELECTION_NO_VOTERS_TEXT', 'Tähän vaaliin ei ole vielä lisätty äänioikeutettuja.');
define('SHOW_ELECTION_VOTERS_TEXT', 'Näytä äänioikeutetut');
define('ELECTION_CANDIDATES_TEXT', 'Vaaliin kuuluvat ehdokkaat');
define('ELECTION_NO_CANDIDATES_TEXT', 'Tähän vaaliin ei ole vielä lisätty ehdokkaita.');
define('SHOW_ELECTION_CANDIDATES_TEXT', 'Näytä ehdokkaat');
define('IMPORT_PAPER_VOTES_TEXT', 'Tuo uurnavaalin äänet');
define('NEW_ELECTION_ADDED_SUCCESSFULLY_TEXT', 'Uusi vaali lisätty onnistuneesti!');
define('ELECTION_INFORMATION_UPDATED_SUCCESSFULLY_TEXT', 'Vaalin tiedot päivitetty onnistuneesti!');
define('ELECTION_REMOVED_TEXT', 'Vaali poistettu');
define('ELECTION_ENDED_TEXT', 'Vaali on päätetty');
define('ELECTION_MOVED_TO_HISTORY_TEXT', 'Vaali siirretty menneisiin vaaleihin');
define('PAPER_VOTES_ADDED_SUCCESSFULLY_TEXT', 'Uurnavaalin äänet lisättiin järjestelmään onnistuneesti!');

define('NEW_ELECTION_TEXT', 'Uusi vaali');
define('EDIT_ELECTION_TEXT', 'Muokkaa vaalia');
define('MOVE_ELECTION_TO_HISTORY_TEXT', 'Siirrä vaali historiaan');
define('END_ELECTION_TEXT', 'Päätä vaali');
define('EXPORT_ELECTION_RESULTS_TEXT', 'Vaalin tulokset');
define('EXPORT_RESULTS_CSV_TEXT', 'Tällä sivulla pystyy tuottamaan CSV-muotoisen tiedoston vaalin tuloksista ja ehdokkaiden äänimääristä.');
define('PAPER_VOTES_WARNING_TEXT', 'Jos uurnavaalin ääniä ei ole syötetty, järjestelmä ei pysty laskemaan tuloksia oikein.');
define('DOWNLOAD_RESULTS_CSV_TEXT', 'Lataa tiedosto painamalla alla olevaa painiketta.');
define('EXPORT_RESULTS_CSV_INFO_TEXT', 'Tulosten laskennassa ja tiedoston luonnissa voi kestää jonkin aikaa.');
define('CSV_FILE_TEXT', 'CSV-tiedostosta');
define('MANUAL_RESULTS_CALC_TEXT', 'Voit myös ladata ehdokkaiden saamat äänet ja annetut äänet erillisinä CSV-tiedostoina manuaalista tulosten laskentaa varten.');
define('IMPORT_PAPER_VOTES_CSV_TEXT', 'Tämä sivu on tarkoitettu uurnavaalin äänten tuontiin järjestelmään.');
define('IMPORT_PAPER_VOTES_CSV_EXPLANATION_TEXT', 'Tyhjät/hylätyt äänet on merkattava CSV-tiedostoon ehdokasnumerolla -1 (miinus yksi).');
define('IMPORT_CANDIDATE_VOTES_TEXT', 'Tuo CSV-tiedosto joka sisältää ehdokkaiden saamat äänet');
define('IMPORT_VOTES_GIVEN_BY_VOTERS_TEXT', 'Tuo CSV-tiedosto joka sisältää äänioikeutettujen antamat äänet');
define('EXPORT_CSV_LIST_TEXT', 'Vie CSV-tiedostona');
define('DOWNLOAD_RESULTS_CSV_BTN_TEXT', 'Lataa tulokset');
define('DOWNLOAD_SEPARATE_VOTES_FILES_TEXT','Lataa annetut äänet manuaalista tulosten laskentaa varten');
define('DOWNLOAD_CANDIDATE_VOTES_TEXT','Lataa ehdokkaiden äänet');
define('DOWNLOAD_VOTER_VOTES_TEXT','Lataa annetut äänet');

define('EDIT_CANDIDATE_TEXT', 'Muokkaa ehdokasta');
define('NO_CANDIDATES_TEXT', 'Ei ehdokkaita');
define('CANDIDATE_NUM_TEXT', 'Ehdokkaan numero');
define('CANDIDATE_TEXT', 'Ehdokas');
define('ALLIANCE_TEXT', 'Vaaliliitto');
define('COALITION_TEXT', 'Vaalirengas');
define('COALITION_NAME_TEXT', 'Vaalirenkaan nimi');
define('ALLIANCE_NAME_TEXT', 'Vaaliliiton nimi');
define('CANDIDATE_VOTES_TEXT', 'Ehdokkaan äänet');
define('RANK_TEXT', 'Sijoitus');
define('COMPARISION_NUM_TEXT', 'Vertailuluku');
define('ALLIANCE_COMPARISION_NUM_TEXT', 'Vaaliliiton vertailuluku');
define('COALITION_COMPARISION_NUM_TEXT', 'Vaalirenkaan vertailuluku');
define('NOT_IN_COALITION_TEXT', '(ei vaalirenkaassa)');
define('CANDIDATE_ADDED_SUCCESSFULLY_TEXT', 'Ehdokkaan lisääminen onnistui!');
define('CANDIDATE_UPDATED_SUCCESSFULLY_TEXT', 'Ehdokkaan tietojen muuttaminen onnistui!');
define('IMPORT_CANDIDATES_TEXT', 'Ehdokkaiden tuonti CSV-tiedostosta');
define('IMPORT_ALLIANCES_TEXT', 'Vaaliliittojen ja vaalirenkaiden tuonti CSV-tiedostosta');
define('IMPORT_CSV_TEXT', 'Csv-tiedoston lukemisessa ja tietojen tallentamisessa voi kestää jonkin aikaa.');
define('CHOOSE_A_FILE_TEXT', 'Valitse ladattava tiedosto');
define('PLEASE_WAIT_TEXT', 'Ole hyvä ja odota sulkematta sivua kunnes prosessi on valmis.');
define('SAVING_INFORMATION_TEXT', 'Tallennetaan tietoja');
define('CANDIDATES_IMPORTED_SUCCESSFULLY_TEXT', 'Ehdokkaiden lisääminen onnistui!');
define('ALLIANCES_IMPORTED_SUCCESSFULLY_TEXT', 'Vaaliliittojen lisääminen onnistui!');
define('REMOVE_ALL_CANDIDATES_TEXT', 'Poista kaikki ehdokkaat');
define('REMOVE_ALL_CANDIDATES_WARNING_TEXT', 'Tämä toiminto poistaa KAIKKI ehdokkaat näistä vaaleista.');
define('CANDIDATES_CANNOT_BE_REMOVED_TEXT', 'Ehdokkaita ei voida poistaa.');
define('MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT', 'Näiden vaalien on oltava suljettuna ennen kuin tämä toimenpide voidaan suorittaa.');
define('ELECTION_HAS_TO_BE_ENDED_TEXT', 'Vaali on päätettävä ennen kuin tämä toimenpide voidaan suorittaa.');
define('UPLOAD_NO_ERR_TEXT', 'Tiedoston tallennus onnistui');
define('BRING_CANDIDATES_FROM_TEXT', 'Tuo ehdokkaat');
define('ADD_CANDIDATES_MANUALLY_TEXT', 'Lisää ehdokkaat käsin');
define('ADD_NEW_ALLIANCE_TEXT', 'Lisää vaaliliitto'); 

define('NO_VOTERS_TEXT', 'Ei äänioikeutettuja');
define('NEW_VOTER_TEXT', 'Uusi äänioikeutettu');
define('ADD_NEW_VOTER_TEXT', 'Lisää uusi äänioikeutettu');
define('MODIFY_VOTER_TEXT', 'Muokkaa äänioikeutettua');
define('REMOVE_ALL_VOTERS_TEXT', 'Poista kaikki äänioikeutetut');
define('REMOVE_ALL_VOTERS_WARNING_TEXT', 'Tämä toiminto poistaa KAIKKI äänioikeutetut näistä vaaleista.');
define('FIRST_NAME_TEXT', 'Etunimi');
define('LAST_NAME_TEXT', 'Sukunimi');
define('EMAIL_TEXT', 'Email');
define('CITY_TEXT', 'Kaupunki');
define('POSTAL_CODE_TEXT', 'Postinumero');
define('RIGHT_TO_VOTE_TEXT', 'Äänioikeus');
define('VOTED_TEXT', 'Äänestänyt');
define('PAPER_VOTE_SHORT_TEXT', 'Paperi');
define('ELECTRONIC_VOTE_SHORT_TEXT', 'Sähköinen');
define('VOTE_METHOD_TEXT', 'Äänestystapa');
define('VOTE_DATE_TEXT', 'Äänestyspäivämäärä');
define('NEW_VOTER_ADDED_SUCCESSFULLY_TEXT', 'Uusi äänioikeutettu lisätty onnistuneesti!');
define('VOTER_INFORMATION_UPDATED_SUCCESSFULLY_TEXT', 'Äänioikeutetun tiedot päivitetty onnistuneesti!');
define('IMPORT_VOTERS_TEXT', 'Äänioikeutettujen tuonti CSV-tiedostosta');
define('VOTERS_IMPORTED_SUCCESSFULLY_TEXT', 'Äänioikeutettujen tuonti onnistui!');
define('VOTERS_CANNOT_BE_REMOVED_EXPLANATION_TEXT', 'Äänioikeutettuja ei voida poistaa. Näiden vaalien on oltava suljettuna ennen kuin tämä toimenpide voidaan suorittaa.');
define('VOTERS_CANNOT_BE_REMOVED_VOTES_GIVEN_TEXT', 'Äänioikeutettuja ei voida poistaa. Osa äänioikeutetuista on jo äänestänyt.');
define('BRING_VOTERS_FROM_TEXT', 'Tuo äänioikeutetut');
define('ADD_VOTERS_MANUALLY_TEXT', 'Lisää äänioikeutetut käsin');
define('HAS_NO_VOTING_RIGHT_TEXT', 'Ei äänioikeutta');
define('HAS_VOTING_RIGHT_TEXT', 'On äänioikeus');

define('ALLIANCES_AND_COALITIONS_TEXT', 'Vaaliliitot ja vaalirenkaat');
define('NO_ALLIANCES_OR_COALITIONS_TEXT', 'Ei vaaliliittoja eikä vaalirenkaita');
define('ALLIANCES_TEXT', 'Vaaliliitot');
define('COALITIONS_TEXT', 'Vaalirenkaat');
define('ID_TEXT', 'Tunnus');
define('EDIT_ALLIANCE_TEXT', 'Muokkaa vaaliliittoa');
define('EDIT_COALITION_TEXT', 'Muokkaa vaalirengasta');
define('NO_ALLIANCES_TEXT', 'Ei vaaliliittoja');
define('NO_COALITIONS_TEXT', 'Ei vaalirenkaita');
define('ALLIANCE_ID_TEXT', 'Vaaliliiton tunnus');
define('COALITION_ID_TEXT', 'Vaalirenkaan tunnus');
define('EMAIL_OF_PERSON_IN_CHARGE_TEXT', 'Vastuuhenkilön sähköposti');
define('COALITION_ADDED_SUCCESSFULLY_TEXT', 'Vaalirenkaan lisääminen onnistui!');
define('COALITION_UPDATED_SUCCESSFULLY_TEXT', 'Vaalirenkaan tietojen muuttaminen onnistui!');
define('ALLIANCE_ADDED_SUCCESSFULLY_TEXT', 'Vaaliliiton lisääminen onnistui!');
define('ALLIANCE_UPDATED_SUCCESSFULLY_TEXT', 'Vaaliliiton tietojen muuttaminen onnistui!');
define('REMOVED_SUCCESSFULLY_TEXT', 'Poistaminen onnistui!');

//Actions
define('NEW_TEXT', 'Uusi');
define('EDIT_TEXT', 'Muokkaa');
define('ADD_TEXT', 'Lisää');
define('REMOVE_TEXT', 'Poista');
define('REMOVE_ALL_TEXT', 'Poista kaikki');
define('SHOW_HISTORY_TEXT', 'Näytä historia');
define('IMPORT_FROM_FILE_TEXT', 'Tuo tiedostosta');
define('SAVE_TEXT', 'Tallenna');
define('CANCEL_TEXT', 'Peruuta');
define('UPLOAD_TEXT', 'Lataa tiedosto');
define('DOWNLOAD_TEXT', 'Lataa tiedosto');
define('FIND_TEXT', 'Hae');

//General messages
define('YOU_HAVE_BEEN_LOGGED_OUT_TEXT', 'Teidät on kirjattu ulos.');
define('NO_OPTION_SELECTED_TEXT', '(ei valintaa)');
define('CLICK_TO_EXPAND_COLLAPSE_TEXT', 'Klikkaa näyttääksesi/piilottaaksesi rivit');
define('ACTION_CONFIRM_TEXT', 'Oletko aivan varma että haluat suorittaa tämän toiminnon');

//Error messages
define('CANDIDATE_WAS_NOT_SELECTED_TEXT', 'Ehdokasta ei valittu.');
define('CANDIDATE_WAS_NOT_FOUND_TEXT', 'Ehdokasta ei löytynyt.');
define('VOTING_FAILED_TEXT', 'Äänestäminen epäonnistui.');
define('NO_USERNAME_OR_PASSWORD_TEXT', 'Käyttäjätunnus- tai salasanakenttä on tyhjä.');
define('WRONG_USERNAME_OR_PASSWORD_TEXT', 'Virheellinen käyttäjätunnus tai salasana.');
define('INVALID_OR_EXPIRED_SESSION_TEXT', 'Istunto on vanhentunut. Ole hyvä ja yritä kirjautua sisään uudestaan.');
define("NO_ACCESS_PRIVILEGES_TEXT", "Sinulla ei ole tämän sivun saantiin tarvittavaa oikeutta.");
define('INVALID_INPUT_PLEASE_CHECK_FIELDS_TEXT', 'Syöttötieto ei kelpaa. Ole hyvä ja tarkista lomakkeen kentät.');
define('MANDATORY_TEXT', 'Pakollinen');
define('HAS_TO_BE_A_NUMBER_TEXT', 'On oltava numero');
define('INVALID_DATEVALUE_TEXT', 'Päivämäärä ei kelpaa');
define('INVALID_TIMEVALUE_TEXT', 'Kellonaika ei kelpaa');
define('END_DATE_INVALID', 'Vaalin päättymisaika ei voi olla pienempi kuin alkuaika');
define('INVALID_EMAIL_TEXT', 'Email-osoite ei kelpaa');
define('UPDATE_FAILED_TEXT', 'Tietojen päivitys epäonnistui.');
define('INSERT_FAILED_TEXT', 'Tietojen lisäys epäonnistui.');
define('REMOVE_FAILED_TEXT', 'Poistaminen epäonnistui.');
define('CANNOT_ADD_ELECTION_TEXT', 'Ei voida lisätä uutta vaalia, jos on vaaleja jotka ovat joko meneillään tai suljettuna');
define('CANNOT_MODIFY_ELECTION_TEXT', 'Vaalia ei voi muokata jos se on meneillään tai päättynyt');
define('STUDENT_NUMBER_ALREADY_TAKEN_TEXT', 'Opiskelijanumero on varattu.');
define('VOTING_COULD_NOT_BE_COMPLETED_BECAUSE_ELECTION_ENDED_TEXT', 'Äänestystoimintoa ei voitu suorittaa loppuun, koska vaalit päättyivät.');
define('ELECTION_WAS_NOT_FOUND_TEXT', 'Vaaleja ei löytynyt.');
define('VOTER_WAS_NOT_FOUND_TEXT', 'Äänioikeutettua ei löytynyt.');
define('VOTER_CANNOT_BE_REMOVED_TEXT', 'Äänioikeutettua ei voida poistaa.');
define('VOTER_CANNOT_BE_MODIFIED_TEXT', 'Äänioikeutettua ei voida muokata.');
define('VOTER_HAS_ALREADY_VOTED_TEXT', 'Äänioikeutettu on jo äänestänyt.');
define('CANDIDATE_INSERT_FAILED_TEXT','Ehdokkaan lisääminen epäonnistui');
define('ALLIANCE_INSERT_FAILED_TEXT','Vaaliliiton lisääminen epäonnistui');
define('EXISTS_ALREADY_TEXT','on jo lisätty');
define('UPLOAD_NO_DIR_TEXT','Tallennuskansiota ei ole olemssa');
define('UPLOAD_EMPTY_FILE_TEXT','Tiedosto on tyhjä');
define('UPLOAD_ERR_INI_SIZE_TEXT','Tiedosto ylittää php.ini-tiedostossa määritellyn tiedoston sallitun maksimikoon');
define('UPLOAD_ERR_FORM_SIZE_TEXT','Tiedosto ylittää HTML-lomakkeen määrittelemän tiedoston sallitun maksimikoon');
define('UPLOAD_ERR_PARTIAL_TEXT','Tiedosto tallennettiin vain osittain');
define('UPLOAD_ERR_NO_FILE_TEXT','Ei tiedostoa');
define('UPLOAD_ERR_NO_TMP_DIR_TEXT','Väliaikainen tallennuskansio puuttuu');
define('UPLOAD_ERR_CANT_WRITE_TEXT','Levylle kirjoitus epäonnistui');
define('UPLOAD_ERR_EXTENSION_TEXT','PHP:n laajennus keskeytti tiedoston tallennuksen');
define('UPLOAD_UNKNOWN_ERR_TEXT','Tuntematon virhe. Tiedostoa ei tallennettu');
define('UPLOAD_FILE_NOT_SUPPORTED_TEXT','Epäkelpo tiedostomuoto');
define('UPLOAD_ERR_TEXT','Tiedoston tallennuksessa tapahtui virhe');
define('FILE_INVALID_COLUMNS_AMOUNT_TEXT','Tiedoston sarakemäärä on epäkelpo');
define('YOU_DO_NOT_HAVE_RIGHT_TO_VOTE_TEXT','Teillä ei ole äänestysoikeutta.');
define('IMPORT_WAS_ABORTED_TEXT', 'Tuonti keskeytettiin.');
define('JAVASCRIPT_HAS_TO_BE_ENABLED_TEXT', 'Hallintapaneeli tarvitsee toimiakseen JavaScript-tuen');
define('ADMIN_PAGES_DO_NOT_SUPPORT_YOUR_BROWSER_TEXT', 'Hallintapaneeli ei tue selaintasi. Ongelmia voi ilmetä. Jos koet ongelmia, kokeile Google Chromea, Mozilla Firefoxia tai Safaria.');
define('CANDIDATE_NUMBER_ALREADY_TAKEN_TEXT', 'Ehdokasnumero on jo käytössä.');
define('ALLIANCE_ID_ALREADY_EXISTS_TEXT', 'Kyseinen tunnus on jo käytössä toisella vaaliliitolla');
define('CANDIDATE_CANNOT_BE_REMOVED_TEXT', 'Ehdokasta ei voida poistaa');
define('CANDIDATE_HAS_VOTES_TEXT', 'Ehdokasta on äänestetty');
define('USERNAME_ALREADY_TAKEN_TEXT', 'Käyttäjätunnus on varattu.');
define('PASSWORDS_DO_NOT_MATCH_TEXT', 'Salasanat eivät täsmää.');
define('PASSWORD_WAS_TOO_SHORT_TEXT', 'Salasana oli liian lyhyt.');
define('PASSWORD_MUST_HAVE_UPPERCASE_TEXT', 'Salasanassa tulee olla vähintään yksi iso kirjain.');
define('PASSWORD_MUST_HAVE_LOWERCASE_TEXT', 'Salasanassa tulee olla vähintään yksi pieni kirjain.');
define('PASSWORD_MUST_HAVE_NUMBER_TEXT', 'Salasanassa tulee olla vähintään yksi numero.');
define('PASSWORD_MUST_HAVE_THESE_TEXT', 'Salasanan tulee olla vähintään 8 merkkiä pitkä ja siinä tulee olla vähintään yksi iso kirjain, yksi pieni kirjain sekä yksi numero.');
define('OWN_ACCOUNT_CANNOT_BE_REMOVED_TEXT', 'Omaa käyttäjätiliä ei voida poistaa.');
define('ACCOUNT_CANNOT_BE_REMOVED_BECAUSE_OF_MARKED_VOTES_TEXT', 'Tätä käyttäjätiliä ei voida poistaa, koska tämä henkilö on merkinnyt äänestyksiä.');
define('ADMIN_WAS_NOT_FOUND_TEXT', 'Hallinnoitsijaa ei löytynyt.');
define('ONLY_ONE_OPEN_ELECTION_ALLOWED_TEXT', 'Vain yksi vaali voi olla kerrallaan avoinna');
define('GENERAL_ERR_TEXT', 'Tapahtui virhe. Yritä myöhemmin uudestaan');
define('MUST_BE_AT_LEAST_ONE_ADMIN_TEXT','Järjestelmässä täytyy olla vähintään yksi hallinnoitsija.');
define('VOTE_COUNT_DID_NOT_MATCH_ERR_TEXT','Ehdokkaiden saama äänimäärä poikkeaa annettujen äänten määrästä.');
define('VOTES_COULD_NOT_BE_COUNTED_ERR_TEXT','Ääniä ei voitu laskea.');
define('PAPER_VOTES_WERE_NOT_ADDED_TEXT','Uurnavaalin ääniä ei lisätty');
define('PAPER_VOTES_IMPORT_NOT_ALLOWED_TEXT','Tätä toimintoa ei voida suorittaa. Ehdokkaiden äänet on jo tuotu järjestelmään, ja ne täsmäävät uurnavaalin äänien kanssa.');
define('ALLIANCE_WITH_GIVEN_ID_EXISTS_TEXT','Vaaliliitto annetulla tunnuksella on jo olemassa');
define('COALITION_WITH_GIVEN_ID_EXISTS_TEXT','Vaalirengas annetulla tunnuksella on jo olemassa');
define('MANDATORY_FIELDS_EMPTY_TEXT','Pakolliset kentät pitää täyttää');
define('COOKIES_NEED_TO_BE_ENABLED_TEXT', 'Evästeiden tulee olla käytössä');
define('COULD_NOT_MOVE_ELECTION_TO_HISTORY_TEXT', "Vaalia ei voitu siirtää historiaan. Joko ääniä ei ole laskettu tai vaalia ei ole päätetty");

//Instructions
define('HOW_TO_VOTE_TEXT', 'Näin äänestät:');
define('INSTRUCTIONS_001_TEXT', '1. Tarvitset äänestämiseen Pajun tai Koivun tunnuksen');
define('INSTRUCTIONS_002_TEXT', '2. Kirjaudu omilla tunnuksillasi');
define('INSTRUCTIONS_003_TEXT', '3. Valitse ehdokas listalta, voit myös äänestää tyhjää');
define('INSTRUCTIONS_004_TEXT', '4. Vahvista valintasi');
define('INSTRUCTIONS_005_TEXT', '5. Saat vahvistuksen onnistuneesta äänestämisestä, sinut kirjataan automaattisesti ulos 15 sekunnin kuluttua');
define('INSTRUCTIONS_006_TEXT', '6. Palaat etusivulle, kiitos äänestäsi!');

//Miscellaneous
define('ROW_TEXT', 'Rivi');
define('NUMBER_OF_ROWS_TEXT', 'Rivien määrä');
define('OR_TEXT', 'tai');
define('YES_TEXT', 'Kyllä');
define('NO_TEXT', 'Ei');
define('CSV_DELIMITER_TEXT', 'Erotin');
define('CSV_TAB_DELIMITER_TEXT', 'Tabi -merkki');
define('CSV_COMMA_DELIMITER_TEXT', 'Pilkku (,)');
define('CSV_PIPE_DELIMITER_TEXT', 'Viiva (|)');
define('CSV_SEMICOLON_DELIMITER_TEXT', 'Puolipiste (;)');
define('UPLOADED_TEXT', 'Tuotu');
define('BYTES_TEXT', 'tavua');
