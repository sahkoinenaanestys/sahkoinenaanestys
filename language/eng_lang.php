<?php
/**
 * This file contains English translations
 * 
 * The naming convention for translation constants is:
 * all caps and suffix '_TEXT' (e.g. 'ELECTRONIC_VOTING_TEXT')
 *
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
define('STUDENT_UNION_OF_THE_UNI_OF_OULU_TEXT', 'The Student Union of the University of Oulu');
define('ELECTRONIC_VOTING_TEXT', 'Online voting');
define('ADMINISTRATION_TEXT', 'Administration');
define('CANDIDATES_TEXT', 'Candidates');
define('INSTRUCTIONS_TEXT','Instructions');
define('STUDENT_UNION_WEB_PAGE_TEXT','OYY.fi');
define('FRONTPAGE_TEXT','Frontpage');
define('LOG_IN_TEXT', 'Log in');
define('LOG_OUT_TEXT', 'Log out');
define('GO_BACK_TEXT', 'Go back');

//Frontpage
define('FRONTPAGE_TITLE_TEXT', 'Welcome to the e-voting system of the Student Union of the University of Oulu!');
define('FRONTPAGE_LOG_IN_INSTRUCTIONS_TEXT', 'You will need a Paju or a Koivu account in order to vote.');

//Login infromation
define('NAME_TEXT', 'Name');
define('STUDENT_NUMBER_TEXT', 'Student number');
define('ADDRESS_TEXT', 'Address');

//Navigation
define('NEXT_TEXT', 'Next');
define('PREVIOUS_TEXT', 'Previous');

//Voting page
define('LOGGED_IN_SUCCESSFULLY_TEXT', 'You have successfully logged in!');
define('LOGGED_OUT_SUCCESSFULLY_TEXT', 'You have successfully logged out!');
define('VOTE_TEXT', 'Vote');
define('PLEASE_CONFIRM_YOUR_CANDIDATE_CHOICE_TEXT', 'Please confirm your candidate choice');
define('YOU_ARE_ABOUT_TO_VOTE_EMPTY_TEXT', 'You are about to give a scratch vote');
define('YOU_ARE_ABOUT_TO_VOTE_FOR_THE_FOLLOWING_CANDIDATE_TEXT', 'You are about to vote for the following candidate');
define('NUMBER_ABBREVIATION_TEXT', 'No.');
define('YOUR_VOTING_WAS_SUCCESSFUL_TEXT', 'Your voting was successful!');
define('YOU_VOTED_FOR_CANDIDATE_TEXT','You voted for candidate');
define('YOU_GAVE_A_SCRATCH_VOTE_TEXT','You gave a scratch vote');
define('THANK_YOU_FOR_YOUR_VOTE_TEXT','Thank you for your vote!');
define('YOU_HAVE_ALREADY_VOTED_TEXT', 'You have already voted');
define('YOU_HAVE_ALREADY_VOTED_INSTRUCTIONS_TEXT', 'You have already voted and your vote cannot be changed afterwards.');
define('PLEASE_LOG_OUT_TEXT', 'Please log out.');
define('THERE_ARE_CURRENTLY_NO_ELECTIONS_UNDERWAY_TEXT', 'There are currently no elections underway');
define('YOU_WILL_BE_AUTOMATICALLY_LOGGED_OUT_TEXT', 'You will be automatically logged out.');
define('TIME_LEFT_TEXT', 'Time left');
define('SECONDS_SYMBOL_TEXT', 's');
define('SELECT_A_CANDIDATE_AND_PRESS_NEXT_TEXT','Select a candidate and press next.');
define('EMPTY_VOTE_TEXT', 'Scratch vote');

//Administration
define('ADMINISTRATION_FRONTPAGE_TEXT', 'This page meant for the trusted members of the Student Union of the University of Oulu for making changes to the online voting system.');
define('ADMINISTRATION_FRONTPAGE_INSTRUCTIONS_TEXT' , 'To log in, please enter your username and password.');
define('USERNAME_TEXT', 'Username');
define('PASSWORD_TEXT', 'Password');
define('ADMINISTRATORS_TEXT', 'Administrators');
define('VOTERS_TEXT', 'Voters');
define('ACCOUNT_TEXT', 'Account');
define('ADD_NEW_ADMIN_TEXT', 'Add new administrator');
define('MODIFY_ADMIN_TEXT', 'Modify administrator');
define('NEW_ADMIN_ADDED_SUCCESSFULLY_TEXT', 'New administrator has been added successfully!');
define('ADMIN_INFORMATION_UPDATED_SUCCESSFULLY_TEXT', 'Administrator information updated successfully!');
define('NO_ADMINISTRATORS_TEXT', 'No administrators');
define('CHANGE_PASSWORD_TEXT', 'Change the password');
define('CONFIRM_PASSWORD_TEXT', 'Confirm password');
define('LEVEL_TEXT', 'Level');
define('ADMINISTRATOR_TEXT', 'Administrator');
define('ELECTION_WORKER_TEXT', 'Election worker');
define('MARK_THE_VOTE_TEXT', 'Mark the vote');
define('MARK_THE_VOTE_CONFIRM_TEXT', 'Are you sure that you want to mark this vote?');
define('SUPPORTED_BROWSERS_TEXT', 'Supported browsers are Mozilla Firefox (Recommended), Google Chrome and Apple Safari');

define('ELECTION_RESULTS_TEXT', 'Election results');
define('ELECTION_TEXT', 'Election');
define('ELECTIONS_TEXT', 'Elections');
define('CURRENT_ELECTIONS_TEXT', 'Current elections');
define('PREVIOUS_ELECTIONS_TEXT', 'Previous elections');
define('ACTIONS_TEXT', 'Actions');
define('NO_ACTIVE_ELECTIONS_TEXT', 'No active elections');
define('NO_PREVIOUS_ELECTIONS_TEXT', 'No previous elections');
define('ELECTION_DETAILS_TEXT', 'Details');
define('ELECTION_NAME_TEXT', 'Name');
define('ELECTION_START_DATE_TEXT', 'Start date');
define('ELECTION_START_TIME_TEXT', 'Start time');
define('ELECTION_END_DATE_TEXT', 'End date');
define('ELECTION_END_TIME_TEXT', 'End time');
define('ELECTION_CALC_METHOD_TEXT', 'Election results calculation method');
define('ELECTION_COUNT_COALITIONS_TEXT', 'Count coalition votes');
define('ELECTION_DO_NOT_COUNT_COALITIONS_TEXT', 'Do not count coaliton votes');
define('ELECTION_STATUS_TEXT', 'Status');
define('ELECTION_STATUS_ACTIVE_TEXT', 'Active');
define('ELECTION_STATUS_CLOSED_TEXT', 'Closed');
define('ELECTION_STATUS_ENDED_TEXT', 'Ended');
define('ELECTION_HAS_NOT_STARTED_YET_TEXT', 'Has not started yet');
define('OPEN_OR_CLOSE_ELECTION_TEXT', 'Stop/open election');
define('TIME_AND_DATE_TEXT', 'Time and date');
define('VOTER_TURNOUT_TEXT', 'Voter turnout');
define('VOTES_TEXT', 'Votes');
define('WWW_VOTES_TEXT', 'Online votes');
define('PAPER_VOTES_TEXT', 'Paper votes');
define('EMPTY_VOTES_TEXT', 'Empty votes');
define('ELECTION_VOTERS_TEXT', 'Voters in this election');
define('ELECTION_NO_VOTERS_TEXT', 'There are no voters added to this election yet.');
define('SHOW_ELECTION_VOTERS_TEXT', 'Show voters');
define('ELECTION_CANDIDATES_TEXT', 'Candidated in this election');
define('ELECTION_NO_CANDIDATES_TEXT', 'There are no candidates added to this election yet.');
define('SHOW_ELECTION_CANDIDATES_TEXT', 'Show candidates');
define('IMPORT_PAPER_VOTES_TEXT', 'Import paper votes');
define('NEW_ELECTION_ADDED_SUCCESSFULLY_TEXT', 'New election added successfully!');
define('ELECTION_INFORMATION_UPDATED_SUCCESSFULLY_TEXT', 'Election information updated successfully!');
define('ELECTION_REMOVED_TEXT', 'Election was removed');
define('ELECTION_ENDED_TEXT', 'Election was ended');
define('ELECTION_MOVED_TO_HISTORY_TEXT', 'Election was moved to past elections');
define('PAPER_VOTES_ADDED_SUCCESSFULLY_TEXT', 'Paper votes were added successfully!');

define('NEW_ELECTION_TEXT', 'New election');
define('EDIT_ELECTION_TEXT', 'Edit election');
define('MOVE_ELECTION_TO_HISTORY_TEXT', 'Move election to history');
define('END_ELECTION_TEXT', 'End election');
define('EXPORT_ELECTION_RESULTS_TEXT', 'Election results');
define('EXPORT_RESULTS_CSV_TEXT', 'In this page you can export a CSV file containing election results and votes each candidate got.');
define('PAPER_VOTES_WARNING_TEXT', 'If paper votes have not been added, the system cannot calculate the results correctly.');
define('DOWNLOAD_RESULTS_CSV_TEXT', 'You can download results as a CSV file by clicking the button below.');
define('EXPORT_RESULTS_CSV_INFO_TEXT', 'It may take some time for the system to calculate the results and create CSV file.');
define('CSV_FILE_TEXT', 'CSV-file');
define('MANUAL_RESULTS_CALC_TEXT','You can also download votes received by candidates and votes given by voters as separate CSV files.');
define('IMPORT_PAPER_VOTES_CSV_TEXT', 'This page is for importing paper votes to the system.');
define('IMPORT_PAPER_VOTES_CSV_EXPLANATION_TEXT', 'Empty/rejected votes must be marked with candidate number -1 (minus one) to the imported CSV file.');
define('IMPORT_CANDIDATE_VOTES_TEXT', 'Import CSV file containing votes received by candidates');
define('IMPORT_VOTES_GIVEN_BY_VOTERS_TEXT', 'Import CSV file containing votes given by voters');
define('EXPORT_CSV_LIST_TEXT', 'Export as CSV file');
define('DOWNLOAD_RESULTS_CSV_BTN_TEXT', 'Download results file');
define('DOWNLOAD_SEPARATE_VOTES_FILES_TEXT','Download given votes in separate files for manual results calculation');
define('DOWNLOAD_CANDIDATE_VOTES_TEXT','Download candidate votes file');
define('DOWNLOAD_VOTER_VOTES_TEXT','Download given votes file');

define('EDIT_CANDIDATE_TEXT', 'Edit candidate');
define('NO_CANDIDATES_TEXT', 'No candidates');
define('CANDIDATE_NUM_TEXT', 'Candidate number');
define('CANDIDATE_TEXT', 'Candidate');
define('ALLIANCE_TEXT', 'Alliance');
define('COALITION_TEXT', 'Coalition');
define('COALITION_NAME_TEXT', 'Coalition name');
define('ALLIANCE_NAME_TEXT', 'Alliance name');
define('RANK_TEXT', 'Rank');
define('COMPARISION_NUM_TEXT', 'Comparision number');
define('ALLIANCE_COMPARISION_NUM_TEXT', 'Alliance comparision number');
define('COALITION_COMPARISION_NUM_TEXT', 'Coalition comparision number');
define('NOT_IN_COALITION_TEXT', '(not in coalition)');
define('CANDIDATE_ADDED_SUCCESSFULLY_TEXT', 'New candidate has been added successfully!');
define('CANDIDATE_VOTES_TEXT', 'Candidate votes');
define('CANDIDATE_UPDATED_SUCCESSFULLY_TEXT', 'Candidate information updated successfully!');
define('IMPORT_CANDIDATES_TEXT', 'Import candidates from CSV-file');
define('IMPORT_ALLIANCES_TEXT', 'Import alliances and coalitions from CSV-file');
define('IMPORT_CSV_TEXT', 'It may take awile for the system to parse the CSV-file and save the data.');
define('CHOOSE_A_FILE_TEXT', 'Choose a file to upload');
define('PLEASE_WAIT_TEXT', 'Please be patient and wait until the process has completed. Do not close this page meanwhile.');
define('SAVING_INFORMATION_TEXT', 'Saving data');
define('CANDIDATES_IMPORTED_SUCCESSFULLY_TEXT', 'Candidates were imported successfully!');
define('ALLIANCES_IMPORTED_SUCCESSFULLY_TEXT', 'Alliances were imported successfully!');
define('REMOVE_ALL_CANDIDATES_TEXT', 'Remove all candidates');
define('REMOVE_ALL_CANDIDATES_WARNING_TEXT', 'This action will remove ALL candidates from this election.');
define('CANDIDATES_CANNOT_BE_REMOVED_TEXT', 'Candidates cannot be removed.');
define('MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT', 'This election has to be closed before this action can be executed.');
define('ELECTION_HAS_TO_BE_ENDED_TEXT', 'This election has to be ended before this action can be executed.');
define('UPLOAD_NO_ERR_TEXT', 'File was uploaded successfully');
define('BRING_CANDIDATES_FROM_TEXT', 'Bring candidates from');
define('ADD_CANDIDATES_MANUALLY_TEXT', 'Add candidates manually');
define('ADD_NEW_ALLIANCE_TEXT', 'Add alliance'); 

define('NO_VOTERS_TEXT', 'No voters');
define('NEW_VOTER_TEXT', 'New voter');
define('ADD_NEW_VOTER_TEXT', 'Add new voter');
define('MODIFY_VOTER_TEXT', 'Modify voter');
define('REMOVE_ALL_VOTERS_TEXT', 'Remove all voters');
define('REMOVE_ALL_VOTERS_WARNING_TEXT', 'This action will remove ALL voters from this election.');
define('FIRST_NAME_TEXT', 'First name');
define('LAST_NAME_TEXT', 'Last name');
define('EMAIL_TEXT', 'Email');
define('CITY_TEXT', 'City');
define('POSTAL_CODE_TEXT', 'Postal code');
define('RIGHT_TO_VOTE_TEXT', 'Right to vote');
define('VOTED_TEXT', 'Voted');
define('PAPER_VOTE_SHORT_TEXT', 'Paper');
define('ELECTRONIC_VOTE_SHORT_TEXT', 'Electronic');
define('VOTE_METHOD_TEXT', 'Vote method');
define('VOTE_DATE_TEXT', 'Vote date');
define('NEW_VOTER_ADDED_SUCCESSFULLY_TEXT', 'New voter has been added successfully!');
define('VOTER_INFORMATION_UPDATED_SUCCESSFULLY_TEXT', 'User information updated successfully!');
define('IMPORT_VOTERS_TEXT', 'Import voters from CSV-file');
define('VOTERS_IMPORTED_SUCCESSFULLY_TEXT', 'Voters were imported successfully!');
define('VOTERS_CANNOT_BE_REMOVED_EXPLANATION_TEXT', 'Voters cannot be removed. This election has to be closed before this action can be executed.');
define('VOTERS_CANNOT_BE_REMOVED_VOTES_GIVEN_TEXT', 'Voters cannot be removed. Some of the voters have already voted.');
define('BRING_VOTERS_FROM_TEXT', 'Bring voters from');
define('ADD_VOTERS_MANUALLY_TEXT', 'Add voters manually');
define('HAS_NO_VOTING_RIGHT_TEXT', 'No voting right');
define('HAS_VOTING_RIGHT_TEXT', 'Has right to vote');

define('ALLIANCES_AND_COALITIONS_TEXT', 'Alliances and coalitions');
define('NO_ALLIANCES_OR_COALITIONS_TEXT', 'No alliances nor coalitions');
define('ALLIANCES_TEXT', 'Alliances');
define('COALITIONS_TEXT', 'Coalitions');
define('ID_TEXT', 'Identifier');
define('EDIT_ALLIANCE_TEXT', 'Edit alliance');
define('EDIT_COALITION_TEXT', 'Edit coalition');
define('NO_ALLIANCES_TEXT', 'No alliances');
define('NO_COALITIONS_TEXT', 'No coalitions');
define('ALLIANCE_ID_TEXT', 'Alliance id');
define('COALITION_ID_TEXT', 'Coalition id');
define('EMAIL_OF_PERSON_IN_CHARGE_TEXT', 'Email of the person in charge');
define('COALITION_ADDED_SUCCESSFULLY_TEXT', 'New coalition has been added successfully!');
define('COALITION_UPDATED_SUCCESSFULLY_TEXT', 'Coalition was updated successfully!');
define('ALLIANCE_ADDED_SUCCESSFULLY_TEXT', 'New alliance has been added successfully!');
define('ALLIANCE_UPDATED_SUCCESSFULLY_TEXT', 'Alliance was updated successfully!');
define('REMOVED_SUCCESSFULLY_TEXT', 'Removed successfully!');

//Actions
define('NEW_TEXT', 'New');
define('EDIT_TEXT', 'Edit');
define('ADD_TEXT', 'Add');
define('REMOVE_TEXT', 'Remove');
define('REMOVE_ALL_TEXT', 'Remove all');
define('SHOW_HISTORY_TEXT', 'Show history');
define('IMPORT_FROM_FILE_TEXT', 'Import from file');
define('SAVE_TEXT', 'Save');
define('CANCEL_TEXT', 'Cancel');
define('UPLOAD_TEXT', 'Upload');
define('DOWNLOAD_TEXT', 'Download file');
define('FIND_TEXT', 'Find');

//General messages
define('YOU_HAVE_BEEN_LOGGED_OUT_TEXT', 'You have been logged out.');
define('NO_OPTION_SELECTED_TEXT', '(no option selected)');
define('CLICK_TO_EXPAND_COLLAPSE_TEXT', 'Click to expand/collapse');
define('ACTION_CONFIRM_TEXT', 'Are you sure you want to execute this action');

//Error messages
define('CANDIDATE_WAS_NOT_SELECTED_TEXT', 'Candidate was not selected.');
define('CANDIDATE_WAS_NOT_FOUND_TEXT', 'Candidate was not found.');
define('VOTING_FAILED_TEXT', 'Voting failed.');
define('NO_USERNAME_OR_PASSWORD_TEXT', 'No username or password entered.');
define('WRONG_USERNAME_OR_PASSWORD_TEXT', 'Wrong username or password.');
define('INVALID_OR_EXPIRED_SESSION_TEXT', 'Your session has expired. Please try logging in again.');
define("NO_ACCESS_PRIVILEGES_TEXT", "You don't have sufficient privileges to access this.");
define('INVALID_INPUT_PLEASE_CHECK_FIELDS_TEXT', 'Invalid input. Please check the form fields.');
define('MANDATORY_TEXT', 'Mandatory');
define('HAS_TO_BE_A_NUMBER_TEXT', 'Has to be a number');
define('INVALID_DATEVALUE_TEXT', 'Invalid date value');
define('INVALID_TIMEVALUE_TEXT', 'Invalid time value');
define('END_DATE_INVALID', 'Election end date cannot be smaller than start date');
define('INVALID_EMAIL_TEXT', 'Invalid email address');
define('UPDATE_FAILED_TEXT', 'Update failed.');
define('INSERT_FAILED_TEXT', 'Insert failed.');
define('REMOVE_FAILED_TEXT', 'Remove failed.');
define('CANNOT_ADD_ELECTION_TEXT', 'Cannot add new election because there are other active or closed elections');
define('CANNOT_MODIFY_ELECTION_TEXT', 'Cannot modify active or ended elections');
define('STUDENT_NUMBER_ALREADY_TAKEN_TEXT', 'The student number is already taken.');
define('VOTING_COULD_NOT_BE_COMPLETED_BECAUSE_ELECTION_ENDED_TEXT', 'Voting operation could not be completed because the election ended.');
define('ELECTION_WAS_NOT_FOUND_TEXT', 'Election was not found.');
define('VOTER_WAS_NOT_FOUND_TEXT', 'Voter was not found.');
define('VOTER_CANNOT_BE_REMOVED_TEXT', 'Voter cannot be removed.');
define('VOTER_CANNOT_BE_MODIFIED_TEXT', 'Voter cannot be edited.');
define('VOTER_HAS_ALREADY_VOTED_TEXT', 'Voter has already voted.');
define('CANDIDATE_INSERT_FAILED_TEXT','Failed to add candidate');
define('ALLIANCE_INSERT_FAILED_TEXT','Failed to add alliance');
define('EXISTS_ALREADY_TEXT','exists already');
define('UPLOAD_NO_DIR_TEXT','Upload directory does not exist');
define('UPLOAD_EMPTY_FILE_TEXT','File is empty');
define('UPLOAD_ERR_INI_SIZE_TEXT','The uploaded file exceeds the upload_max_filesize directive in php.ini');
define('UPLOAD_ERR_FORM_SIZE_TEXT','The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form');
define('UPLOAD_ERR_PARTIAL_TEXT','The uploaded file was only partially uploaded');
define('UPLOAD_ERR_NO_FILE_TEXT','No file was uploaded');
define('UPLOAD_ERR_NO_TMP_DIR_TEXT','Missing a temporary folder');
define('UPLOAD_ERR_CANT_WRITE_TEXT','Failed to write file to disk');
define('UPLOAD_ERR_EXTENSION_TEXT','File upload stopped by extension');
define('UPLOAD_UNKNOWN_ERR_TEXT','Unknown upload error');
define('UPLOAD_FILE_NOT_SUPPORTED_TEXT','Unsupported File');
define('UPLOAD_ERR_TEXT','Error uploading File');
define('FILE_INVALID_COLUMNS_AMOUNT_TEXT','File has invalid number of columns');
define('YOU_DO_NOT_HAVE_RIGHT_TO_VOTE_TEXT','You don\'t have the right to vote.');
define('IMPORT_WAS_ABORTED_TEXT', 'Import was aborted.');
define('JAVASCRIPT_HAS_TO_BE_ENABLED_TEXT', 'Adminstration pages need JavaScript to be enabled');
define('ADMIN_PAGES_DO_NOT_SUPPORT_YOUR_BROWSER_TEXT', 'Administration pages do not support your browser. You may experience problems. If you are having problems, try Google Chrome, Mozilla Firefox or Safari.');
define('CANDIDATE_NUMBER_ALREADY_TAKEN_TEXT', 'Candidate number is already taken.');
define('ALLIANCE_ID_ALREADY_EXISTS_TEXT', 'Alliance with given id already exists');
define('CANDIDATE_CANNOT_BE_REMOVED_TEXT', 'Candidate cannot be removed');
define('CANDIDATE_HAS_VOTES_TEXT', 'Candidate has votes');
define('USERNAME_ALREADY_TAKEN_TEXT', 'Username is already taken.');
define('PASSWORDS_DO_NOT_MATCH_TEXT', 'Passwords do not match.');
define('PASSWORD_WAS_TOO_SHORT_TEXT', 'Password was too short.');
define('PASSWORD_MUST_HAVE_UPPERCASE_TEXT', 'Password must have at least one upper case character.');
define('PASSWORD_MUST_HAVE_LOWERCASE_TEXT', 'Password must have at least one lower case character.');
define('PASSWORD_MUST_HAVE_NUMBER_TEXT', 'Password must have at least one number.');
define('PASSWORD_MUST_HAVE_THESE_TEXT', 'Password must be at least 8 characters long and it must contain at least one upper case character, one lower case character and a number.');
define('OWN_ACCOUNT_CANNOT_BE_REMOVED_TEXT', 'Own account cannot be removed');
define('ACCOUNT_CANNOT_BE_REMOVED_BECAUSE_OF_MARKED_VOTES_TEXT', 'This account cannot be removed because this person has marked votes.');
define('ADMIN_WAS_NOT_FOUND_TEXT', 'Administrator was not found.');
define('ONLY_ONE_OPEN_ELECTION_ALLOWED_TEXT', 'Only one open election is allowed');
define('GENERAL_ERR_TEXT', 'Error happened. Try again later');
define('MUST_BE_AT_LEAST_ONE_ADMIN_TEXT','There has to be at least one administrator in the system.');
define('VOTE_COUNT_DID_NOT_MATCH_ERR_TEXT','Vote count does not match. Votes received by candidates differs from votes given.');
define('VOTES_COULD_NOT_BE_COUNTED_ERR_TEXT','Votes could not be counted.');
define('PAPER_VOTES_WERE_NOT_ADDED_TEXT','Paper votes were not added');
define('PAPER_VOTES_IMPORT_NOT_ALLOWED_TEXT','This action is not allowed. Candidate votes have already been imported, and vote count matches with given paper votes.');
define('ALLIANCE_WITH_GIVEN_ID_EXISTS_TEXT','Alliance with given id already exists');
define('COALITION_WITH_GIVEN_ID_EXISTS_TEXT','Coalition with given id already exists');
define('MANDATORY_FIELDS_EMPTY_TEXT','Mandatory fields are empty');
define('COOKIES_NEED_TO_BE_ENABLED_TEXT', 'Cookies need to be enabled');
define('COULD_NOT_MOVE_ELECTION_TO_HISTORY_TEXT', "Election couldn't be moved to history. Either votes have not been calculated or election has not been ended");

//Instructions
define('HOW_TO_VOTE_TEXT', 'How to vote:');
define('INSTRUCTIONS_001_TEXT', '1. You need a Paju or Koivu account in order to vote');
define('INSTRUCTIONS_002_TEXT', '2. Log in with your account');
define('INSTRUCTIONS_003_TEXT', '3. Choose a candidate from the list, you can also give a scratch vote');
define('INSTRUCTIONS_004_TEXT', '4. Confirm your vote');
define('INSTRUCTIONS_005_TEXT', '5. You will get a confirmation for succesful vote, you will be logged out automatically in 15 seconds');
define('INSTRUCTIONS_006_TEXT', '6. You will be redirected to the front page, thank you for your vote!');

//Miscellaneous
define('ROW_TEXT', 'Row');
define('NUMBER_OF_ROWS_TEXT', 'Number of rows');
define('OR_TEXT', 'or');
define('YES_TEXT', 'Yes');
define('NO_TEXT', 'No');
define('CSV_DELIMITER_TEXT', 'Delimeter');
define('CSV_TAB_DELIMITER_TEXT', 'Tab character');
define('CSV_COMMA_DELIMITER_TEXT', 'Comma (,)');
define('CSV_PIPE_DELIMITER_TEXT', 'Pipe (|)');
define('CSV_SEMICOLON_DELIMITER_TEXT', 'Semicolon (;)');
define('UPLOADED_TEXT', 'Uploaded');
define('BYTES_TEXT', 'bytes');
