<?php
/**
 * Model for coalition database table. 
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Coalition_Model extends Model
{		
	protected $idColumn = 'coalitionId';
	protected $compositeId = array('coalitionId', 'election');
	protected $tableName = 'coalition';
	
	public function searchCoalitionsByName($coalitionName, $electionId) 
	{
		if(strlen($coalitionName) > 0){
		    $sql = "SELECT coalitionName AS name FROM {$this->tableName} WHERE election='{$electionId}'\n". 
			    "AND coalitionName LIKE '".$coalitionName."%' ORDER BY coalitionName ASC LIMIT 0, 20";

		    $result = $this->fetchQueryResults($sql);
		    if(!$result)
			return false;
		    return $result;
		}else{
		    return false;
		}
	}
	
	/**
	 * This method handles updating and inserting coalition data.
	 * 
	 * @param array $coalitionArr
	 * @param int $electionId
	 * @return int
	 */
	public function updateCoalition($coalitionArr, $electionId)
	{	
		$coalitionName = $coalitionArr['coalitionName'];
		$coalitionId = $coalitionArr['coalitionId'];
		$oldCoalitionId = $coalitionArr['oldCoalitionId'];
	    
		$paramArr = array('election'=>$electionId, 'coalitionId'=>$coalitionId, 'coalitionName'=>$coalitionName);
		$sql = "election, coalitionId, coalitionName";
		
		$this->beginTransaction();
		if(empty($oldCoalitionId) || $oldCoalitionId < 1) {
		    
			//check if coalition with given id already exists
			if(is_array($this->fetchByCompositeId(array($coalitionId, $electionId)))) {
				$this->rollBack();
				return parent::PK_EXISTS_ALREADY;
			}
										
			//Insert a new record				
			if($this->insertInto($sql, $paramArr) === false) {
				$this->rollBack();
				return parent::FAILURE;
			}
		}else {
			//check if coalition with given id already exists
			if($oldCoalitionId != $coalitionId){
			    if(is_array($this->fetchByCompositeId(array($coalitionId, $electionId)))) {
				    $this->rollBack();
				    return parent::PK_EXISTS_ALREADY;
			    }
			}
		    
			//Update references to alliances
			$allianceModel = new Alliance_Model;
			$query = "SELECT allianceId FROM alliance \n".
			"WHERE coalition='".$oldCoalitionId."' AND election='".$electionId."'";

                        $alliances = $allianceModel->fetchQueryResults($query);
			if($alliances != false){
			    foreach($alliances as $alliance){
				$params = array($coalitionId, $alliance['allianceId'], $electionId);
				$updateClause = "coalition=? WHERE allianceId=? AND election=?";	

				if($allianceModel->update($updateClause, $params) == false) {
					$this->rollBack();
					return parent::FAILURE;
				}
			    }//for
			}
			//Update references to candidates
			$candidateModel = new Candidate_Model;
			$query2 = "SELECT candidateNum FROM candidate \n".
			"WHERE coalition='".$oldCoalitionId."' AND election='".$electionId."'";

                        $candidates = $candidateModel->fetchQueryResults($query2);
			if($candidates != false){
			    foreach($candidates as $candidate){
				$params2 = array($coalitionId, $candidate['candidateNum'], $electionId);
				$updateClause2 = "coalition=? WHERE candidateNum=? AND election=?";	

				if($candidateModel->update($updateClause2, $params2) == false) {
					$this->rollBack();
					return parent::FAILURE;
				}
			    }//for
			}
		    
			//Update existing record
			$params = array($coalitionId, $coalitionName, $oldCoalitionId, $electionId);
			$updateClause = "coalitionId=?, coalitionName=? WHERE coalitionId=? AND election=?";	

			if($this->update($updateClause, $params) == false) {
				$this->rollBack();
				return parent::FAILURE;
			}
		}
		$this->commit();
		return parent::SUCCESS;				
	}
	
	/**
	 * Used for deleting coalition
	 * 
	 * @param int $coalitionId
	 * @param int $electionId
	 * 
	 * @return array
	 */
	public function removeCoalition($coalitionId, $electionId)
	{
		$this->beginTransaction();

		//Update references to all coalition alliances
		$allianceModel = new Alliance_Model;
		$params = array(0, $coalitionId, $electionId);
		$updateClause = "coalition=? WHERE coalition=? AND election=?";	

		if($allianceModel->update($updateClause, $params) == false) {
			$allianceModel->rollBack();
			return false;
		}
		
		//Update references to all coalition candidates
		$candidateModel = new Candidate_Model;
		$params2 = array(0, $coalitionId, $electionId);
		$updateClause2 = "coalition=? WHERE coalition=? AND election=?";	

		if($candidateModel->update($updateClause2, $params2) == false) {
			$candidateModel->rollBack();
			return false;
		}
		
		if($this->removeByCompositeId(array($coalitionId, $electionId)) === false) {
			$this->rollBack();
			return false;
		}  
		$this->commit();
		return true;	
	}
	
	public function setCoalitionVotes($electionId)
	{
		if($electionId == 0 || !isset($electionId))
		    return false;
		
		$this->beginTransaction();
		$coalitions = $this->fetchByField('election', $electionId);
		
		if(!$coalitions) 
		    return true;
		
		//set coalition votes
		foreach($coalitions as $coalition){    
		    $coalitionId = $coalition['coalitionId'];
		    
		    $query = "SELECT (SELECT COALESCE(SUM(candidate.votes + candidate.paperVotes),0) FROM candidate \n".
			"JOIN coalition ON (candidate.coalition=coalition.coalitionId AND candidate.election = coalition.election) \n".
			"WHERE candidate.election='".$electionId."' AND candidate.coalition='".$coalitionId."' AND candidate.alliance=0) + \n".
			"(SELECT COALESCE(SUM(alliance.votes),0) FROM alliance \n".
			"JOIN coalition ON (alliance.coalition=coalition.coalitionId  AND alliance.election = coalition.election) \n".
			"WHERE alliance.election='".$electionId."' AND alliance.coalition='".$coalitionId."') \n".
			"AS totalVotes";
		    $sth = $this->db->dbh->query($query);

		    if($sth !== false) {
			$temp = $sth->fetch(PDO::FETCH_ASSOC);
			$sum = (int)$temp['totalVotes'];

			$query2 = "UPDATE coalition SET coalition.votes=? WHERE coalitionId=?";
			$stmt = $this->db->dbh->prepare($query2);
			if ($stmt) {
			   $result = $stmt->execute(array($sum,$coalitionId));
			   if($result == false){
			       $this->rollBack();
			       return false;
			   }   
			}
		    }else{
		       $this->rollBack(); 
		       return false; 
		    }
		}//for
		
		$this->commit();
		return true;
	}
	
	/**
	 * This method validates the form's values and returns the valid input fields and error messages in an array.
	 * 
	 * @param array $postArr
	 * @return array
	 */
	public function validateEditForm($postArr) 
	{
		$coalitionArr = array();
		$errors = array();
		$formFields = array('coalitionId','coalitionName');
		$mandatory = array('coalitionId');
		$numeric = array('coalitionId');
		
		//Loop through form input and check that every mandatory field was filled
		foreach ($formFields as $field) {
			if(in_array($field, $mandatory) && empty($postArr[$field])) {
				$errors[$field] = MANDATORY_TEXT;
			}
			else if(in_array($field, $numeric) && (!is_numeric($postArr[$field]) || $postArr[$field] < 1)) {
				$errors[$field] = HAS_TO_BE_A_NUMBER_TEXT;
			}
			 
			if(!isset($errors[$field])){
				$coalitionArr[$field] = $postArr[$field];
			}	
		}		
		return array($coalitionArr, $errors);
	}
}