<?php
/**
 * Model for admin database table.
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Admin_Model extends Model
{		
	protected $idColumn = 'adminId';
	protected $tableName = 'administrator';	
	public $lastInsertId = null;    
	
	//Admin levels
	const LEVEL_NO_RIGHTS = 0;
	const LEVEL_ELECTION_WORKER = 5;
	const LEVEL_ADMIN = 10;

	//Model specific result codes
	const ADMIN_LOGIN_SUCCESS = 3;
	const WORKER_LOGIN_SUCCESS = 4;
	const MUST_BE_AT_LEAST_ONE_ADMIN = 5;
        
	/**
	 * Checks that given username exists in the database and that the password matches the username.
	 * 
	 * @param string $username
	 * @param string $passwd
	 * @return int
	 */
	public function checkAdminLogin($username, $passwd) 
	{
		include_once SERVER_ROOT.'/lib/phpass/PasswordHash.php';
		$hasher = new PasswordHash(8, FALSE);
		
		//Fetch the password hash etc. from the database
		$adminArr = $this->fetchByField('username', $username);
				
		//Return false if this username doesn't exist
		if ($adminArr == false)
			return parent::FAILURE;
		
		//Check if the given password is valid
		if($hasher->CheckPassword($passwd, $adminArr[0]['password']) == true) {
			switch ($adminArr[0]['adminLevel']) {
				case self::LEVEL_ELECTION_WORKER:
					return self::WORKER_LOGIN_SUCCESS;
					break;
				case self::LEVEL_ADMIN:
				default:
					return self::ADMIN_LOGIN_SUCCESS;
					break;
			}	
		}
		return parent::FAILURE;
	}
	
	public function checkAdminLastSeen($adminId)
	{
		$sql = "SELECT * FROM {$this->tableName} WHERE adminId=".$adminId." AND lastSeen > DATE_SUB(NOW(), INTERVAL ".MAX_OFFLINE_TIME." MINUTE);"; 
		$res = $this->fetchQueryResults($sql, array('singleRow' => true));

		if ($res != false && $res > 0)
			return true;
		return false;
	}
	
	/**
	 * Generates sql where clause with parameters using given search string.
	 *
	 * @param string $searchStr
	 * @return array
	 */
	private function generateWhereWithParams($searchStr = null)
	{
		$params = array();
		$where = "";
		if($searchStr != null) {
			$searchWords = explode("+", $searchStr);
			foreach($searchWords as $index => $word) {
				//Do not react to empty search words
				if(trim($word) == "")
					continue;
				$word = urldecode($word);

				$where .= ($index == 0)? "\nWHERE ": "\nAND ";
				$where .= "((username LIKE :username{$index}) OR (firstName LIKE :firstName{$index}) OR (lastName LIKE :lastName{$index}) OR (email LIKE :email{$index}))";
				$params[':username'.$index] = '%'.$word.'%';
				$params[':firstName'.$index] = '%'.$word.'%';
				$params[':lastName'.$index] = '%'.$word.'%';
				$params[':email'.$index] = '%'.$word.'%';
			}
		}
		return array(0 => $where, 1 => $params);
	}
	
	public function countBySearch($searchStr = null)
	{			
		if ($this->db->dbh == null)
			return false;

		$sql = "SELECT count(*) FROM ".$this->tableName;
		list($whereClause, $paramArr) = $this->generateWhereWithParams($searchStr);	
		
		$stmt = $this->db->dbh->prepare($sql.$whereClause);
		$stmt->execute($paramArr);
		return $stmt->fetchColumn(0);
	}
	
	/**
	 * Fetches admin accounts from the database.
	 *
	 * WARNING: Paging is done differently in different database managament systems. This version only supports MySQL.
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param string $searchStr
	 * @return array|false
	 */
	public function fetchAdminAccounts($limit = null, $offset = null, $searchStr = null)
	{	
		if ($this->db->dbh == null)
			return false;
	
		//Generate the limit clause
		$limitClause = "";
		if($limit != null) {
			$limitClause = "\nLIMIT ". (int) $limit;
			if($offset != null) {
				$limitClause .= " OFFSET ".(int) $offset;
			}
		}

		list($whereClause, $paramArr) = $this->generateWhereWithParams($searchStr);
	
		$sql = "SELECT a.* FROM {$this->tableName} AS a {$whereClause} ORDER BY a.username ASC {$limitClause}";
		$stmt = $this->db->dbh->prepare($sql);
		$stmt->execute($paramArr);
		return $stmt->fetchAll();
	}
	
	/**
	 * This method tells if the administrator has marked any (paper)votes.
	 * 
	 * @param string $username
	 * @return boolean
	 */
	public function hasMarkedVotes($username)
	{
		//Check that the administrator exists
		$adminId = $this->getAdminIdByUsername($username);
		if(empty($adminId))
			return false;
				
		//Check if this administrator has marked any votes		
		$sql = "SELECT count(*) FROM voter WHERE electionWorker = ? LIMIT 1";
		$count = $this->fetchQueryResults($sql, array('fetchColumn' => 0), array($adminId));
		
		if($count > 0)
			return true;				
		return false;
	}
	
	/**
	 * This method handles updating and inserting administrator data.
	 *
	 * @param array $adminArr
	 * @return int
	 */
	public function updateAdmin($adminArr)
	{
		$adminId = (int) $adminArr['adminId'];

		//parameters
		$paramArr = array();
		
		//Add adminId to the parameters
		if(!empty($adminId)) {
			$paramArr[':adminId'] = $adminId;
		}
		
		//Add more fields to the query and to the parameters
		$validFields = array('firstName', 'lastName', 'email', 'address', 'city', 'zipCode', 'username', 'adminLevel');
		foreach ($adminArr as $key => $val) {
			if(in_array($key, $validFields)) {
				$sql = (!isset($sql))? $key: $sql.", ".$key;
				$paramArr[':'.$key] = $val;
			}
		}

		//Add password hash to the query and to the parameters
		if(isset($adminArr['password'])) {
			include_once SERVER_ROOT.'/lib/phpass/PasswordHash.php';
			$hasher = new PasswordHash(8, FALSE);			
			$sql .= ", password";
			$paramArr[':password'] = $hasher->HashPassword($adminArr['password']);
		}
		
		//Begin the transaction
		$this->beginTransaction();		
		
		if(empty($adminId)) {
			//Insert a new administrator
			//Check if the new username is already taken
			if(is_array($this->fetchByField('username', $adminArr['username']))) {
				$this->rollBack();
				return parent::PK_EXISTS_ALREADY;
			}

			if($this->insertInto($sql, $paramArr) === false) {
				$this->rollBack();
				return parent::FAILURE;
			}
		}
		else {						
			//Check if the username has changed			
			$oldRecordArr = $this->fetchById($adminId);			
			if (!$oldRecordArr) {
				$this->rollBack();
				return parent::FAILURE;
			}			
			if ($oldRecordArr['username'] != $adminArr['username']) {
				//Check if the new username is already taken
				if(is_array($this->fetchByField('username', $adminArr['username']))) {
					$this->rollBack();
					return parent::PK_EXISTS_ALREADY;
				}	
			}
			
			//Check that at least one administrator exists after this update
			if($paramArr[':adminLevel'] != self::LEVEL_ADMIN) {
				$stmt = $this->db->dbh->prepare("SELECT count(*) FROM {$this->tableName} WHERE adminLevel = :adminLevel AND username <> :username1 AND username <> :username2");
				$stmt->execute(array(':adminLevel' => self::LEVEL_ADMIN, ':username1' => $adminArr['username'], ':username2' => $oldRecordArr['username']));
				if($stmt->fetchColumn() < 1) {
					$this->rollBack();
					return self::MUST_BE_AT_LEAST_ONE_ADMIN;
				}
			}
						
			//Update existing record
			$sql .= "\nWHERE adminId = :adminId";
			if($this->updateSet($sql, $paramArr) == false) {
				$this->rollBack();
				return parent::FAILURE;
			}
		}			
		//Commit the changes if everything went fine
		$this->lastInsertId = $this->db->dbh->lastInsertId();
		$this->commit();		
		return parent::SUCCESS;
	}
	
	public function getAdminIdByUsername($username){
	    $admin = $this->getFirstByField("username", $username);
	    if($admin != false)
		return $admin['adminId'];
	    return false;
	}
	
	/**
	 * This method validates the edit form's $_POST parameters and returns the valid input fields and error messages in an array.
	 *
	 * @param array $postArr
	 * @return array
	 */
	public function validateEditForm($postArr) 
	{
		$adminArr = array();
		$errors = array();
		$formFields = array('firstName', 'lastName', 'username', 'email', 'address', 'city', 'zipCode', 'adminId', 'adminLevel');
		$mandatory = array('firstName', 'lastName', 'username', 'email', 'adminLevel');
		$numeric = array('zipCode');
		
		//Loop through form input and check that every mandatory field was filled
		foreach ($formFields as $field) {
			if(in_array($field, $mandatory) && empty($postArr[$field])) {
				$errors[$field] = MANDATORY_TEXT;
			}
			else if(in_array($field, $numeric) && !is_numeric($postArr[$field]) && !empty($postArr[$field])) {
				$errors[$field] = HAS_TO_BE_A_NUMBER_TEXT;
			}
					
			if(!isset($errors[$field]))
				$adminArr[$field] = $postArr[$field];
		}
		
		//Validate email address
		if(!empty($adminArr['email']) && !filter_var($adminArr['email'], FILTER_VALIDATE_EMAIL)) {
			$errors['email'] = INVALID_EMAIL_TEXT;
		}
		
		//Validate password
		if(isset($postArr['setPassword']) && $postArr['setPassword'] == "true") {
			if($postArr['password'] != $postArr['passwordConfirm']) //Passwords must match
				$errors['password'] = PASSWORDS_DO_NOT_MATCH_TEXT;
			else if(strlen($postArr['password']) < ADMIN_PASSWD_MIN_LENGTH) //At least xx characters
				$errors['password'] = PASSWORD_WAS_TOO_SHORT_TEXT;
			else if(!preg_match('/[A-ZÅÄÖ]/', $postArr['password'])) //At least one upper case
				$errors['password'] = PASSWORD_MUST_HAVE_UPPERCASE_TEXT;
			else if(!preg_match('/[a-zåäö]/', $postArr['password'])) //At least one lower case
				$errors['password'] = PASSWORD_MUST_HAVE_LOWERCASE_TEXT;
			else if(!preg_match('/[0-9]/', $postArr['password'])) //At least one number
				$errors['password'] = PASSWORD_MUST_HAVE_NUMBER_TEXT;
			
			if(!isset($errors['password']))
				$adminArr['password'] = $postArr['password'];
		}
		
		return array($adminArr, $errors);
	}
		
	
}