<?php
/**
 * Model for alliance database table.  
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Alliance_Model extends Model
{		
	protected $idColumn = 'allianceId';
	protected $compositeId = array('allianceId', 'election');
	protected $tableName = 'alliance';
	
	/*public function searchAlliancesByName($allianceName, $electionId) 
	{
		if(strlen($allianceName) > 0){
		    $sql = "SELECT allianceName AS name, coalition FROM {$this->tableName} WHERE election='{$electionId}'\n". 
			    "AND allianceName LIKE '".$allianceName."%' ORDER BY allianceName ASC LIMIT 0, 20";

		    $result = $this->fetchQueryResults($sql);
		    if(!$result)
			return false;
		    return $result;
		}else{
		    return false;
		}
	}*/
	
	public function fetchCoalitionByAlliance($allianceId, $electionId)
	{
		if (!is_numeric($allianceId) || $allianceId == 0)
			return false;
	    
		if ($this->db->dbh == null || $electionId == 0)
			return false;		

		$sql = "SELECT coalition.coalitionId, coalition.coalitionName FROM {$this->tableName} \n".
			"LEFT JOIN coalition ON (alliance.coalition = coalition.coalitionId)\n".
			"WHERE allianceId='".$allianceId."' AND {$this->tableName}.election='".$electionId."' LIMIT 1";
		
		$coalitionArr = $this->fetchQueryResults($sql, array('singleRow' => true));
		
		/*$coalitionModel = new Coalition_Model; 
		if($coalitionModel->countByFieldValues(array('coalitionId'=>$coalitionArr['coalitionId'])) == 0)
		    return false;*/

		if(!$coalitionArr){
		    return array('coalitionId'=>0, 'coalitionName'=>"");
		}
		return $coalitionArr;
	}
	
	/**
	 * Used for inserting a new alliance
	 * 
	 * @param array $allianceArr
	 * @param int $electionId
	 * 
	 * @return array
	 */
	public function insertAlliance($allianceArr, $electionId)
	{	
		$allianceId = (int) $allianceArr['allianceId'];
		$this->beginTransaction();
		
		//Look if allianceId is already taken
		if(is_array($this->fetchByCompositeId(array($allianceId, $electionId)))) {
			$this->rollBack();
			return parent::PK_EXISTS_ALREADY;
		}
		
		$paramArr = array('allianceId'=>$allianceId,'allianceName'=>$allianceArr['allianceName'], 
		    'coalition'=>$allianceArr['coalitionId'], 'election'=>$electionId);
		$sql = "allianceId, allianceName, coalition, election";
		
		if($this->insertInto($sql, $paramArr) === false) {
			$this->rollBack();
			return parent::FAILURE;
		}
		$this->commit();
		return parent::SUCCESS;				
	}
	
	/**
	 * Used for inserting alliances and coalitions from CSV file
	 * 
	 * @param string $allianceName
	 * @param string $personInChargeEmail
	 * @param int $coalitionId
	 * @param int $allianceId
	 * @param int $electionId
	 * 
	 * @return array
	 */
	public function insertAllianceAndCoalition($allianceName, $personInChargeEmail, $coalitionId, $coalitionName, $allianceId, $electionId)
	{	
	    
		//First check that the election exists
                if($electionId == 0 || !isset($electionId))
                    return false;
		
		$coalitionModel = new Coalition_Model;
		if(is_null($coalitionId) || empty($coalitionId) || !is_numeric($coalitionId) || !isset($coalitionId))
                    $coalitionId = 0;
                
		$this->beginTransaction();
		
		//Look if allianceId is already taken
		if(is_array($this->fetchByCompositeId(array($allianceId, $electionId)))) {
			$this->rollBack();
			return parent::PK_EXISTS_ALREADY;
		}
		
		$paramArr = array('allianceId'=>$allianceId,'allianceName'=>$allianceName, 'personInChargeEmail'=>$personInChargeEmail,
		    'coalition'=>$coalitionId, 'election'=>$electionId);
		$sql = "allianceId, allianceName, personInChargeEmail, coalition, election";
		
		if($this->insertInto($sql, $paramArr) === false) {
			$this->rollBack();
			return parent::FAILURE;
		}
		
		//insert coalition
		if($coalitionId != 0){
		    if($coalitionModel->fetchByCompositeId(array($coalitionId, $electionId)) == false){
			$paramArr2 = array(':election'=>$electionId, ':coalitionId'=>$coalitionId, ':coalitionName'=>$coalitionName);   
			$sql2 = "election, coalitionId, coalitionName";
			if($coalitionModel->insertInto($sql2, $paramArr2) === false) {
				$coalitionModel->db->dbh->rollBack();
				return parent::FAILURE;
			}
		    }
		}
		$this->commit();
		return parent::SUCCESS;				
	}
	
	/**
	 * Used for editing alliance
	 * 
	 * @param array $allianceArr
	 * @param int $electionId
	 * 
	 * @return array
	 */
	public function updateAlliance($allianceArr, $electionId)
	{	
		$allianceId = (int) $allianceArr['allianceId'];
		$allianceIdOld = (int) $allianceArr['allianceIdOld'];
		$this->beginTransaction();
		
		//Check if allianceId is already taken
		if($allianceId != $allianceIdOld){
		    if(is_array($this->fetchByCompositeId(array($allianceId, $electionId)))) {
			    $this->rollBack();
			    return parent::PK_EXISTS_ALREADY;
		    }
		}
		
		//Update references to all alliance candidates
		$candidateModel = new Candidate_Model;
		$query = "SELECT candidateNum \n".
		"FROM candidate \n".
		"WHERE alliance='".$allianceIdOld."' AND election='".$electionId."'";

		$candidates = $candidateModel->fetchQueryResults($query);
		if($candidates != false){
		    foreach($candidates as $candidate){
			$params = array($allianceId, $candidate['candidateNum'], $electionId);
			$updateClause = "alliance=? WHERE candidateNum=? AND election=?";	

			if($candidateModel->update($updateClause, $params) == false) {
				$this->rollBack();
				return parent::FAILURE;
			}
		    }//foreach
		}
		
		//Update existing record
		$params = array($allianceId, $allianceArr['allianceName'], $allianceArr['personInChargeEmail'], $allianceArr['coalitionId'], $allianceIdOld, $electionId);
		$updateClause = "allianceId=?, allianceName=?, personInChargeEmail=?, coalition=? WHERE allianceId=? AND election=?";	

		if($this->update($updateClause, $params) == false) {
			$this->rollBack();
			return parent::FAILURE;
		}
		
		$this->commit();
		return parent::SUCCESS;				
	}
	
	/**
	 * Used for deleting alliance
	 * 
	 * @param int $allianceId
	 * @param int $electionId
	 * 
	 * @return array
	 */
	public function removeAlliance($allianceId, $electionId)
	{
		$this->beginTransaction();

		//Update references to all alliance candidates
		$candidateModel = new Candidate_Model;
		$params = array(0, $allianceId, $electionId);
		$updateClause = "alliance=? WHERE alliance=? AND election=?";	

		if($candidateModel->update($updateClause, $params) == false) {
			$candidateModel->rollBack();
			return false;
		}
		
		if($this->removeByCompositeId(array($allianceId, $electionId)) === false) {
			$this->rollBack();
			return false;
		}  
		$this->commit();
		return true;	
	}
	
	public function countAllianceCandidates($allianceId, $electionId)
	{
		    $query = "SELECT COUNT(*) \n".
		    "FROM candidate JOIN {$this->tableName} ON (candidate.alliance={$this->tableName}.allianceId)\n".
		    "WHERE candidate.election={$this->tableName}.election \n".
		    "AND {$this->tableName}.election='".$electionId."'\n".
		    "AND candidate.alliance='".$allianceId."' AND candidate.candidateNum !='".Candidate_Model::EMPTY_VOTE."'"; 
		    
		    $stmt = $this->db->dbh->prepare($query);
		    $stmt->execute();
		    return $stmt->fetchColumn(0);
	}
	
	public function setAllianceVotes($electionId)
	{
		if($electionId == 0 || !isset($electionId))
		    return false;
		
		$this->beginTransaction();
		$alliances = $this->fetchByField('election', $electionId);
		
		if(!$alliances)
		    return true;
		
		//set alliance votes
		foreach($alliances as $alliance){    
		    $allianceId = $alliance['allianceId'];
		    
		    //alliance must have at least 2 candidates
		    //if($this->countAllianceCandidates($allianceId, $electionId) >= 2){
		   
			$query = "(SELECT SUM(candidate.votes + candidate.paperVotes) AS votes \n".
			"FROM candidate LEFT JOIN alliance ON (candidate.alliance = {$this->tableName}.allianceId)\n".
			"WHERE candidate.election = {$this->tableName}.election \n".
			"AND {$this->tableName}.election='".$electionId."' \n".
			"AND candidate.alliance='".$allianceId."' AND candidate.candidateNum !='".Candidate_Model::EMPTY_VOTE."' AND candidate.coalition='0')"; 
			$sth = $this->db->dbh->query($query);
			
			if($sth !== false) {
			    $temp = $sth->fetch(PDO::FETCH_ASSOC);
			    $sum = (int)$temp['votes'];

			    $query2 = "UPDATE alliance SET alliance.votes=? WHERE allianceId=?";
			    $stmt = $this->db->dbh->prepare($query2);
			    if ($stmt) {
			       $result = $stmt->execute(array($sum,$allianceId));
			       if($result == false){
				   $this->rollBack();
				   return false;
			       }   
			    }
			    
			}else{
			   $this->rollBack(); 
			   return false; 
			}
		   // }//if
		}//for
		
		$this->commit();
		return true;
	}
	
	/**
	 * This method validates the form's values and returns the valid input fields and error messages in an array.
	 * 
	 * @param array $postArr
	 * @return array
	 */
	public function validateEditForm($postArr) 
	{
		$allianceArr = array();
		$errors = array();
		$formFields = array('allianceId','allianceIdOld', 'allianceName','personInChargeEmail', 'coalitionId');
		$mandatory = array('allianceId', 'allianceName');
		$numeric = array('allianceId', 'coalitionId');
		$nonzero = array('allianceId');
		
		//Loop through form input and check that every mandatory field was filled
		foreach ($formFields as $field) {
			if(in_array($field, $mandatory) && empty($postArr[$field])) {
				$errors[$field] = MANDATORY_TEXT;
			}
			else if(in_array($field, $numeric) && !is_numeric($postArr[$field])) {
				$errors[$field] = HAS_TO_BE_A_NUMBER_TEXT;
			}
			else if(in_array($field, $nonzero) && $postArr[$field] < 1){
				$errors[$field] = HAS_TO_BE_A_NUMBER_TEXT;
			}
			 
			if(!isset($errors[$field]))
				$allianceArr[$field] = $postArr[$field];
		}
		
		//Validate email address
		if(!empty($allianceArr['personInChargeEmail']) && !filter_var($allianceArr['personInChargeEmail'], FILTER_VALIDATE_EMAIL)) {
			$errors['personInChargeEmail'] = INVALID_EMAIL_TEXT;
		}
		
		return array($allianceArr, $errors);
	}
}