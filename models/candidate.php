<?php
/**
 * Model for candidate database table. 
 * 
 * Has also some general functions which are used by the candidate page. 
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Candidate_Model extends Model
{		
	protected $idColumn = 'candidateId';
	protected $compositeId = array('candidateNum', 'election');
	protected $tableName = 'candidate';
	
	//Votes
	const CANDIDATE_HAS_VOTES = -1;
	
	//Empty vote
	const EMPTY_VOTE = -1;
	
	/**
	 * Used in voting view to group candidates by alliances
	 * 
	 * @param int $electionId
	 * @return array
	 */
	public function fetchCandidatesGroupedByAlliances($electionId) 
	{										
		$electionId = (int) $electionId;
		
		$candidates = array();
		
		//Query string
		$sql = "SELECT alliance, candidateNum, firstName, lastName\n".
			"FROM {$this->tableName}\n".
			"WHERE election = '{$electionId}' AND candidateNum > 0\n".
			"ORDER BY candidateNum ASC";		
		
		//Execute PDO::query
		$sth = $this->db->dbh->query($sql);
		
		if($sth !== false) {
			
			//Fetch all alliance names
			include_once SERVER_ROOT.'/models/alliance.php';
			$allianceModel = new Alliance_Model;
			$allianceArr = $allianceModel->fetchAll();			
			$allianceNames = array(0 => "&nbsp;");
			foreach ($allianceArr as $a) {				
				$name = (empty($a['allianceName']))? $a['allianceId']: $a['allianceName'];
				
				//Alliance names need to be unique
				while(in_array($name, $allianceNames)): $name .= "&nbsp"; endwhile;
				
				$allianceNames[$a['allianceId']] = $name;												
			}
			
			//order the candidates by their aĺliances
			foreach ($sth->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP) as $alliance => $row) {				
				if(isset($allianceNames[$alliance]))
					$candidates[$allianceNames[$alliance]] = $row;
				else			
					$candidates[$alliance] = $row;
			}	
		}					
		
		return $candidates;
	}
	
	public function countByElectionAndSearch($electionId, $searchStr = null)
	{
		$electionId = (int) $electionId;
	
		if ($this->db->dbh == null || $electionId == 0)
			return false;
	
		list($whereClause, $paramArr) = $this->generateWhereWithParams($electionId, $searchStr);
	
		$sql = "SELECT count(*) FROM {$this->tableName} {$whereClause}";
		$stmt = $this->db->dbh->prepare($sql);
		$stmt->execute($paramArr);
		return $stmt->fetchColumn(0);
	}
    
    /**
	 * Used in show_candidates view to list all candidates and their alliance and coalition 
	 * 
	 * @param int $electionId
	 * @param int $limit
	 * @param int $offset
	 * @param string $searchStr
	 * @return array
	 */
	public function fetchCandidatesAlliancesCoalitions($electionId, $limit = null, $offset = null, $searchStr = null, $order = "") 
	{										
		$electionId = (int) $electionId;
		
		$limitClause = "";
		if($limit != null) {
			$limitClause = " LIMIT ". (int) $limit;
			if($offset != null) {
				$limitClause .= " OFFSET ".(int) $offset;
			}
		}
		list($whereClause, $paramArr) = $this->generateWhereWithParams($electionId, $searchStr);
		
		$sql = "SELECT * FROM (SELECT coalition.coalitionId, coalition.coalitionName, alliance.allianceId, alliance.allianceName, {$this->tableName}.candidateNum,\n".
		"{$this->tableName}.comparisionNum, {$this->tableName}.votes, {$this->tableName}.allianceComparisionNum, {$this->tableName}.coalitionComparisionNum, {$this->tableName}.sortHash, {$this->tableName}.rank,\n". 
                "{$this->tableName}.firstName, {$this->tableName}.lastName, {$this->tableName}.email, {$this->tableName}.election\n".             
                "FROM {$this->tableName}\n". 
		"LEFT JOIN alliance ON ({$this->tableName}.alliance = alliance.allianceId AND {$this->tableName}.election = alliance.election)\n". 
		"JOIN coalition ON (alliance.coalition = coalition.coalitionId  AND {$this->tableName}.election = coalition.election) {$whereClause}\n".

		"UNION DISTINCT SELECT coalition.coalitionId, coalition.coalitionName, alliance.allianceId, alliance.allianceName, {$this->tableName}.candidateNum,\n". 
		"{$this->tableName}.comparisionNum, {$this->tableName}.votes, {$this->tableName}.allianceComparisionNum, {$this->tableName}.coalitionComparisionNum, {$this->tableName}.sortHash, {$this->tableName}.rank,\n". 
                "{$this->tableName}.firstName, {$this->tableName}.lastName, {$this->tableName}.email, {$this->tableName}.election\n".             
                "FROM {$this->tableName}\n". 
		"LEFT JOIN alliance ON ({$this->tableName}.alliance = alliance.allianceId AND {$this->tableName}.election = alliance.election)\n".
		"LEFT JOIN coalition ON (candidate.coalition = coalition.coalitionId AND {$this->tableName}.election = coalition.election) {$whereClause}) x GROUP BY candidateNum ";
		$wholeSql = $sql.$limitClause.$order;
		
		$stmt = $this->db->dbh->prepare($wholeSql);
		$stmt->execute($paramArr);
		return $stmt->fetchAll();		
	}
	
	/**
	 * Candidate specific customization of fetchByCompositeId method
	 * 
	 * @param int $candidateNum
	 * @param int $electionId
	 * @return array
	 */
	public function fetchCandidateByCompositeId($candidateNum, $electionId) 
	{										
		$electionId = (int) $electionId;
		
		$sql = "SELECT {$this->tableName}.alliance, alliance.coalition AS allianceCoalition, {$this->tableName}.coalition, {$this->tableName}.candidateNum,\n". 
                "{$this->tableName}.firstName, {$this->tableName}.lastName, {$this->tableName}.email, {$this->tableName}.lastName, {$this->tableName}.election\n".             
                "FROM {$this->tableName}\n". 
                "LEFT JOIN alliance ON ({$this->tableName}.alliance = alliance.allianceId)\n". 
                "WHERE {$this->tableName}.election='".$electionId."' AND {$this->tableName}.candidateNum='".$candidateNum."'";		
		
		$sth = $this->db->dbh->query($sql);
		
		if($sth !== false) {
                    $candidate = $sth->fetch(PDO::FETCH_ASSOC);	
		    if ($candidate != null && count($candidate) >0) {
                        
			if(!is_null($candidate['allianceCoalition']) && $candidate['allianceCoalition'] > 0){
			    $candidate['coalition'] = $candidate['allianceCoalition'];
			}
			return $candidate;
                    }
		}						
		return false;
	}
	
	public function fetchIndependentCandidates($electionId)
	{
		$candidates = array();
		
		$sql = "SELECT candidateNum, SUM({$this->tableName}.votes + {$this->tableName}.paperVotes) AS votes, alliance, comparisionNum, sortHash, \n". 
                "allianceComparisionNum, coalitionComparisionNum, firstName, lastName, email \n".             
                "FROM {$this->tableName} \n". 
                "WHERE election='".$electionId."' AND alliance='0' AND candidate.candidateNum !='".Candidate_Model::EMPTY_VOTE."' \n".	
		"GROUP BY candidateNum ORDER BY votes DESC, sortHash DESC";
		
		//Execute PDO::query
		$sth = $this->db->dbh->query($sql);
		
		if($sth !== false) {
                    foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $row) {
			$candidates[] = $row;
                    }	
		}						
		return $candidates;
	}

	public function fetchCandidatesByAlliance($allianceId, $electionId)
	{
		$candidates = array();
		
		$sql = "SELECT candidateNum, SUM(votes+paperVotes) AS votes, alliance, comparisionNum, sortHash, \n". 
                "allianceComparisionNum, coalitionComparisionNum, firstName, lastName, email \n".             
                "FROM {$this->tableName} \n". 
                "WHERE election='".$electionId."' AND alliance='".$allianceId."' AND candidate.candidateNum !='".Candidate_Model::EMPTY_VOTE."' \n".
		"AND candidate.coalition=0 GROUP BY candidateNum ".	
		"ORDER BY votes DESC, sortHash DESC";
		
		//Execute PDO::query
		$sth = $this->db->dbh->query($sql);
		
		if($sth !== false) {
                    foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $row) {
			$candidates[] = $row;
                    }	
		}						
		return $candidates;
	}
	
	/**
	 * Gets all candidates in given coalition
	 * 
	 * @param int $coalitionName
	 * @param int $electionId
	 * @return array
	 */
	public function fetchCandidatesByCoalition($coalitionId, $electionId)
	{
		$candidates = array();
		
		$sql = "SELECT * FROM (SELECT candidateNum, alliance, coalition.coalitionId, comparisionNum, sortHash, \n". 
                "allianceComparisionNum, coalitionComparisionNum, firstName, lastName, email \n".              
                "FROM {$this->tableName}\n". 
		"LEFT JOIN alliance ON ({$this->tableName}.alliance = alliance.allianceId AND {$this->tableName}.election = alliance.election)\n". 
		"JOIN coalition ON (alliance.coalition = coalition.coalitionId  AND {$this->tableName}.election = coalition.election) \n".
		"WHERE coalition.election='".$electionId."' AND coalition.coalitionId='".$coalitionId."'\n".
		"UNION DISTINCT SELECT candidateNum, alliance, coalition.coalitionId, comparisionNum, sortHash, \n". 
                "allianceComparisionNum, coalitionComparisionNum, firstName, lastName, email \n".             
                "FROM {$this->tableName}\n". 
		"LEFT JOIN alliance ON ({$this->tableName}.alliance = alliance.allianceId AND {$this->tableName}.election = alliance.election)\n".
		"LEFT JOIN coalition ON (candidate.coalition = coalition.coalitionId AND {$this->tableName}.election = coalition.election) ".
		"WHERE coalition.election='".$electionId."' AND coalition.coalitionId='".$coalitionId."') x ".
		"GROUP BY candidateNum ORDER BY comparisionNum DESC, sortHash DESC";
		
		//Execute PDO::query
		$sth = $this->db->dbh->query($sql);
		
		if($sth !== false) {
                    foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $row) {
			$candidates[] = $row;
                    }	
		}						
		return $candidates;
	}
	
	/**
	 * Sorts candidates based on their final comparisionNum and sortHash
	 * 
	 * @param int $electionId
	 * @param int $totalSeats
	 * 
	 * @return array
	 */
	public function fetchCandidatesByComparision($electionId, $limitBySeats=false)
	{
		$candidates = array();
		
//		$sql = "SELECT candidateNum, alliance.allianceId, alliance.allianceName, coalition.coalitionId, coalition.coalitionName, \n". 
//		"comparisionNum, {$this->tableName}.votes, \n". 
//                "allianceComparisionNum, coalitionComparisionNum, firstName, lastName \n".             
//                "FROM {$this->tableName} \n". 
//		"LEFT JOIN alliance ON ({$this->tableName}.alliance = alliance.allianceId)\n". 
//                "LEFT JOIN coalition ON (alliance.coalition = coalition.coalitionId)\n".
//                "WHERE {$this->tableName}.election='".$electionId."' AND {$this->tableName}.candidateNum !='".self::EMPTY_VOTE."' \n".
//		"ORDER BY comparisionNum DESC,sortHash DESC";
		
		$sql = "SELECT * FROM (SELECT candidateNum, alliance.allianceId, alliance.allianceName, coalition.coalitionId, coalition.coalitionName, comparisionNum,\n". 
		"{$this->tableName}.votes, {$this->tableName}.paperVotes,\n". 
		"allianceComparisionNum, coalitionComparisionNum, firstName, lastName, sortHash\n".             
                "FROM {$this->tableName}\n". 
		"LEFT JOIN alliance ON ({$this->tableName}.alliance = alliance.allianceId AND {$this->tableName}.election = alliance.election)\n". 
		"JOIN coalition ON (alliance.coalition = coalition.coalitionId  AND {$this->tableName}.election = coalition.election) \n".
		"WHERE {$this->tableName}.election='".$electionId."' AND {$this->tableName}.candidateNum !='".self::EMPTY_VOTE."' \n".	
		"UNION DISTINCT SELECT candidateNum, alliance.allianceId, alliance.allianceName, coalition.coalitionId, coalition.coalitionName, comparisionNum,\n". 
		"{$this->tableName}.votes, {$this->tableName}.paperVotes,\n". 
		"allianceComparisionNum, coalitionComparisionNum, firstName, lastName, sortHash\n".             
                "FROM {$this->tableName}\n". 
		"LEFT JOIN alliance ON ({$this->tableName}.alliance = alliance.allianceId AND {$this->tableName}.election = alliance.election)\n".
		"LEFT JOIN coalition ON (candidate.coalition = coalition.coalitionId AND {$this->tableName}.election = coalition.election) \n".
		"WHERE {$this->tableName}.election='".$electionId."' AND {$this->tableName}.candidateNum !='".self::EMPTY_VOTE."' )\n".
		"x GROUP BY candidateNum ORDER BY comparisionNum DESC,sortHash DESC";
		
		if($limitBySeats)
		    $sql .= " LIMIT 0,".Election_Model::TOTAL_SEATS;
		
		//Execute PDO::query
		$sth = $this->db->dbh->query($sql);
		
		if($sth !== false) {
                    foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $row) {
			$candidates[] = $row;
                    }	
		}						
		return $candidates;
	}
	
	public function countCandidates($electionId)
	{
		$electionId = (int)$electionId;
		if ($this->db->dbh == null || $electionId == 0)
			return false;

		$whereClause = "WHERE election=? AND candidateNum !=".self::EMPTY_VOTE;
		$sql = "SELECT COUNT(*) FROM {$this->tableName} {$whereClause}";
		$paramArr = array($electionId);
		
		$stmt = $this->db->dbh->prepare($sql);
		$stmt->execute($paramArr);
		return $stmt->fetchColumn(0);
	}
	
	public function countVotes($electionId, $votesType = null) 
	{				
		$electionId = (int)$electionId;
		if ($this->db->dbh == null || $electionId == 0)
			return false;

		$whereClause = "WHERE election=?";
		$sql = "SELECT SUM(";
		if(isset($votesType)){
		    $sql .= $votesType.") ";
		}else{
		    $sql .= "paperVotes + votes) ";
		}
		$sql .= "FROM {$this->tableName} {$whereClause}";
		$paramArr = array($electionId);
		
		$stmt = $this->db->dbh->prepare($sql);
		$stmt->execute($paramArr);
		return $stmt->fetchColumn(0);
	}
        
	public function fetchCandidateVotes($electionId) 
	{										
		$electionId = (int) $electionId;
		$paramArr = array(':election' => (int)$electionId);
		$whereClause = "\nWHERE {$this->tableName}.election=:election";
		
		$sql = "SELECT * FROM (SELECT coalition.coalitionId, coalition.coalitionName, alliance.allianceId, alliance.allianceName, {$this->tableName}.candidateNum,\n".
		"{$this->tableName}.votes, {$this->tableName}.paperVotes,\n". 
                "{$this->tableName}.firstName, {$this->tableName}.lastName, {$this->tableName}.email, {$this->tableName}.election\n".             
                "FROM {$this->tableName}\n". 
		"LEFT JOIN alliance ON ({$this->tableName}.alliance = alliance.allianceId AND {$this->tableName}.election = alliance.election)\n". 
		"JOIN coalition ON (alliance.coalition = coalition.coalitionId  AND {$this->tableName}.election = coalition.election) {$whereClause}\n".

		"UNION DISTINCT SELECT coalition.coalitionId, coalition.coalitionName, alliance.allianceId, alliance.allianceName, {$this->tableName}.candidateNum,\n". 
		"{$this->tableName}.votes, {$this->tableName}.paperVotes,\n". 
                "{$this->tableName}.firstName, {$this->tableName}.lastName, {$this->tableName}.email, {$this->tableName}.election\n".             
                "FROM {$this->tableName}\n". 
		"LEFT JOIN alliance ON ({$this->tableName}.alliance = alliance.allianceId AND {$this->tableName}.election = alliance.election)\n".
		"LEFT JOIN coalition ON (candidate.coalition = coalition.coalitionId AND {$this->tableName}.election = coalition.election) {$whereClause}) x GROUP BY candidateNum ";
		
		$stmt = $this->db->dbh->prepare($sql);
		$stmt->execute($paramArr);
		return $stmt->fetchAll();
	}
	
	/**
	 * Used to add a vote for candidate
	 * 
	 * @param int $candidateNum
	 * @param int $studentNum
	 * @param int $electionId
	 * @return boolean
	 */
	public function giveVote($candidateNum, $studentNum, $electionId)
	{		
		//Parameters should be integer
		$candidateNum = (int) $candidateNum;
		$electionId = (int) $electionId;
		$studentNum = (int) $studentNum;
		
		//Begin the transaction
		$this->db->dbh->beginTransaction();
		
		//Increment candidate's votes by one
		$sql = "UPDATE {$this->tableName} SET votes = votes + 1\n".
			"WHERE candidateNum = '{$candidateNum}'\n".
			"AND election = '{$electionId}'";
		$count = $this->db->dbh->exec($sql);
		
		//Roll back the changes if there was a mistake.
		//One row should be affected - no more, no less
		if($count !== 1) {
			$this->db->dbh->rollBack();
			return false;
		}						
		
		//Found out if the voter actually exists
		include_once SERVER_ROOT.'/models/voter.php';
		$voterModel = new Voter_Model;
		if($voterModel->fetchByCompositeId(array($studentNum, $electionId)) == false)
			return false;		
		
		//Update voter's information
		$sql = "UPDATE voter SET voteDate = NOW(), voteMethod = 'www', hasVoted = 1\n".
			"WHERE studentNum = '{$studentNum}'\n".
			"AND election = '{$electionId}'";
		$count = $this->db->dbh->exec($sql);
		
		//Roll back the changes if there was a mistake.
		//One row should be affected - no more, no less
		if($count !== 1) {
			$this->db->dbh->rollBack();
			return false;
		}
		
		//Commit the changes if everything went fine
		$this->db->dbh->commit();
		return true;		
	}
	
	/**
	 * This function is used for inserting paper votes to the system.
	 * 
	 * @param int $electionId
	 * @param int $candidatePaperVotesArr
	 * 
	 * @return boolean
	 */
	public function insertPaperVotes($electionId, $candidatePaperVotesArr)
	{
		$this->beginTransaction();
		if(!is_null($electionId) && $electionId != 0) {
		    
			foreach($candidatePaperVotesArr as $candidate){ 
			    $params = array($candidate['votes'], $candidate['candidateNum'], $electionId);
			    $updateClause = "paperVotes=? WHERE candidateNum=? AND election=?";	

			    if($this->update($updateClause, $params) == false) {
				    $this->rollBack();
				    return false;
			    }  
			}//foreach

		}else{
		    return false;
		}
		$this->commit();
		return true;
	}
        
	/**
	 * This method is used only when importing candidates from CSV file.
	 * For inserting candidate to DB use updateCandidate instead of this
	 * 
	 * @param int $electionId
	 * @param int $candidateNum
	 * @param string $lastName
	 * @param string $firstName
	 * @param string $email
	 * @param string $allianceId
	 * @return int
	 */
        public function insertCandidate($electionId, $candidateNum, $lastName, $firstName, $email, $allianceId)
        {
                //First check that the election exists
                if($electionId == 0 || !isset($electionId))
                    return false;
                
                $allianceModel = new Alliance_Model;
                //$coalitionModel = new Coalition_Model;
		
		if(is_null($allianceId) || $allianceId == 0 || !is_numeric($allianceId))
                    $allianceId = 0;
                
                /*if(is_null($coalitionName) || strlen($coalitionName) == 0)
                    $coalitionName = '';*/
                
                //Begin the transaction
		$this->db->dbh->beginTransaction();							
                
                //Check if candidate with given candidateNum already exists
                if(is_array($this->fetchByCompositeId(array($candidateNum, $electionId)))) {
                        $this->db->dbh->rollBack();
                        return parent::PK_EXISTS_ALREADY;
                }
		$sortHash = $this->createSortHash($lastName, $firstName, $candidateNum);
		
                $paramArr = array(':election' => $electionId, ':candidateNum' => $candidateNum, ':lastName' => $lastName, 
                                    ':firstName' => $firstName, ':email' => $email, ':alliance' => $allianceId, ':sortHash' => $sortHash);
                $sql = "election, candidateNum, lastName, firstName, email, alliance, sortHash";
                if($this->insertInto($sql, $paramArr) === false) {
                        $this->db->dbh->rollBack();
                        return parent::FAILURE;
                }
                
		if($allianceId != 0){
		    if($allianceModel->fetchByCompositeId(array($allianceId, $electionId)) == false){
			$paramArr = array(':election' => $electionId, ':allianceId' => $allianceId);   
			$sql = "election, allianceId";
			if($allianceModel->insertInto($sql, $paramArr) === false) {
				$this->db->dbh->rollBack();
				return parent::FAILURE;
			}
		    }
		}
		$this->db->dbh->commit();
                return parent::SUCCESS;
        }
	
	/**
	 * This method handles updating and inserting candidate data.
	 * 
	 * This method cannot be used to modify these fields on a existing record: election, votes, comparisionNums
	 * 
	 * @param array $candidateArr
	 * @param int $electionId
	 * @return int
	 */
	public function updateCandidate($candidateArr, $electionId)
	{	
		$candidateNum = (int) $candidateArr['candidateNum'];
		$candidateNumOld = (int) $candidateArr['candidateNumOld'];
		$allianceId = !isset($candidateArr['allianceId']) ? 0 : $candidateArr['allianceId'];
		$coalitionId = !isset($candidateArr['coalitionId']) ? 0 : $candidateArr['coalitionId'];
		
		if($allianceId != 0) $coalitionId = 0;
		
		$electionModel = new Election_Model;
		if($electionModel->fetchById($electionId) == false)
			return false;
				
		$sortHash = $this->createSortHash($candidateArr['lastName'], $candidateArr['firstName'], $candidateArr['candidateNum']);
		
		$paramArr = array('election'=>$electionId, 'candidateNum'=>$candidateArr['candidateNum'], 'firstName'=>$candidateArr['firstName'],
		    'lastName'=>$candidateArr['lastName'],"alliance"=>$allianceId, "coalition"=>$coalitionId, "sortHash"=>$sortHash, "email"=>$candidateArr['email']);	
		$sql = "election, candidateNum, firstName, lastName, alliance, coalition, sortHash, email";

		$this->beginTransaction();
		
		if($candidateNum != $candidateNumOld) { //Insert a new record

			//Look if candidateNum is already taken
			if(is_array($this->fetchByCompositeId(array($candidateNum, $electionId)))) {
				$this->rollBack();
				return parent::PK_EXISTS_ALREADY;
			}

			//Delete existing record
			if($candidateNumOld != 'new') {
				//Prevent deleting existing record if the candidate already has votes
				$oldRecord = $this->fetchByCompositeId(array($candidateNum, $electionId));
				if($oldRecord['votes'] > 0) {
					$this->rollBack();
					return self::CANDIDATE_HAS_VOTES;
				}
				
				if($this->removeByCompositeId(array($candidateNumOld, $electionId)) === false) {
					$this->rollBack();
					return parent::FAILURE;
				}
			}
															
			if($this->insertInto($sql, $paramArr) === false) {
				$this->rollBack();
				return parent::FAILURE;
			}
		}else { //Update existing record
			
			$paramArr[':electionId'] = $electionId;
			$paramArr[':candidateNum'] = $candidateNum;
			$sql .= "\nWHERE election = :electionId AND candidateNum = :candidateNum";			
			if($this->updateSet($sql, $paramArr) == false) {
				$this->rollBack();
				return parent::FAILURE;
			}				
		}
		$this->commit();
		return parent::SUCCESS;				
	}
	
	/**
	 * Used for setting comparisionNum.
	 * 
	 * Can be used to set either allianceComparisionNum, coalitionComparisionNum or comparisionNum depending of fieldName
	 * 
	 * @param string $fieldName
	 * @param int $candidateNum
	 * @param int $electionId
	 * @return bool
	 */
	public function setComparisionNum($fieldName, $candidate, $electionId)
	{
		if(empty($fieldName) || strlen($fieldName) < 1)
		    return false;
	    
		$paramArr = array($fieldName=>$candidate[$fieldName]);	
		$sql = $fieldName;
		
		$paramArr[':electionId'] = $electionId;
		$paramArr[':candidateNum'] = $candidate['candidateNum'];
		$sql .= "\nWHERE election = :electionId AND candidateNum = :candidateNum";			
		if($this->updateSet($sql, $paramArr) == false) {
		    return false;
		}
		return true;
	}
	
	public function setRank($candidateNum, $rank, $electionId)
	{
		$paramArr = array('rank'=>$rank);	
		$sql = "rank";
		
		$paramArr[':electionId'] = $electionId;
		$paramArr[':candidateNum'] = $candidateNum;
		$sql .= "\nWHERE election = :electionId AND candidateNum = :candidateNum";			
		if($this->updateSet($sql, $paramArr) == false) {
		    return false;
		}
		return true;
	}
	
	public function checkIfHasVotes($electionId, $candidateNum = null) 
	{	
		if(isset($candidateNum)){
		    $candidate = $this->fetchByCompositeId(array($candidateNum, $electionId));
		    if($candidate['votes'] > 0 || $candidate['paperVotes'] > 0)
			return false;
		}else{
		    $sql = "SELECT candidateNum FROM {$this->tableName} WHERE (votes > 0 OR paperVotes > 0) AND election='".$electionId."'";
		    $candidates = $this->fetchQueryResults($sql);
		    if(!empty($candidates))
			return false;
		}
		return true;
	}
	
	public function removeAllCandidates($electionId) 
	{	
		$electionModel = new Election_Model;
		if($electionModel->fetchById($electionId) == false)
			return false;
		
		if ($this->db->dbh == null) return false;
                $sql = "DELETE FROM {$this->tableName} WHERE election=:election AND candidateNum != :candidateNum";
                $stmt = $this->db->dbh->prepare($sql); 
                $result = $stmt->execute(array(':election'=>$electionId,':candidateNum'=>self::EMPTY_VOTE));
                if($result !== false)
			return true;
		return false;
	}
        
	/**
	 * This method validates the form's values and returns the valid input fields and error messages in an array.
	 * 
	 * @param array $postArr
	 * @return array
	 */
	public function validateEditForm($postArr) 
	{
		$candidateArr = array();
		$errors = array();
		$formFields = array('candidateNum','candidateNumOld', 'lastName', 'firstName', 'email', 'allianceId', 'coalitionId');
		$mandatory = array('candidateNum', 'lastName', 'firstName');
		$numeric = array('candidateNum','allianceId', 'coalitionId');
		$nonzero = array('candidateNum');
		
		//Loop through form input and check that every mandatory field was filled
		foreach ($formFields as $field) {
			if(in_array($field, $mandatory) && empty($postArr[$field])) {
				$errors[$field] = MANDATORY_TEXT;
			}
			else if(in_array($field, $numeric) && !empty($postArr[$field]) && !is_numeric($postArr[$field])) {
				$errors[$field] = HAS_TO_BE_A_NUMBER_TEXT;
			}
			else if(in_array($field, $nonzero) && $postArr[$field] < 1){
				$errors[$field] = HAS_TO_BE_A_NUMBER_TEXT;
			}
			 
			if(!isset($errors[$field]))
				$candidateArr[$field] = $postArr[$field];
		}
		
		//Validate email address
		if(!empty($candidateArr['email']) && !filter_var($candidateArr['email'], FILTER_VALIDATE_EMAIL)) {
			$errors['email'] = INVALID_EMAIL_TEXT;
		}
		
		return array($candidateArr, $errors);
	}
	
	/*
	 * Section 90§ of the Finnish election states that
	 * "Jos äänimäärät tai vertausluvut ovat yhtä suuret, niiden keskinäinen
	 * järjestys ratkaistaan arpomalla."
	 *
	 * The "random" ordering is not specified in more detail but for the
	 * implementation it is important to provide a consistent ordering in these
	 * cases. To maintain a consistent pseudo-random ordering for the candidates with
	 * equal vote counts we use the following two-tuple for the sorting key
	 * 
	 * (votes, sortHash(candidate.lastname, candidate.firstname, candidate.number))
	 *
	 * Where the createSortHash() function calculates a salted SHA1 hash over the
	 * candidate name and number and the resulting hash provides a consistent
	 * secondary ordering for tied votes.
	 * 
	 * @param string $lastname
	 * @param string $firstname
	 * @param int $candidateNum
	 * 
	 * @return string
	 */
        private function createSortHash($lastname, $firstname, $candidateNum)
        {
		$salt = '5f2b5e892dc48fc7bd3372a0fae18b0719ea57ba';
		return sha1($salt.$lastname.$salt.$firstname.$salt.$candidateNum);
        }
	
	private function generateWhereWithParams($electionId, $searchStr = null)
	{
		$params = array(':election' => (int) $electionId);
		$where = "\nWHERE {$this->tableName}.election=:election AND candidateNum !=".Candidate_Model::EMPTY_VOTE;
		if($searchStr != null) {
			$searchWords = explode("+", $searchStr);
			foreach($searchWords as $index => $word) {
				//Do not react to empty search words
				if(trim($word) == "")
					continue;
				$word = urldecode($word);
	
				$where .= "\nAND (({$this->tableName}.candidateNum LIKE :candidateNum{$index}) ".
					"OR ({$this->tableName}.firstName LIKE :firstName{$index}) ".
					"OR ({$this->tableName}.lastName LIKE :lastName{$index}) ".
					"OR ({$this->tableName}.email LIKE :email{$index}))";
				$params[':candidateNum'.$index] = '%'.$word.'%';
				$params[':firstName'.$index] = '%'.$word.'%';
				$params[':lastName'.$index] = '%'.$word.'%';
				$params[':email'.$index] = '%'.$word.'%';
			}
		}
		return array(0 => $where, 1 => $params);
	}
        
	
}