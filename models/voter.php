<?php
/**
 * Model for voter database table.
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Voter_Model extends Model
{		
	protected $idColumn = null;
	protected $compositeId = array('studentNum', 'election');
	protected $tableName = 'voter';	
	
	//Voting rights
	const NO_VOTING_RIGHT = 0;
	const HAS_VOTING_RIGHT = 1;
	
	//Vote methods
	const VOTE_METHOD_NONE = 'none';
	const VOTE_METHOD_PAPER = 'paper';
	const VOTE_METHOD_WWW = 'www';

	//Model specific result codes
	const VOTER_ALREADY_VOTED = 3;
        
	public function hasVoted($studentNum) 
	{
		//TODO 1) find out if student number can have preceding zeros. 2)how we should get the electionId
		$studentNum = (int) $studentNum;
		
		//Include the election model file if it has not already been included
		include_once SERVER_ROOT.'/models/election.php';		
		
		$sql = "SELECT {$this->tableName}.hasVoted\n". 
			"FROM {$this->tableName}\n".
			"LEFT JOIN election ON {$this->tableName}.election = election.electionId\n".
			"WHERE {$this->tableName}.studentNum = {$studentNum}\n".
			"AND election.status = '".Election_Model::STATE_ACTIVE."' AND election.startDate < NOW() AND election.endDate > NOW()\n".
			"LIMIT 1";
		$hasVoted = $this->fetchQueryResults($sql, array('fetchColumn' => 0));		
		
		if($hasVoted != 0)
			return true;		
		return false;
	}
		
	public function canVote($studentNum)
	{
		//TODO 1) find out if student number can have preceding zeros. 2)how we should get the electionId
		$studentNum = (int) $studentNum;
		
		//Include the election model file if it has not already been included
		include_once SERVER_ROOT.'/models/election.php';
		
		$sql = "SELECT {$this->tableName}.canVote\n".
				"FROM {$this->tableName}\n".
				"LEFT JOIN election ON {$this->tableName}.election = election.electionId\n".
				"WHERE {$this->tableName}.studentNum = {$studentNum}\n".
				"AND election.status = '".Election_Model::STATE_ACTIVE."' AND election.startDate < NOW() AND election.endDate > NOW()\n".
				"LIMIT 1";
		$canVote = $this->fetchQueryResults($sql, array('fetchColumn' => 0));
		
		if($canVote == 1)
			return true;		
		return false;
	}
	
	public function countByElectionAndSearch($electionId, $searchStr = null)
	{
		$electionId = (int) $electionId;
		
		if ($this->db->dbh == null || $electionId == 0)
			return false;		
		
		list($whereClause, $paramArr) = $this->generateWhereWithParams($electionId, $searchStr);
		
		$sql = "SELECT count(*) FROM {$this->tableName} {$whereClause}";
		$stmt = $this->db->dbh->prepare($sql);
		$stmt->execute($paramArr);		
		return $stmt->fetchColumn(0);
	}
	
	/**
	 * Fetches voters from the database with the given election id.
	 * 
	 * WARNING: Paging is done differently in different database managament systems. This version only supports MySQL.
	 * 
	 * @param int $electionId
	 * @param int $limit
	 * @param int $offset
	 * @param string $searchStr
	 * @return array|false
	 */
	public function fetchVotersByElection($electionId, $limit = null, $offset = null, $searchStr = null) 
	{				
		$electionId = (int) $electionId;
		
		if ($this->db->dbh == null || $electionId == 0)
			return false;
		
		//Generate the limit clause
		$limitClause = "";
		if($limit != null) {
			$limitClause = "\nLIMIT ". (int) $limit;
			if($offset != null) { 
				$limitClause .= " OFFSET ".(int) $offset;
			}	
		}
		
		list($whereClause, $paramArr) = $this->generateWhereWithParams($electionId, $searchStr);
		
		$sql = "SELECT v.* FROM {$this->tableName} AS v {$whereClause} ORDER BY v.studentNum ASC {$limitClause}";		
		$stmt = $this->db->dbh->prepare($sql);
		$stmt->execute($paramArr);
		return $stmt->fetchAll();
	}
	
	public function countVotes($electionId, $voteMethod = null) 
	{				
		$electionId = (int)$electionId;
		
		if ($this->db->dbh == null || $electionId == 0)
			return false;
		
		$whereClause = "WHERE (";
		if(isset($voteMethod)){
		    $whereClause .= " voteMethod='".$voteMethod."' ";
		}else{
		    $whereClause .= " voteMethod='".self::VOTE_METHOD_PAPER."' OR voteMethod='".self::VOTE_METHOD_WWW."'";
		}
		$whereClause .= ") AND hasVoted=:hasVoted AND election=:election";
		$sql = "SELECT count(*) FROM {$this->tableName} {$whereClause}";
		$paramArr = array(':hasVoted'=>true, ':election'=>$electionId);
		
		$stmt = $this->db->dbh->prepare($sql);
		$stmt->execute($paramArr);
		return $stmt->fetchColumn(0);
	}
	
	public function fetchVotes($electionId) 
	{				
		$electionId = (int)$electionId;
		
		if ($this->db->dbh == null || $electionId == 0)
			return false;
		
		$whereClause = "WHERE {$this->tableName}.election=:election";
		$sql = "SELECT studentNum,firstName,lastName,hasVoted,voteMethod,voteDate,election FROM {$this->tableName} {$whereClause}";
		$paramArr = array(':election'=>$electionId);
		
		$stmt = $this->db->dbh->prepare($sql);
		$stmt->execute($paramArr);
		return $stmt->fetchAll();
	}
	
	/**
	 * Sets a hash string to a voter table row
	 * 
	 * @param string $loginHash
	 * @param int $studentNum
	 * @param int $electionId
	 * @return bool
	 */
	public function setLoginHash($loginHash, $studentNum, $electionId)
	{
		$studentNum = (int) $studentNum;
		$electionId = (int) $electionId;
		$params = array($loginHash, $studentNum, $electionId);
		$updateClause = "loggedIn=? WHERE studentNum=? AND election=?";
		
		return $this->update($updateClause, $params);
	}
	
	/**
	 * Gets the login hash from the voter table
	 * 
	 * @param int $studentNum
	 * @param int $electionId
	 * @return string|false
	 */
	public function getLoginHash($studentNum, $electionId)
	{
		$studentNum = (int) $studentNum; 
		$electionId = (int) $electionId;		
		$sql = "SELECT loggedIn FROM {$this->tableName} WHERE studentNum=? AND election=?";		
		return $this->fetchQueryResults($sql, array('fetchColumn' => 0), array($studentNum, $electionId));
	}
	
	/**
	 * This method is used to mark paper votes.
	 * 
	 * These fields are being updated by this function: voteDate, voteMethod, hasVoted & electionWorker
	 * 
	 * @param int $studentNum
	 * @param int $electionId
	 * @param int $adminId
	 * @return int
	 */
	public function markPaperVote($studentNum, $electionId, $adminId)
	{
		//All parameters should be integer
		$studentNum = (int) $studentNum; 
		$electionId = (int) $electionId;
		$adminId = (int) $adminId; 
		
		//First check that the election exists
		include_once SERVER_ROOT.'/models/election.php';
		$electionModel = new Election_Model;
		if($electionModel->fetchById($electionId) == false)
			return self::FAILURE;
		
		//Check that the administrator exists
		include_once SERVER_ROOT.'/models/admin.php';
		$adminModel = new Admin_Model;
		if($adminModel->fetchById($adminId) == false)
			return self::FAILURE;
		
		//Fetch voter's current information
		$voterArr = $this->fetchByCompositeId(array($studentNum, $electionId));
		if(empty($voterArr))
			return self::FAILURE;
		
		//Check that voter has not voted yet
		if($voterArr['hasVoted'] == 1)
			return self::VOTER_ALREADY_VOTED;
		
		//Check that voter can vote
		if($voterArr['canVote'] != 1)
			return self::FAILURE;
		
		//Begin the transaction
		$this->beginTransaction();
		
		//Update voter's existing information
		$sql = "UPDATE voter SET voteDate = NOW(), voteMethod = '".self::VOTE_METHOD_PAPER."', hasVoted = 1, electionWorker = {$adminId}\n".
			"WHERE studentNum = '{$studentNum}'\n".
			"AND election = '{$electionId}'";
		$count = $this->db->dbh->exec($sql);
		
		//Roll back the changes if there was a mistake.
		//One row should be affected - no more, no less
		if($count !== 1) {
			$this->rollBack();
			return self::FAILURE;
		}
		
		//Commit the changes if everything went fine
		$this->commit();
		return self::SUCCESS;
	}
	
	/**
	 * This method handles updating and inserting voter data.
	 * 
	 * This method cannot be used to modify these fields on a existing record: election, voteDate, voteMethod, hasVoted.
	 * 
	 * @param array $voterArr
	 * @param int $electionId
	 * @return int
	 */
	public function updateVoter($voterArr, $electionId)
	{	
		$studentNum = (int) $voterArr['studentNum'];
		$studentNumOld = (int) $voterArr['studentNumOld'];
		
		//First check that the election exists
		include_once SERVER_ROOT.'/models/election.php';
		$electionModel = new Election_Model;
		$electionId = (int) $electionId;
		if($electionModel->fetchById($electionId) == false)
			return parent::FAILURE;
				
		//Add election id & studentNum to the parameters				
		$paramArr = array(':election' => $electionId, ':studentNum' => $studentNum);
		
		//Add rest of the fields to the query and to the parameters	
		$validFields = array('firstName', 'lastName', 'email', 'address', 'city', 'zipCode', 'canVote');
		foreach ($voterArr as $key => $val) {
			if(in_array($key, $validFields)) {
				$sql = (!isset($sql))? $key: $sql.", ".$key;				
				$paramArr[':'.$key] = $val;				
			}
		}
		//Add canVote value 0 to the query and to the prarameters if needed (radio button)
		if(!isset($paramArr[':canVote'])) {
			$sql .= ", canVote";
			$paramArr[':canVote'] = 0;
		}
		
		//Begin the transaction
		$this->beginTransaction();
		
		if($studentNum != $studentNumOld) {
			//Look if the new student number is already taken
			if(is_array($this->fetchByCompositeId(array($studentNum, $electionId)))) {
				$this->rollBack();
				return parent::PK_EXISTS_ALREADY;
			}
			
			//Delete existing record if this is not a new record
			if($studentNumOld != 'new') {
				//Prevent deleting existing record if the voter has already voted
				$oldRecord = $this->fetchByCompositeId(array($studentNumOld, $electionId));
				if($oldRecord['hasVoted'] == 1) {
					$this->rollBack();
					return self::VOTER_ALREADY_VOTED;
				}
				
				if($this->removeByCompositeId(array($studentNumOld, $electionId)) === false) {
					$this->rollBack();
					return parent::FAILURE;
				}
			}
										
			//Insert a new record
			$sql = "election, studentNum, ".$sql;					
			if($this->insertInto($sql, $paramArr) === false) {
				$this->rollBack();
				return parent::FAILURE;
			}
		} else { //Update existing record						
			//Prevent updating existing record if the voter has already voted
			$oldRecord = $this->fetchByCompositeId(array($studentNum, $electionId));
			if($oldRecord['hasVoted'] == 1) {
				$this->rollBack();
				return self::VOTER_ALREADY_VOTED;
			}
															
			$sql .= "\nWHERE election = :election AND studentNum = :studentNum";			
			if($this->updateSet($sql, $paramArr) == false) {
				$this->rollBack();
				return parent::FAILURE;
			}				
		}
			
		//Commit the changes if everything went fine
		$this->commit();
		return parent::SUCCESS;				
	}
	
	private function generateWhereWithParams($electionId, $searchStr = null)
	{
		$params = array(':election' => (int) $electionId);
		$where = "\nWHERE election = :election";
		if($searchStr != null) {
			$searchWords = explode("+", $searchStr);
			foreach($searchWords as $index => $word) {
				//Do not react to empty search words
				if(trim($word) == "")
					continue;
				$word = urldecode($word);
				
				$where .= "\nAND ((studentNum LIKE :studentNum{$index}) OR (firstName LIKE :firstName{$index}) OR (lastName LIKE :lastName{$index}))";						
				$params[':studentNum'.$index] = '%'.$word.'%';
				$params[':firstName'.$index] = '%'.$word.'%';
				$params[':lastName'.$index] = '%'.$word.'%';
			}
		}
		return array(0 => $where, 1 => $params);
	}
	
	/**
	 * This method validates the edit form's $_POST parameters and returns the valid input fields and error messages in an array.
	 * 
	 * @param array $postArr
	 * @return array
	 */
	public function validateEditForm($postArr) 
	{
		$voterArr = array();
		$errors = array();
		$formFields = array('firstName', 'lastName', 'studentNum', 'studentNumOld', 'email', 'address', 'city', 'zipCode', 'canVote');
		$mandatory = array('firstName', 'lastName', 'studentNum', 'email');
		$numeric = array('studentNum', 'zipCode');
		
		//Loop through form input and check that every mandatory field was filled
		foreach ($formFields as $field) {
			if(in_array($field, $mandatory) && empty($postArr[$field])) {
				$errors[$field] = MANDATORY_TEXT;
			}
			else if(in_array($field, $numeric) && !is_numeric($postArr[$field]) && !empty($postArr[$field])) {
				$errors[$field] = HAS_TO_BE_A_NUMBER_TEXT;
			}
			 
			if(!isset($errors[$field]))
				$voterArr[$field] = $postArr[$field];
		}

		//Validate email address
		if(!empty($voterArr['email']) && !filter_var($voterArr['email'], FILTER_VALIDATE_EMAIL)) {
			$errors['email'] = INVALID_EMAIL_TEXT;
		}
		
		return array($voterArr, $errors);
	}	
}
