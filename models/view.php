<?php
/**
 * This model handles the view
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class View_Model
{
	private $data = array();
	private $view;
	private $controllerName;
	private $render = false;
	
	/**
	 * Constructor
	 */
	public function __construct($data, $view, $template, $controllerName)
	{
		$this->data = $data;
		$this->view = $view;
		$this->controllerName = $controllerName;
		
		$template = SERVER_ROOT.'/views/templates/'.$template.'.php';
		if(file_exists($template))
			$this->render = $template;	//renderer includes this file when this model is destroyed 
	}
	
	/**
	 * Desctructor
	 */	
	public function __destruct()
	{
		//Parse object's variables into local variables
        $data = $this->data;
        $view = $this->view;
        $controllerName = $this->controllerName;
        
        //render view
        include($this->render);
	}
}