<?php
/**
 * Application model class
 * 
 * This class is the super class to other model classes which use the database
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Model {
	
	//variables
	protected $db = null;
	protected $idColumn = 'id';
	protected $compositeId = array('id', 'election');
	protected $tableName;
        protected $lastSql;
        protected $lastError;
        
        //Result codes
        const FAILURE = 0;
        const SUCCESS = 1;
	const PK_EXISTS_ALREADY = 2; 
			
	/**
	 * Constructor
	 */
	public function __construct() 
	{
		//Start database connection
		include_once SERVER_ROOT.'/database/DB.php';
		$this->db = new DB_Class($GLOBALS['dbConfig']);
                $this->db->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
	}
        
        /*public function getLastSql() 
        {
            return $this->lastSql;
        }

        public function getLastError() 
        {
            return $this->lastError;
        }*/


        public function lastInsertId() 
        {
            return $this->db->dbh->lastInsertId();
        }
	
	public function beginTransaction() 
	{
	    $this->db->dbh->beginTransaction();
	}    

	public function rollBack() 
	{
	    $this->db->dbh->rollBack();
	}

	public function commit() 
	{
	    $this->db->dbh->commit();
	}

        /*public function prepareExecute($sql, $paramArr) 
        {
            if ($this->db->dbh == null)
                return;
            $this->lastSql = $sql;

            $stmt = $this->db->dbh->prepare($sql);
            if ($stmt != null) {
                $this->lastError = $stmt->errorInfo();

                $stmt->execute($paramArr);
                $this->lastError = $stmt->errorInfo();
            }
            return $stmt;
        }*/
	
	/**
	 * Fetches every row from the table and returns an associative array.
	 * 
	 * @return array
	 */
	public function fetchAll()
	{		
		$sql = 'SELECT * FROM '.$this->tableName;					
		$sth = $this->db->dbh->query($sql);
				
		$resultArr = array();		
		foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $row) {
			$resultArr[] = $row;
		}		
		
		return $resultArr;
	}
	
	/**
	 * Fetches a row from table with the given id and returns it as an associative array.
	 * 
	 * @param $id
	 * @return array|false
	 */
	public function fetchById($id)
	{
		if ($this->db->dbh == null) return false;
		$sql = "SELECT * FROM {$this->tableName} WHERE {$this->idColumn} = :id LIMIT 1";

                $stmt = $this->db->dbh->prepare($sql); 
                $stmt->execute(array(':id'=>$id));
 
                if ($stmt && $stmt->rowCount() > 0) {
                    $result = $stmt->fetch(PDO::FETCH_ASSOC);
                    if ($result != null && count($result) >0) {
                        return $result;
                    }
                }
		return false;
	}
        
        
        /**
	 * Fetches a row from table with the given key values and returns it as an associative array.
	 * 
	 * @param $idValues
	 * @return array|false
	 */
	public function fetchByCompositeId($idValues = array())
	{
		if ($this->db->dbh == null) return false;
	  
		if (count($this->compositeId) != count($idValues))
			return false;

		$sql =  "SELECT * FROM {$this->tableName} WHERE\n";

		for($i=0; $i<count($this->compositeId); $i++) {
			if($i > 0)
				$sql .= "AND\n";
			$sql .= "{$this->compositeId[$i]} = ?\n"; //{$idValues[$i]}
		}
		$sql .= "LIMIT 1";

		$stmt = $this->db->dbh->prepare($sql);
		$stmt->execute($idValues);
		if ($stmt && $stmt->rowCount() > 0) {
			$result = $stmt->fetch(PDO::FETCH_ASSOC);
			if ($result != null && count($result) >0) {
				return $result;
			}
		}
		return false;


		$sth = $this->db->dbh->query($sql);
		if($sth !== false)
			return $sth->fetch(PDO::FETCH_ASSOC);
		return false;
	}

        
        /**
         * Fetch rows from table with the given fieldName.
         * Example: $electionModel->fetchByField('status', 1);
         * @param $fieldName, $val
         * @return array|false
         */
        public function fetchByField($fieldName, $val)
	{
		if ($this->db->dbh == null) return false;
		$sql = "SELECT * FROM {$this->tableName} WHERE ".$fieldName." = :val";

                $stmt = $this->db->dbh->prepare($sql); 
                $stmt->execute(array(':val'=>$val));
 
                if ($stmt && $stmt->rowCount() > 0) {
                    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    if ($result != null && count($result) >0) {
                        return $result;
                    }
                }
		return false;
	}
	
	/**
	 * Fetch the first row that has $fieldName = $val
	 * Example: $admin = $adminModel->getFirstByField('username', $username);
	 * @return one row or false (if not found)
	 */
	public function getFirstByField($fieldName, $val)
	    {
	    $res = $this->fetchByField($fieldName, $val);
	    if ($res != false && count($res) >0) {
		return $res[0];
	    }
	    return false;
	}

	/**
	 * Executes PDO::query with given sql parameter and as default returns the results as an associative array.
	 * 
	 * Options for fetching the results:
	 * fetchColumn = int $column_number - if this is set this function returns a value from the wanted column index
	 * singleRow = boolean - if this is set to true this function returns a single result row (associative array)	   
	 * 
	 * @param string $sql
	 * @param array $values holds an array of values for sql query
	 * @param array $options holds an array of different options that can be set 
	 * @return mixed
	 */
	public function fetchQueryResults($sql, $options = array(), $values = array())
	{
		if ($this->db->dbh == null) return false;
		if(empty($values)){
		    $sth = $this->db->dbh->query($sql);
		}else{
		    $sth = $this->db->dbh->prepare($sql); 
		    $sth->execute($values);
		}
		
		if($sth == false) {
			die("Execution of an SQL statement failed.");
		}
		
		//Check if $options have been set
		if(isset($options['fetchColumn'])) {
			if(!is_int($options['fetchColumn']))
				die("The value of 'fetchColumn' parameter was not an integer.");			
			return $sth->fetchColumn($options['fetchColumn']);
		}
		else if(isset($options['singleRow']) && $options['singleRow'] == true) {						
			return $sth->fetch(PDO::FETCH_ASSOC);
		}
		else {
			$resultArr = array();
			foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $row) {
				$resultArr[] = $row;
			}
			return $resultArr;
		}
		
	}
        
        public function countAll()
	{
	        if ($this->db->dbh == null) return;
	        $sql = 'SELECT COUNT(*) FROM '.$this->table;
	 
	        $stmt = $this->db->dbh->prepare($sql);
	        if ($stmt && $stmt->rowCount() > 0) {
	            return $stmt->fetchColumn();
	        }
	        return 0;
	}
    
	/**
	 * Returns the total number of rows matching the given field values.
	 *
	 * @param array $fields
	 * @return int|false
	 */
	public function countByFieldValues($fields)
	{
		if ($this->db->dbh == null || empty($fields))
			return false;
			
		$sql = "SELECT count(*) FROM {$this->tableName}";
	
		$paramArr = array();
		$whereClause = "";
                foreach($fields as $field => $value) {
                    $whereClause = ($whereClause == "")? "\nWHERE ": $whereClause." AND ";
                    $whereClause .= $field." = :".$field;
                    $paramArr[':'.$field] = $value;
                }
                $sql .= $whereClause;
                $stmt = $this->db->dbh->prepare($sql);
                $stmt->execute($paramArr);

                return $stmt->fetchColumn(0);
	}

        public function removeById($id)
	{       
                $sql = "DELETE FROM {$this->tableName} WHERE {$this->idColumn} = '{$id}'";
	        $stmt = $this->db->dbh->prepare($sql); 
                $result = $stmt->execute(array(':id'=>$id));
                if($result !== false)
			return true;
		return false;
	}
        
	public function removeByCompositeId($idValues = array())
	{
		if (count($this->compositeId) != count($idValues))
			return false;
				
		$sql = "DELETE FROM {$this->tableName} WHERE\n";
		
		$executeArr = array();
		for($i=0; $i<count($this->compositeId); $i++) {
			if($i > 0)
            	$sql .= "\nAND ";
			$sql .= "{$this->compositeId[$i]} = :{$this->compositeId[$i]}";
			$executeArr[':'.$this->compositeId[$i]] = $idValues[$i];
		}		
		$sql .= " LIMIT 1";
		
		$stmt = $this->db->dbh->prepare($sql);		
		return $stmt->execute($executeArr);
	}
	
        public function removeByField($fieldName, $val)
	{
		if ($this->db->dbh == null) return false;
                $sql = "DELETE FROM {$this->tableName} WHERE {$fieldName} = :val";
                $stmt = $this->db->dbh->prepare($sql); 
                $result = $stmt->execute(array(':val'=>$val));
                if($result !== false)
			return true;
		return false;
	}
        
	public function update($updateClause, $paramArr)
	{
                $sql = "UPDATE {$this->tableName} SET {$updateClause}";

                $stmt = $this->db->dbh->prepare($sql); 
                $result = $stmt->execute($paramArr);
                if($result !== false)
			return true;
		return false;
	}

        /**
         * Function to execute UPDATE-SET query by generating a full query.
         * Example: $electionModel->updateSet('electionId, status WHERE electionId = :electionId', $paramArr );
         * Array $paramArr is a Key => Value array, has the same number of parameters in the string.
         * @param $strInput, $paramArr
         * @return array|false
         */
        public function updateSet($strInput, $paramArr)
        {
                $pos = strpos($strInput, 'WHERE ');
                if ($pos > 0) {
                    $strWhere = substr($strInput, $pos); // keep WHERE clause
                    $strInput = str_replace($strWhere, '', $strInput); // remove WHERE clause
                }

                $arr = explode(',', $strInput);
                $str = ''; // build VALUES(...)
                for ($i = 0, $cnt = count($arr); $i < $cnt; $i++) {
                    $field = trim($arr[$i]);
                    $str .= ($i == 0 ? "$field = :$field" : ", $field = :$field" );
                }
                $sql = "UPDATE {$this->tableName} SET $str $strWhere";
		
                $stmt = $this->db->dbh->prepare($sql); 
                $result = $stmt->execute($paramArr);
                if($result !== false)
                        return true;
                return false;
        }

        /**
         * Function to execute INSERT INTO query by generating a full query.
         * Example: $adminModel->insertInto('adminId, username, password, email', $values);
         * Inserted id can be get with function lastInsertId()
         * @param $strInput, $paramArr
         * @return array|false
         */
        public function insertInto($strInput, $paramArr)
        {
                $arr = explode(',', $strInput);
                $str = ''; 
                for ($i = 0, $cnt = count($arr); $i < $cnt; $i++) {
                    $field = trim($arr[$i]);
                    $str .= ($i == 0 ? ":$field" : ", :$field");
                }
                $sql = "INSERT INTO {$this->tableName} ({$strInput}) VALUES($str)";
                
                $stmt = $this->db->dbh->prepare($sql);
                $result = $stmt->execute($paramArr);
                if($result !== false)
                	return true;
                return false;
        }

	/**
	* Prepares pagination data which can be used in the view.
	*
	* @param int $limit
	* @param int $offset
	* @param int $totalNum
	* @return array
	*/
	public function getPaginationData($limit, $offset, $totalNum)
	{
	    //Round the offset value down to nearest page offset
	    $offset = floor($offset/$limit) * $limit;
	    if($offset > $totalNum): $offset = floor($totalNum/$limit) * $limit; endif;
	    if($offset < 0): $offset = 0; endif;

	    //Page numbers
	    $totalPages = ceil($totalNum/$limit);
	    $curPage = (($offset/$limit+1) > $totalPages)? $totalPages: ($offset/$limit+1);
	    if($curPage < 1): $curPage = 1; endif;

	    //Maximum offset
	    $maxOffset = (floor($totalNum/$limit) * $limit);
	    if($maxOffset == $totalNum): $maxOffset -= $limit; endif;
	    if($maxOffset < 0): $maxOffset = 0; endif;
	    if($maxOffset < $offset): $offset = $maxOffset; endif;

	    //Pagination data
	    return array('offset' => $offset, 'limit' => $limit, 'totalPages' => $totalPages, 'curPage' => $curPage, 'maxOffset' => $maxOffset);
	}
            
        
	/**
	 * Desctructor
	 */
	public function __destruct()
	{
		//Close the database connection
		if($this->db != null)
			$this->db->close();
	}
}
