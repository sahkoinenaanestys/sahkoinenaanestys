<?php
/**
 * Controller for the instructions page
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Instructions_Controller extends Controller
{
	public $view = 'instructions';
	
	/**
	 * Called by router.php
	 * 
	 * @param array $getVars
	 */
	public function main($getVars = array())
	{
		
		// If we come from voting pages after the count down for automatic logout has been started
		if(isset($_SESSION['logoutAfterVote']) && $_SESSION['logoutAfterVote'] == true) {
			header('Location: '.SITE_ROOT.'/index.php?logout&action=samlLogout');
			exit;
		}
		
		$data = array();
		if($this->isLoggedIn === true) {
			//Load model file(s)
			$this->loadModelFiles(array('voter'));
			$voterModel = new Voter_Model;
			
			//Check if the voter has already voted
			if($voterModel->hasVoted($_SESSION[$this->userId]['studentNum']) === true) {
				header('Location: '.SITE_ROOT.'/index.php?vote&action=given');
				exit;
			}
			
			//Check if the voter can vote
			if($voterModel->canVote($_SESSION[$this->userId]['studentNum']) === false) {
				header('Location: '.SITE_ROOT.'/index.php?logout&error=logoutCannotVote');
				exit;
			}									
		}		
		$this->loadView($data);		
	}			
}