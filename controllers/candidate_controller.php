<?php
/**
 * Controller for candidates
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Candidate_Controller extends Controller
{
	public $view = 'candidates_show';
	
        /**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	
		//Prevent users that are not logged in as admins from accessing these pages
		if($this->isLoggedIn === false){
                        header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=invalidorexpiredsession');
			exit;
		}
		else if($this->isAdmin === false){
			header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=noaccessprivileges'); 
			exit;
		}
	}
        
	/**
	 * Called by router.php
	 * 
	 * @param array $getVars
	 */
	public function main($getVars = array())
	{		
                $this->template = 'template_adm2';
                $data = array('loggedIn' => true);

                if(!isset($getVars['view'])): $getVars['view'] = ''; endif;
                
		switch($getVars['view']){
                    
		    case 'candidates_import':
			    $this->loadModelFiles(array('election'));
			    $electionModel = new Election_Model;

			    $electionId = $_SESSION[$this->userId]['activeElection'];

			    //Fetch election information & redirect if election not found
			    $data['election'] = $electionModel->fetchById($electionId);
			    if(empty($data['election'])) {
				    header('Location: '.SITE_ROOT.'/index.php?election');
				    exit;
			    }

			    $this->view = 'candidates_import';
			    $this->loadView($data);
			break;

		    default:
			$this->loadModelFiles(array('candidate', 'election'));
			$candidateModel = new Candidate_Model;
			$electionModel = new Election_Model;

			//Search string
			if(isset($_POST['search'])) {
				//Redirect to same page but this time the search string is in get variable (Post/Redirect/Get Pattern)
				header('Location: '.SITE_ROOT.'/index.php?candidate&search='.urlencode($_POST['search']));
				exit;
			}
			else if(isset($getVars['search'])) {
				$data['searchStr'] = $getVars['search'];
			}
			else {
				$data['searchStr'] = null;
			}
			
			
			$electionId = $_SESSION[$this->userId]['activeElection'];

			//Fetch election information & redirect if election not found
			$data['election'] = $electionModel->fetchById($electionId);
			if(empty($data['election'])) {
			    header('Location: '.SITE_ROOT.'/index.php?election');
			    exit;
			}
			$data = $this->messages($getVars, $data);

			//Pagination data
			$limit = 15;	//Number of candidates per page
			$offset = (isset($getVars['offset']))? (int) $getVars['offset']: 0;

			//Fetch total number of rows
			$totalRows = $candidateModel->countByElectionAndSearch($electionId, $data['searchStr']);

			//Pagination data
			$data['pagination'] = $candidateModel->getPaginationData($limit, $offset, $totalRows);

			if(($candidates = $candidateModel->fetchCandidatesAlliancesCoalitions($electionId, $data['pagination']['limit'], $data['pagination']['offset'], $data['searchStr'])) != false) {   
			    $data['candidates'] = $candidates;
			}
			$this->view = 'candidates_show';
			$this->loadView($data);
			break;
		}//switch			
	}
        
        
	public function editCandidate($getVars = array())
	{		
                $this->template = 'template_adm2';
		$data = array('loggedIn' => true);

		if(!isset($_SESSION[$this->userId]['activeElection'])) {
			header('Location: '.SITE_ROOT.'/index.php?election');
			exit;
		}    	
		$data['electionId'] = (int) $_SESSION[$this->userId]['activeElection'];

		$this->loadModelFiles(array('election','candidate','alliance', 'coalition'));
		$electionModel = new Election_Model;
		$candidateModel = new Candidate_Model;
		$back = SITE_ROOT.'/index.php?candidate';

		$data['election'] = $electionModel->fetchById($data['electionId']);
		if(empty($data['election'])) {
			header('Location: '.SITE_ROOT.'/index.php?election');
			exit;
		}

		if(isset($_POST['candidateSubmit'])) { //form was submitted
			$back = $_POST['back'];

			$this->loadModelFiles(array('coalition'));
			list($candidateArr, $formErrors) = $candidateModel->validateEditForm($_POST);    		

			if(empty($formErrors)) { //no input errors  	
				if($_POST['candidateNumOld'] == 'new') { //Insert a new candidate
				    
					$resultCode = $candidateModel->updateCandidate($candidateArr, $data['electionId']); 				    				

					switch ($resultCode) {
						case Candidate_Model::PK_EXISTS_ALREADY:
							$data['error'] = INSERT_FAILED_TEXT."<br>".CANDIDATE_NUMBER_ALREADY_TAKEN_TEXT;
							$data['formErrors']['candidateNum'] = '!';
							break;
						case Candidate_Model::SUCCESS:
							header('Location: '.$back.'&success=added_new');
							exit;
							break;    							
						default:
							$data['error'] = INSERT_FAILED_TEXT;
							break;    					
					}
				}else {    //Edit candidate 				
					$resultCode = $candidateModel->updateCandidate($candidateArr, $data['electionId']);

					switch ($resultCode) {
						case Candidate_Model::PK_EXISTS_ALREADY:
							$data['error'] = UPDATE_FAILED_TEXT."<br>".CANDIDATE_NUMBER_ALREADY_TAKEN_TEXT;   
							$data['formErrors']['candidateNum'] = '!';
							break;
						case Candidate_Model::SUCCESS:
							header('Location: '.SITE_ROOT.'/index.php?candidate&success=edited');
							exit;
							break;
						default:
							$data['error'] = UPDATE_FAILED_TEXT;
							break;
					}
				}   			
			}else { //has input errors
				$data['error'] = INVALID_INPUT_PLEASE_CHECK_FIELDS_TEXT;
				$data['formErrors'] = $formErrors;
			}
			//Repopulate the form fields
			$candidateNumOld = (isset($data['error']))? $_POST['candidateNumOld']: $_POST['candidateNum'];
			$data['candidate'] = array
			(
				'candidateNum' => $_POST['candidateNum'],
				'candidateNumOld' => $candidateNumOld,
				'firstName' => $_POST['firstName'],
				'lastName' => $_POST['lastName'],
				'alliance' => $_POST['allianceId'],
				'coalition' => $_POST['coalitionId'],
				'email' => $_POST['email']
			);
			$allianceModel = new Alliance_Model;
			$coalitionModel = new Coalition_Model;
			$data['alliances'] = $allianceModel->fetchByField('election', $data['electionId']);
			$data['coalitions'] = $coalitionModel->fetchByField('election', $data['electionId']);
			
		}else {  //form has not been submitted  
		    
			$allianceModel = new Alliance_Model;
			$coalitionModel = new Coalition_Model;
			$data['alliances'] = $allianceModel->fetchByField('election', $data['electionId']);
			$data['coalitions'] = $coalitionModel->fetchByField('election', $data['electionId']);
		    
			if(isset($getVars['candidateNum'])) { //edit candidate
			    $candidateNum = $getVars['candidateNum'];
			    
			    $candidateModel = new Candidate_Model;
			    $data['candidate'] = $candidateModel->fetchCandidateByCompositeId($candidateNum, $data['electionId']);
			    if(empty($data['candidate'])) {
				    header('Location: '.SITE_ROOT.'/index.php?candidate');
				    exit;
			    }
			    $data['candidate']['candidateNumOld'] = $data['candidate']['candidateNum'];	
			    
			}else{ //new candidate
			    $data['candidate'] = array('candidateNum'=>'', 'candidateNumOld'=>'new', 'firstName'=>'', 'lastName'=>'', 'alliance'=>0, 'coalition'=>0, 'email'=>'');
			    
			    //redirect back to previous page
			    if(isset($getVars['back']) && $getVars['back'] == "showActiveElection"){
				$back = SITE_ROOT.'/index.php?election&action='.$getVars['back'].'&electionId='.$data['electionId'];
			    }
			}
		} 
		
		if($data['election']['status'] != Election_Model::STATE_CLOSED){
			header('Location: '.$back.'&error=cannoteditcandidate');
			exit;
		}
		$data['returnPage'] = $back;
		$this->view = 'candidate_edit';
		$this->loadView($data);			
	}
	
	public function removeAllCandidates($getVars = array()) 
	{
	    $data = array('loggedIn' => true);

	    $this->loadModelFiles(array('election', 'candidate'));
	    $candidateModel = new Candidate_Model;
	    $electionId = (int) $_SESSION[$this->userId]['activeElection'];
	    
	    $electionModel = new Election_Model;
	    $election = $electionModel->fetchById((int) $electionId);
	    if(empty($election)){
		header('Location: '.SITE_ROOT.'/index.php?election');
		exit;
	    }
	    if($election['status'] != Election_Model::STATE_CLOSED){
		header('Location: '.SITE_ROOT.'/index.php?candidate&error=cannot_remove_candidates');
		exit;
	    }

	    if(isset($_POST['bulkDeleteCandidates'])) {
		
		    if($candidateModel->checkIfHasVotes($electionId) == false) {    		   		    		
			header('Location: '.SITE_ROOT.'/index.php?candidate&error=candidatehasvotes');
			exit;
		    }
		    $result = $candidateModel->removeAllCandidates($electionId);
		    
		    if($result === true) {
			    header('Location: '.SITE_ROOT.'/index.php?candidate&success=removed_all');
			    exit;
		    }else {
			    $data['error'] = REMOVE_FAILED_TEXT;
		    }
	    }    	
	    $this->view = 'candidates_bulk_delete';
	    $this->loadView($data);
	}
        
        public function removeCandidateAjax($getVars = array())
	{
                $result = array();
		$result['status'] = false;
		
                $candidateNum = $getVars['candidateNum'];
                $electionId = (int)$getVars['electionId'];

                $this->loadModelFiles(array('candidate', 'election'));
                $candidateModel = new Candidate_Model; 
		
		$electionModel = new Election_Model;
		$election = $electionModel->fetchById((int) $electionId);
		if(empty($election)){
		    echo json_encode($result);
		    exit;
		}
		if($election['status'] != Election_Model::STATE_CLOSED){
		    $result['msg'] = CANDIDATE_CANNOT_BE_REMOVED_TEXT.'. '.MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT;
		    echo json_encode($result);
		    exit;
		}
		
		if($candidateModel->checkIfHasVotes($electionId, $candidateNum) == false) {    		   		    		
			$result['msg'] = CANDIDATE_CANNOT_BE_REMOVED_TEXT.'. '.CANDIDATE_HAS_VOTES_TEXT;
			echo json_encode($result);
			exit;
		}
		if($candidateModel->removeByCompositeId(array($candidateNum, $electionId)) === false) {
		    $result['msg'] = REMOVE_FAILED_TEXT;
		}else{
		    $result['status'] = true; 
		    $result['msg'] = REMOVED_SUCCESSFULLY_TEXT;
		}      
                echo json_encode($result);
        }
        
        public function importFromCsvAjax($getVars = array())
        {
		$electionId = (int)$_SESSION[$this->userId]['activeElection'];
		$this->loadModelFiles(array('election'));
		$electionModel = new Election_Model;

		$election = $electionModel->fetchById($electionId);
		if(empty($election) || $election['status'] != Election_Model::STATE_CLOSED){
		    echo '<div class="errorMsgDiv">'.MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT.'</div>';
		    exit;
		}
	    
		$data = parent::uploadFile();
                
                $response = json_decode($data);
                if($response->{'status'} == true){
                    $filename = $response->{'file'};
                    
                    $minCols = 4+1;
                    $maxCols = 5+1;
                    $errors = array();
                    $electionId = $_SESSION[$this->userId]['activeElection'];
                    
                    parent::changeFileEncoding($filename, UPLOAD_DIR, 'UTF-8');
                    
                    require_once SERVER_ROOT.'/lib/parsecsv/parsecsv.lib.php';
                    require_once SERVER_ROOT.'/lib/ForceUTF8/Encoding.php';
                    
                    $csv = new parseCSV();
                    $csv->encoding('UTF-8', 'UTF-8');
                    $csv->delimiter = isset($_POST['csvDelimiter']) ? $_POST['csvDelimiter'] : ","; 
                    $csv->parse(UPLOAD_DIR.$filename); 
                        
                    $this->loadModelFiles(array('candidate', 'alliance'/*, 'coalition'*/));
                    $candidateModel = new Candidate_Model; 

                    foreach($csv->data as $rowIndex => $row){

			if(count($row) < $minCols || count($row) > $maxCols){
				$errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).') '.FILE_INVALID_COLUMNS_AMOUNT_TEXT;
				break;
			}
			
			$candidate = array();
			$i = 0;				
			foreach($row as $value) {
				switch ($i) {
					case 0: $candidate['candidateNum'] = $value; break;
					case 1: $candidate['firstName'] = Encoding::fixUTF8($value); break;
					case 2: $candidate['lastName'] = Encoding::fixUTF8($value); break;
					case 3: $candidate['email'] = Encoding::fixUTF8($value); break;
					case 4: $candidate['allianceId'] = $value; break;
				}
				$i++;
			}
                        
                        if(is_numeric($candidate['candidateNum'])){
                            $result = $candidateModel->insertCandidate($electionId, $candidate['candidateNum'],$candidate['lastName'],$candidate['firstName'],$candidate['email'],$candidate['allianceId']);

                            if($result == Candidate_Model::FAILURE){
                                $errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).') '.CANDIDATE_INSERT_FAILED_TEXT.' '.$candidate['candidateNum'].': '.$candidate['lastName'].' '.$candidate['firstName'];
				break;
			    }else if($result == Candidate_Model::PK_EXISTS_ALREADY){
                                $errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).') '.CANDIDATE_TEXT.' '.$candidate['candidateNum'].' '. EXISTS_ALREADY_TEXT;
				break;
			    }
                        }else{
                            $errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).') '.CANDIDATE_INSERT_FAILED_TEXT.' '.$candidate['candidateNum'].': '.$candidate['lastName'].' '.$candidate['firstName'];
			    break;
                        }
                    }//foreach 
                    
                    if(count($errors) > 0){ 
			echo '<div class="errorMsgDiv">';
                        foreach($errors as $error){
			    echo $error.'<br>';
			} 
			echo '</div>';
                    }else{
			echo '<div class="successMsgDiv">'.CANDIDATES_IMPORTED_SUCCESSFULLY_TEXT.'</div>';
                    }
		    unlink(UPLOAD_DIR.$filename); 
                }else{
		    echo '<div class="errorMsgDiv">'.$response->{'msg'}.'</div>';
                }
        }
	
	//not used
	/*public function addAllianceAjax($getVars = array())
	{
		$result = array();
		$result['status'] = false;
		$result['msg'] = GENERAL_ERR_TEXT;
		
		$allianceName = isset($getVars['allianceName']) ? urldecode(trim($getVars['allianceName'])) : "";
		$allianceId = $getVars['allianceId'];
		$electionId = $_SESSION[$this->userId]['activeElection'];
		
		if(!is_numeric($allianceId)){
		    $result['msg'] = HAS_TO_BE_A_NUMBER_TEXT;
		    echo json_encode($result);
		    exit;
		}
		
                $this->loadModelFiles(array('candidate', 'election'));
		$electionModel = new Election_Model;
		$election = $electionModel->fetchById((int) $electionId);
		if(empty($election)){
		    echo json_encode($result);
		    exit;
		}
		if($election['status'] != Election_Model::STATE_CLOSED){
		    $result['msg'] = MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT;
		    echo json_encode($result);
		    exit;
		}

		if($allianceId > 0){
		    $this->loadModelFiles(array('alliance'));
		    $allianceModel = new Alliance_Model; 

		    $data['result'] = $allianceModel->fetchByCompositeId(array($allianceId, $electionId));

		    if(empty($data['result'])){ //vaaliliittoa ei löydy ennestään
			$params = array('allianceId'=>$allianceId, 'allianceName'=>$allianceName, 'coalitionId'=>0);
			$data['alliance'] = $allianceModel->insertAlliance($params, $electionId);
			if($data['alliance'] !== false){
			    $result['status'] = true;
			    $result['msg'] = ALLIANCE_ADDED_SUCCESSFULLY_TEXT;
			    $result['allianceId'] = $allianceId;
			    $result['allianceName'] = ID_TEXT.': '.$allianceId.', '.mb_strtolower(NAME_TEXT, 'UTF-8').': '.$allianceName;
			}else{
			    $result['msg'] = INSERT_FAILED_TEXT;
			}
		    }else{
			$result['msg'] = ALLIANCE_WITH_GIVEN_ID_EXISTS_TEXT;
		    }
		}else{
		    $result['msg'] = MANDATORY_FIELDS_EMPTY_TEXT;
		}
                echo json_encode($result);
	}*/
	
	public function getCoalitionAjax($getVars = array())
	{
		$result = array();
		$result['status'] = false;
		
		$allianceId = $getVars['allianceId'];
		$electionId = $_SESSION[$this->userId]['activeElection'];
		
		if($allianceId > 0){
		
		    $this->loadModelFiles(array('alliance', 'coalition'));
		    $allianceModel = new Alliance_Model; 

		    $data['coalition'] = $allianceModel->fetchCoalitionByAlliance($allianceId, $electionId);

		    if($data['coalition'] !== false){ 
			$result['status'] = true;
			$result['coalitionName'] = $data['coalition']['coalitionName'];
			$result['coalitionId'] = $data['coalition']['coalitionId'];
		    }else{
			$result['msg'] = "Could not find a match";
		    }
		}else if($allianceId == 0){
		    $result['status'] = true;
		    $result['coalitionName'] = "";
		    $result['coalitionId'] = 0;
		}else{
		    $result['msg'] = "Invalid alliance";
		}
                echo json_encode($result);
	}
	
//	public function searchAllianceAjax($getVars = array())
//	{
//                $allianceName = $getVars['allianceName'];
//		$electionId = $_SESSION[$this->userId]['activeElection'];
//
//                $this->loadModelFiles(array('alliance', 'coalition'));
//                $allianceModel = new Alliance_Model; 
//		
//		$data['alliances'] = $allianceModel->searchAlliancesByName($allianceName, $electionId);
//
//                if($data['alliances'] !== false) {
//		    /*$this->loadModelFiles(array('coalition'));
//		    $coalitionModel = new Coalition_Model; 
//		    
//		    $data['coalition'] = $coalitionModel->searchByName($coalitionName, $electionId);
//		    
//		    if(!empty($data['coalition'])) {
//			$data['alliance']['coalition'] = $data['coalition'];
//		    }else{
//			$data['alliance']['coalition'] = array();
//		    }*/
//		    
//
//		    echo json_encode($data['alliances']);
//                }else{
//		    echo json_encode('');
//		}
//        }
//	
//	public function searchCoalitionAjax($getVars = array())
//	{
//                $coalitionName = $getVars['coalitionName'];
//		$electionId = $_SESSION[$this->userId]['activeElection'];
//
//                $this->loadModelFiles(array('coalition'));
//                $coalitionModel = new Coalition_Model; 
//		
//		$data['coalitions'] = $coalitionModel->searchCoalitionsByName($coalitionName, $electionId);
//
//                if($data['coalitions'] !== false) {
//                    echo json_encode($data['coalitions']);
//                }else{
//		    echo json_encode('');
//                }
//        }
	
	/*
	 * Used for getting candidate votes for manual results calculation
	 * 
	 * @param array getVars 
	 */
	public function exportCandidateVotes($getVars = array())
        {
                $electionId = $_SESSION[$this->userId]['activeElection'];
		$this->loadModelFiles(array('election','candidate'));
		$electionModel = new Election_Model;
		$candidateModel = new Candidate_Model;
		
		$election = $electionModel->fetchById($electionId);
		if(empty($election)){
		    header('Location: '.SITE_ROOT.'/index.php?election');
                    exit;
		}
		
		if($election['status'] == Election_Model::STATE_ENDED){ 
		    $candidates = $candidateModel->fetchCandidateVotes($electionId);
		    
		    if(!is_null($candidates) || !empty($candidates)){

			require_once SERVER_ROOT.'/lib/parsecsv/parsecsv.lib.php';

			$csv = new parseCSV();
			$csv->encoding('UTF-8', 'UTF-8');
			$fileName = date('d.m.Y').'_'.mb_strtolower($election['name'],'UTF-8').'_'.'candidate_votes.csv';
			$csvHeader = array(CANDIDATE_NUM_TEXT,NAME_TEXT,VOTES_TEXT,ALLIANCE_ID_TEXT,ALLIANCE_NAME_TEXT,COALITION_ID_TEXT,COALITION_NAME_TEXT); 

			//convert into 2D array according to header
			$csvData = array();
			foreach($candidates as $candidate){
			    $candidateNum = $candidate['candidateNum'] == Candidate_Model::EMPTY_VOTE ? "" : $candidate['candidateNum'];
			    $candidateName = $candidate['candidateNum'] == Candidate_Model::EMPTY_VOTE ? EMPTY_VOTES_TEXT : $candidate['firstName'].' '.$candidate['lastName'];
			    $votes = (int)$candidate['votes']+(int)$candidate['paperVotes'];
			    $allianceId = $candidate['allianceId'];
			    $allianceName = $candidate['allianceName'];
			    $coalitionId = $candidate['coalitionId'];
			    $coalitionName = $candidate['coalitionName'];
			    
			    $csvData[] = array($candidateNum,$candidateName,$votes,$allianceId,$allianceName,$coalitionId,$coalitionName);
			}
			echo '';
			$csv->output($fileName, $csvData, $csvHeader, CSV_SEMICOLON_DELIMITER);
		    }
		}else{
		    header('Location: '.SITE_ROOT.'/index.php?election&view=election_results&error=electionnotended');
                    exit;
		}  
        }
		
	private function messages($messages = array(), $data) 
	{		
		if (isset($messages['success'])) {
		    switch ($messages['success']) {
			case 'added_new':
			    $data['success'] = CANDIDATE_ADDED_SUCCESSFULLY_TEXT;
			    break;
			case 'edited':
			    $data['success'] = CANDIDATE_UPDATED_SUCCESSFULLY_TEXT;
			    break;
			case 'removed_all':
			    $data['success'] = REMOVED_SUCCESSFULLY_TEXT;
			    break;
			case 'candidateremoved':
			    $data['success'] = REMOVED_SUCCESSFULLY_TEXT;
			    break;
		    }
		}else if(isset($messages['error'])) {
		    switch ($messages['error']) {
			case 'cannoteditcandidate':
			    $data['error'] = MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT;
			    break;
			case 'candidatehasvotes':
			    $data['error'] = REMOVE_FAILED_TEXT.' '.CANDIDATE_HAS_VOTES_TEXT;
			    break;
			case 'cannot_remove_candidate':
			    $data['error'] = CANDIDATE_CANNOT_BE_REMOVED_TEXT.'. '.MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT;
			    break;
			case 'cannot_remove_candidates':
			    $data['error'] = CANDIDATES_CANNOT_BE_REMOVED_TEXT.' '.MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT;
			    break;
		    }
		}
		return $data;
	}
       
}
