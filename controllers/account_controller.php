<?php
/**
 * Controller for the account pages. 
 * 
 * This controller should only be accessed by the administrator.
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Account_Controller extends Controller
{
	public $view = 'account_list';
	public $template = 'template_adm';
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		//Prevent users that are not logged in as admins from accessing these pages
		if($this->isLoggedIn === false || $this->isAdmin === false)
		{
			header('Location: '.SITE_ROOT.'/');
			exit;
		}
	}
	
	/**
	 * Called by router.php
	 * 
	 * @param array $getVars
	 */
	public function main($getVars = array())
	{
	    //__Contstruct checks that the user is logged in
	    $data = array('loggedIn' => true); 

	    //Search string
	    if(isset($_POST['search'])) {
	    	//Redirect to same page but this time the search string is in get variable (Post/Redirect/Get Pattern)
	    	header('Location: '.SITE_ROOT.'/index.php?account&search='.urlencode($_POST['search']));
	    	exit;
	    }
	    else if(isset($getVars['search'])) {
	    	$data['searchStr'] = $getVars['search'];
	    }
	    else {
	    	$data['searchStr'] = null;
	    }
	    	    
	    //Load model file(s)
	    $this->loadModelFiles(array('admin'));
	    $adminModel = new Admin_Model;

	    $data = $this->messages($getVars, $data);
	    
	    //Pagination data
	    $limit = 15;	//Number of admin accounts per page
	    $offset = (isset($getVars['offset']))? (int) $getVars['offset']: 0;
	    
	    //Fetch total number of rows
	    $totalRows = $adminModel->countBySearch($data['searchStr']);	    	    	

	    //Pagination data
	    $data['pagination'] = $adminModel->getPaginationData($limit, $offset, $totalRows);    	

	    //Fetch admins
	    $data['admins'] = $adminModel->fetchAdminAccounts($data['pagination']['limit'], $data['pagination']['offset'], $data['searchStr']);
	    $this->loadView($data);
	}			
        
	public function edit($getVars = array())
	{
	    //__Contstruct checks that the user is logged in
	    $data = array('loggedIn' => true);    	    

	    //Load model file(s)
	    $this->loadModelFiles(array('admin'));
	    $adminModel = new Admin_Model;

	    //Check if form was submitted
	    if(isset($_POST['accountSubmit'])) {
		    //Validate the input
			    list($adminArr, $formErrors) = $adminModel->validateEditForm($_POST);    		

		    if(empty($formErrors)) {     			    			    			
			    $isUpdate = (empty($_POST['adminId']))? false: true;

			    //Check if $_SESSION data needs to be updated
			    $updateSession = false;
			    $curAdmin = $adminModel->fetchByField('username', $_SESSION[$this->userId]['username']);    			
			    if(isset($curAdmin[0]['adminId'])
					    && $curAdmin[0]['adminId'] == (int) $adminArr['adminId']
					    && $_SESSION[$this->userId]['username'] != $adminArr['username']
					    && $isUpdate == true) {
				    $updateSession = true;
			    }

			    //Insert/Update administrator
			    $resultCode = $adminModel->updateAdmin($adminArr);    			    			    			

			    //Set the result message
			    switch ($resultCode) {
				    case Admin_Model::PK_EXISTS_ALREADY:
					    if($isUpdate)
						    $data['error'] = UPDATE_FAILED_TEXT."<br>".USERNAME_ALREADY_TAKEN_TEXT;
					    else 
						    $data['error'] = INSERT_FAILED_TEXT."<br>".USERNAME_ALREADY_TAKEN_TEXT;
					    $data['formErrors']['studentNum'] = '!';
					    break;
				    case Admin_Model::MUST_BE_AT_LEAST_ONE_ADMIN:
					    if($isUpdate)
						    $data['error'] = UPDATE_FAILED_TEXT."<br>".MUST_BE_AT_LEAST_ONE_ADMIN_TEXT;
					    else
						    $data['error'] = INSERT_FAILED_TEXT."<br>".MUST_BE_AT_LEAST_ONE_ADMIN_TEXT;
					    $data['formErrors']['adminLevel'] = MUST_BE_AT_LEAST_ONE_ADMIN_TEXT;
					    break;
				    case Admin_Model::SUCCESS:
					    if($isUpdate) { 
						    //$data['success'] = ADMIN_INFORMATION_UPDATED_SUCCESSFULLY_TEXT;

						    if($updateSession == true) {
							    $tmpArr = $adminModel->fetchById((int) $adminArr['adminId']);
							    $_SESSION[$this->userId]['username'] = $tmpArr['username'];
							    unset($tmpArr);
						    }
						    header('Location: '.SITE_ROOT.'/index.php?account&success=adminedited');
						    exit;
					    }
					    else { 
						    header('Location: '.SITE_ROOT.'/index.php?account&success=adminadded');
						    exit;
					    }    					
					    break;
				    default:
					    if($isUpdate)
						    $data['error'] = UPDATE_FAILED_TEXT;
					    else 
						    $data['error'] = INSERT_FAILED_TEXT;
					    break;
			    }

				    if($isUpdate)
					    $adminId = (int) $adminArr['adminId'];				
				    else if(isset($data['success']))
					    $adminId = (int) $adminModel->lastInsertId;
				    else
					    $adminId = 0;    			    			
		    }
		    else {
			    $data['error'] = INVALID_INPUT_PLEASE_CHECK_FIELDS_TEXT;
			    $data['formErrors'] = $formErrors;

			    $adminId = (empty($_POST['adminId']))? 0: (int) $_POST['adminId'];
		    }

		    //Repopulate the form fields    		
		    $data['admin'] = array
		    (
			    'username' => $_POST['username'],
			    'firstName' => $_POST['firstName'],
			    'lastName' => $_POST['lastName'],
			    'email' => $_POST['email'],
			    'address' => $_POST['address'],
			    'city' => $_POST['city'],
			    'zipCode' => $_POST['zipCode'],
			    'adminId' => $adminId,
			    'adminLevel' => $_POST['adminLevel']
		    );
	    }
	    else {
		    //Give the form some dummy variables if user is not modifying an existing administrator
		    if(!isset($getVars['adminId'])) {
			    $data['admin'] = array('firstName' => '', 'lastName' => '', 'email' => '', 'address' => '', 'city' => '', 'zipCode' => '', 'username' => '', 'adminLevel' => 10);    			
		    }
		    else {
			    //Fetch administrator information & redirect if the administrator is not found
			    $data['admin'] = $adminModel->fetchById((int)$getVars['adminId']);
			    if(empty($data['admin'])) {
				    header('Location: '.SITE_ROOT.'/index.php?account');
				    exit;
			    }    			
		    }
	    }

	    $this->view = 'account_edit';
	    $this->loadView($data);
	}


	public function ajaxRemove($getVars = array())
	{
	    $result = array('success' => false, 'msg' => '');

	    //Prevent deleting own account
	    if($getVars['username'] == $_SESSION[$this->userId]['username']) {
		    $result['msg'] = OWN_ACCOUNT_CANNOT_BE_REMOVED_TEXT;
		    echo json_encode($result);
		    exit;
	    }

	    //Load model file(s)
	    $this->loadModelFiles(array('admin'));
	    $adminModel = new Admin_Model;

	    //Prevent deleting an account which has marked votes.
	    if($adminModel->hasMarkedVotes($getVars['username'])) {
		    $result['msg'] = ACCOUNT_CANNOT_BE_REMOVED_BECAUSE_OF_MARKED_VOTES_TEXT;
		    echo json_encode($result);
		    exit;
	    }

	    //Check that this administrator exists
	    $adminArr = $adminModel->fetchByField('username', $getVars['username']);
	    if(empty($adminArr)) {
		    $result['msg'] = ADMIN_WAS_NOT_FOUND_TEXT;
		    echo json_encode($result);
		    exit;
	    }

	    //Delete administrator
	    $adminModel->beginTransaction();
	    $removed = $adminModel->removeById($adminArr[0]['adminId']);
	    if($removed == false) {
		    $adminModel->rollBack();
		    $result['msg'] = REMOVE_FAILED_TEXT;
		    echo json_encode($result);
		    exit;
	    }    	
	    $adminModel->commit();

	    $result['success'] = true;
	    echo json_encode($result);
	    exit;
	}        

	public function ajaxFetchInfo($getVars = array())
	{
	    $result = array('success' => false, 'msg' => '');

	    //Load model file(s)
	    $this->loadModelFiles(array('admin'));
	    $adminModel = new Admin_Model;

	    //Check that this administrator exists
	    $adminArr = $adminModel->fetchById($getVars['adminId']);
	    if(empty($adminArr)) {
		    $result['msg'] = ADMIN_WAS_NOT_FOUND_TEXT;
		    echo json_encode($result);
		    exit;
	    }

	    //Put admin info in an html table
	    $result['infoTable'] = '<table class="loginInformation">'.
		    '<tr><th>'.USERNAME_TEXT.':</th><td>'.$adminArr['username'].'</td></tr>'.
		    '<tr><th>'.NAME_TEXT.':</th><td>'.$adminArr['firstName'].' '.$adminArr['lastName'].'</td></tr>'.
		    '<tr><th>'.EMAIL_TEXT.':</th><td>'.$adminArr['email'].'</td></tr>'.
			    '</table>';

	    $result['success'] = true;
	    echo json_encode($result);
	    exit;
	}

	private function messages($messages = array(), $data) 
	{		
		if (isset($messages['success'])) {
		    switch ($messages['success']) {
			case 'adminadded':
			    $data['success'] = NEW_ADMIN_ADDED_SUCCESSFULLY_TEXT;
			    break;
			case 'adminedited':
			    $data['success'] = ADMIN_INFORMATION_UPDATED_SUCCESSFULLY_TEXT;
			    break;
			case 'adminremoved':
			    $data['success'] = REMOVED_SUCCESSFULLY_TEXT;
			    break;
			case 'removedAllSuccessfully':
			    $data['success'] = REMOVED_SUCCESSFULLY_TEXT;
			    break;
		    }
		}else if(isset($messages['error'])) {
		    switch ($messages['error']) {
			default:
			    $data['error'] = GENERAL_ERR_TEXT;
			    break;
		    }
		}
		return $data;
	}
}
