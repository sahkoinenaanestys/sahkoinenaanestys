<?php
/**
 * Controller for the language selecting functionality
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Language_Controller extends Controller
{	
	/**
	 * Called by router.php
	 * 
	 * @param array $getVars
	 */
	public function main($getVars = array())
	{
		//Set languge to $_SESSION data
		if(in_array($getVars['lang'], array('FIN', 'ENG')))
			$_SESSION['language'] = $getVars['lang'];

		// If we come from voting pages after the count down for automatic logout has been started
		if(isset($_SESSION['logoutAfterVote']) && $_SESSION['logoutAfterVote'] == true) {
			header('Location: '.SITE_ROOT.'/index.php?logout&action=samlLogout');
			exit;
		}
		
		if($_SESSION['previous_url'] == "/" && isset($getVars['caller']) && $getVars['caller'] == "admin") {
			header('Location: '.SITE_ROOT.'/index.php?admin');
			exit;
		}

		header('Location: '.SITE_ROOT.$_SESSION['previous_url']);
        exit;		
	}			
}