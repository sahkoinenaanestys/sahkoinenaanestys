<?php
/**
 * Controller for the frontpage
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Admin_Controller extends Controller
{
	public $view = 'admin';
	public $template = 'template_adm';		
	
	/**
	 * Called by router.php
	 * 
	 * @param array $getVars
	 */
	public function main($getVars = array())
        {		            
            $data = array();
            
            //Error messages
            if (isset($getVars['error']) && $_SESSION['previous_url'] != $_SERVER['REQUEST_URI']) {
                switch ($getVars['error']) {
                    case 'nousernameorpasswd':
                        $data['error'] = NO_USERNAME_OR_PASSWORD_TEXT;
                        break;
                    case 'wrongusernameorpasswd':
                        $data['error'] = WRONG_USERNAME_OR_PASSWORD_TEXT;
                        break;
                    case 'invalidorexpiredsession':
                        $data['error'] = INVALID_OR_EXPIRED_SESSION_TEXT;
                        break;
		    		case 'noaccessprivileges':
                        $data['error'] = NO_ACCESS_PRIVILEGES_TEXT;
                        break;
		    		case 'cookiesdisabled':
		    			$data['error'] = COOKIES_NEED_TO_BE_ENABLED_TEXT;
		    			break;
                }
            }

            //Success messages
            if (isset($getVars['success']) && $_SESSION['previous_url'] != $_SERVER['REQUEST_URI']) {
            	switch ($getVars['success']) {            		
            		case 'logout':
            			$data['success'] = LOGGED_OUT_SUCCESSFULLY_TEXT;
            			break;
            		default:
            			break;
            	}
            }
            
            //Session
            if (isset($_SESSION[$this->userId]) && !isset($getVars['error'])) {
                header('Location: '.SITE_ROOT.'/index.php?election');
                exit;
            } else {
                $this->loadView($data);
            }
    }			
}