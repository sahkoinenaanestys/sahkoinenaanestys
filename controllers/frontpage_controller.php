<?php
/**
 * Controller for the frontpage
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Frontpage_Controller extends Controller
{
	public $view = 'frontpage';
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
						
		//Redirect to the administration page if user is logged in as an admin
		if($this->isLoggedIn === true && $this->isAdmin === true) {
			header('Location: '.SITE_ROOT.'/index.php?admin');
			exit;
		}
		else if($this->isAdmin === false && $this->isElectionWorker === true) {
			//Redirect election workers to their page
			header('Location: '.SITE_ROOT.'/index.php?worker');
			exit;
		}
	}
	
	/**
	 * Called by router.php
	 * 
	 * @param array $getVars
	 */
	public function main($getVars = array())
	{
		// If we come from voting pages after the count down for automatic logout has been started
		// E.g. user visits some other site and then comes back
		if(isset($_SESSION['logoutAfterVote']) && $_SESSION['logoutAfterVote'] == true) {
			header('Location: '.SITE_ROOT.'/index.php?logout&action=samlLogout');
			exit;
		}
		
		$data = array();
		
		//Error messages
		if(isset($getVars['error']) && $_SESSION['previous_url'] != $_SERVER['REQUEST_URI']) {
			switch ($getVars['error']) {
				case 'sessionexpired':					
					$data['error'] = INVALID_OR_EXPIRED_SESSION_TEXT;
					break;
				case 'logoutElectionEnded':
					$data['error'] = VOTING_COULD_NOT_BE_COMPLETED_BECAUSE_ELECTION_ENDED_TEXT.'<br>'.
						YOU_HAVE_BEEND_LOGGED_OUT_TEXT;
					break;
				case 'logoutCannotVote':
					$data['error'] = YOU_DO_NOT_HAVE_RIGHT_TO_VOTE_TEXT.'<br>'.
						YOU_HAVE_BEEN_LOGGED_OUT_TEXT;
					break;
			}			
		}
		
		//Success messages
		if(isset($getVars['success']) && $_SESSION['previous_url'] != $_SERVER['REQUEST_URI']) {
			$data['success'] = LOGGED_OUT_SUCCESSFULLY_TEXT;			
		}
		
		//Check if there is an ongoing election
		$this->loadModelFiles(array('election'));
		$electionModel = new Election_Model;		
		$electionArr = $electionModel->fetchCurrentElection();
		
		if(empty($electionArr)) {
			$this->view = 'vote_closed';
		} else {
			$data['electionName'] = $electionArr['name'];
			$data['ends'] = date('d.m.Y H:i', strtotime($electionArr['endDate']));
		}
		
		$this->loadView($data);		
	}			
}