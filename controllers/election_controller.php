<?php
/**
 * Controller for elections
 * 
 * This module provides functionality for calculating the results of the
 * election. The results are calculated using the D'Hondt method (see
 * http://en.wikipedia.org/wiki/D'Hondt_method).
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Election_Controller extends Controller
{
	public $view = 'active_elections';
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	
		//Prevent users that are not logged in as admins from accessing these pages
		if($this->isLoggedIn === false){
                        header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=invalidorexpiredsession');
			exit;
		}
		else if($this->isAdmin === false && $this->isElectionWorker === true) {
			//Redirect election workers to their page
			header('Location: '.SITE_ROOT.'/index.php?worker');
			exit;
		}
		else if($this->isAdmin === false){
			header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=noaccessprivileges'); 
			exit;
		}
	}
	
	/**
	 * Called by router.php
	 * 
	 * @param array $getVars
	 */
	public function main($getVars = array())
	{		
                $data = array('loggedIn' => true);

		if(!isset($getVars['view'])): $getVars['view'] = ''; endif;

		switch($getVars['view']){

		    case 'election_results':
			$this->template = 'template_adm2';
			$data = $this->messages($getVars, $data);
						
			$electionId = $_SESSION[$this->userId]['activeElection'];
			$this->loadModelFiles(array('election'));
			$electionModel = new Election_Model;			
			$data['election'] = $electionModel->fetchById($electionId);
			
			$this->view = 'election_results';
			$this->loadView($data);
			break;

		    case 'election_votes_import':
			$this->template = 'template_adm2';
			
			$electionId = $_SESSION[$this->userId]['activeElection'];
			$this->loadModelFiles(array('election'));
			$electionModel = new Election_Model;

			$data['election'] = $electionModel->fetchById($electionId);
			if(empty($data['election'])){
			    header('Location: '.SITE_ROOT.'/index.php?election');
			    exit;
			}

			$this->view = 'election_votes_import';
			$this->loadView($data);
			break;

		    default:
			$this->template = 'template_adm';

			$this->loadModelFiles(array('election'));
			$electionModel = new Election_Model;
			
			if(($elections = $electionModel->fetchActiveElections()) != false) {   
			    $data['elections'] = $elections;
			}
			$data = $this->messages($getVars, $data);
			
			$this->view = 'active_elections';
			$this->loadView($data);
			break;
		}			
	}
        
	public function showActiveElection($getVars = array())
	{		
                $this->template = 'template_adm2';
		$data = array('loggedIn' => true);
		$data = $this->messages($getVars, $data);
		
                $electionId = isset($getVars['electionId']) ? (int)$getVars['electionId'] : 0;
                $_SESSION[$this->userId]['activeElection'] = $electionId;
		
		$this->loadModelFiles(array('election','voter','candidate'));
		$electionModel = new Election_Model;
		$election = $electionModel->fetchById($electionId);

                if ($election != false && $election['status'] != Election_Model::STATE_HISTORY) {
		    $election['startDate'] = date('d.m.Y H:i', strtotime($election['startDate']));
		    $election['endDate'] = date('d.m.Y H:i', strtotime($election['endDate']));
		    $data['election'] = $election;
                    
		    $this->loadModelFiles(array('voter','candidate'));
		    $voterModel = new Voter_Model;
		    $voterCount = $voterModel->countByFieldValues(array('election' => $electionId));
		    $data['voterCount'] = $voterCount;

		    $candidateModel = new Candidate_Model;
		    $candidateCount = $candidateModel->countCandidates($electionId);
		    $data['$candidateCount'] = $candidateCount;

                    $this->view = 'election_show';
                    $this->loadView($data);
                } else {
                    header('Location: '.SITE_ROOT.'/index.php?election');
                    exit;
                }			
	}
        
        public function editElection($getVars = array())
        {
                $this->template = 'template_adm';
		$data = array('loggedIn' => true);
                
                //Load model file(s)
                $this->loadModelFiles(array('election'));
                $electionModel = new Election_Model;
		$back = SITE_ROOT.'/index.php?election';

                //Check if form was submitted
                if(isset($_POST['electionSubmitForm'])) {
			$electionId = $_POST['electionId'];
			$back = $_POST['back'];
		    
                        //Validate the input
                        list($electionArr, $formErrors) = $electionModel->validateEditForm($_POST);

                        if(empty($formErrors)) {
				$this->loadModelFiles(array('candidate'));
				$resultCode = $electionModel->updateElection($electionArr, $electionId);
				$action = $_POST['do'];
				
				//Set the result message
				switch ($resultCode) {
					case Election_Model::SUCCESS:
						if($action == 'new')
						    header('Location: '.SITE_ROOT.'/index.php?election&success=newelectionaddded');
						else if($action == 'edit')
						    header('Location: '.$back.'&success=electionmodified');
						exit;
						break;
					case Election_Model::FAILURE:
						if($action == 'new')
						    $data['error'] = INSERT_FAILED_TEXT;
						else if($action == 'edit')
						    $data['error'] = UPDATE_FAILED_TEXT;
						break;    
					case Election_Model::ONLY_ONE_OPEN_ELECTION_ALLOWED:
						$data['error'] = ONLY_ONE_OPEN_ELECTION_ALLOWED_TEXT;
						break;  
					default:
						if($action == 'new')
						    $data['error'] = INSERT_FAILED_TEXT;
						else if($action == 'edit')
						   $data['error'] = UPDATE_FAILED_TEXT;
						break;
				}
 			
			}else {
				$data['error'] = INVALID_INPUT_PLEASE_CHECK_FIELDS_TEXT;
				$data['formErrors'] = $formErrors;
			}	
			
			//Repopulate the form fields
			$data['election'] = array(
				'electionId' => $_POST['electionId'],
				'name' => $_POST['name'],
				'startDate' => $_POST['startDate'],
				'startTime' => $_POST['startTime'],
				'endDate' => $_POST['endDate'],
				'endTime' => $_POST['endTime'],
				'calcMethod' => $_POST['calcMethod'],
				'status' => $_POST['status'],
				'do' => $_POST['do']
			);
                }else{ //form hasn't been submitted
                    if(isset($getVars['electionId'])) { //edit election
                        $electionId = $getVars['electionId'];

                        $electionArr = $electionModel->fetchById($electionId);
                        if(!$electionArr) {
                            header('Location: '.SITE_ROOT.'/index.php?election&do=edit&error=notfound');
                            exit;	
                        }
			
			//redirect back to previous page
			if(isset($getVars['back']) && $getVars['back'] == "showActiveElection"){
			    $back .= '&action='.$getVars['back'].'&electionId='.$electionId;
			}
			if($electionArr['status']==Election_Model::STATE_ACTIVE || $electionArr['status']==Election_Model::STATE_ENDED) {
			    header('Location: '.$back.'&error=cannotmodifyelection');
			    exit;	
			}
			
                        //Populate the form fields
                        $data['election'] = array(
				'do' => 'edit',
				'electionId' => $electionArr['electionId'],
                                'name' => $electionArr['name'],
                                'startDate' => date('d.m.Y', strtotime($electionArr['startDate'])),
                                'startTime' => date('H:i', strtotime($electionArr['startDate'])),
                                'endDate' => date('d.m.Y', strtotime($electionArr['endDate'])),
                                'endTime' => date('H:i', strtotime($electionArr['endDate'])),
				'calcMethod' => (int)$electionArr['calcMethod'],    
				'status' => (int)$electionArr['status']
                        );
                    }else{ //new election
			if($electionModel->fetchByField('status', Election_Model::STATE_ACTIVE) != false || $electionModel->fetchByField('status', Election_Model::STATE_CLOSED) != false){
			    header('Location: '.SITE_ROOT.'/index.php?election&error=cannotaddelection');
			    exit;	
			}
			$data['election']['electionId'] = 0;
			$data['election']['calcMethod'] = Election_Model::CALCULATE_COALITIONS;
			$data['election']['status'] = Election_Model::STATE_ACTIVE; 
                        $data['election']['do'] = 'new'; 
                    }
                }
                $data['returnPage'] = $back;
                $this->view = 'election_edit';
                $this->loadView($data);
        }
        
	public function removeElectionAjax($getVars = array())
	{
                $result = array();
		
		$result['status'] = false;
		$result['msg'] = REMOVE_FAILED_TEXT;
		
                $electionId = $getVars['electionId'];
                $this->loadModelFiles(array('election', 'candidate', 'voter', 'alliance', 'coalition'));
                $electionModel = new Election_Model; 
		
		$election = $electionModel->fetchById($electionId);
		if(empty($election)){
		    echo json_encode($result);
		    return;
		}
		
		if($election['status'] != Election_Model::STATE_ENDED){
		    echo json_encode($result);
		    return;
		}

		$voterModel = new Voter_Model; 
		$candidateModel = new Candidate_Model; 
		$vVoteCount = $voterModel->countVotes($electionId);
		$cVoteCount = $candidateModel->countVotes($electionId);
		
		if(($vVoteCount > 0 && $cVoteCount > 0) && $election['resultsCalculated'] == 0){
		    echo json_encode($result);
		    return;
		}
		
                if($electionModel->removeElection($electionId) != false){
                    $result['status'] = true;
		    $result['msg'] = ELECTION_REMOVED_TEXT;
                }
                echo json_encode($result);
        }
	
	public function endElectionAjax($getVars = array())
	{
		$result = array();
		
		$result['status'] = false;
		$result['msg'] = GENERAL_ERR_TEXT;
		
                $electionId = $getVars['electionId'];
                $this->loadModelFiles(array('election'));
                $electionModel = new Election_Model; 
		
		$election = $electionModel->fetchById($electionId);
		if(empty($election) || ($election['status']==Election_Model::STATE_HISTORY || $election['status']==Election_Model::STATE_ENDED)){
		    echo json_encode($result);
		    return;
		}
		
                if($electionModel->endElection($electionId) !== false){
                    $result['status'] = true;
		    $result['msg'] = ELECTION_ENDED_TEXT;
                }
                echo json_encode($result);
	}
	
	public function startOrStopElectionAjax($getVars = array())
	{
		$result = array();
		
		$result['status'] = false;
		$result['msg'] = GENERAL_ERR_TEXT;
		
                $electionId = $getVars['electionId'];
                $this->loadModelFiles(array('election'));
                $electionModel = new Election_Model; 
		
		$election = $electionModel->fetchById($electionId);
		if(empty($election) || ($election['status']==Election_Model::STATE_ENDED || $election['status']==Election_Model::STATE_HISTORY)){
		    echo json_encode($result);
		    return;
		}
		$oldStatus = $election['status'];
		
                if($electionModel->startOrStopElectionElection($electionId, $oldStatus) !== false){
                    $result['status'] = true;
		    
		    if($oldStatus == Election_Model::STATE_ACTIVE){
			$result['msg'] = ELECTION_TEXT.' '.mb_strtolower(ELECTION_STATUS_CLOSED_TEXT,'UTF-8');
			$result['election_status'] = Election_Model::STATE_CLOSED;
		    }else if($oldStatus == Election_Model::STATE_CLOSED){
			$result['msg'] = ELECTION_TEXT.' '.mb_strtolower(ELECTION_STATUS_ACTIVE_TEXT,'UTF-8');
			$result['election_status'] = Election_Model::STATE_ACTIVE;
		    }
                }
                echo json_encode($result);
	}
	
	public function putElectionToHistoryAjax($getVars = array())
	{
		$result = array();
		
		$result['status'] = false;
		$result['msg'] = GENERAL_ERR_TEXT;
		
                $electionId = $getVars['electionId'];
                $this->loadModelFiles(array('election'));
                $electionModel = new Election_Model; 
		
		$election = $electionModel->fetchById($electionId);
		if(empty($election) || ($election['status'] != Election_Model::STATE_ENDED  || $election['resultsCalculated'] == 0)){
		    echo json_encode($result);
		    return;
		}
		
		$this->loadModelFiles(array('candidate','voter'));
		$voterModel = new Voter_Model; 
		$candidateModel = new Candidate_Model; 
		$vVoteCount = $voterModel->countVotes($electionId);
		$cVoteCount = $candidateModel->countVotes($electionId);
		
		if($vVoteCount == false || $cVoteCount == false){
		    echo json_encode($result);
		    return;
		}
		
		$this->loadModelFiles(array('voter'));
                if($electionModel->putElectionToHistory($electionId) !== false){
                    $result['status'] = true;
		    $result['msg'] = ELECTION_MOVED_TO_HISTORY_TEXT;
                }
                echo json_encode($result);
	}
        
        public function exportElectionResults($getVars = array())
        {
                $electionId = $_SESSION[$this->userId]['activeElection'];
		$this->loadModelFiles(array('election'));
		$electionModel = new Election_Model;
		
		$election = $electionModel->fetchById($electionId);
		if(empty($election)){
		    header('Location: '.SITE_ROOT.'/index.php?election');
                    exit;
		}
		
		//if not ended, do not calculate results
		if($election['status'] == Election_Model::STATE_ENDED){ 

		    $electionModel->setResultsCalculated($electionId, false);
		    $winners = $this->calculateElectionResults($election);
		    
		    if(!is_null($winners) || !empty($winners)){
			//mark election resultsCalculated
			$electionModel->setResultsCalculated($electionId, true);

			require_once SERVER_ROOT.'/lib/parsecsv/parsecsv.lib.php';

			$csv = new parseCSV();
			$csv->encoding('UTF-8', 'UTF-8');
			$fileName = date('d.m.Y').'_'.mb_strtolower($election['name'],'UTF-8').'_'.'results.csv';
			$csvHeader = array(RANK_TEXT,ALLIANCE_TEXT,CANDIDATE_TEXT,VOTES_TEXT,COMPARISION_NUM_TEXT,COALITION_COMPARISION_NUM_TEXT); 

			//convert into 2D array according to header
			$csvData = array();
			$rank = 1;
			foreach($winners as $winner){
			    $alliance = $winner['allianceName'];
			    $candidate = $winner['firstName'].' '.$winner['lastName'];
			    $votes = (int)$winner['votes'] + (int)$winner['paperVotes'];
			    $comparision = number_format((double)$winner['comparisionNum'], 2, '.', '');
			    $coalitionComparision = number_format((double)$winner['coalitionComparisionNum'], 2, '.', '');
			    $csvData[] = array($rank, $alliance, $candidate, $votes, $comparision, $coalitionComparision);
			    $rank++;
			}
			echo '';
			$csv->output($fileName, $csvData, $csvHeader, CSV_SEMICOLON_DELIMITER);
		    }
		}else{
		    header('Location: '.SITE_ROOT.'/index.php?election&view=election_results&error=electionnotended');
                    exit;
		} 
        }
        
	/*
	 * For a Finnish explanation of the results calculation process refer to
	 * http://www.vaalit.fi/34845.htm.
	 *
	 * The Finnish legislation for results calculation is at
	 * http://www.finlex.fi/fi/laki/ajantasa/1998/19980714
	 * and the most relevant parts are in sections 89§ - 91§.
	 *
	 * The simplify the terminology used in the documentation and comments we will
	 * use the term "party" to refer to both political parties and other groupings
	 * and "coalition" to refer to groupings of these "parties".
	 *
	 * The results are calculated in the following manner per voting district:
	 *
	 * 1. Split the alliances into groups based on their coalitions, if available.
	 *	Each group will be compared to others in subsequent steps.
	 *
	 * 2. Calculate the total amount of votes received per group.
	 *
	 * 3. Sort the candidates within a group in descending order based on the votes
	 *     they received.
	 *
	 * 4. Assign a proportial representation number to each candidate
	 *     within a group so that for the
	 *     - candidate with N most votes comparision number = group votes / N
	 *
	 *     where N grows up to the maximum number of seats available within the
	 *     voting district.
	 *
	 *  5. Sort the candidates based on their comparision number values.
	 */
        private function calculateElectionResults($election)
        {
		$data = array();
		$electionId = $election['electionId'];
                
		$this->loadModelFiles(array('election', 'candidate', 'alliance', 'coalition', 'voter'));
		$candidateModel = new Candidate_Model;
		$voterModel = new Voter_Model;
		
		//check that votes given matches with votes received by candidates
		$vVoteCount = $voterModel->countVotes($electionId);
		$cVoteCount = $candidateModel->countVotes($electionId);
		
		if($vVoteCount == false || $cVoteCount == false){
		    header('Location: '.SITE_ROOT.'/index.php?election&view=election_results&error=couldnotcountvotes');
                    exit;
		}
		if($cVoteCount != $vVoteCount){
		    header('Location: '.SITE_ROOT.'/index.php?election&view=election_results&error=votescountdoesnotmatch');
                    exit;
		}
		
		//reset comparisionNums and ranks
		$updateClause = "rank=?, comparisionNum=?, allianceComparisionNum=?, coalitionComparisionNum=? WHERE election=?";
		$candidateModel->update($updateClause, array(0,0,0,0,$electionId));
		
		//calculate alliance and coalition votes
		$allianceModel = new Alliance_Model;
		if($allianceModel->setAllianceVotes($electionId) == false){
		    header('Location: '.SITE_ROOT.'/index.php?election&view=election_results&error=couldnotcalculatealliancevotes');
                    exit;
		}
		$coalitionModel = new Coalition_Model;
		if($coalitionModel->setCoalitionVotes($electionId) == false){
		    header('Location: '.SITE_ROOT.'/index.php?election&view=election_results&error=couldnotcalculatecoalitionvotes');
                    exit;
		}
		
		//calculate comparisions
		if($this->calculateComparisions($election, Election_Model::TOTAL_SEATS) == false){
		    header('Location: '.SITE_ROOT.'/index.php?election&view=election_results&error=couldnotcalculatecomparisions');
                    exit;
		}
		//for testing only
//                if(($candidates = $candidateModel->fetchCandidateVotes($electionId)) != false) {   
//                    $data['candidates'] = $candidates;
//                }
//                  
		//order candidates by comparisions
		$allCandidates = $candidateModel->fetchCandidatesByComparision($electionId);
		$winners = array();
		$seat = 1;
		
		//set ranks and winners
		foreach($allCandidates as $candidate){
		    $candidateModel->setRank($candidate['candidateNum'], $seat, $electionId);
		    if($seat <= Election_Model::TOTAL_SEATS)
			$winners[] = $candidate;
		    $seat++;
		}
                $data['winners'] = $winners;

		//for testing only
                //$this->view = 'election_results'; 
                //$this->loadView($data);
		return $winners;
        }
	
	private function calculateComparisions($election, $totalSeats)
	{
		$electionId = $election['electionId'];
		$calcMethod = $election['calcMethod'];

		//calculate comparisionNum for candidates not in alliance or coalition
		$candidateModel = new Candidate_Model;
		$independentCandidates = $candidateModel->fetchIndependentCandidates($electionId);
		
		if($independentCandidates != false){
		    foreach ($independentCandidates as $candidate) {
			
			//if($calcMethod == Election_Model::CALCULATE_COALITIONS){
			    //if(empty($candidate['coalition']) || strlen($candidate['coalition']) < 1){
				$candidate['comparisionNum'] = $candidate['votes']; 
				$result = $candidateModel->setComparisionNum('comparisionNum', $candidate, $electionId);
				if($result == false) return false;
			    //}
			//}
		    }//for
		}
	    
		//calculate allianceComparisionNum
		$allianceModel = new Alliance_Model;
		$alliances = $allianceModel->fetchByField('election', $electionId);
		if($alliances != false){

		    for($i=0; $i < count($alliances); $i++){
			  $alliance = $alliances[$i];

			  if(!empty($alliance['allianceId']) && $alliance['allianceId'] > 0){
			    $allianceCandidates = $candidateModel->fetchCandidatesByAlliance($alliance['allianceId'], $electionId);

			    $data = array();
			    $seat = 1;
			    foreach ($allianceCandidates as $candidate) {

			      if ($seat <= $totalSeats) {
				  $candidate['allianceComparisionNum'] = $alliance['votes'] / $seat;

				  /*if($calcMethod == Election_Model::CALCULATE_COALITIONS){
				      if(empty($alliance['coalition']) || $alliance['coalition'] < 1){
					$candidate['comparisionNum'] = $candidate['allianceComparisionNum']; 
					$result = $candidateModel->setComparisionNum('comparisionNum', $candidate, $electionId);
					if($result == false)return false;
				      }
				  }else{*/
					$candidate['comparisionNum'] = $candidate['allianceComparisionNum'];
					$result = $candidateModel->setComparisionNum('comparisionNum', $candidate, $electionId);
					if($result == false) return false;
				  //}
				  $result = $candidateModel->setComparisionNum('allianceComparisionNum', $candidate, $electionId);
				  if($result == false) return false;  
			      } //if
			      $data[] = $candidate;
			      $seat++;
			    } //foreach

			    if(!empty($data)) $alliance['candidates'] = $data;
			  }
		    }//for
		}
		
		//calculate coalitionComparisionNum
		if($calcMethod == Election_Model::CALCULATE_COALITIONS){
		    $coalitionModel = new Coalition_Model;
		    $coalitions = $coalitionModel->fetchByField('election', $electionId);
		    if($coalitions != false){

			for($i=0; $i < count($coalitions); $i++){
			      $coalition = $coalitions[$i];

			      if(!empty($coalition['coalitionId'])){
				$coalitionCandidates = $candidateModel->fetchCandidatesByCoalition($coalition['coalitionId'], $electionId);
				
				$data = array();
				$seat = 1;
				foreach ($coalitionCandidates as $candidate) {

				  if ($seat <= $totalSeats) {
				      $candidate['coalitionComparisionNum'] = $coalition['votes'] / $seat;
				      $candidate['comparisionNum'] = $candidate['coalitionComparisionNum'];

				      $result = $candidateModel->setComparisionNum('coalitionComparisionNum', $candidate, $electionId);
				      $result2 = $candidateModel->setComparisionNum('comparisionNum', $candidate, $electionId);
				      if($result == false || $result2 == false) return false;
				  }
				  $data[] = $candidate;
				  $seat++;

				} //foreach 
				if(!empty($data)) $coalition['candidates'] = $data;
			      }//if
			}//for
		    }
		}
		return true;
	}
	
	public function importCsvFileAjax($getVars = array())
        {
		$electionId = (int)$_SESSION[$this->userId]['activeElection'];
		
		$this->loadModelFiles(array('election'));
		$electionModel = new Election_Model;
		$election = $electionModel->fetchById($electionId);
		if(empty($election) || ($election['status'] != Election_Model::STATE_ENDED)){
		    echo '<div class="errorMsgDiv">'.ELECTION_HAS_TO_BE_ENDED_TEXT.'</div>';
		    return;
		}
		$csvDelimiter = isset($_POST['csvDelimiter']) ? $_POST['csvDelimiter'] : ",";
		$data = parent::uploadFile();

                $response = json_decode($data);
                if($response->{'status'} == true){
                    $filename = $response->{'file'};
		    parent::changeFileEncoding($filename, UPLOAD_DIR, 'UTF-8');
		    $this->addPaperVotesAjax($filename, $csvDelimiter, $electionId);
			
		    if(file_exists(UPLOAD_DIR.$filename) && is_writable(UPLOAD_DIR.$filename))
                          unlink(UPLOAD_DIR.$filename);
                }else{
		    echo '<div class="errorMsgDiv">'.$response->{'msg'}.'</div>';;
                }
        }
	
	private function addPaperVotesAjax($filename, $csvDelimiter, $electionId)
	{
		$this->loadModelFiles(array('voter','candidate'));
		$voterModel = new Voter_Model;
		$candidateModel = new Candidate_Model;
		
		$vVoteCount = $voterModel->countVotes($electionId, Voter_Model::VOTE_METHOD_PAPER);
		//$cVoteCount = $candidateModel->countVotes($electionId, 'paperVotes');

		//prevent importing paper votes if paper votes in db already matches total paper votes given
		if($vVoteCount == false /*&& $cVoteCount != false*/){
		    /*if($cVoteCount == $vVoteCount){
			echo '<div class="errorMsgDiv">'.PAPER_VOTES_IMPORT_NOT_ALLOWED_TEXT.'</div>';
			exit;
		    }*/
		    echo '<div class="errorMsgDiv">'.VOTES_COULD_NOT_BE_COUNTED_ERR_TEXT.'</div>';
		    exit;
		}

		//otherwise parse csv file
		$candidateVotes = $this->parseCandidateVotesCsv($filename, $csvDelimiter, $electionId);
		if($candidateVotes['status'] == false){
		    $errors = $candidateVotes['msg'];
		    echo '<div class="errorMsgDiv">';
		    foreach($errors as $error){
			echo $error.'<br>';
		    } 
		    echo '</div>';
		    exit;
		}
		
		//check that total given votes matches total votes received by candidates
		if($candidateVotes['totalVotes'] == $vVoteCount){
		    $success = $candidateModel->insertPaperVotes($electionId, $candidateVotes['candidateVotes']);

		    if($success == true){
			echo '<div class="successMsgDiv">'.PAPER_VOTES_ADDED_SUCCESSFULLY_TEXT.'</div>';
		    }else{
			echo '<div class="errorMsgDiv">'.GENERAL_ERR_TEXT.'.<br> '.PAPER_VOTES_WERE_NOT_ADDED_TEXT.'</div>';
		    }
		}else{
		    echo '<div class="errorMsgDiv">'.VOTE_COUNT_DID_NOT_MATCH_ERR_TEXT.' '.PAPER_VOTES_WERE_NOT_ADDED_TEXT.'</div>';
		}	
	}
	
	private function parseCandidateVotesCsv($filename, $csvDelimiter, $electionId)
	{
		$result = array();
		$result['status'] = false;
		$result['msg'] = "";
	    
		$minCols = 2;
		$maxCols = 2;
		$errors = array();
		
		$totalVotes = 0;
		$candidateVotes = array();
		$electionId = $_SESSION[$this->userId]['activeElection'];

		require_once SERVER_ROOT.'/lib/parsecsv/parsecsv.lib.php';

		$csv = new parseCSV();
		$csv->encoding('UTF-8', 'UTF-8');
		$csv->delimiter = $csvDelimiter; 
		$csv->parse(UPLOAD_DIR.$filename); 

		$this->loadModelFiles(array('candidate'));
		$candidateModel = new Candidate_Model; 

		foreach($csv->data as $rowIndex => $row){

			if(count($row) < $minCols || count($row) > $maxCols){
				$errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).'). '.FILE_INVALID_COLUMNS_AMOUNT_TEXT;
				break;
			}
			$candidate = array();
			$i = 0;				
			foreach($row as $value) {
				switch ($i) {
					case 0: $candidate['candidateNum'] = $value; break;
					case 1: $candidate['votes'] = $value; break;
				}
				$i++;
			}
                        
                        if(is_numeric($candidate['candidateNum']) && is_numeric($candidate['votes'])){
                            
			    //check that candidate is found
			    $res = $candidateModel->fetchByCompositeId(array($candidate['candidateNum'], $electionId));
			    if($res !== false){
				$candidateVotes[] = $candidate;
				$totalVotes = $totalVotes + (int)$candidate['votes'];
			    }else{
				$errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).'). '.CANDIDATE_WAS_NOT_FOUND_TEXT;
				break;
			    }
                        }else{
                            $errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).'). '.HAS_TO_BE_A_NUMBER_TEXT;
			    break;
                        }
                    }//foreach 
                    
                    if(count($errors) > 0){ 
                        $result['msg'] = $errors; 
                    }else{
			$result['status'] = true;
			$result['candidateVotes'] = $candidateVotes;
			$result['totalVotes'] = $totalVotes;
                    }
		    return $result;
	}
	
	private function messages($messages = array(), $data) 
	{		
		if (isset($messages['success'])) {
		    switch ($messages['success']) {
			case 'newelectionaddded':
			    $data['success'] = NEW_ELECTION_ADDED_SUCCESSFULLY_TEXT;
			    break;
			case 'electionmodified':
			    $data['success'] = ELECTION_INFORMATION_UPDATED_SUCCESSFULLY_TEXT;
			    break;
			case 'electionremoved':
			    $data['success'] = ELECTION_REMOVED_TEXT;
			    break;
			case 'electionended':
			    $data['success'] = ELECTION_ENDED_TEXT;
			    break;
			case 'added_new':
			    $data['success'] = CANDIDATE_ADDED_SUCCESSFULLY_TEXT;
			    break;
			case 'addedvoter':
			    $data['success'] = NEW_VOTER_ADDED_SUCCESSFULLY_TEXT;
			    break;
			case 'electionopensuccess':
			    $data['success'] = ELECTION_TEXT.' '.mb_strtolower(ELECTION_STATUS_ACTIVE_TEXT, 'UTF-8');
			    break;
			case 'electionclosesuccess':
			    $data['success'] = ELECTION_TEXT.' '.mb_strtolower(ELECTION_STATUS_CLOSED_TEXT, 'UTF-8');
			    break;
			case 'electionmovedtohistory':
			    $data['success'] = ELECTION_MOVED_TO_HISTORY_TEXT;
			    break;
		    }//switch
		}else if(isset($messages['error'])) {
		    switch ($messages['error']) {
			case 'electionnotended':
			    $data['error'] = ELECTION_HAS_TO_BE_ENDED_TEXT;
			    break;
			case 'electionremovefailure':
			    $data['error'] = REMOVE_FAILED_TEXT;
			    break;
			case 'cannotmodifyelection':
			    $data['error'] = CANNOT_MODIFY_ELECTION_TEXT;
			    break;
			case 'cannoteditcandidate':
			    $data['error'] = MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT;
			    break;
			case 'cannotaddelection':
			    $data['error'] = CANNOT_ADD_ELECTION_TEXT;
			    break;
			case 'electionhidefailed':
			    $data['error'] = COULD_NOT_MOVE_ELECTION_TO_HISTORY_TEXT;
			    break;
			case 'couldnotcountvotes':
			    $data['error'] = VOTES_COULD_NOT_BE_COUNTED_ERR_TEXT;
			    break;
			case 'votescountdoesnotmatch':
			    $data['error'] = VOTE_COUNT_DID_NOT_MATCH_ERR_TEXT;
			    break;
			default:
			    $data['error'] = GENERAL_ERR_TEXT;
			    break;
		    }//switch
		}
		return $data;
	}
}
