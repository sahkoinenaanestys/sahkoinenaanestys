<?php
/**
 * router.php
 * 
 * This file handles all the page requests
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */

//Request string passed to the system
$request = $_SERVER['QUERY_STRING'];

//Parse all GET variables
$requestArr = explode('&' , $request);

//The page name
if(empty($requestArr[0])) {
	$page = 'frontpage';
	$requestArr = array();
}	
else
{
	$page = array_shift($requestArr);
}
	
//Default method call
$method = 'main';
  
/*
 * Parse variables out of GET statement.
 * Variable name 'action' is reserved for method calls
 */
$getVars = array();
foreach ($requestArr as $r) {
	//Split the variable name and the value
	$tempArr = explode('=', $r, 2);
	if(count($tempArr) < 2)
		continue;
	list($var, $val) = $tempArr; 
	
	/* 
	 * Check if variable name equals 'action'.
	 * This variable can be sent if want to call some other method instead of 'main'
	 */
	if($var == 'action')
		$method = $val;
	else
		$getVars[$var] = $val;
}

//Load controller
$controllerFile = SERVER_ROOT . '/controllers/' . $page . '_controller.php';

if (file_exists($controllerFile))
{
    include_once($controllerFile);

    //Classname should be following the naming convention (Classname_Controller)
    $class = ucfirst($page) . '_Controller';

    //Instantiate the class
    if (class_exists($class))
    {
        $controller = new $class;
    }
    else
    {
        die('class doesn\'t exist!');
    }
}
else
{
    //Controller file doesn't exist 
    die('page doesn\'t exist!');
}

//Call a method of the controller class
if (method_exists($controller, $method))
	call_user_func( array($controller, $method), $getVars);
else 
	call_user_func( array($controller, "main"), array()); //Call the main method if the given method doesn't exist 

