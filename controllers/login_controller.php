<?php
/**
 * Controller for the log in functionality
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Login_Controller extends Controller
{
	
	public function main($getVars = array())
	{						
		//Require SAML authentication
		require_once(SERVER_ROOT.'/lib/simplesamlphp-1.10.0/lib/_autoload.php');
		$as = new SimpleSAML_Auth_Simple('default-sp');

		if(!$as->isAuthenticated()) {
			$as->login(array("ReturnTo" => SITE_ROOT."/index.php?vote", "ErrorURL" => SITE_ROOT."/index.php?logout&action=samlLogout&error=sessionexpired"));
		}
	}
	
	/**
	* Checks the admin login
	* 
	* @param array $getVars
	*/
	public function adminLogin($getVars = array())
	{
		//Check that cookies are enabled
		if (count($_COOKIE) == 0) {
			header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=cookiesdisabled');
			exit;
		}
		
		$username = (isset($_POST['username'])) ? $_POST['username'] : '';
		$passwd = (isset($_POST['passwd'])) ? $_POST['passwd'] : '';

		if(!is_null($username) && (strlen($username) > 0) && (!is_null($passwd) && strlen($passwd) > 0) ) {
			$this->loadModelFiles(array('admin'));
			$adminModel = new Admin_Model;

			$resultCode = $adminModel->checkAdminLogin($username, $passwd);
			if($resultCode != Admin_Model::FAILURE) {
				$loginHash = sha1(microtime(true).mt_rand(10000,90000)); //random login hash
				$adminId = $adminModel->getAdminIdByUsername($username);
				
				$this->setLoginHash($adminId, $loginHash, $adminModel);
				$this->setLastSeen($adminId, $adminModel);
				
				session_regenerate_id(true); // Protect against session fixation
				$this->userId = session_id();
				
				$_SESSION[$this->userId]['username'] = $username;
				$_SESSION[$this->userId]['loggedIn'] = true;				
				$_SESSION[$this->userId]['loginHash'] = $loginHash;
				$_SESSION[$this->userId]['_USER_IP'] = $_SERVER['REMOTE_ADDR'];
				$_SESSION[$this->userId]['_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
				$_SESSION[$this->userId]['isAdmin'] = false;
				$_SESSION[$this->userId]['isElectionWorker'] = false;
				
				if($resultCode == Admin_Model::ADMIN_LOGIN_SUCCESS) {
					$_SESSION[$this->userId]['isAdmin'] = true;
					header('Location: '.SITE_ROOT.'/index.php?election');
					exit;
				}					
				else if($resultCode == Admin_Model::WORKER_LOGIN_SUCCESS) {
					$_SESSION[$this->userId]['isElectionWorker'] = true;
					header('Location: '.SITE_ROOT.'/index.php?worker');
					exit;					
				}
			}
			else {
				header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=wrongusernameorpasswd');
				exit;
			}						
		}
		else {			
			header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=nousernameorpasswd');
			exit;
		}
		$data = array();
		$this->loadView($data);
	}
	
	private function setLoginHash($adminId, $loginHash, $adminModel)
	{
		$params = array($loginHash, $adminId);
		$updateClause = "loggedIn=? WHERE adminId=?";	

		return $adminModel->update($updateClause, $params);
	}
        
}
