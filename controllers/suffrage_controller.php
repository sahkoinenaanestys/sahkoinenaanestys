<?php
/**
 * Controller for the suffrage pages. 
 * 
 * This controller should only be accessed by the administrator.
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Suffrage_Controller extends Controller
{
	public $view = 'suffrage_list';
	public $template = 'template_adm2';
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		//Prevent users that are not logged in as admins from accessing these pages
		if($this->isLoggedIn === false){
                        header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=invalidorexpiredsession');
			exit;
		}
		else if($this->isAdmin === false){
			header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=noaccessprivileges'); 
			exit;
		}
	}
	
	/**
	 * Called by router.php
	 * 
	 * @param array $getVars
	 */
	public function main($getVars = array())
	{
	    //__Contstruct checks that the user is logged in
	    $data = array('loggedIn' => true);

	    //Search string
	    if(isset($_POST['search'])) {
		    //Redirect to same page but this time the search string is in get variable (Post/Redirect/Get Pattern)
		    header('Location: '.SITE_ROOT.'/index.php?suffrage&search='.urlencode($_POST['search']));
		    exit;
	    }
	    else if(isset($getVars['search'])) {
		    $data['searchStr'] = $getVars['search'];
	    }
	    else {
		    $data['searchStr'] = null;
	    }

	    //Redirect the user to election page if the election id was not defined    	    	
	    if(!isset($_SESSION[$this->userId]['activeElection'])) {
		    header('Location: '.SITE_ROOT.'/index.php?election');
		    exit;
	    }    	
	    $data['electionId'] = (int) $_SESSION[$this->userId]['activeElection'];

	    //Load model file(s)
	    $this->loadModelFiles(array('voter', 'election'));
	    $electionModel = new Election_Model;
	    $voterModel = new Voter_Model;

	    //Fetch election information & redirect if election not found
	    $data['election'] = $electionModel->fetchById($data['electionId']);
	    if(empty($data['election'])) {
		    header('Location: '.SITE_ROOT.'/index.php?election');
		    exit;
	    }

	    //Pagination data
	    $limit = 15;	//Number of voters per page    	
	    $offset = (isset($getVars['offset']))? (int) $getVars['offset']: 0;	    	

	    //Fetch total number of rows
	    $totalRows = $voterModel->countByElectionAndSearch($data['electionId'], $data['searchStr']);    		    	    	

	    //Pagination data
	    $data['pagination'] = $voterModel->getPaginationData($limit, $offset, $totalRows);    	

	    //Fetch voters
	    $data['voters'] = $voterModel->fetchVotersByElection($data['electionId'], $data['pagination']['limit'], $data['pagination']['offset'], $data['searchStr']);    	

	    //Base64 encode get variables of current url as referer parameter
	    $data['referer'] = base64_encode("&search=".$data['searchStr']."&offset=".$offset);

	    $data = $this->messages($getVars, $data);
	    $this->loadView($data);
	}			

	public function edit($getVars = array())
	{
	    //__Contstruct checks that the user is logged in
	    $data = array('loggedIn' => true);

	    //Redirect the user to election page if the election id was not defined
	    if(!isset($_SESSION[$this->userId]['activeElection'])) {
		    header('Location: '.SITE_ROOT.'/index.php?election');
		    exit;
	    }    	
	    $data['electionId'] = (int) $_SESSION[$this->userId]['activeElection'];

	    //Load model file(s)
	    $this->loadModelFiles(array('voter', 'election'));
	    $electionModel = new Election_Model;
	    $voterModel = new Voter_Model;

	    //Fetch election information & redirect if election not found
	    $data['election'] = $electionModel->fetchById($data['electionId']);
	    if(empty($data['election'])) {
		    header('Location: '.SITE_ROOT.'/index.php?election');
		    exit;
	    }

	    //Check that the election state is closed if we are modifying an existing voter
	    if($data['election']['status'] != Election_Model::STATE_CLOSED && 
	    		((isset($_POST['studentNumOld']) && $_POST['studentNumOld'] != 'new') || isset($getVars['studentNum']))) {
		    header('Location: '.SITE_ROOT.'/index.php?suffrage&error=cannoteditvoter');
		    exit;
	    }

	    $back = SITE_ROOT.'/index.php?suffrage';
		    if(isset($getVars['referer']))
			    $back .= base64_decode($getVars['referer']);	


	    //Check if form was submitted
	    if(isset($_POST['voterSubmit'])) {
			    $back = $_POST['back'];

		    //Validate the input
		    list($voterArr, $formErrors) = $voterModel->validateEditForm($_POST);    		

		    if(empty($formErrors)) {    			
			    if($_POST['studentNumOld'] == 'new') {
				    //Insert a new voter
				    $resultCode = $voterModel->updateVoter($voterArr, $data['electionId']);     				    				

				    //Set the result message
				    switch ($resultCode) {
					    case Voter_Model::PK_EXISTS_ALREADY:
						    $data['error'] = INSERT_FAILED_TEXT."<br>".STUDENT_NUMBER_ALREADY_TAKEN_TEXT;
						    $data['formErrors']['studentNum'] = '!';
						    break;
					    case Voter_Model::SUCCESS:
						    header('Location: '.$back.'&success=addedvoter');
						    exit;
						    break;    							
					    default:
						    $data['error'] = INSERT_FAILED_TEXT;
						    break;    					
				    }
			    }
			    else {    				
				    //Update the voter
				    $resultCode = $voterModel->updateVoter($voterArr, $data['electionId']);

				    //Set the result message
				    switch ($resultCode) {
					    case Voter_Model::PK_EXISTS_ALREADY:
						    $data['error'] = UPDATE_FAILED_TEXT."<br>".STUDENT_NUMBER_ALREADY_TAKEN_TEXT;
						    $data['formErrors']['studentNum'] = '!';    						
						    break;
					    case Voter_Model::VOTER_ALREADY_VOTED:
						    $data['error'] = VOTER_CANNOT_BE_MODIFIED_TEXT."<br>".VOTER_HAS_ALREADY_VOTED_TEXT;
						    break;
					    case Voter_Model::SUCCESS:
							    header('Location: '.$back.'&success=editedvoter');
							    exit;
						    break;
					    default:
						    $data['error'] = UPDATE_FAILED_TEXT;
						    break;
				    }
			    }   			
		    }
			    else {
				    $data['error'] = INVALID_INPUT_PLEASE_CHECK_FIELDS_TEXT;
				    $data['formErrors'] = $formErrors;
			    }

			    //Repopulate the form fields
			    $studentNumOld = (isset($data['error']))? $_POST['studentNumOld']: $_POST['studentNum'];
			    $data['voter'] = array
			    (
				    'studentNum' => $_POST['studentNum'],
				    'studentNumOld' => $studentNumOld,
				    'firstName' => $_POST['firstName'],
				    'lastName' => $_POST['lastName'],
				    'email' => $_POST['email'],
				    'canVote' => (int) $_POST['canVote'],
				    'address' => $_POST['address'],
				    'city' => $_POST['city'],
				    'zipCode' => $_POST['zipCode']
			    );			
	    }
	    else {    		    		
		    //Give the form some dummy variables if user is not modifying existing voter
		    if(!isset($getVars['studentNum'])) {
			    //Generate link to previous page if we came from "showActiveElection"
			    if(isset($getVars['back']) && $getVars['back'] == "showActiveElection"){
				    $back = SITE_ROOT.'/index.php?election&action='.$getVars['back'].'&electionId='.$data['electionId'];
			    }
			    $data['voter'] = array('studentNum' => '', 'studentNumOld' => 'new', 'firstName' => '', 'lastName' => '', 'email' => '', 'canVote' => 1, 'address' => '', 'city' => '', 'zipCode' => '');
		    }
		    else {
			    //Fetch the voter information & redirect if voter not found
			    $data['voter'] = $voterModel->fetchByCompositeId(array((int)$getVars['studentNum'], $data['electionId']));
			    if (empty($data['voter'])) {
				    header('Location: '.SITE_ROOT.'/index.php?suffrage&error=voternotfound');
				    exit;
			    }

			    //Do not proceed if voter has voted
			    if ($data['voter']['hasVoted'] == 1) {
				    header('Location: '.$back.'&error=cannoteditalreadyvoted');
				    exit;	
			    }

			    $data['voter']['studentNumOld'] = $data['voter']['studentNum'];		
		    }
	    }    	    	    	    	
	    $data['returnPage'] = $back;
	    $this->view = 'suffrage_edit';
	    $this->loadView($data);
	}

	public function removeAll($getVars = array()) 
	{
	    //__Contstruct checks that the user is logged in
	    $data = array('loggedIn' => true);

	    $this->loadModelFiles(array('election', 'voter'));
	    $electionModel = new Election_Model;
	    $voterModel = new Voter_Model;


	    $electionId = (int) $_SESSION[$this->userId]['activeElection'];

	    //Check that voters can be removed
	    if(!$electionModel->canBeDeleted($electionId)) {
		    header('Location: '.SITE_ROOT.'/index.php?suffrage&error=cannot_remove_voter');
		    exit;
	    }

	    //Fetch election information & redirect if election not found
	    $data['election'] = $electionModel->fetchById($electionId);
	    if(empty($data['election'])) {
		    header('Location: '.SITE_ROOT.'/index.php?election');
		    exit;
	    }    

	    //Remove all voters if it is requested
	    if(isset($_POST['removeAll'])) {

		    // Check that nobody has given a vote
		    if ($voterModel->countByFieldValues(array("election" => $electionId, "hasVoted" => "1")) > 0) {
			    header('Location: '.SITE_ROOT.'/index.php?suffrage&error=cannot_remove_votes_given');
			    exit;
		    }

		    $result = $voterModel->removeByField('election', $electionId);
		    if($result === true) {
			    //Redirect to the voter listing page
			    header('Location: '.SITE_ROOT.'/index.php?suffrage&success=removedAllSuccessfully');
			    exit;
		    }    			
		    else {
			    $data['error'] = REMOVE_FAILED_TEXT;
		    }
	    }    	

	    $this->view = 'suffrage_delete';
	    $this->loadView($data);
	}

	public function ajaxRemove($getVars = array())
	{
	    $result = array('success' => false, 'msg' => '');    	

	    $studentNum = $getVars['studentNum'];


	    $this->loadModelFiles(array('election', 'voter'));
	    $electionModel = new Election_Model;
	    $voterModel = new Voter_Model;

	    //Check that this election exists
	    $electionArr = $electionModel->fetchById((int)$_SESSION[$this->userId]['activeElection']);
	    if(empty($electionArr)) {
		    $result['msg'] = ELECTION_WAS_NOT_FOUND_TEXT;
		    echo json_encode($result);
		    exit;
	    }

	    //Check that the election state is closed
	    if($electionArr['status'] != Election_Model::STATE_CLOSED){
		    $result['msg'] = VOTERS_CANNOT_BE_REMOVED_EXPLANATION_TEXT;
		    echo json_encode($result);
		    exit;
	    }

	    //Check that this voter exists 
	    $voterArr = $voterModel->fetchByCompositeId(array((int)$studentNum, (int) $electionArr['electionId']));    	
	    if(empty($voterArr)) {
		    $result['msg'] = VOTER_WAS_NOT_FOUND_TEXT;
		    echo json_encode($result);
		    exit;
	    }

	    //Prevent removing a voter who has already voted
	    if($voterArr['hasVoted'] == 1) {
		    $result['msg'] = VOTER_CANNOT_BE_REMOVED_TEXT.'<br>'.VOTER_HAS_ALREADY_VOTED_TEXT;
		    echo json_encode($result);
		    exit;
	    }

	    //Delete voter
	    $voterModel->beginTransaction();
	    $removed = $voterModel->removeByCompositeId(array((int)$studentNum, (int) $electionArr['electionId']));
	    if($removed == false) {
		    $voterModel->rollBack();
		    $result['msg'] = REMOVE_FAILED_TEXT;
		    echo json_encode($result);
		    exit;
	    }
	    $voterModel->commit();    	

	    $result['success'] = true;    	
	    echo json_encode($result);
	    exit;	    
	}        

	public function import($getVars = array())
	{
	    //__Contstruct checks that the user is logged in
	    $data = array('loggedIn' => true);    	    	

	    //Load model file(s)
	    $this->loadModelFiles(array('voter', 'election'));
	    $electionModel = new Election_Model;
	    $voterModel = new Voter_Model;

	    //Redirect the user to election page if the election id was not defined
	    if(!isset($_SESSION[$this->userId]['activeElection'])) {
		    header('Location: '.SITE_ROOT.'/index.php?election');
		    exit;
	    }
	    $data['electionId'] = (int) $_SESSION[$this->userId]['activeElection'];

		//Fetch election information & redirect if election not found
	    $electionArr = $electionModel->fetchById($data['electionId']);
	    if(empty($electionArr)) {
		    header('Location: '.SITE_ROOT.'/index.php?election');
		    exit;
	    }
	    $data['election'] = $electionArr;

	    $this->view = 'suffrage_import';
	    $this->loadView($data);
	}

	public function ajaxImport($getVars = array()) 
	{
	    //Load model file(s)
	    $this->loadModelFiles(array('voter', 'election'));
	    $voterModel = new Voter_Model;
	    $electionModel = new Election_Model;

	    //Check that this election exists
	    $electionId = (int) $_SESSION[$this->userId]['activeElection'];
	    $electionArr = $electionModel->fetchById($electionId);
	    if(empty($electionArr)) {
		    echo '<div class="errorMsgDiv">'.ELECTION_WAS_NOT_FOUND_TEXT.'</div>';
		    exit;
	    }

	    // Allow importing voters only when the state is closed or active
	    if($electionArr['status'] != Election_Model::STATE_CLOSED && $electionArr['status'] != Election_Model::STATE_ACTIVE){
		    echo '<div class="errorMsgDiv">'.MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT.'</div>';
		    exit;
	    }

	    //Upload the file
	    $data = $this->uploadFile();    	
	    $response = json_decode($data);
	    $filename = $response->{'file'};

	    if($response->{'status'} == true) {
		    $minCols = 4;
		    $maxCols = 8;

		    $errors = array();

		    $this->changeFileEncoding($filename, UPLOAD_DIR, 'UTF-8');

		    require_once SERVER_ROOT.'/lib/parsecsv/parsecsv.lib.php';
		    require_once SERVER_ROOT.'/lib/ForceUTF8/Encoding.php';

		    $csv = new parseCSV();
		    $csv->encoding('UTF-8', 'UTF-8');
		    $csv->delimiter = isset($_POST['csvDelimiter']) ? $_POST['csvDelimiter'] : ",";  
		    $csv->parse(UPLOAD_DIR.$filename);												

		    foreach($csv->data as $rowIndex => $row) {
			    //Set a time limit for each row because these CSV files can be huge
			    set_time_limit(3);

			    //Check that this row has acceptable number of columns
			    if(count($row) < $minCols || count($row) > $maxCols){
				    $errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).') '.FILE_INVALID_COLUMNS_AMOUNT_TEXT;
				    break;
			    }				

			    //$row is an associative array with string keys taken from the first row of the csv file
			    //We won't be using those keys though..
			    $voter = array();
			    $i = 0;				
			    foreach($row as $value) {			    	
				    switch ($i) {
					    case 0: $voter['studentNum'] = $value; break;
					    case 1: $voter['firstName'] = Encoding::fixUTF8($value); break;
					    case 2: $voter['lastName'] = Encoding::fixUTF8($value); break;
					    case 3: $voter['email'] = Encoding::fixUTF8($value); break;
					    case 4: $voter['canVote'] = $value; break;
					    case 5: $voter['address'] = Encoding::fixUTF8($value); break;
					    case 6: $voter['city'] = Encoding::fixUTF8($value); break;
					    case 7: $voter['zipCode'] = $value; break;
				    }
				    $i++;
			    }								
			    $voter['studentNumOld'] = 'new';	//updateVoter method needs this when inserting a new row				

			    if(is_numeric($voter['studentNum'])) {					
				    $resultCode = $voterModel->updateVoter($voter, $electionId);
				    //Set error messages
				    switch ($resultCode) {
					    case Voter_Model::SUCCESS:
						    //Do nothing
						    break;
					    case Voter_Model::PK_EXISTS_ALREADY:
						    $errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).') '.INSERT_FAILED_TEXT.' '.STUDENT_NUMBER_ALREADY_TAKEN_TEXT.' "'.$voter['studentNum'].'"';
						    break 2;						
					    default:
						    $errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).') '.INSERT_FAILED_TEXT.' "'.implode($csv->delimiter, $voter).'"';
						    break 2;
				    }
			    }else {
				    $errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).') '.INSERT_FAILED_TEXT.' studentNum: '.$voter['studentNum'];
				    break;
			    }
		    } //foreach

		    if(!empty($errors)) {
			echo '<div class="errorMsgDiv">';
			foreach($errors as $error){
			    echo $error.'<br>';
			} 
			echo '</div>';
		    }else {
			echo '<div class="successMsgDiv">'.VOTERS_IMPORTED_SUCCESSFULLY_TEXT.'<br>'.NUMBER_OF_ROWS_TEXT.': '.($rowIndex+1).'.'.'</div>';;
		    }	
	    }else {
		    echo '<div class="errorMsgDiv">'.$response->{'msg'}.'</div>';;
	    }

	    if(file_exists(UPLOAD_DIR.$filename))
		    unlink(UPLOAD_DIR.$filename);
	}
    
	/*
	 * Used for getting votes for manual results calculation
	 * 
	 * @param array getVars 
	 */
	public function exportVotes($getVars = array())
        {
                $electionId = $_SESSION[$this->userId]['activeElection'];
		$this->loadModelFiles(array('election','voter'));
		$electionModel = new Election_Model;
		$voterModel = new Voter_Model;
		
		$election = $electionModel->fetchById($electionId);
		if(empty($election)){
		    header('Location: '.SITE_ROOT.'/index.php?election');
                    exit;
		}
		
		if($election['status'] == Election_Model::STATE_ENDED){ 
		    $voters = $voterModel->fetchVotes($electionId);
		    
		    if(!is_null($voters) || !empty($voters)){

			require_once SERVER_ROOT.'/lib/parsecsv/parsecsv.lib.php';

			$csv = new parseCSV();
			$csv->encoding('UTF-8', 'UTF-8');
			$fileName = date('d.m.Y').'_'.mb_strtolower($election['name'],'UTF-8').'_'.'given_votes.csv';
			$csvHeader = array(STUDENT_NUMBER_TEXT,NAME_TEXT,VOTED_TEXT,VOTE_DATE_TEXT,VOTE_METHOD_TEXT);

			//convert into 2D array according to header
			$csvData = array();
			foreach($voters as $voter){
			    $studentNum = $voter['studentNum'];
			    $name = $voter['firstName'].' '.$voter['lastName'];
			    $voteDate = $voter['hasVoted'] == 1 ? date('d.m.Y H:i:s', strtotime($voter['voteDate'])) : "";
			    $hasVoted = $voter['hasVoted'] == 1 ? YES_TEXT : NO_TEXT;
			    
			    switch ($voter['voteMethod']) {
				case Voter_Model::VOTE_METHOD_PAPER:
					$voteMethod = PAPER_VOTE_SHORT_TEXT;
					break;
				case Voter_Model::VOTE_METHOD_WWW:
					$voteMethod = ELECTRONIC_VOTE_SHORT_TEXT;
					break;
				default:
					$voteMethod = "";
					break;
			    }//switch
			    $csvData[] = array($studentNum,$name,$hasVoted,$voteDate,$voteMethod);
			}
			echo '';
			$csv->output($fileName, $csvData, $csvHeader, CSV_SEMICOLON_DELIMITER);
		    }
		}else{
		    header('Location: '.SITE_ROOT.'/index.php?election&view=election_results&error=electionnotended');
                    exit;
		}  
        }
    
	private function messages($messages = array(), $data)
	{    	
	    if (isset($messages['success'])) {
		    switch ($messages['success']) {
			    case 'addedvoter':
				    $data['success'] = NEW_VOTER_ADDED_SUCCESSFULLY_TEXT;
				    break;
			    case 'editedvoter':
				    $data['success'] = VOTER_INFORMATION_UPDATED_SUCCESSFULLY_TEXT;
				    break;
			    case 'voterremoved':
				    $data['success'] = REMOVED_SUCCESSFULLY_TEXT;
				    break;
			    case 'removedAllSuccessfully':
				    $data['success'] = REMOVED_SUCCESSFULLY_TEXT;
				    break;
		    }
	    } else if(isset($messages['error'])) {
		    switch ($messages['error']) {
			    case 'voternotfound':
				    $data['error'] = VOTER_WAS_NOT_FOUND_TEXT;
				    break;
			    case 'cannoteditalreadyvoted':
				    $data['error'] = VOTER_CANNOT_BE_MODIFIED_TEXT."<br>".VOTER_HAS_ALREADY_VOTED_TEXT;
				    break;
			    case 'cannoteditvoter':
				    $data['error'] = MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT;
				    break;
			    case 'cannot_remove_voter':    			
				    $data['error'] = VOTERS_CANNOT_BE_REMOVED_EXPLANATION_TEXT;
				    break;
			    case 'cannot_remove_votes_given':
				    $data['error'] = VOTERS_CANNOT_BE_REMOVED_VOTES_GIVEN_TEXT;
				    break;
			    default:
				    $data['error'] = GENERAL_ERR_TEXT;
				    break;
		    }
	    }
	    return $data;
	}
}