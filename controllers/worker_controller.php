<?php
/**
 * Controller for the pages for the election workers.
 *
 * This controller should only be accessed by the election workers.
 *
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Worker_Controller extends Controller
{
	public $view = 'worker_list';
	public $template = 'template_worker';
		
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	
		//Prevent users that are not logged in as election workers from accessing these pages
		if($this->isLoggedIn === false || $this->isElectionWorker === false) {
			header('Location: '.SITE_ROOT.'/');
			exit;
		}
	}
	
	public function main($getVars = array())
	{
		$data = array();
			
		//Search string
		if(isset($_POST['search'])) {
			//Redirect to same page but this time the search string is in get variable (Post/Redirect/Get Pattern)
			header('Location: '.SITE_ROOT.'/index.php?worker&search='.urlencode($_POST['search']));
			exit;
		}
		else if(isset($getVars['search'])) {
			$data['searchStr'] = $getVars['search'];
		}
		else {
			$data['searchStr'] = null;
		}
		
		//Load model file(s)
		$this->loadModelFiles(array('voter', 'election'));
		$electionModel = new Election_Model;
		$voterModel = new Voter_Model;
	
		//Fetch current election
		$electionId = $electionModel->fetchCurrentElection('id');
		if($electionId == false) {
			//No ongoing elections
			$this->view = 'worker_closed';
			$this->loadView($data);
			exit;
		}
		 
		//Pagination data
		$limit = 15;	//Number of voters per page
		$offset = (isset($getVars['offset']))? (int) $getVars['offset']: 0;
	
		//Fetch total number of rows
		$totalRows = $voterModel->countByElectionAndSearch($electionId, $data['searchStr']);
	
		//Pagination data
		$data['pagination'] = $voterModel->getPaginationData($limit, $offset, $totalRows);
	
		//Fetch voters
		$data['voters'] = $voterModel->fetchVotersByElection($electionId, $data['pagination']['limit'], $data['pagination']['offset'], $data['searchStr']);
		
		//Base64 encode current url as referer
		//Base64 encode get variables of current url as referer parameter
// 		$data['referer'] = base64_encode(SITE_ROOT."/index.php?worker&search=".$data['searchStr']."&offset=".$offset);
		$data['referer'] = base64_encode("&search=".$data['searchStr']."&offset=".$offset);
		
		//Success messages
		if(isset($getVars['success']) && isset($getVars['t']) && time() < base_convert($getVars['t'], 36, 10) + 2) {
			switch ($getVars['success']) {
				case 'voteMarked':
					$data['success'] = VOTER_INFORMATION_UPDATED_SUCCESSFULLY_TEXT;
					break;
				default:
					break;
			}
		}
		$this->loadView($data);
	}
	
	public function confirm($getVars = array()) 
	{
		$data = array();
		
		if(isset($_POST['listReferer']))
			$data['referer'] = $_POST['listReferer'];
		else
			$data['referer'] = base64_decode($getVars['referer']);				
		
		//Remove possible success messages from $data['referer']
		$data['referer'] = preg_replace("/&success=voteMarked/i", "", $data['referer']);		
				
		//Load model file(s)
		$this->loadModelFiles(array('voter', 'election', 'admin'));
		$electionModel = new Election_Model;
		$voterModel = new Voter_Model;
		$adminModel = new Admin_Model;
		
		//Fetch current election
		$electionId = $electionModel->fetchCurrentElection('id');
		if($electionId == false) {
			//No ongoing elections
			$this->view = 'vote_closed';
			$this->loadView($data);
			exit;
		}
		
		if(isset($_POST['markVote'])) { 
			$studentNum = (int) $_POST['studentNum'];
			$adminId = $adminModel->getAdminIdByUsername($_SESSION[$this->userId]['username']);			
			$resultCode = $voterModel->markPaperVote($studentNum, $electionId, $adminId);
			switch ($resultCode) {
				case Voter_Model::SUCCESS:					
					header('Location: '.SITE_ROOT.'/index.php?worker'.$data['referer'].'&success=voteMarked&t='.base_convert(time(), 10, 36));
					exit;
					break;
				case Voter_Model::VOTER_ALREADY_VOTED:
					$data['error'] = VOTER_HAS_ALREADY_VOTED_TEXT;
					break;
				default:
					$data['error'] = UPDATE_FAILED_TEXT;
					break;
			}						
		}
		else if(isset($getVars['studentNum'])) {
			$studentNum = (int)$getVars['studentNum'];
		}
		else {
			header('Location: '.SITE_ROOT.'/index.php?worker'.$data['referer']);
			exit;
		}
						
		$data['voter'] = $voterModel->fetchByCompositeId(array($studentNum, $electionId));
		if(empty($data['voter'])) {
			header('Location: '.SITE_ROOT.'/index.php?worker'.$data['referer']);
			exit;
		}
		
		$this->view = 'worker_confirm';
		$this->loadView($data);
	}
}