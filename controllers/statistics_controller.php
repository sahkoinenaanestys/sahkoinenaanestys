<?php
/**
 * Controller for the statistics pages. 
 * 
 * This controller should only be accessed by the administrator.
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Statistics_Controller extends Controller
{
	public $view = 'past_elections';
	public $template = 'template_adm';
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		if($this->isLoggedIn === false){
			header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=invalidorexpiredsession');
			exit;
		}else if($this->isAdmin === false){
			header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=noaccessprivileges');
			exit;
		}
	}
	
	/**
	 * Called by router.php
	 *
	 * @param array $getVars
	 */
	public function main($getVars = array())
	{
		$data = array('loggedIn' => true);
		
		$this->loadModelFiles(array('election','candidate','voter'));
		$electionModel = new Election_Model;

		if(($elections = $electionModel->fetchPastElections()) != false) {   
		    $data['elections'] = $elections;
		}
		$data = $this->messages($getVars, $data);
		$this->loadView($data);
	}
	
	public function showCandidates($getVars = array())
	{
		$data = array('loggedIn' => true);
	    
		$this->loadModelFiles(array('candidate','election'));
		$electionId = (int) $getVars['electionId'];
		$order = parent::getOrder(array('rank','candidateNum'));
		
		$candidateModel = new Candidate_Model;
		$electionModel = new Election_Model;

		$election = $electionModel->fetchById($electionId);
		if(empty($election) || $election['status'] != Election_Model::STATE_HISTORY){
		    header('Location: '.SITE_ROOT.'/index.php?statistics');
		    return;
		}
		$data['election']['electionId'] = $electionId;
		
		if(($candidates = $candidateModel->fetchCandidatesAlliancesCoalitions($electionId, null, null, null, $order)) != false) {   
			$data['candidates'] = $candidates;
		}
		
		$this->view = 'past_election_results';
		$this->loadView($data);
	}
	
	public function exportCsvResults($getVars = array())
	{
		$data = array('loggedIn' => true);
		
		$this->loadModelFiles(array('candidate','election'));
		$electionId = (int) $getVars['electionId'];
		$candidateModel = new Candidate_Model;
		$electionModel = new Election_Model;

		$election = $electionModel->fetchById($electionId);
		if(empty($election) || $election['status'] != Election_Model::STATE_HISTORY){
		    header('Location: '.SITE_ROOT.'/index.php?statistics');
		    return;
		}
		   
		$order = parent::getOrder(array('rank','candidateNum'));
		$candidates = $candidateModel->fetchCandidatesAlliancesCoalitions($electionId, null, null, null, $order);  
		    
		if(!is_null($candidates) || !empty($candidates)){

		    require_once SERVER_ROOT.'/lib/parsecsv/parsecsv.lib.php';

		    $csv = new parseCSV();
		    $csv->encoding('UTF-8', 'UTF-8');
		    $fileName = date('d.m.Y').'_'.mb_strtolower($election['name'],'UTF-8').'_'.'statistics.csv';
		    
		    $csvHeader = array(RANK_TEXT,CANDIDATE_NUM_TEXT,NAME_TEXT,CANDIDATE_VOTES_TEXT,COMPARISION_NUM_TEXT,COALITION_COMPARISION_NUM_TEXT,ALLIANCE_TEXT,COALITION_TEXT); 

		    //convert into 2D array according to header
		    $csvData = array();
		    foreach($candidates as $candidate){
			$rank = $candidate['rank'];
			$candidateNum = $candidate['candidateNum'] == Candidate_Model::EMPTY_VOTE ? "" : $candidate['candidateNum'];
			$candidateName = $candidate['candidateNum'] == Candidate_Model::EMPTY_VOTE ? EMPTY_VOTES_TEXT : $candidate['firstName'].' '.$candidate['lastName'];
			$votes = $candidate['votes'];
			$comparision = number_format((double)$candidate['comparisionNum'], 2, '.', '');
			$coalitionComparision = number_format((double)$candidate['coalitionComparisionNum'], 2, '.', '');
			$alliance = $candidate['allianceName'];
			$coalition = $candidate['coalitionName'];
			
			$csvData[] = array($rank,$candidateNum,$candidateName,$votes,$comparision,$coalitionComparision,$alliance,$coalition);
		    }
		    echo '';
		    $csv->output($fileName, $csvData, $csvHeader, CSV_SEMICOLON_DELIMITER);
		}else{
		    header('Location: '.SITE_ROOT.'/index.php?statistics&error=candidatesnotfound');
		    exit;
		}
	}
	
	
	public function setOrder($getVars = array()) {
		parent::setOrder($getVars);
	}
	
	private function messages($messages = array(), $data) 
	{		
		if (isset($messages['success'])) {
		    switch ($messages['success']) {
		    }//switch
		}else if(isset($messages['error'])) {
		    switch ($messages['error']) {
			default:
			    $data['error'] = GENERAL_ERR_TEXT;
			    break;
		    }//switch
		}
		return $data;
	}
}
