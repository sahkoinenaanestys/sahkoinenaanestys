<?php
/**
 * Controller for the log out functionality
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Logout_Controller extends Controller
{	
	/**
	 * Called by router.php
	 * 
	 * @param array $getVars
	 */
	public function main($getVars = array())
	{	
		//Temporarily save the language setting
		$lang = $_SESSION['language'];		

		//Possible error message
		$msgVars = '';
		if(isset($getVars['error'])){
			$msgVars .= '&error='.$getVars['error'];
		} else if (isset($_SESSION['saml_logout_msg'])) {
			$msgVars .= $_SESSION['saml_logout_msg'];
		}
		//Logout success message
		if($msgVars == '')
			$msgVars .= '&success=logout';

		session_unset();
		session_destroy();
		foreach ($_COOKIE as $key => $val): setcookie($key, "", 1); endforeach; //Clear all cookies

		//Start new session
		session_set_cookie_params(COOKIE_EXPIRY, "/", COOKIE_DOMAIN, COOKIE_SECURE, true);
		session_start();
		session_regenerate_id(true);
		$_SESSION['language'] = $lang;
		
		//Let's load the front page by redirecting the browser 
		if(isset($getVars['caller']) && $getVars['caller'] == 'admin') {
			header('Location: '.SITE_ROOT.'/index.php?admin'.$msgVars);
			exit;
		}
		else {
			header('Location: '.SITE_ROOT.'/index.php?frontpage'.$msgVars);
			exit;
		}
	}
	
	public function samlLogout($getVars = array())
	{		
		//Possible error messages
		if(isset($getVars['error'])){
			$_SESSION['saml_logout_msg'] = '&error='.$getVars['error'];			
		}

		//Load a file which registers the simpleSAMLphp classes with the autoloader
		require_once(SERVER_ROOT.'/lib/simplesamlphp-1.10.0/lib/_autoload.php');
		
		//Select authentication source
		$as = new SimpleSAML_Auth_Simple('default-sp');
				
		//Logout
		if ((boolean)IDP_LOGOUT_URL) {
			header('Location: '.IDP_LOGOUT_URL.'?return='.SITE_ROOT.'/index.php?logout');
			exit;
		}
		$as->logout(SITE_ROOT.'/index.php?logout'); //Seems to work properly only with SimpleSAMLphp idp				
	}
}