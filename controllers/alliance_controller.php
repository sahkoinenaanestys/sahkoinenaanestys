<?php
/**
 * Controller for alliances and coalitions
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */

class Alliance_Controller extends Controller
{
	public $view = 'candidates_show';
	
        /**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	
		//Prevent users that are not logged in as admins from accessing these pages
		if($this->isLoggedIn === false){
                        header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=invalidorexpiredsession');
			exit;
		}
		else if($this->isAdmin === false){
			header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=noaccessprivileges'); 
			exit;
		}
	}
        
	/**
	 * Called by router.php
	 * 
	 * @param array $getVars
	 */
	public function main($getVars = array())
	{		
                $this->template = 'template_adm2';
                $data = array('loggedIn' => true);

		$electionId = $_SESSION[$this->userId]['activeElection'];

		$this->loadModelFiles(array('alliance', 'coalition', 'election'));
		$allianceModel = new Alliance_Model;
		$coalitionModel = new Coalition_Model;
		$electionModel = new Election_Model;

		//Fetch election information & redirect if election not found
		$data['election'] = $electionModel->fetchById($electionId);
		if(empty($data['election'])) {
		    header('Location: '.SITE_ROOT.'/index.php?election');
		    exit;
		}
		$data = $this->messages($getVars, $data);
		
		if(!isset($getVars['view'])): $getVars['view'] = ''; endif;
                
		switch($getVars['view']){
                    
		    case 'alliances_import':
			$this->view = 'alliances_import';
			$this->loadView($data);
			break;

		    default:
			if(($alliances = $allianceModel->fetchByField("election", $electionId)) !== false ) {   
			    $data['alliances'] = $alliances;
			}
			if(($coalitions = $coalitionModel->fetchByField("election", $electionId)) !== false ) {   
			    $data['coalitions'] = $coalitions;
			}

			$this->view = 'alliances_show';
			$this->loadView($data);	
			break;
		}//switch
	}
        
	/**
	 * Used for adding new alliances
	 * 
	 * @param array $getVars
	 */
	public function addAlliance($getVars = array())
	{	
                $this->template = 'template_adm2';
		$data = array('loggedIn' => true);

		if(!isset($_SESSION[$this->userId]['activeElection'])) {
			header('Location: '.SITE_ROOT.'/index.php?election');
			exit;
		}    	
		$electionId = (int) $_SESSION[$this->userId]['activeElection'];

		$this->loadModelFiles(array('election','alliance','coalition'));
		$electionModel = new Election_Model;
		$allianceModel = new Alliance_Model;

		$data['election'] = $electionModel->fetchById($electionId);
		if(empty($data['election'])) {
			header('Location: '.SITE_ROOT.'/index.php?election');
			exit;
		}
		if($data['election']['status'] != Election_Model::STATE_CLOSED) {
			header('Location: '.SITE_ROOT.'/index.php?alliance&error=actionnotallowed');
			exit;
		}

		if(isset($_POST['allianceSubmit'])) { //form was submitted
		    
			list($allianceArr, $formErrors) = $allianceModel->validateEditForm($_POST);    		

			if(empty($formErrors)) { //no input errors  			
				$resultCode = $allianceModel->insertAlliance($allianceArr, $electionId); 				    				

				switch ($resultCode) {
					case Alliance_Model::PK_EXISTS_ALREADY:
						$data['error'] = INSERT_FAILED_TEXT."<br>".ALLIANCE_ID_ALREADY_EXISTS_TEXT;
						break;
					case Alliance_Model::SUCCESS:
						header('Location: '.SITE_ROOT.'/index.php?alliance&success=allianceaddded');
						exit;
						break;    							
					default:
						$data['error'] = INSERT_FAILED_TEXT;
						break;    					
				}
			}else { //has input errors
				$data['error'] = INVALID_INPUT_PLEASE_CHECK_FIELDS_TEXT;
				$data['formErrors'] = $formErrors;
			}
			//Repopulate the form fields
			$coalitionModel = new Coalition_Model;
			$data['coalitions'] = $coalitionModel->fetchByField('election', $electionId);
			
			$data['alliance'] = array
			(
				'allianceId' => $_POST['allianceId'],
				'allianceName' => $_POST['allianceName'],
				'personInChargeEmail' => $_POST['personInChargeEmail'],
				'coalitionId' => $_POST['coalitionId']
			);
		}else {  //form has not been submitted 
			$coalitionId = 0;
			if(isset($getVars['coalitionId'])) { //add to coalition
			    $coalitionId = $getVars['coalitionId'];
			    
			    //check that coalitionId is valid
			    $coalitionModel = new Coalition_Model;
			    $res = $coalitionModel->countByFieldValues(array("coalitionId"=>$coalitionId, "election"=>$electionId));
			    if($res == false || $res < 1) {
				    header('Location: '.SITE_ROOT.'/index.php?alliance');
				    exit; 
			    }
			}
			$coalitionModel = new Coalition_Model;
			$data['coalitions'] = $coalitionModel->fetchByField('election', $electionId);

			$data['alliance'] = array('allianceId'=>'','allianceName'=>'','personInChargeEmail'=>'', 'coalitionId'=>$coalitionId);
		}    	    	    	    	
		$this->view = 'alliance_add';
		$this->loadView($data);			
	}
        
	/**
	 * Used for editing alliances
	 * 
	 * @param array $getVars
	 */
	public function editAlliance($getVars = array())
	{	
                $this->template = 'template_adm2';
		$data = array('loggedIn' => true);

		if(!isset($_SESSION[$this->userId]['activeElection'])) {
			header('Location: '.SITE_ROOT.'/index.php?election');
			exit;
		}    	
		$electionId = (int)$_SESSION[$this->userId]['activeElection'];

		$this->loadModelFiles(array('election','alliance','coalition'));
		$electionModel = new Election_Model;
		$allianceModel = new Alliance_Model;

		$data['election'] = $electionModel->fetchById($electionId);
		if(empty($data['election'])) {
			header('Location: '.SITE_ROOT.'/index.php?election');
			exit;
		}
		if($data['election']['status'] != Election_Model::STATE_CLOSED) {
			header('Location: '.SITE_ROOT.'/index.php?alliance&error=actionnotallowed');
			exit;
		}

		if(isset($_POST['allianceSubmit'])) { //form was submitted
		    
			list($allianceArr, $formErrors) = $allianceModel->validateEditForm($_POST);   		

			if(empty($formErrors)) { //no input errors  
			    
				$this->loadModelFiles(array('candidate'));
				$resultCode = $allianceModel->updateAlliance($allianceArr, $electionId); 				    				

				switch ($resultCode) {
					case Alliance_Model::PK_EXISTS_ALREADY:
						$data['error'] = INSERT_FAILED_TEXT."<br>".ALLIANCE_ID_ALREADY_EXISTS_TEXT;
						break;
					case Alliance_Model::SUCCESS:
						header('Location: '.SITE_ROOT.'/index.php?alliance&success=alliancemodified');
						exit;
						break;    							
					default:
						$data['error'] = INSERT_FAILED_TEXT;
						break;    					
				}
				
			}else { //has input errors
				$data['error'] = INVALID_INPUT_PLEASE_CHECK_FIELDS_TEXT;
				$data['formErrors'] = $formErrors;
			}
			//Repopulate the form fields
			$allianceIdOld = (isset($data['error']))? $_POST['allianceIdOld']: $_POST['allianceId'];
			
			$coalitionModel = new Coalition_Model;
			$data['coalitions'] = $coalitionModel->fetchByField('election', $electionId);
			
			$data['alliance'] = array
			(
				'allianceId' => $_POST['allianceId'],
				'allianceIdOld' => $allianceIdOld,
				'allianceName' => $_POST['allianceName'],
				'personInChargeEmail' => $_POST['personInChargeEmail'],
				'coalitionId' => $_POST['coalitionId']
			);
			
		}else {  //form has not been submitted  
			$coalitionId = 0;
			
			if(isset($getVars['coalitionId'])) { //add to coalition
			    $coalitionId = $getVars['coalitionId'];
			    
			    //check that coalitionId is valid
			    $coalitionModel = new Coalition_Model;
			    $res = $coalitionModel->countByFieldValues(array("coalitionId"=>$coalitionId, "election"=>$electionId));
			    if($res == false || $res < 1) {
				    header('Location: '.SITE_ROOT.'/index.php?alliance');
				    exit; 
			    }
			}
			
			$coalitionModel = new Coalition_Model;
			$data['coalitions'] = $coalitionModel->fetchByField('election', $electionId);

			$allianceId = $getVars['allianceId'];
			$data['alliance'] = $allianceModel->fetchByCompositeId(array($allianceId, $electionId));
			if(empty($data['alliance'])) {
				header('Location: '.SITE_ROOT.'/index.php?alliance');
				exit;
			}
			$data['alliance']['allianceIdOld'] = $data['alliance']['allianceId'];
		}    	    	    	    	
		$this->view = 'alliance_edit';
		$this->loadView($data);			
	}
        
	/**
	 * Used for deleting alliances
	 * 
	 * @param array $getVars
	 */
        public function removeAllianceAjax($getVars = array())
	{
		$result = array();
		$result['status'] = false;
		
                $allianceId = $getVars['allianceId'];
                $electionId = $getVars['electionId'];
		
		$this->loadModelFiles(array('alliance', 'election','candidate'));
                
		$electionModel = new Election_Model;
		if($electionModel->canBeDeleted($electionId) == false) {    		   		    		
			$result['msg'] = REMOVE_FAILED_TEXT.' '.MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT;
			echo json_encode($result);
			exit;
		}

		$allianceModel = new Alliance_Model; 
		$alliance = $allianceModel->fetchByCompositeId(array($allianceId, $electionId));
		if(empty($alliance)) {
			header('Location: '.SITE_ROOT.'/index.php?alliance');
			exit;
		}
		
		if($allianceModel->removeAlliance($allianceId, $electionId) == false) {
		    $result['msg'] = REMOVE_FAILED_TEXT;
		}else{
		    $result['status'] = true; 
		    $result['msg'] = REMOVED_SUCCESSFULLY_TEXT;
		}   
                echo json_encode($result);
        }
	
	/**
	 * Used for adding and editing coalitions
	 * 
	 * @param array $getVars
	 */
	public function editCoalition($getVars = array())
	{		
		$this->template = 'template_adm2';
		$data = array('loggedIn' => true);

		if(!isset($_SESSION[$this->userId]['activeElection'])) {
			header('Location: '.SITE_ROOT.'/index.php?election');
			exit;
		}    	
		$electionId = (int)$_SESSION[$this->userId]['activeElection'];

		$this->loadModelFiles(array('election','coalition', 'alliance'));
		$electionModel = new Election_Model;
		$coalitionModel = new Coalition_Model;

		$data['election'] = $electionModel->fetchById($electionId);
		if(empty($data['election'])) {
			header('Location: '.SITE_ROOT.'/index.php?election');
			exit;
		}
		if($data['election']['status'] != Election_Model::STATE_CLOSED) {
			header('Location: '.SITE_ROOT.'/index.php?alliance&error=actionnotallowed');
			exit;
		}

		if(isset($_POST['coalitionSubmit'])) { //form was submitted
		    
			$this->loadModelFiles(array('coalition', 'candidate'));
			list($coalitionArr, $formErrors) = $coalitionModel->validateEditForm($_POST);    		

			if(empty($formErrors)) {
				$coalitionArr['oldCoalitionId'] = $_POST['oldCoalitionId'];
				
				$resultCode = $coalitionModel->updateCoalition($coalitionArr, $electionId);
				$action = $_POST['do'];
				
				switch ($resultCode) {
					case Coalition_Model::SUCCESS:
						if($action == 'new')
						    header('Location: '.SITE_ROOT.'/index.php?alliance&success=coalitionaddded');
						else if($action == 'edit')
						    header('Location: '.SITE_ROOT.'/index.php?alliance&success=coalitionmodified');
						exit;
						break;
					case Coalition_Model::FAILURE:
						if($action == 'new')
						    $data['error'] = INSERT_FAILED_TEXT;
						else if($action == 'edit')
						    $data['error'] = UPDATE_FAILED_TEXT;
						break;    
					default:
						if($action == 'new')
						    $data['error'] = INSERT_FAILED_TEXT;
						else if($action == 'edit')
						   $data['error'] = UPDATE_FAILED_TEXT;
						break;
				}  					    					
 			
			}else {
				$data['error'] = INVALID_INPUT_PLEASE_CHECK_FIELDS_TEXT;
				$data['formErrors'] = $formErrors;
			}	
			//Repopulate the form fields
			$oldCoalitionId = (isset($data['error']))? $_POST['oldCoalitionId']: $_POST['coalitionId'];
			
			$data['coalition'] = array(
				'coalitionId' => $_POST['coalitionId'],
				'coalitionName' => $_POST['coalitionName'],
				'oldCoalitionId' => $oldCoalitionId,
				'do' => $_POST['do']
			);
			
                }else{ //form hasn't been submitted
                    if(isset($getVars['coalitionId'])) { //edit coalition
                        $coalitionId = $getVars['coalitionId'];
			
			$data['coalition'] = $coalitionModel->fetchByCompositeId(array($coalitionId, $electionId));
			if(empty($data['coalition'])) {
				header('Location: '.SITE_ROOT.'/index.php?alliance');
				exit;
			}

                        //Populate the form fields
                        $data['coalition']['do'] = "edit";
			$data['coalition']['oldCoalitionId'] = $data['coalition']['coalitionId'];
                    }else{ //new coalition
			$data['coalition']['coalitionId'] = "";
			$data['coalition']['oldCoalitionId'] = 0;
			$data['coalition']['coalitionName'] = "";
                        $data['coalition']['do'] = 'new'; 
                    }
                }    	    	    	    	
		$this->view = 'coalition_edit';
		$this->loadView($data);			
	}
        
	/**
	 * Used for deleting coalitions
	 * 
	 * @param array $getVars
	 */
        public function removeCoalitionAjax($getVars = array())
	{
                $result = array();
		$result['status'] = false;
		
                $coalitionId = $getVars['coalitionId'];
                $electionId = $getVars['electionId'];

		$this->loadModelFiles(array('coalition','election','alliance','candidate'));
                
		$electionModel = new Election_Model;
		if($electionModel->canBeDeleted($electionId) == false) {    		   		    		
			$result['msg'] = REMOVE_FAILED_TEXT.' '.MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT;
			echo json_encode($result);
			exit;
		}
		
		$coalitionModel = new Coalition_Model; 
		$coalition = $coalitionModel->fetchByCompositeId(array($coalitionId, $electionId));
		if(empty($coalition)) {
			header('Location: '.SITE_ROOT.'/index.php?alliance');
			exit;
		}
		
		if($coalitionModel->removeCoalition($coalitionId, $electionId) == false) {
		    $result['msg'] = REMOVE_FAILED_TEXT;
		}else{
		    $result['status'] = true; 
		    $result['msg'] = REMOVED_SUCCESSFULLY_TEXT;
		}  
                echo json_encode($result);
        }
	
	public function importFromCsvAjax($getVars = array())
        {
		$electionId = (int)$_SESSION[$this->userId]['activeElection'];
		$this->loadModelFiles(array('election'));
		$electionModel = new Election_Model;

		$election = $electionModel->fetchById($electionId);
		if(empty($election) || $election['status'] != Election_Model::STATE_CLOSED){
		    echo '<div class="errorMsgDiv">'.MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT.'</div>';
		    exit;
		}
		$data = parent::uploadFile();
                
                $response = json_decode($data);
                if($response->{'status'} == true){
                    $filename = $response->{'file'};
                    
                    $minCols = 3;
                    $maxCols = 4+1;
                    $errors = array();
                    $electionId = $_SESSION[$this->userId]['activeElection'];
                    
                    parent::changeFileEncoding($filename, UPLOAD_DIR, 'UTF-8');
                    
                    require_once SERVER_ROOT.'/lib/parsecsv/parsecsv.lib.php';
                    require_once SERVER_ROOT.'/lib/ForceUTF8/Encoding.php';

                    $csv = new parseCSV();
                    $csv->encoding('UTF-8', 'UTF-8');
                    $csv->delimiter = isset($_POST['csvDelimiter']) ? $_POST['csvDelimiter'] : ","; 
                    $csv->parse(UPLOAD_DIR.$filename); 
                        
                    $this->loadModelFiles(array('alliance', 'coalition'));
                    $allianceModel = new Alliance_Model; 

                    foreach($csv->data as $rowIndex => $row){

			if(count($row) < $minCols || count($row) > $maxCols){
				$errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).') '.FILE_INVALID_COLUMNS_AMOUNT_TEXT;
				break;
			}
			$alliance = array();
			$i = 0;				
			foreach($row as $value) {
				switch ($i) {
					case 0: $alliance['allianceName'] = Encoding::fixUTF8($value); break;
					case 1: $alliance['personInChargeEmail'] = Encoding::fixUTF8($value); break;
					case 2: $alliance['allianceId'] = $value; break;
					case 3: $alliance['coalitionId'] = $value; break;
					case 4: $alliance['coalitionName'] = Encoding::fixUTF8($value); break;
				}
				$i++;
				$alliance['coalitionId'] = isset($alliance['coalitionId']) ? $alliance['coalitionId'] : 0;
				$alliance['coalitionName'] = isset($alliance['coalitionName']) ? $alliance['coalitionName'] : "";
			}

                        if(is_numeric($alliance['allianceId']) && $alliance['allianceId'] > 0){
                            $result = $allianceModel->insertAllianceAndCoalition($alliance['allianceName'],$alliance['personInChargeEmail'],$alliance['coalitionId'], $alliance['coalitionName'], $alliance['allianceId'], $electionId);

                            if($result == Alliance_Model::FAILURE){
                                $errors[] = '1'.IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).') '.ALLIANCE_INSERT_FAILED_TEXT.': '.$alliance['allianceId'].': '.$alliance['allianceName'];
				break;
			    }else if($result == Alliance_Model::PK_EXISTS_ALREADY){
                                $errors[] = IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).') '.ALLIANCE_TEXT.' '.$alliance['allianceId'].' '. EXISTS_ALREADY_TEXT;
				break;
			    }
                        }else{
                            $errors[] = '2'.IMPORT_WAS_ABORTED_TEXT.' ('.ROW_TEXT.' '.($rowIndex+2).') '.ALLIANCE_INSERT_FAILED_TEXT.' '.$alliance['allianceId'].': '.$alliance['allianceName'];
			    break;
                        }
                    }//foreach 
                    
                    if(count($errors) > 0){ 
			echo '<div class="errorMsgDiv">';
                        foreach($errors as $error){
			    echo $error.'<br>';
			} 
			echo '</div>';
                    }else{
			echo '<div class="successMsgDiv">'.ALLIANCES_IMPORTED_SUCCESSFULLY_TEXT.'</div>';
                    }
		    unlink(UPLOAD_DIR.$filename); 
                }else{
		    echo '<div class="errorMsgDiv">'.$response->{'msg'}.'</div>';
                }
        }
		
	private function messages($messages = array(), $data) 
	{		
		if (isset($messages['success'])) {
		    switch ($messages['success']) {
			case 'coalitionaddded':
			    $data['success'] = COALITION_ADDED_SUCCESSFULLY_TEXT;
			    break;
			case 'coalitionmodified':
			    $data['success'] = COALITION_UPDATED_SUCCESSFULLY_TEXT;
			    break;
			case 'allianceaddded':
			    $data['success'] = ALLIANCE_ADDED_SUCCESSFULLY_TEXT;
			    break;
			case 'alliancemodified':
			    $data['success'] = ALLIANCE_UPDATED_SUCCESSFULLY_TEXT;
			    break;
			case 'removedsuccessfully':
			    $data['success'] = REMOVED_SUCCESSFULLY_TEXT;
			    break;
		    }
		}else if(isset($messages['error'])) {
		    switch ($messages['error']) {
			case 'actionnotallowed':
			    $data['error'] = MODIFYING_ACTIVE_ELECTION_NOT_ALLOWED_TEXT;
			    break;
			case 'cannot_remove_candidates':
			    $data['error'] = CANDIDATES_CANNOT_BE_REMOVED_EXPLANATION_TEXT;
			    break;
		    }
		}
		return $data;
	}
       
}
