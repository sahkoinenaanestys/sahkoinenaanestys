<?php
/**
 * Controller for the voting page
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Vote_Controller extends Controller
{
	public $view = 'vote_page';	//default view for the vote controller methods
	private $loginInfo = array();
	private $electionId = null;
	private $hasVoted = false;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{		
		parent::__construct();
		
		//Prevent admins from accessing this page
		if($this->isAdmin === true || $this->isElectionWorker === true) {
			header('Location: '.SITE_ROOT.'/');
			exit;
		}
		
		//Check SAML authentication
		require_once(SERVER_ROOT.'/lib/simplesamlphp-1.10.0/lib/_autoload.php');		
		$as = new SimpleSAML_Auth_Simple('default-sp');
		if (!$as->isAuthenticated()) {
			if(isset($_SESSION['hasPreviouslyLoggedIn']) && $_SESSION['hasPreviouslyLoggedIn'] == true) {
				header('Location: '.SITE_ROOT.'/index.php?logout&action=samlLogout&error=sessionexpired');
				exit;
			} else if(isset($_SESSION['logoutAfterVote']) && $_SESSION['logoutAfterVote'] == true) {								
				header('Location: '.SITE_ROOT.'/index.php?logout&action=samlLogout');
				exit;
			} else {
				header('Location: '.SITE_ROOT.'/index.php?frontpage');
				exit;
			}						
		}			
		
		$attributes = $as->getAttributes();
		
		$this->loadModelFiles(array('voter', 'election'));
		$voterModel = new Voter_Model;
		$electionModel = new Election_Model;
				
		$this->electionId = $electionModel->fetchCurrentElection('id');
		if(!$this->electionId) {
			//No ongoing elections, so we will logout the user
			header('Location: '.SITE_ROOT.'/index.php?logout&action=samlLogout&error=logoutElectionEnded');
			exit;
		}
		
		$studentNum = 0;
		if(isset($attributes[IDP_STUDENTNUM_ATTRIBUTE][0])) {
			$studentNum = (int)(str_replace(IDP_STUDENTNUM_ATTRIBUTE_VALUE_NAMESPACE.":", "", $attributes[IDP_STUDENTNUM_ATTRIBUTE][0]));
		}

		$voterArr = $voterModel->fetchByCompositeId(array($studentNum, $this->electionId));
		if( $voterArr == false || $studentNum == 0 ||
				$voterModel->canVote($studentNum) === false ||
				$voterArr['studentNum'] != $studentNum)
		{
			//Voter was not in the voter list or voter did not have right to vote
			header('Location: '.SITE_ROOT.'/index.php?logout&action=samlLogout&error=logoutCannotVote');
			exit;
		}
		
		if(!isset($_SESSION['hasPreviouslyLoggedIn'])) {
			//Set session infromation so we know if user's session has expired
			$_SESSION['hasPreviouslyLoggedIn'] = true;
			
			//Use random login hash to prevent simultaneous logins with the same account
			$loginHash = sha1(microtime(true).mt_rand(10000,90000));
			$_SESSION[$this->userId]['loginHash'] = $loginHash;
			$voterModel->setLoginHash($loginHash, $voterArr['studentNum'], $this->electionId);
		} else {
			//Check that login hash in the database matches the hash in the session
			$dbHash = $voterModel->getLoginHash($voterArr['studentNum'], $this->electionId);
			
			if($dbHash != $_SESSION[$this->userId]['loginHash']) {
				header('Location: '.SITE_ROOT.'/index.php?logout&action=samlLogout&error=sessionexpired');
				exit;
			}
		}
		
		//Check if the voter has already voted
		if($voterModel->hasVoted($voterArr['studentNum']) === true) {
			$this->hasVoted = true;
		}
		
		$this->loginInfo = array
		(
			'name' => $voterArr['firstName'].' '.$voterArr['lastName'],
			'studentNum' => $voterArr['studentNum'],
			'email' => $voterArr['email']
		);								
	}
	
	/**
	 * Called by router.php
	 * 
	 * @param array $getVars
	 */
	public function main($getVars = array())
	{
		//Redirect if the voter already voted
		if($this->hasVoted === true) {
			header('Location: '.SITE_ROOT.'/index.php?vote&action=given');
			exit;
		}
		
		//__Contstruct already checks that the user is logged in
    	$data = array('loggedIn' => true, 'user' => $this->loginInfo);
		
		//Load model file(s)
		$this->loadModelFiles(array('candidate', 'election'));		
		$candidateModel = new Candidate_Model;
		$electionModel = new Election_Model;								
					
		//Get the name of the current election
		$electionArr = $electionModel->fetchCurrentElection();
		if(!$electionArr) {
			//No ongoing elections, so we will logout the user
			header('Location: '.SITE_ROOT.'/index.php?logout&action=samlLogout&error=logoutElectionEnded');
			exit;	
		}
		$data['election'] = $electionArr['name'];				
		
		//Get candidates
		$data['candidates'] = $candidateModel->fetchCandidatesGroupedByAlliances($electionArr['electionId']);				

		//Error messages
		if(isset($getVars['error']) && isset($getVars['t']) && time() < base_convert($getVars['t'], 36, 10) + 2) {
			switch ($getVars['error']) {
				case 'novote':					
					$data['error'] = CANDIDATE_WAS_NOT_SELECTED_TEXT;
					break;
				case 'candidateNotFound':
					$data['error'] = CANDIDATE_WAS_NOT_FOUND_TEXT;
					break;
				case 'votingfailed':
					$data['error'] = VOTING_FAILED_TEXT;
					break;
			}			
		}				
		
		//Load view
		$this->loadView($data);		
	}			
	
	public function confirm($getVars = array())
	{	
		//Redirect if the voter already voted
		if($this->hasVoted === true) {
			header('Location: '.SITE_ROOT.'/index.php?vote&action=given');
			exit;
		}
		
		//__Contstruct already checks that the user is logged in
		$data = array('loggedIn' => true, 'user' => $this->loginInfo);				
		
		//Parse the post variable
		if(isset($_POST['candidate']) && is_numeric($_POST['candidate'])) {
			$candidateNum = (int) $_POST['candidate'];
		}
		else {
			//Show error message when candidate not selected
			header('Location: '.SITE_ROOT.'/index.php?vote&error=novote&t='.base_convert(time(), 10, 36));
			exit;
		}
	
		//Load model file(s)
		$this->loadModelFiles(array('candidate'));
		$candidateModel = new Candidate_Model;		

		//Fetch the candidate		
		$candidate = $candidateModel->fetchByCompositeId(array($candidateNum, $this->electionId));
		if(!$candidate) {
			//Candidate not found
			header('Location: '.SITE_ROOT.'/index.php?vote&error=candidateNotFound&t='.base_convert(time(), 10, 36));
			exit;
		}
		
		if($candidate['candidateNum'] > 0) {
			$data['candidateId'] = $candidateNum;
			$data['candidateNum'] = $candidate['candidateNum'];
			$data['firstName'] = $candidate['firstName'];
			$data['lastName'] = $candidate['lastName'];
			$data['isEmpty'] = false;
		} else if ($candidate['candidateNum'] == -1) {
			$data['isEmpty'] = true;
			$data['candidateNum'] = -1;
		} else {
			header('Location: '.SITE_ROOT.'/index.php?vote&error=candidateNotFound&t='.base_convert(time(), 10, 36));
			exit;
		}
		
		//Load view
		$this->view = 'vote_confirm';
		$this->loadView($data);
	}
	
	public function accept($getVars = array())
	{
		//Redirect if the voter already voted
		if($this->hasVoted === true) {
			header('Location: '.SITE_ROOT.'/index.php?vote&action=given');
			exit;
		}

		//__Contstruct already checks that the user is logged in
		$data = array('loggedIn' => true, 'user' => $this->loginInfo);
		
		//Parse the post variable
		if(isset($_POST['candidate']) && is_numeric($_POST['candidate'])) {
			$candidateNum = (int) $_POST['candidate'];
		} else {
			//Show error message when candidate not selected
			header('Location: '.SITE_ROOT.'/index.php?vote&error=novote&t='.base_convert(time(), 10, 36));
			exit;
		}
		
		//Load model file(s)
		$this->loadModelFiles(array('candidate'));
		$candidateModel = new Candidate_Model;		
		
		//Give the vote
		if($candidateModel->giveVote($candidateNum, $this->loginInfo['studentNum'], $this->electionId) !== true) {
			//Show error message that voting failed
			header('Location: '.SITE_ROOT.'/index.php?vote&error=votingfailed&t='.base_convert(time(), 10, 36));
			exit;
		}
		
		//Get the candidate
		$candidate = $candidateModel->fetchByCompositeId(array($candidateNum, $this->electionId));		
		$data['success'] = YOUR_VOTING_WAS_SUCCESSFUL_TEXT;				
		if($candidate['candidateNum'] > 0) {
			$data['candidateNum'] = $candidate['candidateNum'];
			$data['firstName'] = $candidate['firstName'];
			$data['lastName'] = $candidate['lastName'];
			$data['isEmpty'] = false;
		} else {
			$data['isEmpty'] = true;
		}
				
		//Destroy session data and clear cookies
		$lang = $_SESSION['language']; //Temporarily save the language setting
		foreach ($_COOKIE as $key => $val): setcookie($key, "", 1); endforeach;
		session_unset();
		session_destroy();
		session_set_cookie_params(COOKIE_EXPIRY, "/", COOKIE_DOMAIN, COOKIE_SECURE, true);
		session_start(); //Start new session
		session_regenerate_id(true);
		$this->userId = session_id();
		
		$_SESSION['language'] = $lang;
		$_SESSION['logoutAfterVote'] = true;
		
		//Load view
		$this->view = 'vote_success';						
		$data['automaticLogout'] = true;
		$this->loadView($data);				
	}
	
	public function given() 
	{
		//Redirect if the voter has not voted
		if($this->hasVoted === false) {
			header('Location: '.SITE_ROOT);
			exit;
		}
		
		//__Contstruct already checks that the user is logged in
		$data = array('loggedIn' => true, 'user' => $this->loginInfo);									
		
		//Destroy session data and clear cookies
		$lang = $_SESSION['language']; //Temporarily save the language setting
		foreach ($_COOKIE as $key => $val): setcookie($key, "", 1); endforeach;
		session_unset();
		session_destroy();
		session_set_cookie_params(COOKIE_EXPIRY, "/", COOKIE_DOMAIN, COOKIE_SECURE, true);
		session_start(); //Start new session
		session_regenerate_id(true);
		$this->userId = session_id();
		
		$_SESSION['language'] = $lang;
		$_SESSION['logoutAfterVote'] = true;
		
		//Load view
		$this->view = 'vote_given';
		$data['automaticLogout'] = true;		
		$this->loadView($data);
	}			
}
