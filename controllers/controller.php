<?php
/**
 * Application controller class
 * 
 * This class is the super class to other controller classes
 *  
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Controller {
	public $view = 'frontpage';	//default view
	public $template = 'template';	//default template
	public $controllerName = '';
	protected $isLoggedIn = false;
	protected $isAdmin = false;
	protected $isElectionWorker = false;
	protected $userId = '';
	
	/**
	 * Constructor
	 */
	public function __construct() 
	{
		//Store previous url
		$urlStorageBlackList = array('Language_Controller', 'Logout_Controller', 'Vote_Controller');
		if(!in_array(get_class($this), $urlStorageBlackList) && !preg_match("/ajax/i", $_SERVER['REQUEST_URI']) && !preg_match("/ajax/i", $_SERVER['REQUEST_URI'])) {			
			$currentUrl = '/';
			if(strpos($_SERVER['REQUEST_URI'], "/index.php?") !== false) {
				list(, $currentUrl) = explode("/index.php?", $_SERVER['REQUEST_URI']);
				$currentUrl = '/index.php?'.$currentUrl;
			}										
			
			if (isset($_SESSION['previous_url']) && $_SESSION['previous_url'] != $currentUrl) {
				$_SESSION['previous_url'] = $currentUrl;
			}	
		}
		if (!isset($_SESSION['previous_url'])) {
			$_SESSION['previous_url'] = '/';
		}
		
		//Controller name
		$this->controllerName = strtolower(str_replace("_Controller", "", get_class($this)));
		
		//Check if user is logged in
		$this->userId = session_id();				
		if(get_class($this) != 'Logout_Controller')			
			$this->checkLogin();
	}
	
	/**
	 * Checks if the current user is logged in
	 */
	protected function checkLogin() 
	{		
		//Login check functionality		
		if(isset($_SESSION[$this->userId]['loggedIn']) && $_SESSION[$this->userId]['loggedIn'] == true){
			$this->isLoggedIn = true;
		}

		//Check if user is admin
		if(isset($_SESSION[$this->userId]['isAdmin']) && ($_SESSION[$this->userId]['isAdmin'] == true || 
				$_SESSION[$this->userId]['isElectionWorker'] == true)){
		    
			if($_SESSION[$this->userId]['isAdmin'] == true)
				$this->isAdmin = true;
			if($_SESSION[$this->userId]['isElectionWorker'] == true)
				$this->isElectionWorker = true;
		    
		    //Check if database loginHash matches session loginHash
		    $this->loadModelFiles(array('admin'));
		    $adminModel = new Admin_Model;
		    
		    $username = isset($_SESSION[$this->userId]['username']) ? $_SESSION[$this->userId]['username'] : "";
		    $loginHash = isset($_SESSION[$this->userId]['loginHash']) ? $_SESSION[$this->userId]['loginHash'] : "";
		    
		    $adminId = $adminModel->getAdminIdByUsername($username);
		    if($adminModel->countByFieldValues(array('adminId'=>$adminId,'loggedIn'=>$loginHash)) < 1){
				header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=invalidorexpiredsession');
				exit;
		    }
		    
		    //Check if user has been inactive for too long
		    if($adminModel->checkAdminLastSeen($adminId) == false) {
				header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=invalidorexpiredsession');				
				exit;
		    } else {
				$this->setLastSeen($adminId, $adminModel); //update timestamp
		    }
		    
		    //Check if both the IP address AND the user agent have changed
		    If ($_SESSION[$this->userId]['_USER_IP'] != $_SERVER['REMOTE_ADDR']
					&& $_SESSION[$this->userId]['_USER_AGENT'] != $_SERVER['HTTP_USER_AGENT'])
		    {
		    	header('Location: '.SITE_ROOT.'/index.php?logout&caller=admin&error=invalidorexpiredsession');
		    	exit;
		    }
		    
		} else {
			//Give voters access to only some of the controllers
			$voterWhitelist = array('Language_Controller', 'Logout_Controller', 'Vote_Controller', 'Instructions_Controller');
			
			if(!in_array(get_class($this), $voterWhitelist)) {
				//Redirect to the voting page if user is logged in as voter
				require_once(SERVER_ROOT.'/lib/simplesamlphp-1.10.0/lib/_autoload.php');
				$as = new SimpleSAML_Auth_Simple('default-sp');
				if($as->isAuthenticated()) {
					header('Location: '.SITE_ROOT.'/index.php?vote');
					exit;
				}
			}
		}
	}
	
	protected function setLastSeen($adminId, $model)
	{
		$params = array($adminId);
		$updateClause = "lastSeen=NOW() WHERE adminId=?";	

		return $model->update($updateClause, $params);
	}
	
	protected function loadModelFiles($modelArr = array()) 
	{
		if(!empty($modelArr))
		{
			foreach($modelArr as $model)
			{
				$modelFile = SERVER_ROOT.'/models/'. $model.'.php';
				
				if(file_exists($modelFile)) {
					include_once($modelFile);
				}
				else {
					die('File "'.$modelFile.'" not found.');
				}
								
			}					
		}
	}
	
	protected function loadView($data = array()) 
	{		
		//Check that template and view files exist
		if(!file_exists(SERVER_ROOT.'/views/'.$this->view.'.php'))
			die('View file not found.');
		if(!file_exists(SERVER_ROOT.'/views/templates/'.$this->template.'.php'))
			die('Template file not found.');
		
		new View_Model($data, $this->view, $this->template, $this->controllerName);		
	}
	
	protected function getOrder($params = array())
	{
		$order = "";
		if (isset($_SESSION['sort_by']) && $_SESSION['sort_by'] != "") {
		    if (in_array($_SESSION['sort_by'], $params)) 
			$order = "ORDER BY ".$_SESSION['sort_by']." ".$_SESSION['sort_order'];
		}
		return $order;
	}
	
	protected function setOrder($getVars = array())
	{
		if (isset($getVars['sort-by'])){
		    $sort_by = $getVars['sort-by'];

		    if (!isset($_SESSION['sort_order'])) {
			$sort_order = 'asc';
		    } else {
			switch ($_SESSION['sort_order']) {
			case 'asc':
				$sort_order = 'desc';
				break;
			case 'desc':
				$sort_order = 'asc';
				break;
			default:
				$sort_order = 'asc';
			}
		    }
		    $_SESSION['sort_by'] = $sort_by;
		    $_SESSION['sort_order'] = $sort_order;
		}
	}
        
        public function uploadFile()
        {
                $result = array();
                
                if (!@file_exists(UPLOAD_DIR)) {
                    $result['status'] = false;
                    $result['msg'] = UPLOAD_NO_DIR_TEXT;
                    return json_encode($result);
                }

                if(!isset($_FILES['mFile']) /*|| !is_uploaded_file($_FILES['mFile']['tmp_name'])*/){
                    $result['status'] = false;
                    $result['msg'] = UPLOAD_EMPTY_FILE_TEXT;
                    return json_encode($result);
                }

                if($_FILES['mFile']['error']){
                    $result['status'] = false;
                    $result['msg'] = uploadErrors($_FILES['mFile']['error']);
                    return json_encode($result);
                }
                
                $fileName           = strtolower($_FILES['mFile']['name']); //uploaded file name
                $fileExt            = substr($fileName, strrpos($fileName, '.')); //file extension
                $fileTitle          = substr($fileName, 0, strrpos($fileName, '.')); //file title
                $fileType           = $_FILES['mFile']['type']; //file type
                $fileSize           = $_FILES['mFile']["size"]; //file size
                $randNum            = rand(0, 9999999999); //Random number to make filenames unique
                $upload_date        = date("Ymd_His");

                switch(strtolower($fileType))
                {
                    //allowed file types
                    //case 'image/png': //png file
                    //case 'image/gif': //gif file
                    //case 'image/jpeg': //jpeg file
                    //case 'application/pdf': //PDF file
                    //case 'application/msword': //ms word file
                    //case 'application/vnd.ms-excel': //ms excel file
                    //case 'application/x-zip-compressed': //zip file                	
                	case 'application/vnd.ms-excel':	//Microsoft Office 2010 (Finnish), CSV (luetteloerotin)(*.csv)
                	case 'text/plain': //text/csv file
                	case 'text/csv': //mac-csv file	
			case 'application/csv': //MS Excel Mac CSV
			case 'text/comma-separated-values': //MS Excel Mac CSV
                        break;
                    default:
                        $result['status'] = false;
                        $result['msg'] = UPLOAD_FILE_NOT_SUPPORTED_TEXT;
                        return json_encode($result);
                }

                $newFileName = /*$fileTitle.*/'upload_file_'.$upload_date.$randNum.$fileExt;
                
                //chmod(UPLOAD_DIR, 0755);
                
                if(move_uploaded_file($_FILES['mFile']["tmp_name"], UPLOAD_DIR . $newFileName ))
                {
                    $result['status'] = true;
                    //$result['msg'] = UPLOAD_NO_ERR_TEXT;
                    $result['file'] = $newFileName;
		    $result['fileSize'] = $fileSize;
		    $result['fileType'] = $fileType;
                    return json_encode($result);
                }else{
                    $result['status'] = false;
                    $result['msg'] = UPLOAD_ERR_TEXT;
                    return json_encode($result);
                }
        }     
        
        private function uploadErrors($err_code) 
        {
                switch ($err_code) {
                    case UPLOAD_ERR_INI_SIZE:
                        return UPLOAD_ERR_INI_SIZE_TEXT;
                    case UPLOAD_ERR_FORM_SIZE:
                        return UPLOAD_ERR_FORM_SIZE_TEXT;
                    case UPLOAD_ERR_PARTIAL:
                        return UPLOAD_ERR_PARTIAL_TEXT;
                    case UPLOAD_ERR_NO_FILE:
                        return UPLOAD_ERR_NO_FILE_TEXT;
                    case UPLOAD_ERR_NO_TMP_DIR:
                        return UPLOAD_ERR_NO_TMP_DIR_TEXT;
                    case UPLOAD_ERR_CANT_WRITE:
                        return UPLOAD_ERR_CANT_WRITE_TEXT;
                    case UPLOAD_ERR_EXTENSION:
                        return UPLOAD_ERR_EXTENSION_TEXT;
                    default:
                        return UPLOAD_UNKNOWN_ERR_TEXT;
                }
        }
        
        public function changeFileEncoding($fileName, $filePath, $encoding)
        {
            $data = file_get_contents($filePath.$fileName);
            $data = mb_convert_encoding($data, $encoding);
            file_put_contents($filePath.$fileName, $data);
        }
	
	
}
