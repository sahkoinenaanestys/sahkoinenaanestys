<?php
/**
 * Utils DataValidator
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class DataValidator
{
    public function __construct(){}
    
    /**
    * Sanitize a password stripping out dangerous characters
    * 
    * @param array $settings
    * @return string
    */
    public static function sanitizePassword($password) 
    {
        return preg_replace('/[\t\'"%<>\-\(\)\n\r]/i', '', $password);
    }
    
    /**
    * Sanitize a username, alpha-numeric chars only
    * 
    * @param array $settings
    * @return string
    */
    public static function sanitizeUsername($username) 
    {
        return preg_replace('/[^a-z0-9]/i', '', $username);
    }
    
    /**
    * Checks if date is in DD.MM.YYYY format.
    * 
    * @param string $date
    * @return boolean
    */
    public static function isValidDate($date) 
    {
	list($dd,$mm,$yy) = explode(".",$date);
	if (is_numeric($yy) && is_numeric($mm) && is_numeric($dd)){
	    return checkdate($mm,$dd,$yy);
	} 	     
    }
    
    /**
    * Checks if time is in HH:MM:SS format.
    * 
    * @param string $date
    * @return boolean
    */
    public static function isValidTime($time) {
	
	if(strlen($time) > strlen("HH:MM:SS"))
	    return false;
	
	$tPieces = explode(":",$time);
	$hour = $tPieces[0];
	$min = $tPieces[1];
	$sec = isset($tPieces[2]) ? $tPieces[2] : "";  
	
	if (is_numeric($hour) && $hour > -1 && $hour < 24 && is_numeric($min) && $min > -1 && $min < 60) {
	    if(strlen($sec) > 0){
		if(is_numeric($sec) && $sec > -1 && $sec < 60)
		    return true;
	    }else{
		return true;
	    }
	}
	return false;
    }
}
?>
