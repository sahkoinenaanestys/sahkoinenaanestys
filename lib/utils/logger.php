<?php
/**
 * Class for writing textual log file
 * 
 * Copyright (c) 2013 Janne Seppänen, Antti Ranta, Matias Ylipelto
 * This program is made available under the terms of the MIT License.
 */
class Logger
{
	private $filePath;
	private $name;
	private $fh;
        private $settings;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->settings = unserialize(LOG_I);
            
		if (!$this->settings['enabled']) return;
		$this->name = $this->settings['log_file'];
		$this->filePath = LOG_DIR;				
		$this->openLogFile();
	}
	
	function __destruct() {
            if(!is_null($this->fh))
		fclose($this->fh); 
	}

	private function openLogFile()
	{	
                //chmod($this->filePath, 0775);
		$fpath = $this->filePath . $this->name;
		
		if (file_exists($fpath) && filesize($fpath) > $this->settings['log_max_file_size'])
		{
			// log file exceeded max size => roll log filename
			$maxidx = $this->settings['log_max_backup_index'];
			if ($maxidx > 0) // roll log files: log.ext is most recent, then log.ext.1...
			{
				$cnt = 1;
				while (file_exists($fpath.'.'.$cnt)) $cnt++;
				if ($cnt > $maxidx) @unlink($fpath.'.'.$maxidx); // delete the oldest file					
				for ($i = $cnt-1; $i >= 1; $i--) {
					rename($fpath.'.'.$i, $fpath.'.'.($i+1)); // shift the rest to older index
				}
				rename($fpath, $fpath.'.1');
			} else {
				@unlink($fpath); // no roll
			}
		}
		$this->fh = fopen($fpath, 'a') or exit("Can't open log file!");		
	}
	
  	private function writeLogFile($type, $msg)
  	{
  		$df = $this->settings['log_time_format'];  		
  		if (strpos($df, 'ms') !== false) {
  			$addMilliSecs = 1;
  			$df2 = str_replace('ms', '~~', $df);
  			$now = date($df2);  			
  			$mt = microtime(); // example: 0.1234567 xxxxxxx
  			$now = str_replace('~~', $mt[2].$mt[3].$mt[4], $now); // insert milliseconds
  		}
  		else {
			$now = date($df);
		}
		$s = $this->settings['log_pattern'];
		$s = str_replace('{time}', $now, $s);
		$s = str_replace('{name}', $this->_name, $s);
		$s = str_replace('{msg}', $msg, $s);
		fwrite($this->fh, $s."\n");
  	}
	
        /*
         * Usage: 
         * $log = new Logger();
         * $log->write('Hello world!');
         */
	public function write($msg)
	{
		if (!$this->settings['enabled']) return;
		$this->writeLogFile('', $msg);
	}

}
