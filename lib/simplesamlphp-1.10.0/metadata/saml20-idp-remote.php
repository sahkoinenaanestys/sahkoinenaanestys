<?php
/**
 * SAML 2.0 remote IdP metadata for simpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://rnd.feide.no/content/idp-remote-metadata-reference
 */

// $metadata[IDP_ENTITY_ID] = array(
// 	'name' => array(
// 		'en' => 'Electronic voting - students',
// 		'fi' => 'Sähköinen äänestys - opiskelijat',
// 	),
// 	'description'          => 'Here you can login using your Paju account.',
		
// 	'SingleSignOnService'  => IDP_SINGLE_SIGN_ON_SERVICE,
// 	'SingleLogoutService'  => IDP_SINGLE_LOGOUT_SERVICE,
// 	'certificate'		   => IDP_CERTIFICATE,	
// );

$metadata[IDP_ENTITY_ID] = array (
		'entityid' => IDP_ENTITY_ID,
		'contacts' => array (),
		'metadata-set' => 'saml20-idp-remote',
		'SingleSignOnService' => unserialize(IDP_SINGLE_SIGN_ON_SERVICE),
		'SingleLogoutService' => unserialize(IDP_SINGLE_LOGOUT_SERVICE),
		'ArtifactResolutionService' => unserialize(IDP_ARTIFACT_RESOLUTION_SERVICE),
		'certificate' => IDP_CERTIFICATE,
// 		'keys' => unserialize(IDP_KEYS),
		'scope' => unserialize(IDP_SCOPE),
);